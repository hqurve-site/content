= Polynomials

Let stem:[\mathbb{K}] be a field. Then, we define stem:[\mathbb{K}[X\]] be 
the set of all polynomials with finitely many terms. That is
[stem]
++++
\DeclareMathOperator{\deg}{deg}
\mathbb{K}[X] = \{a_0 + a_1x +\cdots a_mx_m \ | \ m \geq 0, a_i \in \mathbb{K}\}
++++
Then, stem:[\mathbb{K}[X\]] forms a euclidean domain with euclidean function being
the degree of the polynomial.


Characteristic Polynomial::
Let stem:[A] be a square matrix. Then, the characteristic polynomial of stem:[A] ,
stem:[\rho_A] is defined as
+
[stem]
++++
\rho_A(x) = \det(xI -A)
++++


== All Ideals are principal
Let stem:[I \subseteq \mathbb{K}[X\]] be an xref:math2272::rings/index.adoc#_ideals[ideal]. That is, stem:[I]
is a subring for stem:[\mathbb{K}[X\]] where 
[stem]
++++
I \mathbb{K}[X] = \{f(x)g(x) \ | \ f \in I, g \in \mathbb{K}[X]\} \subseteq I
++++
Then, stem:[I] is a principal ideal. That is, stem:[\exists f \in I] such that stem:[f]
is monic and 
[stem]
++++
(f) = \{f(x) g(x) \ | \ g \in \mathbb{K}[X]\} = I
++++

.Proof
[%collapsible]
====
NOTE: This is the proof presented in class

Let stem:[f] be the monic polynomial of smalles degree. Then, by the closure of stem:[I], stem:[(f) \subseteq I].
Next, consider arbriary stem:[g \in I], then, 
[stem]
++++
g(x) = q(x)f(x) + r(x)
++++
for some stem:[q, r \in \mathbb{K}[X\]] such that stem:[r = 0] or stem:[\deg(r) < \deg(f)]. If stem:[r \neq 0],
[stem]
++++
r(x) = g(x) - q(x)f(x) \in I
++++
since stem:[qf \in I] since stem:[I] is an ideal.
Then we can construct a monic from stem:[r] of the same degree that is in stem:[I], which contradicts the minimality fo 
stem:[f]. Hence, stem:[r = 0] and we get that 
[stem]
++++
g(x) = q(x)f(x) \in (f)
++++
and hence stem:[I \subseteq (f)] and we are done.
====

== GCD
Let stem:[f, g \in \mathbb{K}[X\]], then,
[stem]
++++
I = \{f(x)u(x) + g(x)v(x) \ | \ u,v \in \mathbb{K}[X]\} = (f) +(g)
++++
is an ideal. Then, stem:[\exists] monic stem:[h \in I] which generates stem:[I]. Then this stem:[I] is the gcd of stem:[f] and stem:[g].

NOTE: This is not a definiton but actually a theorem since stem:[h] is a common divisor fo stem:[f]  and stem:[g], and any common 
divisor of stem:[f] and stem:[g] must also divide stem:[h] since stem:[h] is a linear combination of stem:[f] ang stem:[g].

== Unique factorization domain
A polynomial stem:[p \in \mathbb{K}[X\]] is called _irriducable_ if 

* stem:[p] is non-constant
* stem:[p(x) = g(x)h(x)] implies that stem:[g] or stem:[h] is contant.

Then, for any stem:[f \in \mathbb{K}[X\]] there exists a unique set of monic irriducable polynomials stem:[p_1,\ldots p_n \in \mathbb{K}[X\]] 
and constant stem:[c \in \mathbb{K}] such that 
[stem]
++++
f(x) = c p_1(x)\cdots p_n(x)
++++

