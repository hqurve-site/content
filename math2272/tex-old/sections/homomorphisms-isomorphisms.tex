\section{Homomorphisms and Isomorphisms}
Let $(G, \cdot)$ and $(H, \star)$ be two groups and $f: G\to H$.
Then $f$ is called a \emph{group homomorphism} if 
\[
    f(a\cdot b) = f(a) \star f(b) \ \forall a, b \in G
\]
That is, $f$ ``preserves the structure of $G$.''  


\subsubsection{Nice properties}
Let $(G, \cdot)$ and $(H, \star)$ be groups with $f$ being an homomorphism 
from $G$ to $H$ then 
\begin{enumerate}[label=\roman*)]
    \item $f(e_G) = e_H$
    \item $f(a^{-1}) = f(a)^{-1}\ \forall a \in G$
\end{enumerate}
and hence the image of $f$ is a subgroup of $H$.
\begin{proofi}
    Firstly, notice that 
    \[
        f(e_G) \star f(e_G) = f(e_G \cdot e_G) = f(e_G) = f(e_G) \star e_H
    \]
    and the result follows by the cancellation property.

    Next, let $a \in G$ and notice that 
    \[
        f(a) \star f(a^{-1}) = f(a \cdot a^{-1}) = f(e_G) = e_H = f(a) \star f(a)^{-1}
    \]
    and again the result follows by the cancellation property.
\end{proofi}

\subsubsection{Order}
Let $(G, \cdot)$ and $(H, \star)$ be groups with $f\colon G \to H$ being an homomorphism
and $a \in G$. Then, if the order of $a$ is finite, then the order of $f(a)$ divides 
the order of $a$.

\begin{proofi}
    Let $a \in G$ have finite order $n$. Then
    \[
        f(a)^n = f(a^n) = f(e_G) = e_H
    \]
    therefore, the order of $f(a) \leq n$ and is also finite. Next, let the order
    of $f(a)$ be $m$ and by the usual division algorithm procedure, $n = qm$ and we
    are done.
\end{proofi}

\subsection{Isomorphisms}
Let $(G, \cdot)$ and $(H, \star)$ be groups with $f\colon G \to H$ being an homomorphism.
Then, if $f$ is bijective, we call $f$ an \emph{isomorphism}. Furthermore,
if there exists an isomorphism between the two groups, we call
them isomorphic and write
\[
    (G, \cdot) \cong (H, \star)
\]
Note that $\cong$ is an equivalence relation (the necessary properties will be proven later).
Additionally, if $(G, \cdot) = (H, \star)$  we call $f$ an \emph{automorphism}.


\subsubsection{Identity as an automorphism}
Let $(G, \cdot)$ be a group.
Then $(G, \cdot)$ is isomorphic with itself since the identity is an automorphism.
This is easily seen since
\[
    \forall a, b \in G: id(a) \cdot id(b) = a \cdot b = id(a\cdot b)
\]
\subsubsection{Inverse as isomorphism}
Let $(G, \cdot)$ and $(H, \star)$ be groups with $f\colon:G \to H$
being an isomorphism. Then, we claim that $f^{-1}$ is also an isomorphism
and hence
\[
    (G, \cdot) \cong (H, \star) \implies (H, \star) \cong (G, \cdot)
\]
\begin{proofi}
    Firstly, since $f$ is bijective, then $f^{-1}$ exists and is also bijective. Then,
    consider $x, y \in H$. Then we get
    \[
        f^{-1}(x \star y) 
        = f^{-1}(f(f^{-1}(x)) \star f(f^{-1}(y))) 
        = f^{-1}(f(f^{-1}(x) \cdot f^{-1}(y)))
        = f^{-1}(x) \cdot f^{-1}(y)
    \]
    and we are done.
\end{proofi}
\subsubsection{Composition of isomorphisms}
Let $(F, \oplus)$, $(G, \cdot)$ and $(H, \star)$ be groups
with $f\colon F \to G$ and $g\colon G\to H$ both being isomorphisms.
Then, $f\circ g$ is also an isomorphism.

\begin{proofi}
    Firstly, since $f$ and $g$ are both bijections, $f\circ g$ is also bijective.
    Now, consider $a, b \in F$, then
    \[
        fg(a\oplus b) = f(g(a) \cdot g(b)) = fg(a) \star fg(b)
    \]
    and we are done.
\end{proofi}

\subsubsection{Preservation of order}
Let $(G, \cdot)$ and $(H, \star)$ be groups and $f\colon G \to H$
be an isomorphism. Then if the order of $a \in G$ is finite, 
it is equal to the order of $f(a)$. Otherwise, if it is infinite,
both have infinite order.

\begin{proofi}
    First, consider when the order of $a$ is finite.
    Then, the order of $f(a)$ is also finite and divides the order of $a$.
    Next, since $f^{-1}$ is also an isomorphism, and $f(a)$ has finite order,
    the order of $f^{-1}f(a) = a$ divides the order of $f(a)$. Therefore, 
    the two orders are equal.

    On the other hand, suppose that $a$ had infinite order, then $f(a)$
    must also have infinite order otherwise we would get a contradiction
    since $f^{-1}$ is an isomorphism.
\end{proofi}

\subsubsection{Cylic isomorphic groups}
Let $(G, \cdot)$ be a cyclic group with generator $g \in G$.
Then, $(G, \cdot)$ is isomorphic to $C_n$ where $|G| = n$ is finite,
otherwise, $(G, \cdot)$ is isomorphic to $(\mathbb{Z}, +)$.

\begin{proofi}
    Firstly, suppose that $|G| = n$ is finite, then, let $C_n = gp(c)$.
    We define the function $f\colon G \to C_n$ as 
    \[
        f(g^k) = c^k
    \]
    Then, note that each element in $G$ and $C_n$ can be can be written
    as a unique power of the generator when restricted to $\{0\ldots, (n-1)\}$.
    So, $f$ is also bijective. Also $f$ is a homomorphism since
    \[
        f(g^j g^k) = f(g^{j+k}) = c^{j+k} = c^j c^k = f(g^j)f(c^k)
    \]
    and hence $(G, \cdot) \cong (C_n, \cdot)$
    

    Now, suppose that $|G|$ is infinite. Then we define the function
    $f\colon G \to \mathbb{Z}$ as
    \[
        f(g^k) = k
    \]
    Since each element in $g$ can be written as a unique power of $g$ (otherwise
    infiniteness would be invalidated) and hence $f$ is injective. Also, 
    clearly, $f$ is surjective. Finally, note that $f$ is a homomorphism since
    \[
        f(g^j g^k) = f(g^{j+k}) = j+k = f(g^j)f(c^k)
    \]
    and hence $(G, \cdot) \cong (\mathbb{Z}, +)$.
\end{proofi}

\subsection{Kernel}
Let $(G, \cdot)$ and $(H, \star)$ be two groups with $f\colon G \to H$
being a homomorphism. Then, the \emph{kernel} of $f$ is defined as
\[
    \ker f = \{g \in G: f(g) = e_H\} = f^{-1}\{e_H\}
\]
then
\begin{enumerate}[label=\roman*)]
    \item $\ker f$ is a normal subgroup of $G$.
    \item $f$ is injective iff $\ker f = \{e_G\}$
\end{enumerate}
\begin{proofi}
    .
    \paragraph*{Part (i)}
    Firstly, note that $\ker f \neq \varnothing$ since $f(e_G) = e_H$ and hence $e_G \in \ker f$.
    Also, consider arbitrary $g \in G$ and $h \in H$, then
    \[
        f(g^{-1}hg) 
        = f(g)^{-1}f(h) f(g)
        = f(g)^{-1}e_H f(g)
        = f(g)^{-1} f(g)
        = e_H
    \]
    and hence $g^{-1} h g \in H$. Therefore, by the normal subgroup criterion, the result follows.

    \paragraph*{Part (ii)}
    The forward direction is immediate since $f(e_G) = e_H$. Now, consider the converse
    and suppose $f(g_1) = f(g_2)$ where $g_1, g_2 \in G$. Then
    \[
        f(g_1g_2^{-1}) = f(g_1)f(g_2)^{-1} = f(g_1)f(g_1)^{-1} = e_H
    \]
    therefore $g_1g_2^{-1} = e_H = g_2g_2^{-1}$ and the result follows by cancellation
    property.

\end{proofi}

\subsection{Direct products}
Let $(G, \cdot)$ and $(H, \star)$ be groups, then $(G\times H, \circ)$ is also a
group where $\circ$ is defined by
\[
    \forall (g_1, h_1), (g_2, h_2) \in G\times H:
    (g_1, h_1)\circ (g_2, h_2) = (g_1 \cdot g_2, h_1\star h_2)
\]
\subsection{First Isomorphism Theorem}
Let $(G, \cdot)$ and $(H, \star)$ be two groups with $f\colon G \to H$
being a homomorphism. Then $G/\ker f$ is isomorphic to the image of $G$ in $f$
with isomorphism $\Psi\colon G/\ker f \to f(G)$  defined by $\Psi(Kg) = f(g)$.

\subsubsection{Nice result}
Let $G_1$ and $G_2$ be groups with subgroups $K_1 \Delta G_1$ and $K_2 \Delta G_2$.
Then, 
\[
    (K_1 \times K_2) \Delta (G_1 \times G_2)
    \quad\text{and}\quad
    (G_1 \times G_2)/(K_1 \times K_2) \cong (G_1 / K_1) \times (G_2 / K_2)
\]
Note that this follows from the first isomorphism theorem since $f:(G_1 \times G_2) \to  (G_1 / K_1) \times (G_2 / K_2)$
is an onto homomorphism with kernel $K_1 \times K_2$ defined by 
\[
    f(g_1, g_2) \to (g_1K, g_2K)
\]


\subsubsection{Second Isomorphism Theorem}
Let $G$ be a group with subgroup $G$ and normal subgroup $N$. THen
$HN$ (set of products) is a subgroup of $G$, $H \cap N$ is a normal subgroup of $H$
and 
\[
    H / H \cap N \cong HN / N
\]

\subsubsection{Thid Isomorphism Theorem}
Let $G$ be a group with normal subgroups $M$ and $N$ where $N \Delta M$. Then, $\phi: G/N \to G/M$
where $\phi(Ng) = Mg$ is a well defined homomorphism and 
\[
    (G/N) / (M/N) \cong G /M
\]

