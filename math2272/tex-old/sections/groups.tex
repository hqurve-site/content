\section{Groups}
The following table show the properties of the algebraic structures:
groupoids, semigroups, monoids, groups. Let $G$ be a non-empty set
\begin{center}
    % \begin{tabular}{|*{6}{>{\rowmac}c}<{\clearrow}}\hline
    %     \setrow{\bfseries}Property & Definition & Groupoid & Semigroup & Monoid & Group\\\hline
    % \end{tabular}
    \renewcommand{\arraystretch}{1.4}
    \begin{tabular}{|~>{\bfseries}c|*{5}{^c|}}\hline
        \rowstyle{\bfseries}Property & Definition & Groupoid & Semigroup & Monoid & Group\\\hline
        Closure & $\star:G\times G \to G$ & \checkmark & \checkmark & \checkmark & \checkmark\\ \hline
        Associative & $(a\star b)\star c = a \star (b\star c)$ & & \checkmark & \checkmark & \checkmark\\\hline
        Identity & $\exists e: \in G:\forall a \in G: a\star e = e\star a = a $ & & & \checkmark & \checkmark\\\hline
        Inverse & $\forall a \in G:\exists a^{-1} \in G: a\star a^{-1} = a^{-1}\star a = e $ & & & & \checkmark\\\hline
    \end{tabular}
\end{center}
Furthermore, a group is called
\begin{itemize}
    \item \emph{abelian} iff $\star$ is commutative.
    \item \emph{trivial} iff $G = \{e\}$ (the singleton of the identity). Additionally, this is the smallest subgroup.
    \item \emph{finite} iff $|G|$ is finite and \emph{infinite} otherwise.
\end{itemize} 
Note: for the rest of this section, groups are used unless otherwise stated. Also,
for brevity sake, $a\star b$ would be written as $ab$ instead. Additionally, we
we define powers of an element as
\[
    a^n = \begin{cases}
        a \star a^{n-1} \quad&\text{if } n > 0\\
        e \quad&\text{if } n = 0\\
        a^{-1} \star a^{n+1} \quad&\text{if } n < 0
    \end{cases}
\]
Notice that powers are well defined since $\star$ is associative and both the inverse and identity
are unique (as discussed below). Furthermore, it is not hard to show that 
\begin{itemize}
    \item $(a^{n})^{-1} = a^{-n}$
    \item $(a^n)^m = a^{nm}$
    \item $a^na^m = a^{n+m}$
\end{itemize}

\subsection{Nice properties}
\subsubsection{Uniquness of Identity}
\begin{proofi}
    Let $e_1$ and $e_2$ be identity elements in $G$, then 
    \[
        e_1 = e_2 e_1 = e_2
    \]
\end{proofi}
\subsubsection{Uniquness of Inverse}
\begin{proofi}
    Suppose, $b$ and $c$ are inverses of $a \in G$, then 
    \[
        b = be = b(ac) = (ba)c = ec = c
    \]
\end{proofi}
\subsubsection{Special cases of inverse}
Let $a,b \in G$, then 
\begin{itemize}
    \item $(a^{-1})^{-1} = a$.
    \item $(ab)^{-1} = b^{-1}a^{-1}$ 
\end{itemize}
Both of these could be verified using the definition of the inverse and its uniqueness.

\subsubsection{Cancellation property}
Let $a, b, c \in G$ where $ab = ac$ or $ba=ca$, then $b=c$. This property
is proven by either pre or post multiplying by $a^{-1}$.

\subsection{Order}
The \emph{order} of a group $G$ is simply its cardinality. 

The \emph{order} of an element $g \in G$ is the least positive
power of $g$ that is equal to the identity. If no such power exists,
$g$ is said to have infinite order.



\subsection{Subgroups}
A \emph{subgroup} of $(G, \star)$ is a pair $(H, \star)$ that is also a group where $H \subseteq G$.
Note that $H \neq \varnothing$.

\subsubsection{One step test}
$(H, \star)$ is a subgroup of $(G, \star)$ iff $\forall a, b \in H: a b^{-1} \in H$.
\begin{proofi}
    Clearly, the forward direction is true by the closure and existence of an inverse. Then,
    suppose $\forall a, b \in H: a b^{-1} \in H$.
    \setlist{leftmargin=*, style=nextline, align=left}
    \begin{itemize}
        \item[Identity: ] Let $b=a$. Then by the definition of the inverse, $aa^{-1} = e \in H$.
        \item[Inverse: ]  Let $a=e$. Then $eb^{-1} = b \in H$.
        \item[Associativity: ] Inherited from $G$.
        \item[Closure: ] Let $c \in G$ and $b = c^{-1}$. Then $a(c^{-1})^{-1} = ac \in H$.
    \end{itemize}
\end{proofi}

\subsubsection{Two step test}
$(H, \star)$ is a subgroup of $(G, \star)$ iff $\forall a, b \in H: a b \in H \land a^{-1} \in H$.
\begin{proofi}
    The forward direction is clearly true by closure and existence of an inverse. 
    Also, the reverse direction follows from the one step test since 
    \[
        b^{-1} \in H \implies ab^{-1} \in H
    \]
\end{proofi}

\subsubsection{Finite equality subgroup test}
If $H$ is a (non-empty) finite subset of $G$, then $(H, \star)$
is a subgroup of $(G, \star)$ iff $H \star H = H$, where
\[
    A\star B = \{a \star b\mid a \in A, b \in B\}
\]
NB: I came up with that name on my own.
\begin{proofi}
    For the forward direction, notice that $H\star H \subseteq H$
    by closure of $\star$, while $H \subseteq H\star H$ since,
    \[
        \forall h \in H: h = h\star e \in H \star H
    \]
    where $e$ is the identity.

    For the reverse direction, firstly, notice that $H$ is closed under star
    since $H \star H = H$. Also, it inherits the associative from $G$.
    Now, let $n= |H|$ (since $H$ is finite). Then, we can write 
    $H = \{h_1, h_2, \ldots, h_n\}$ and consider the following sequence,
    \[
        h_i\star h_1, h_i\star h_2, \ldots h_i\star h_n \in H
    \]
    where $h_i$ is an arbitrary element in $H$.
    Then, by the cancellation property, all of these $n$ elements must be unique.
    Furthermore, since there are exactly, $n$ elements in $H$, at least one
    of these must be equal to $h_i$. That is $\exists h_j \in H: h_i\star h_j = h_i$.
    Again, by the cancellation property, since $h_ih_j = h_i = h_ie$ (where $e \in G$),
    then $e = h_j$ and $e\in H$.

    Finally, consider the same sequence. Then, by the same logic, $\exists h_k \in H:
    h_i \star h_k = e$. Then, by the cancellation property, $h_k = h_i^{-1} \in H$.
\end{proofi}

\subsubsection{Finite subgroup test}
If $H$ is a (non-empty) finite subset of $G$, then $(H, \star)$
is a subgroup of $(G, \star)$ iff $\star$ is closed under $\star$ (ie $H\star H \subseteq H$).

NB: I believe this is equivalent to the `finite equality subgroup test' and the same proof
can be applied but I saw the proof as presented below.
\begin{proofi}
    Note that the forward direction is assured by the `finite equality subgroup test.' Now,
    for the reverse direction, we only need to show that $\forall h \in H: h^{-1} \in H$
    and the result will follow by the `two step test.' Now, consider an arbitrary $h \in H$.
    Then,
    \begin{itemize}
        \item If $a = e$, $a^{-1} = e\in H$.
        \item If $a \neq e$ consider the sequence $a, a^2, \ldots$. 
                Then, by closure this sequence is a subset of $H$
                and since $H$ is finite, not all of the elements of this sequence can be distinct.
                Say $a^{i} = a^{j}$ where $i > j$ then 
                \[
                    a^{i} = a^{i-j}a^{j} = a^j = e a^j
                \]
                and by the cancellation property, $a^{i-j} = e$ and since $a \neq e$, $i-j > 1 \implies a-j-1 \geq 1$.
                Then $a^{i-j} = a^{i-j-1}a = e = a^{-1}a$ and again by the cancellation property, 
                $a^{-1} = a^{i-j-1} \in H$ since $a-j-1 \geq 1$ and we are done.
    \end{itemize}
\end{proofi}

\subsection{Examples}
\subsubsection{Linear Groups}
Let $\mathbb{F}$ be a field and $n \in \mathbb{Z}^+$. Then,
\begin{itemize}
    \item the set $\mathbb{M}_n(\mathbb{F})$ is the set of all $n\times n$ matrices with entries from $\mathbb{F}$.
    \item the \emph{general linear group} $GL_n(\mathbb{F})$ is the set of invertible matrices from $\mathbb{M}_n(\mathbb{F})$.
    \item the \emph{special linear group} $SL_n(\mathbb{F})$ is the set of matrices from $\mathbb{M}_n(\mathbb{F})$ with determinant $1$ (multiplciative identity).
\end{itemize}
Then, both $(GL_n(\mathbb{F}), \cdot)$ and $(SL_n(\mathbb{F}), \cdot)$ are both groups
where $\cdot$ is standard matrix multiplication. Furthermore, $SL_n(\mathbb{F})$ is a subgroup
of $GL_n(\mathbb{F})$.
\subsubsection{Center of a group}
Let $G$ be a group, then we define the \emph{center of the group} as
\[
    Z(G) = \{a \in G: \forall g \in G: ga = ag\}
\]
that is, $Z(G)$ is the set of all elements in $G$ which commute with all other elements in $G$.
Then, $Z(G)$ is a subgroup of $G$ and furthermore $Z(G)$ is abelian.
\begin{proofi}
    Firstly, take two elements $a, b\in Z(G)$ and an arbitrary element $g \in G$. Then,
    by the associativity of the operation and since $a$ and $b$ commute with all elements in $G$,
    \[
        (ab)g = a(bg) = a(gb) = (ag)b = (ga)b = g(ab)
    \]
    Then, $ab$ commutes with $g$ and $ab \in Z(G)$. That is $Z(G)$ is closed.

    Next, since $g^{-1} \in G$ and the inverse is unique
    \[
        ag^{-1} = g^{-1}a
        \implies (ag^{-1})^{-1} = (g^{-1}a)^{-1}
        \implies ga^{-1} = a^{-1}g
    \]
    Then, $a^{-1}$ also commutes with $G$ and hence $a^{-1}\in Z(G)$.
    Therefore, by the two step subgroup test, $Z(G)$ is a subgroup of $G$.
\end{proofi}

\subsubsection{Symmetric group}
Let $X$ be a set and we denote $S(X)$ as the set of all bijections on $X$.
Then $(S(X), \circ)$ forms a group called the \emph{symmetric group of $X$} where $\circ$ is functional composition.
This is a result of the following facts
\begin{itemize}
    \item $I_X$ (the identity function on $X$) is bijective
    \item the composition of bijections is bijective
    \item function composition is associative
    \item a function has an inverse iff it is bijective. 
\end{itemize}

\subsection{Cayley's Theorem}
Note, this section is to be moved.
Cayley's theorem asserts that every group $G$ is isomorphic to a subgroup of its 
symmetric group $S(G)$.
\begin{proofi}
    We simply need to find an injective function $f: G \to S(G)$ such that its
    image forms a subgroup of $S(G)$ and group operations are ``preserved''. We define
    \[
        f(x) = g_x \quad\text{where }g_x: G \to G \text{ defined by } g_x(t) = xt
    \]
    Clearly, $f$ is injective and $g_x$ is bijective. Additionally
    \[
        f(xy)(t) = g_{xy}(t) = xyt = g_x(yt) = g_x(g_y(t)) = (f_x \circ f_y)(t)
    \]
    so $G$ is isomorphic to the range of $S(G)$ which forms a subgroup since
    group operations are preserved.
\end{proofi}