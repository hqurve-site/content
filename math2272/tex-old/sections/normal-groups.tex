\section{Normal Subgroups}
Let $G$ be a group. Then a subgroup $H$ of $G$ is \emph{normal} (or \emph{invariant})
if 
\[
    \forall g \in G: gH = Hg
\]
and we write $H \Delta G$. That is, all the right cosets of $H$ are equal to the left cosets of $H$.

\subsubsection{Normal subgroup criterion}
Let $G$ be a group and $H \subseteq G$, then 
the following statements are equivalent.
\begin{enumerate}[label=\roman*)]
    \item $H$ is a normal subgroup of $G$
    \item $\forall g \in G: \forall h \in H: g^{-1} h g \in H$ 
\end{enumerate}
\begin{proofi}
    This will be proven directly
    \begin{align*}
        &H \Delta G\\
        &\iff \forall g \in G: gH = Hg\\
        &\iff \forall g \in G: gH \subseteq Hg \ \land\ Hg \subseteq gH\\
        &\iff \forall g \in G: \brack{\forall h \in H: gh \in Hg} \ \land\ \brack{\forall h \in H: hg \in gH}\\
        &\iff \forall g \in G: \brack{\forall h \in H: ghg^{-1} \in H} \ \land\ \brack{\forall h \in H: g^{-1}hg \in H}\quad\text{as proven in Cosets}\\
        &\iff \brack{\forall g \in G: \forall h \in H: ghg^{-1} \in H} \ \land\ \brack{\forall g \in G: \forall h \in H: g^{-1}hg \in H}\\
        &\iff \brack{\forall g \in G: \forall h \in H: g^{-1}hg \in H} \ \land\ \brack{\forall g \in G: \forall h \in H: g^{-1}hg \in H}\\
        &\iff \forall g \in G: \forall h \in H: g^{-1}hg \in H
    \end{align*}
\end{proofi}

\subsubsection{Abelian subgroups}
Let $G$ be a group and $H$ be an abelian subgroup, then clearly, $H$ is also normal.

\subsection{Quotient Groups}
Let $G$ be a group and $N \Delta G$. Then, consider the following set
\[
    G/N = \{gN: g \in G\}
\]
as well as the binary operation $\cdot$ on $G/N$ defined by
\[
    Na \cdot Nb = N(ab) \quad\text{where } a, b\in G
\]
Then, $\cdot$ is well defined and $(G/N, \cdot)$ forms a group. We call this
group the \emph{quotient group} (or \emph{factor group}) of $G$ by $N$.

\begin{proofi}
    Firstly, we will prove that $\cdot$ is well defined. Let, 
    $a_1N = a_2N \in G/N$ and $b_1N = b_2N \in G/N$. 
    Note that $a_1N = a_2N \implies a_1^{-1}a_2\in N$ and since
    $N \Delta G$,
    \[
        b_2^{-1}(a_1^{-1}a_2)b_2 \in N
    \]
    Additionally, $b_1N = b_2N \implies b_1^{-1}b_2 \in N$ and by the closure of $N$,
    \[
        (b_1^{-1}b_2)b_2^{-1}(a_1^{-1}a_2)b_2 = b_1^{-1}a_1^{-1} a_2b_2 = (a_1b_1)^{-1}a_2b_2 \in N
    \]
    and hence $(a_1b_1)N = (a_2b_2)N$ and the operation is well defined. 

    Now, onto group operations
    \setlist{leftmargin=*, style=nextline, align=left}
    \begin{itemize}
        \item[Closure: ] Consider $Na, Nb \in G/N$ then $ab \in G$ and $Na\cdot Nb = N(ab) \in G/N$.
        \item[Associativity: ] Consider $Na, Nb, Nc \in  G/N$ then, by the associativity in $G$ 
                $a(bc) = (ab)c$ and hence 
                \[
                    Na \cdot (Nb \cdot Nc) =  N(a(bc)) = N((ab)c) = (Na \cdot Nb) \cdot Nc
                \]
        \item[Identity: ] Consider $Na \in G/N$ and let $e$ be the identity in $G$, then 
                \[
                    Na \cdot Ne = N(ae) = Na = N(ea) = Ne \cdot Na
                \]
                Quick observation: note that $Ne = N$.
        \item[Inverse: ] Consider $Na \in G/n$ and notice that $Na^{-1} \in G/n$ and
                \[
                    Na \cdot Na^{-1} = N(aa^{-1}) = Ne = N(a^{-1}a) = Na^{-1} \cdot Na
                \]
    \end{itemize}
    Therefore $G/N$ is a group. Additionally, notice that all of its properties were inherited from $G$.
\end{proofi}

\subsubsection{Nice theorem}
Let $G$ ba a group with subgroup $H$. Then if $|G:H| = 2$, $H\Delta G$  and 
$G/H$ is a cyclic group with order $2$.
\begin{proofi}
    Let the two cosets be $H$ and $Hg$ where $g \in G - H$. Then, 
    consider an arbitrary element $a \in G$, we have two cases
    \begin{itemize}
        \item $a \in H$, then $a^{-1}h a \in H\ \forall h \in H$  by the closure of subgroups.
        \item $a \in Hg$, then $Hg = Ha$. Then suppose BWOC, $\exists h \in H: a^{-1}ha \in Ha$,
            the $\exists h_1 \in H: a^{-1}ha = h_1a \implies a^{-1}=h_1h^{-1} \implies a = hh_1^{-1} \in H$.
            This is a contradiction and hence, 
            \[
                \forall h \in H: a^{-1}ha \in H
            \]
    \end{itemize}
    Then, $H \Delta G$. Also, note that $(gH)^2 = H$ otherwise, $gH = H$ which is a contradiction,
    therefore since $|G/H| = 2$ and the order of $gH$ is $2$, $G/H$ is a cyclic group 
    of order $2$.
\end{proofi}

