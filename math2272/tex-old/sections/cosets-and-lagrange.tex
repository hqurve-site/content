\section{Cosets and Lagrange's Theorem}
Let $G$ be a group, $H$ be a subgroup of $G$ and $g \in G$. Then, the \emph{left coset}
of $H$ with representative $g$ is defined as 
\[
    gH = \{gh: h \in H\}
\]
Similarly, the \emph{right coset} with representative $g$ is defined as
\[
    Hg = \{hg: h \in H\}
\]
Note that if $G$ is abelian, we simply refer to $Hg = gH$ as the \emph{cosel}
of $H$ with representative $g$.

\subsubsection{Nice properties}
Let $H$ be a subgroup of $G$ and  $g_1, g_2 \in G$, then the following statements are equivalent
where each column is independent
\begin{multicols}{2}
    \begin{enumerate}[label=\roman*)]
        \item $g_1 H = g_2H$
        \item $H g_1^{-1} = H g_2^{-1}$
        \item $g_1 H \subseteq g_2 H$
        \item $g_2 \in g_1 H$
        \item $g_1^{-1}g_2 \in H$
    \end{enumerate}    
    \begin{enumerate}[label=\roman*)]
        \item $H g_1 = H g_2$
        \item $g_1^{-1} H = g_2^{-1} H $
        \item $H g_1 \subseteq H g_2$
        \item $g_1 \in H g_2$
        \item $g_1g_2^{-1} \in H$
    \end{enumerate}    
\end{multicols}
Note that the second column is equivalent to the first by using $g_1^{-1}$
and $g_2^{-1}$ instead of $g_1$ and $g_2$ respectively and using $(i)\iff(ii)$ in the first column.
Therefore only the first column would be proven.

Note2: I am not saying that $g_1H = g_2H$ implies $Hg_1 = Hg_2$ but rather that the proofs for
both columns are essentially identical.

\begin{proofi}
    I'm using the first order I came up with, however, by the end, everything is equivalent.

    \paragraph*{$(v) \iff (iii)$: } 
    \begin{align*}
        g_1^{-1}g_2 \in H 
        &\iff \exists h \in H: g_1^{-1}g_2 =h \\
        &\iff \forall h_1 \in H: \exists h_2 \in H: g_1^{-1}g_2 =h_1h_2^{-1}= h \text{ by letting }h_2 = h^{-1}h_1 \\
        &\iff \forall h_1 \in H: \exists h_2 \in H: g_1h_1 = g_2h_2\\
        &\iff \forall h_1 \in H: g_1h_1 \in g_2H\\
        & g_1H \in g_2H
    \end{align*}
    \paragraph*{$(v) \iff (iv)$: } Note
    \[
        g_1^{-1}g_2 \in H 
        \iff \exists h \in H: g_1^{-1}g_2 \in h
        \iff \exists h \in H: g_2 \in g_1h
        \iff g_2 \in g_1H
    \]
    \paragraph*{$(i) \iff (iii)$: } Firstly, clearly, $(i) \implies (iii)$. Conversely,
    since $(v) \iff (iii)$
    \[
        g_1H \subseteq g_2H \implies g_1^{-1}g_2 \in H \implies g_2^{-1}g_1 \in H \implies g_2H \subseteq g_1H
    \]
    Therefore, $g_1H = g_2H$.
    \paragraph*{$(i) \iff (v)$: } Follow carefully
    \begin{align*}
        Hg_1^{-1} \subseteq Hg_2^{-1}
        &\iff \forall h_1 \in H: h_1g_1^{-1} \in Hg_2^{-1}\\
        &\iff \forall h_1 \in H: \exists h_2 \in H: h_1g_1^{-1} = h_2g_2^{-1}\\
        &\iff \forall h_1 \in H: \exists h_2 \in H: g_1^{-1}g_2 = h_1^{-1}h_2\\
        &\iff \exists h \in H: g_1^{-1}g_2 = h\\
        &\iff g_1^{-1}g_2 \in H
    \end{align*}
    Therefore, $Hg_1^{-1} = Hg_2^{-1} \implies g_1^{-1}g_2 \in H$. Conversely,
    $g_1^{-1}g_2 \in H \implies Hg_1^{-1} \subseteq Hg_2^{-1}$ and
    \[
        g_1^{-1}g_2 \in H 
        \implies g_2^{-1}g_1 \in H 
        \implies Hg_2^{-1} \subseteq Hg_1^{-1}
    \]
    and hence the result follows.
\end{proofi}


\subsubsection{Equal Cosets}
Let $G$ be a group with subgroup $H$ and $g \in G$ then
\[
    gH = Hg = H \iff g \in H
\]
This result follows directly from the closure of subgroups.

\subsubsection{Partitioning}
Let $G$ be a group with subgroup $H$ then the left cosets of $H$ partition $G$.
Additionally, the right cosets of $H$ also partition $G$.
\begin{proofi}
    Only result for left cosets will be proven since the process is similar for right cosets.

    Clearly, the union of set of left cosets is $G$ since $\forall g \in G: g \in gH$. 
    Additionally, suppose that $g_1H$ and $g_2H$ have elements in common. Let one such element
    be $x$. Then $x \in g_1H \implies xH = g_1H$ and similarly, $x \in g_2H \implies xH=g_2H$.
    Therefore, $g_1H = xH = g_2H$ and hence two left cosets are either disjoint or equal.
    Hence, the result follows.
\end{proofi}

\subsubsection{Cardinality of a coset}
Let $G$ be a group with subgroup $H$ and $a \in G$ then
\[
    |H| = |aH| = |Ha|
\]
\begin{proofi}
    On the proof that $|H| = |aH|$ will be provided
    as $|H| = |Ha|$ is almost identical.

    We defined $f(h) = ah$ then, clearly,
    $f$ is surjective. Furthermore,
    by the cancellation property of groups
    \[
        f(h_1) = f(h_2) \implies ah_1 = bh_2 \implies h_1 = h_2
    \]
    therefore $f$ is also injective and the result follows.
\end{proofi}

\subsubsection{Index of a subgroup}
Let $G$ be a group with subgroup $H$. Then the \emph{index} of $H$ is the 
number of left cosets of $H$. We denote the index as $[G: H]$.

Additionally, the number of left cosets is equal to the number of right cosets and hence, 
the index will also be equal to the number of right cosets of $H$.

\begin{proofi}Note, the proof given below is quite convoluted (unnecessarily so), so
    it might be best to reference the proof of theorem 6.8 in Judson.


    We will prove that the cardinality of left cosets is equal to that of the right cosets
    by defining a bijection between the two. 

    Let $a \in G$. Then, we define $Left_a = \{x: aH = xH\}$ and $Right_a = \{x: Ha = Hx\}$. Then,
    $Left_a$ and $Right_a$ can be thought of as equivalence classes of $a$ and are
    hence well defined. 
    
    Additionally, we define $Left = \{Left_a: a \in G\}$ and $Right = \{Right_a: a \in G\}$. 
    Then, these two sets have immediate bijections with the left and right cosets of $H$ respectively.
    Now, we define the function $f: Left \to Right$ as
    \[
       f(Left_a) = Right_{a^{-1}}
    \]
    Firstly, note that the function is well defined since
    \begin{align*}
        Left_a = Left_b \\
        &\iff aH = bH\\
        &\iff Ha^{-1} = Hb^{-1}\\
        & \iff Right_{a^{-1}} = Left_{b^{-1}}
    \end{align*}
    Note, the jump from line 2 to 3 was proven before. Also, equivalence classes
    are equal iff any of their elements are related.
    Additionally, this immediately proves that $f$ is injective. Furthermore, $f$ is surjective
    since
    \[
        \forall Right_b \in Right: f(Left_{b^{-1}}) = Right_{(b^{-1})^{-1}} = Right_b
    \]
    Therefore, $f$ is a bijection from $Left$ to $Right$ and it follows $|Left| = |Right|$.
\end{proofi}


\subsection{Lagrange's Theorem}
Let $G$ be a finite group with subgroup $H$. Then
\[
    [G:H] = \frac{|G|}{|H|} \in \mathbb{N}
\]
That is the order of a subgroup always divides the order of its parent group.
\begin{proofi}The proof of this is quite straight forward.
    Let $[G:H] = k$ and $a_1H \ldots a_kH$ be the left cosets of $H$.
    Then since the left cosets are disjoint
    \[
        |G| 
        = \abs{\bigcup_{i=1}^k a_i H} 
        = \bigcup_{i=1}^k |a_i H|
        = \bigcup_{i=1}^k |H|
        = k|H|
        = [G:H]|H|
    \]
    and the result follows.
\end{proofi}

\subsubsection{Corollaries}
Let $G$ be a group and $g \in G$ then
\begin{enumerate}[label=\roman*)]
    \item the order of an element $g$ divides $G$.
    \item $g^{|G|} = e$ (the identity in $G$).
    \item if $G$ has prime order, then $G$ is cyclic.
\end{enumerate}
\begin{proofi}.
    \paragraph*{Part (i)} Note that the order of $g$ is equal to the order of the group generated
        by $g$. Then by Lagrange's theorem $|gp(g)|$ divides $|G|$ since $gp(g)$ is a subgroup of $G$.

    \paragraph*{Part (ii)} This follows immediately from part (i) since $g^k = e$ iff the order of $g$ divides $k$.

    \paragraph*{Part (iii)} Suppose $|G|$ is prime then $|G| \geq 2$ and $\exists x \in G: x \neq e$.
        Then the order of $x$ is not $1$ and divides $|G|$. Since $|G|$ is prime the order of $x$
        is equal to $|G|$. Therefore, since $G$ has an element with order equal to the order of the group, 
        $G$ is cyclic.
    
\end{proofi}

\subsection{Congruence}
Let $G$ be a group with subgroup $H$. Then \emph{$a$ is congruent to $b$ modulo $H$} 
iff $ab^{1} \in H$. This is written as
\[
    a \equiv b \Mod{H} \iff ab^{-1} \in H
\]
Note that this relation is an equivalence relation since
\begin{itemize}
    \item $aa^{-1} = e \in H \implies a \equiv a \Mod{H}$
    \item $a \equiv b \Mod{H} \implies ab^{-1} \in H \implies ba^{-1} \in H \implies b\equiv a \Mod{H}$
    \item $a \equiv b \Mod{H}$ and $b \equiv c \Mod{H} \implies ab^{-1} \in H \text{ and } bc^{-1} \in H \implies ab^{-1}bc^{-1} = ac^{-1} \in H \implies a \equiv c \Mod{H}$
\end{itemize}

