\section{Rings}
Let $(R, +, \cdot)$ be a non-empty set with two binary operations. We call $+$, \emph{addition}
and $\cdot$, \emph{multiplication}. Then $(R, +, \cdot)$ is called a \emph{ring} if
\begin{enumerate}
    \item $\forall a, b \in R: a + b = b + a$
    \item $\forall a, b, c, \in R: (a+b) + c = a+ (b+c)$
    \item $\exists z \in R: \forall a \in R: a  +z = a$
    \item $\forall a \in R: \exists (-a) \in R: a + (-a) = z$
    \item $\forall a, b, c \in R: (a\cdot b) \cdot c = a \cdot (b \cdot c)$
    \item $\forall a, b, c \in R: a\cdot (b+c) = (a\cdot b) + (a \cdot c) \text{ and } (b+c) \cdot a = (b \cdot a) + (c\cdot a)$
\end{enumerate}
Alternatively, we could simplify the above definition as follows. $(R, +, \cdot)$ is a ring iff
\begin{enumerate}
    \item $(R, +)$ forms an abelian group.
    \item $(R, \cdot)$ forms a semigroup.
    \item $\cdot$ distributes over $+$ in $R$.
\end{enumerate}
Furthermore, $(R, + , \cdot)$ is called
\begin{itemize}
    \item a \emph{commutative ring} if $\cdot$ is commutative
    \item a \emph{ring with unity} if $\exists u \in R: \forall a \in R: a\cdot u = u \cdot a$. That is, $(R, \cdot)$ forms a monoid.
\end{itemize}
We call $z$ the \emph{zero element} and $u$ \emph{unity}.

\subsubsection{Nice properties}
Let $(R, +, \cdot)$ be a ring and $a, b, c \in R$. Then we have the following properties
\begin{enumerate}
    \item $a + b = a \implies b = z$ (uniqueness of identity).
    \item $-(-a) = a$
    \item $-(a+b) = (-a) + (-b)$
    \item $(a + (-b)) + (-c) = a + (-(b+c))$
    \item $a\cdot z = z\cdot a = z$
    \item $a\cdot( -b) = (-a)\cdot b = - (a\cdot b)$
\end{enumerate}

\subsubsection{Convensions}
Let $(R, +, \cdot)$ be a ring and $a, b \in R$ and $m, n \in \mathbb{Z}^+$, then we define the following 
convensions
\begin{enumerate}
    \item $a -b := a + (-b)$
    \item $ab := a\cdot b$
    \item $-ab := -(ab) = (-a)b = a(-b)$ 
    \item $ma := (m-1)a + a$ where $1a := a$.
    \item $(-m)a = m(-a)$
    \item $a^m = a^{m-1}a$ where $a^1 = a$.
    \item $a^0 = u$ if $u$ exists.
    \item $a^{-m} = (a^{-1})^m$ if $u$ and $a^{-1}$ exist.
\end{enumerate}

\subsubsection{Zero divisors}
Let $R$ be a ring, then $a \in R - \{z\}$ is a \emph{zero divisior} iff
\[
    \exists b \in S - \{z\}: ab = z
\]

\subsection{Subrings}
Let $R$ be a ring. Then, a \emph{subring} $S$ of $R$ is a subset of $R$ which is also a 
ring with the same operations.

\subsubsection{Two step subring test}
Let $R$ be a ring and $S$ be a non-empty subset of $R$, then $S$
is a subring of $R$ iff $\forall a,b \in S: a-b \in S \ \land \ ab \in S$.

\subsubsection{Three step subring test}
Let $R$ be a ring and $S$ be a non-empty subset of $R$, then $S$
is a subring iff $\forall a, b \in S: a+b \in S\ \land\ ab \in S\ \land\ -a \in S$.

\subsection{Characteristic}
Let $R$ be a ring then
\begin{itemize}
    \item if $\exists n \in \mathbb{Z}^+: \forall r \in R: nr = z$, the smallest such $n$ is called the \emph{characteristic of} $R$.
    \item otherise, $R$ has characteristic $0$.
\end{itemize}

\subsection{Integral domain}
Let $R$ be a commutative ring with unity, then $R$ is called
an \emph{integral domain} iff $R$ contains no zero divisors. That is,
\[
    \forall a, b \in R: ab = z \iff a = z \text{ or } b = z
\]
then, 
\begin{itemize}
    \item the cancellation law for multiplication holds. That is
        \[
            \forall a \in R -\{z\}: \forall b,c \in R: ab = ac \ \lor \ ba = ca \implies b = c
        \]
\end{itemize}

\subsubsection{Integers modolo n}
Consider the ring of $\mathbb{Z}_n$ and notice that it is a commutative ring with unity.
Then, we have the following theorem
\[
    \mathbb{Z}_n \text{ is an integral domain} \iff n \text{ is prime}
\]

\subsection{Division ring}
Let $R$ be a ring. Then, $R$ is called a \emph{division ring} iff $(R -\{z\}, \cdot)$ forms
a group.


\subsection{Ideals}
Let $R$ be a ring. Then, $I \subseteq R$ is a
\begin{itemize}
    \item \emph{right ideal} of $R$ if $(I, +)$ is a subgroup of $(R, +)$ and
        \[
            \forall r \in R: \forall a \in I: ar \in I
        \]
    \item \emph{left ideal} of $R$ if $(I, +)$ is a subgroup of $(R, +)$ and
        \[
            \forall r \in R: \forall a \in I: ra \in I
        \]
\end{itemize}
Furthermore $I$ is simply called a \emph{ideal} of $R$ if it is 
both a right and left ideal. Also, we call $I = \{z\}$ and $I = R$
\emph{trivial ideals}.

\paragraph*{Note} this is a stronger condition than closure of multiplication which
is used in subrings. Also, ideals are said to \emph{absorb} multiplication (from the left
and/or right).

\subsubsection{Nice property}
Let $R$ be a ring with unity. Then if $I \subseteq R$ is an ideal (left or right)
\[
    e \in I \iff I = R
\]
\proofinline{3}

\subsubsection{Principal Ideals}
Let $R$ be a commutative ring with unity, then 
\[
    (a) = \{ar: r \in R\}
\]
is called a \emph{principal ideal} where $a \in \mathbb{R}$. 
Then, $(a)$ is also a ideal.\proofref{4}.

\subsubsection{One sided principal ideals}
Let $R$ be a ring with ideal $I$. Then, $I$ is called a 
\begin{itemize}
    \item \emph{right principal ideal} if $\exists a \in I$ such that $I = \{ar : r \in R\}$
    \item \emph{left principal ideal} if $\exists a \in I$ such that $I = \{ra : r \in R\}$
\end{itemize}
If $I$ is both a left and right principal ideal, $I$ is also a principal ideal.
Note that the condition that $I$ be an ideal is necessary otherwise absorbtion
from both sides would not be guaranteed.

\subsubsection{All ideals in the integers are principal ideals}
Consider the ring of integers with ideal $I$, then $I$ is a principal ideal.\proofref{5}

\subsection{Quotient Rings}
Let $R$ be a ring with ideal $J$. Then, the set of additive cosets of $J$
form a ring where 
\[
    \forall (a + J), (b + J) \in R/J: (a+J) + (b + J) = (a+b) + J
\]
and 
\[
    \forall (a+ J), (b + J) \in R/ J: (a + J)(b + J) = ab + J
\]
both of these operations are well defined satisfy the ring axioms.\proofref{6}
We call this ring the \emph{quotient ring of} $J$ in $R$.

\subsection{Ring homomorphisms}
Let $R$ and $S$ be rings (not necessarily with the same operations, then
$f\colon R \to S$ is called a \emph{ring homomorphism} if 
\[
    \forall a, b \in R: f(a + b) = f(a) + f(b) \text{ and } f(a\cdot b) = f(a) \cdot f(b)
\]
Furthermore, if $f$ is bijective, $f$ is called a \emph{isomorphism} and $R$
is said to be \emph{isomorphic} to $S$.

\subsubsection{Nice properties}
Let $R$ and $S$ be rings with homomorphism $f \colon R \to S$, then
\begin{itemize}
    \item $f(z_R) = z_S$
    \item $f(e_R) = e_S$ if $R$ is a ring with unity and $f$ is surjective
\end{itemize}
\proofinline{9}

\subsubsection{Kernel}
Let $R$ and $S$ be rings with homomorphism $f\colon R\to S$, then 
\[
    \ker f = \{r \in R: f(r) = z \in S\}
\]
Then, $\ker f = \{z\} \iff f$  is injective.\proofref{8}
