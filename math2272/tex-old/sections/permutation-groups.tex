\section{Permutation Groups}
A \emph{permutation} $\sigma$ is defined as a bijection on a finite set.
Since the set is finite there is an equivalence to $J_n$
and hence we can equivalently represent $\sigma \in S(J_n)$.
Furthermore, we call $S_n = S(J_n)$ the \emph{symmetric group of degree} $n$.
Then immediately using combinatorial arguments, $|S_n| = n!$. Additionally,
a subgroup of a symmetric group is called a \emph{permutation group}.

\subsubsection{Notation}
Let $\sigma \in S_n$, then we can write
\[
    \sigma
    = \begin{pmatrix}
        1 & 2 & 3 & \cdots & n\\
        \sigma(1) & \sigma(2) & \sigma(3) & \cdots & \sigma(n)
    \end{pmatrix}
\]
additionally if $\sigma_1, \sigma_2 \in S_n$, we use the following notation
to represent functional composition (the group operation)
\[
    \sigma_1 \sigma_2 (x) = (\sigma_1 \circ \sigma_2) (x) = \sigma_1(\sigma_2(x))
\]


\subsection{Cycles}
A permutation $\sigma \in S_n$ is called an $r$-cycle if
$\exists a_1, a_2, \ldots ,a_r \in J_n$ where all $a_i$ are distinct
such that
\[
    \sigma(a_1) = a_2
    ,\quad
    \sigma(a_2) = a_3
    ,\quad \cdots \quad,
    \sigma(a_{r-1}) = a_{r}
    ,\quad
    \sigma(a_r) = a_1
\]
and $\sigma(x) = x$ if $x \notin \{a_1, a_2, \ldots, a_r\}$.
Additionally, we can write $\sigma$ as
\[
    \sigma = (a_1\; a_2\; a_3\; \cdots\; a_r)
\]
For intuition, the following diagram is presented
\begin{center}
\begin{tikzpicture}
    \tikzstyle{edge} = [line width =1pt];
    \tikzstyle{arrow} = [edge,->, >=stealth] 

    \node (A1) at (0, 0) {$a_1$};
    \node (A2) at (1, 0) {$a_2$};
    \node (A3) at (2, 0) {$a_3$};
    
    \node (Ar) at (4, 0) {$a_r$};
    \node (Amid) at ($(A3)!0.5!(Ar)$) {$.$};

    \path (A1) edge[arrow] (A2);
    \path (A2) edge[arrow] (A3);
    \path (A3) edge[edge] (Amid);
    \path (Amid) edge[arrow] (Ar);

    % \draw [arrow] (Ar) to[out=290, in=-90] (A1);
    \path ($(Ar) - (0, 0.2)$) edge[arrow, bend left =20] ($(A1) - (0, 0.2)$);

    \node at ($(A1) - (0.5, 0)$) {$($};
    \node at ($(Ar) + (0.5, 0)$) {$)$};
    % \path (A1)
    % \node (A4) at (0, 0) {$a_1$};
    % \node (A5) at (0, 0) {$a_1$};
    % \node (A) at (90:2) {hi};
    % \node (B) at (180:2) {hola};
\end{tikzpicture}
\end{center}
Note that the representation of a cycle is not unique however by convention, where possible,
the first element is the smallest.

\subsubsection{Disjoint and simplification}
Two cycles $\sigma_1 = (a_1\; a_2\; \cdots\; a_m)$ and $\sigma_2 = (b_1\; b_2\; \cdots\; b_n)$
are disjoint if $a_i \neq b_j$ $\forall\; i, j$. Furthermore,
it is easy to see that if $\sigma_1$ are $\sigma_2$ are disjoint, then they commute; ie
$\sigma_1\sigma_2 = \sigma_2\sigma_1$. Additionally,
this product cannot be simplified (merged into a single cycle) since each $a_i$ must map
to some $a_j$ and similarly for the elements of $\sigma_2$.

\subsubsection{Cycle decomposition}
Each permutation $\sigma \in S_n$ can be uniquely written as a product of disjoint cycles (up to permutation
since composition is commutative).
\begin{proofi}
    Let $\sigma \in S_n$. Then, we define
    \[
        X_1 = \{1, \sigma(1), \sigma^2(2), \sigma(3), \ldots\}
    \]
    where $\sigma^n = \sigma \cdot (\sigma^{n-1})$ (ie repeated application).
    Then since $J_n$ is finite, so is $X_1 \subseteq J_n$. Now,
    if we have have $X_1, \ldots X_k$ there are two cases
    \begin{itemize}
        \item If $\bigcup_{i=1}^k X_i = J_n$ we continue. Note that each of the $X_i$ are disjoint.
        \item $\exists x \in J_n: x \notin \bigcup_{i=1}^k X_i$ then we define $X_{k+1} = \{x, \sigma(x), \sigma^2(x), \ldots\}$ and repeat this check.
    \end{itemize}
    This process must end since $J_n$ is finite. Now, we now define 
    \[
        \sigma_i = \text{ cycle defined by } X_i
    \]
    for example $\sigma_1 = (1\;\; \sigma(1)\;\cdots)$. Note that the choice of the first element
    does not matter in each cycle. Then,
    \[
        \sigma = \sigma_1\sigma_2\cdots \sigma_k
    \]
    For the uniqueness of the above product (up to permutation of the disjoint cycles), consider $x \in J_n$.
    Therefore, each of the $X_i$ are uniquely defined and hence so are each of the $\sigma_i$.
\end{proofi}

\subsubsection{Order}
The order of an $r$-cycle is $r$. Furthermore, the order of a general permutation is 
the lowest common multiple of the orders of its disjoint cycles.

\subsubsection{Cycles of equal length}
Two cycles $\tau, \mu \in S_n$ have the same length if and only if $\exists \sigma \in S_n$
such that $\mu = \sigma \tau \sigma^{-1}$.
\begin{proofi}
    This proof is taken from theorem 6.16 in Judson however, note that the second part is worded slightly
    differently. Also, for brevity we define $a_{k+1} = a_1$ and $b_{k+1}=b_1$.

    Firstly, suppose that 
    \[
        \tau = (a_1\; a_2 \; \cdots\; a_k)
        \quad\text{and}\quad
        \mu = (b_1\; b_2 \; \cdots\; b_k)
    \]
    Then we define the permutation $\sigma \in S_n$
    \[
        \sigma(a_i) = b_i \forall i = 1,\ldots,k
        \quad\text{and }
        \sigma(x) =x \text{ otherwise}
    \]
    Then note that when $t = b_i$, 
    \[
        \sigma\tau\sigma^{-1}(b_i)
        = \sigma\tau(a_i)
        = \sigma(a_{i+1})
        = b_{i+1}
        = \mu(b_i)
    \]
    while if $t \neq b_i$, $\sigma^{-1}(t) = t \neq a_i$ and
    hence
    \[
        \sigma\tau\sigma^{-1}(t) 
        = \sigma\tau(t)
        = \sigma(t)
        = t
        = \mu(t)
    \]
    Therefore, $\mu = \sigma\tau\sigma^{-1}$.

    On the other hand, suppose that $\tau = (a_1\; a_2 \; \cdots\; a_k)$
    and $\mu = \sigma\tau\sigma^{-1}$. Then, consider $b \in J_n$, 
    \begin{itemize}
        \item If $\sigma^{-1}(b) \neq a_i$, $\mu(b) = \sigma\tau\sigma^{1}(b) = \sigma\sigma^{-1}(b) = b$.
        \item If $\sigma^{-1}(b) = a_i$, $\mu(\sigma(a_i)) =\mu(b) = \sigma\tau\sigma^{-1}(b) = \sigma\tau(a_i) = \sigma(a_{i+1})$.
    \end{itemize}
    Therefore, we get that 
    \[
        \mu = \sigma\tau\sigma^{1} = \paren{\sigma(a_1)\; \sigma(a_2)\;\cdots\; \sigma(a_k)}
    \]
    and the result follows.
\end{proofi}


\subsection{Transpositions}
The simplest permutation is one which interchanges just two elements and is 
hence a $2$-cycle. Such a permutation is called a \emph{transposition}.
Additionally, each cycle can be written as a product of transpositions as follows
\[
    (a_1\; a_2\; a_3 \cdots a_r) = (a_1\; a_r)(a_1\; a_{r-1})\cdots (a_1\; a_3)(a_1 \; a_2)
\]
and as a result any permutation can be written as a product of transpositions.
\paragraph*{Acknowledgement} 
This section was taken from ``Abstract Algebra Theory and Applications'' by Judson and Beezer.
The following two proofs are taken almost verbatim from the aforementioned book.

\subsubsection{Identity as a product of transpositions}
If $(1)$ the identity permutation is written as a product of transpositions, then
the product consists of an even number of transpositions.
\begin{proofi}
    We will prove this using induction. We write $(1)$ as
    \[
        (1) = \tau_1\tau_2\cdots \tau_r
    \] 
    For the base case, notice that a single 
    transposition cannot be the identity, then we consider $r=2$. In such a case
    we are done.

    Now suppose that $r > 2$ and all other shorter representations of the identity 
    are products of an even number of transpositions. 
    Focus on a fixed element $a \in J_n$ which occurs in the product.
    Then, the product of the first two transpositions $\tau_{1}\tau_{2}$ can be one of the following cases
    \begin{align*}
        (ab)(ab) &= (1)\\
        (ab)(ac) &= (bc)(ab)\\
        (ab)(cd) &= (cd)(ab)\\
        (cd)(ab) & \\
        (bc)(de) &
    \end{align*}
    Where $a,b,c,d,e$ are all distinct. Then if the first case occurs, we are done since all
    shorter representations consists of an even number of transpositions. On the 
    other hand, if it is any of the other four, we replace the product $\tau_1\tau_2$
    with their equivalent representation on the right hand side (where applicable).

    Next, we focus on the new $\tau_2\tau_3$ and repeat the same process. This process
    would either end when we get the first case or we are left with $a$ only occurring in the last
    transposition. However, the second case cannot occur since the value mapping onto $a$ would
    never get remapped and hence the overall product would not be the identity. Therefore,
    by the inductive hypothesis, we are done.
\end{proofi}

\subsubsection{Parity of a permutation}
Clearly, the product which a permutation can be written as is not unique however,
its parity (evenness or oddness) is. The \emph{parity} of a permutation
is the parity of the number of transpositions used in one of its product representations.
That is if $\sigma  \in S_n$ and
\[
    \sigma = \sigma_1\sigma_2\cdots \sigma_m = \tau_1\tau_2\cdots \tau_n
\]
\begin{proofi}
    Notice that the inverse of $\sigma $ can be written as
    \[
        \sigma^{-1} = \sigma_m\sigma_{m-1}\cdots\sigma_1
    \]
    then
    \[
        (1) = \sigma\sigma^{-1} = (\tau_1\tau_2\cdots \tau_n)(\sigma_m\sigma_{m-1}\cdots\sigma_1)
    \]
    and the result follows since all representations of the identity permutation consists of an even number of 
    transpositions.
\end{proofi}

\subsubsection{Alternating Group}
The \emph{Alternating group of degree $n$} denoted $A_n$ is the subset of even permutations in $S_n$. 
It is clear that $A_n$ is in fact a subgroup of $S_n$ by the two step subgroup test since
\begin{itemize}
    \item the composition of two even permutations is also even (count the sum of transpositions)
    \item the inverse of an even permutation is even (the inverse is simply given by reversing the order of the product).
\end{itemize}
Furthermore $|A_n| = \frac{|S_n|}{2} = \frac{n!}{2}$.
\paragraph*{Note: }TODO: I do not know how to prove that the order $A_n$ is half that of $S_n$.


% \subsection{Symmetries}
% ????
% TODO:
%  - definition and notation
%  -cycles
%  -orbit
%  -cycle decomposition
%  -order ? (put this in groups?)
%  -transposition
%     -generating for cycles
%    -parity

