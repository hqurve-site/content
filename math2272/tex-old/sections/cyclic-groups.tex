\section{Cyclic groups}
A group $G$ is called \emph{cyclic} if $\exists g\in G$ called a \emph{generator}
such that 
\[
    G = \{g^n: i \in \mathbb{Z}\} = gp(g)
\]
or alternatively, $\forall a \in G: \exists n\in \mathbb{Z}: g^n = a$. Note that we don't need
to show that $\{g^n\} \subseteq G$ since the operation is closed. Furthermore,
note that $G$ must be abelian. 

If the cyclic group $G$ is finite, we say we can represent it as $G = C_n$ 
(where $n=|G|$) due to the immediate isomorphism with other
cyclic groups of the same order. Additionally, note that the order of the generator $g$ is
also $n$.

\subsubsection{More on generated groups}
Let $G$ be a group and $x \in G$. Then the set $gp(x)$ is the smallest subgroup
of $G$ that contains $x$.
\begin{proofi}
    Firstly, note that $gp(x)$ is a subgroup of $G$ by the one step subgroup test since for any $a = x^m, b = x^n \in gp(x)$
    \[
        ab^{-1} = x^m(x^n)^{-1} = x^{m-n} \in gp(x)
    \]
    Now suppose that $S$ is a subgroup of $G$ that also contains $x$. Then
    $x^m \in S$ $\forall m \in \mathbb{Z}$ by closure and existence and uniqueness of both 
    inverse and identity. Therefore $gp(x) \subseteq S$ and the result follows.
\end{proofi}

\subsubsection{More on generators}
Let $G$ be a finite cyclic group of order $n$ and generator $g$. Then
$g^k = e$ (the identity) if and only if $n$ divides $k$.
\begin{proofi}This proof was taken from proposition 4.12 in Judson.
    
    Firstly, by the division algorithm $k = nq + r$ (where $0 \leq r < n$)
    Then
    \[
        g^{k} = g^{nq + r} = (g^n)^q g^r = e^qg^r = g^r = e
    \]
    Then since the order of $g$ is $n$, $r = 0$. Conversely if $k =qn$
    \[
        g^k = g^{qn} = (g^n)^q = e^q = e
    \]
\end{proofi}
\subsubsection{Order of elements}
Let $G$ be a finite cyclic group of order $n$ and generator $g$. Then
$b = g^k$ has order $\frac{n}{d}$ where $d = \gcd(k, n)$.
\begin{proofi}
    This proof was taken from theorem 4.13 in Judson.

    Let $m$ a positive integer such that $b^m = g^{km} = e$. Then by the above theorem,
    $n | km$ and hence $\frac{n}{d}| \frac{k}{d}m$. Note that
    \[
        \gcd(n, k) = d \implies \gcd\paren{\frac{n}{d}, \frac{k}{d}}
    \]
    Therefore $\frac{n}{d} | m$ and the least value for $m$ is $m = \frac{n}{d}$.
\end{proofi}


\subsection{Finite cyclic groups}
If $G$ is a finite group of order $n$ and $g \in G$ has order $n$, then
$G$ is a cyclic group.
\begin{proofi}
    We will show that $G = \{g^i: i \in \mathbb{Z}^+\}$ by showing that $|gp(g)| = n$.
    Suppose that two there exists $0 < i < j < n$ such that $g^i=g^j$. Then
    \[
        g^{j-i} = (g^j)(g^{-i}) = g^{i}g^{-i} = e
    \]
    We now have a contradiction since $0 < j- i < n$ while $n$ is the order of $g$.
\end{proofi}

\subsection{Subgroups}
If $G$ is a cyclic group with generator $x$, and $H \subseteq G$ is a subgroup of $G$ then $H$
is also cyclic. Furthermore, either $H = gp(x^0) = \{e\}$ or $H= gp(x^k)$ where $x^k$ is the
least (positive) power of $x$ in $H$.

\begin{proofi}This proof is basically the same as that given during class.

    Let $G = gp(x)$ and $e$ be the identity.
    Then, note that since $H$ is a subgroup, $e \in H$. If $H = \{e\}$
    we are done. Consider the case when $H \neq \{e\}$, then we
    can choose the least positive integer $k$ such that $x^k \in H$.
    By the closure of subgroups, $gp(x^k) \subseteq H$.

    Now, consider an other element $x^j \in H$. By the division algorithm, 
    we can represent $j = qk + r$ such that $0 \leq r < k$. Then
    by the closure of subgroups
    \[
        x^j x^{-qk} = x^{qk + r} x^{-qk} = x^r \in H
    \]
    Note that $r = 0$ would contradict the minimality of $k$ and hence 
    \[
        x^j = x^qk = (x^k)^q \in gp(x^k)  
    \]
    and the result follows.
\end{proofi}

Note that as a result, if $G$ is finite, each of its (non trivial) subgroups is uniquely identified by 
their least positive power of $x$. Then, by B\'ezout's identity that this least positive power $k$
must divide the order of $G$ (otherwise minimality would be contradicted) and the number of non-trivial subgroups
is equal to the number of proper divisors of the order of $G$. Therefore, the number
of subgroups of a finite cyclic group is equal to the number of divisors of its order.


\subsection{Generating Sets}
Let $G$ be a group and $M \subseteq G$ be a non-empty subset. Then, we define
$\langle M \rangle$ be the intersection of all subgroups of $G$ which are supersets of $M$.
Then immediately, $\langle M \rangle$ must be the smallest subgroup of $G$ which contains $M$.
Also, we say $M$ is the \emph{generating set} of $\langle M \rangle$, or $M$ \emph{generates} (or consists
of \emph{generators} of) $\langle M \rangle$. Furthermore, if $M$ is finite,
we say that $\langle M \rangle$ is \emph{finitely generated}.

\subsubsection{Elements in generated set}
Let $G$ be a group and $M \subseteq G$ be a non-empty subset. 
Then, $m \in \langle M \rangle$ exactly when $\exists g_1, g_2, \ldots g_n \in M$
and $\alpha_1, \alpha_2, \ldots, \alpha_n \in \mathbb{Z}$
such that 
\[
    m = g_1^{\alpha_1} g_2^{\alpha_2} \cdots g_n^{\alpha_n}
\]
where the $g_i$ are not necessarily distinct (due to issues with non-commutativity).

\begin{proofi}
    This proof is taken the Mr Daaga's notes. I don't know where he sourced it from.

    Firstly, let $K$ be the set of all products of the form $g_1^{\alpha_1} g_2^{\alpha_2} \cdots g_n^{\alpha_n}$.
    Then, clearly, $K$ is a subset of $\langle M \rangle$ by the closure of subgroups and $M \subseteq K$. We only need to 
    show that $K$ is also a subgroup and since $\langle M \rangle$ is the smallest subgroup containing $M$,
    the result will follow.
    Firstly, note that $K$ is non-empty since $g^0_i = e \in K$. Also,
    clearly closure is exhibited in $K$. Now consider $g_1^{\alpha_1} g_2^{\alpha_2} \cdots g_n^{\alpha_n} \in K$,
    its inverse is given by
    \[
        (g_1^{\alpha_1} g_2^{\alpha_2} \cdots g_n^{\alpha_n})^{-1} = g_n^{-\alpha_n}\cdots g_2^{-\alpha_2} g_1^{-\alpha_1} \in K
    \]
    and by the two-step subgroup test, the result follows.
\end{proofi} 