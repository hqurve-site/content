= Rings

Let stem:[ (R, + , \cdot)] be a non-empty set with two binary
operations. We call stem:[ + ] _addition_ and stem:[\cdot]
_multiplication_. Then stem:[(R, + , \cdot)] is called a _ring_ if

. stem:[\forall a, b \in R: a + b = b + a]
. stem:[\forall a, b, c, \in R: (a+b) + c = a+ (b+c)]
. stem:[\exists z \in R: \forall a \in R: a  +z = a]
. stem:[\forall a \in R: \exists (-a) \in R: a + (-a) = z]
. stem:[\forall a, b, c \in R: (a\cdot b) \cdot c = a \cdot (b \cdot c)]
. stem:[\forall a, b, c \in R: a\cdot (b+c) = (a\cdot b) + (a \cdot c) \text{ and } (b+c) \cdot a = (b \cdot a) + (c\cdot a)]

Alternatively, we could simplify the above definition as follows.
stem:[(R, +, \cdot)] is a ring iff

. stem:[(R, +)] forms an abelian group.
. stem:[(R, \cdot)] forms a semigroup.
. stem:[\cdot] distributes over stem:[+] in
stem:[R].

Furthermore, stem:[(R, + , \cdot)] is called

* a _commutative ring_ if stem:[\cdot] is commutative
* a _ring with unity_ if
stem:[\exists u \in R: \forall a \in R: a\cdot u = u \cdot a].
That is, stem:[(R, \cdot)] forms a monoid.

We call stem:[z] the _zero element_ and stem:[u] _unity_.

== Nice properties

Let stem:[(R, +, \cdot)] be a ring and
stem:[a, b, c \in R]. Then we have the following properties

. stem:[a + b = a \implies b = z] (uniqueness of identity).
. stem:[-(-a) = a]
. stem:[-(a+b) = (-a) + (-b)]
. stem:[(a + (-b)) + (-c) = a + (-(b+c))]
. stem:[a\cdot z = z\cdot a = z]
. stem:[a\cdot( -b) = (-a)\cdot b = - (a\cdot b)]

== Convensions

Let stem:[(R, + , \cdot )] be a ring and stem:[a, b \in R]
and stem:[m, n \in \mathbb{Z}^+ ], then we define the following
convensions

. stem:[a -b := a + (-b)]
. stem:[ab := a\cdot b]
. stem:[-ab := -(ab) = (-a)b = a(-b)]
. stem:[ma := (m-1)a + a] where stem:[1a := a].
. stem:[(-m)a = m(-a)]
. stem:[a^m = a^{m-1}a] where stem:[a^1 = a].
. stem:[a^0 = u] if stem:[u] exists.
. stem:[a^{-m} = (a^{-1})^m] if stem:[u] and
stem:[a^{-1}] exist.

== Zero divisors

Let stem:[R] be a ring, then stem:[a \in R - \{z\}] is a
_zero divisior_ iff

[stem]
++++
\exists b \in S - \{z\}: ab = z
++++

== Subrings

Let stem:[R] be a ring. Then, a _subring_ stem:[S] of
stem:[R] is a subset of stem:[R] which is also a ring with
the same operations.

=== Two step subring test

Let stem:[R] be a ring and stem:[S] be a non-empty subset
of stem:[R], then stem:[S] is a subring of stem:[R]
iff stem:[\forall a,b \in S: a-b \in S \ \land \ ab \in S].

=== Three step subring test

Let stem:[R] be a ring and stem:[S] be a non-empty subset
of stem:[R], then stem:[S] is a subring iff
stem:[\forall a, b \in S: a+b \in S\ \land\ ab \in S\ \land\ -a \in S].

== Characteristic

Let stem:[R] be a ring then

* if stem:[\exists n \in \mathbb{Z}^+: \forall r \in R: nr = z],
the smallest such stem:[n] is called the _characteristic of_
stem:[R].
* otherise, stem:[R] has characteristic stem:[0].

== Integral domain

Let stem:[R] be a commutative ring with unity, then
stem:[R] is called an _integral domain_ iff stem:[R]
contains no zero divisors. That is,

[stem]
++++
\forall a, b \in R: ab = z \iff a = z \text{ or } b = z
++++
then,

* the cancellation law for multiplication holds. That is
+
[stem]
++++
\forall a \in R -\{z\}: \forall b,c \in R: ab = ac \ \lor \ ba = ca \implies b = c
++++

=== Integers modolo n

Consider the ring of stem:[\mathbb{Z}_n] and notice that it is a
commutative ring with unity. Then, we have the following theorem

[stem]
++++
\mathbb{Z}_n \text{ is an integral domain} \iff n \text{ is prime}
++++

== Division ring

Let stem:[R] be a ring. Then, stem:[R] is called a
_division ring_ iff stem:[(R -\{z\}, \cdot)] forms a group.

== Ideals

Let stem:[R] be a ring. Then, stem:[I \subseteq R] is a

* _right ideal_ of stem:[R] if stem:[(I, +)] is a subgroup
of stem:[(R, +)] and
+
[stem]
++++
\forall r \in R: \forall a \in I: ar \in I
++++
* _left ideal_ of stem:[R] if stem:[(I, +)] is a subgroup
of stem:[(R, +)] and
+
[stem]
++++
\forall r \in R: \forall a \in I: ra \in I
++++

Furthermore stem:[I] is simply called a _ideal_ of
stem:[R] if it is both a right and left ideal. Also, we call
stem:[I = \{z\}] and stem:[I = R] _trivial ideals_.

NOTE: This is a stronger condition than closure of multiplication which is
used in subrings. Also, ideals are said to _absorb_ multiplication (from
the left and/or right).

=== The only ideal to contain unity is the ring itself

Let stem:[R] be a ring with unity. Then if
stem:[I \subseteq R] is an ideal (left or right)

[stem]
++++
e \in I \iff I = R
++++

.Proof
[%collapsible]
====
Notice that the reverse direction is trivial. Now, suppose WLOG
that stem:[I] is a left ideal of stem:[R] where stem:[e\in I]. Then
[stem]
++++
\forall r \in R: re = r \in I
++++
and we are done.
====

=== Principal Ideals

Let stem:[R] be a commutative ring with unity, then

[stem]
++++
(a) = \{ar: r \in R\}
++++
is called a _principal ideal_ where stem:[a \in \mathbb{R}].
Then, stem:[(a)] is also a ideal.

.Proof
[%collapsible]
====
Firstly, note that by the one step subgroup test, stem:[((a), + )] is a 
subgroup of stem:[(R, + )] since
[stem]
++++
    \forall ar, as \in (a): ar - as = a(r-s) \in (a)
++++
since stem:[r-s \in R]. Finally, notice that stem:[(a)] is an ideal since
[stem]
++++
    \forall t \in R: \forall ar \in (a): t(ar) = (ar)t  = a(rt) \in (a)
++++
====

=== One sided principal ideals

Let stem:[R] be a ring with ideal stem:[I]. Then,
stem:[I] is called a

* _right principal ideal_ if stem:[\exists a \in I] such that
stem:[I = \{ar : r \in R\}]
* _left principal ideal_ if stem:[\exists a \in I] such that
stem:[I = \{ra : r \in R\}]

If stem:[I] is both a left and right principal ideal,
stem:[I] is also a principal ideal. Note that the condition that
stem:[I] be an ideal is necessary otherwise absorbtion from both
sides would not be guaranteed.

=== All ideals in the integers are principal ideals

Consider the ring of integers with ideal stem:[I], then
stem:[I] is a principal ideal.

.Proof
[%collapsible]
====
Let stem:[I \subseteq \mathbb{Z}] be an ideal and let stem:[a] be the least positive integer
in stem:[I].

NOTE: If stem:[I = \{0\}] it is automatically principal. Otherwise, there exists stem:[x \in I \backslash \{0\}].
Then either stem:[x] or stem:[-x \in I], one of which must be positive. Therefore stem:[a] exists.

Now, consider any integer stem:[b \in I] and perform the division algorithm on
it with respect to stem:[a]. That is, we write stem:[b = ka + r] where stem:[k \in \mathbb{Z}]
and stem:[0 \leq r < a]. Then, since stem:[I] is an ideal, stem:[ka \in I] and hence
stem:[r = b-ka \in I]. Notice that since stem:[a] is minimal, stem:[r = 0] and stem:[b = ka] and
we are done.
====

== Quotient Rings

Let stem:[R] be a ring with ideal stem:[J]. Then, the set
of additive cosets of stem:[J] form a ring where

[stem]
++++
\forall (a + J), (b + J) \in R/J: (a+J) + (b + J) = (a+b) + J
++++
and

[stem]
++++
\forall (a + J), (b + J) \in R/ J: (a + J)(b + J) = ab + J
++++
both of these operations are well defined satisfy the ring axioms.
We call this ring the _quotient ring of_ stem:[J] in
stem:[R].

.Proof of well defined
[%collapsible]
====
Let stem:[a, a', b, b' \in R] such that
[stem]
++++
a + J = a' + J \quad\text{and}\quad b + J = b' + J
++++
Then, stem:[a - a' \in J] and stem:[b - b' \in J]. Next, notice that
since stem:[J] is an ideal
[stem]
++++
(a-a') + (b - b') = (a+b) - (a' + b') \in J
\implies (a+b) + J = (a' + b') + J
++++
Therefore, addition is well defined.

Next, notice that
[stem]
++++
\begin{aligned}
&(a-a') b = ab - a'b \in J\quad\text{and}\quad a'(b-b') = a'b - a'b' \in J
\\&\quad\implies ab - a'b' \in J
\\&\quad\implies ab + J = a'b'+ J
\end{aligned}
++++
Therefore, multiplication is well defined.
====

.Proof of ring axioms
[%collapsible]
====
We would do this in two parts.

Firstly, lets prove that stem:[(R/J, + )] is an abelian group.
Notice that since stem:[(R, + )] is an abelian group, stem:[(J, + )] is a normal subgroup,
and hence stem:[(R/J, + )] is a xref:normal/index.adoc#_quotient_groups[quotient group]. Furthermore,
it is abeliean.

Next, we need to show that stem:[(R/J, \cdot)] forms a xref:groups/index.adoc[semigroup].

Closure:: Consider stem:[(a + J), (b + J) \in R/J] then by definition of its operation stem:[ab + J \in R/J]

Associative:: Since stem:[(R, \cdot)] forms a semigroup, stem:[\cdot] is associative and hence if stem:[(a + J), (b + J), (c+ J) \in R/J]
+
[stem]
++++
\begin{aligned}
[(a+J)(b+J)](c+J)
&= (ab + J)(c+J)
\\&= (ab)c + J
\\&= a(bc) + J
\\&= (a + J)(bc + J)
\\&= (a + J)[ (b + J)(c + J) ]
\end{aligned}
++++

====

== Ring homomorphisms

Let stem:[R] and stem:[S] be rings (not necessarily with
the same operations, then stem:[f\colon R \to S] is called a
_ring homomorphism_ if

[stem]
++++
\forall a, b \in R: f(a + b) = f(a) + f(b) \text{ and } f(a\cdot b) = f(a) \cdot f(b)
++++
Furthermore, if stem:[f] is bijective, stem:[f] is called
a _isomorphism_ and stem:[R] is said to be _isomorphic_ to
stem:[S].

=== Nice properties

Let stem:[R] and stem:[S] be rings with homomorphism
stem:[f \colon R \to S], then

* stem:[f(z_R) = z_S]
* stem:[f(e_R) = e_S] if stem:[R] is a ring with unity and
stem:[f] is surjective

.Proof
[%collapsible]
====
The first property follows directly from the fact that stem:[(R, + )] and stem:[(S, + )] 
are groups and hence stem:[f] is also a group homomorphism from stem:[R] to stem:[S].

For the second. Consider stem:[s \in S]. Then since stem:[f] is surjective, there exists
stem:[r \in R] such that stem:[f(r) = s]. Then
[stem]
++++
s f(e_R) = f(r)(e_R) = f(re_R) = f(r) = s = f(r) = f(e_R r ) = f(e_R) f(r) = f(e_R) s
++++
and hence stem:[f(e_R)] is the unity within stem:[S].
====

=== Kernel

Let stem:[R] and stem:[S] be rings with homomorphism
stem:[f\colon R\to S], then

[stem]
++++
\ker f = \{r \in R: f(r) = z \in S\}
++++
Then, stem:[\ker f = \{z\} \iff f] is injective.

NOTE: This directly follows from the fact that stem:[(R, + )] and stem:[(S, + )] are groups
and hence stem:[f] is a group homomorphism between the two.
