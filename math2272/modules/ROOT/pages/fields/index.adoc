= Fields

We may define fields in one of the following equivalent ways,

. A ring in which non-zero elements form an abelian group under
multiplication
. A commutative division ring

== Finite integral domains

Let stem:[R] be a finite integral domain, then stem:[R] is
a field.^[7]^
