= Stationary points and Optimization

== Stationary points

Let stem:[f: D \rightarrow \mathbb{R}] be a function, such that
stem:[D \subseteq \mathbb{R}^n]. A _critical (stationary or
level) point_ of stem:[f] is a point stem:[\bm{x}_0] at
which all first partial derivatives vanish, ie

[stem]
++++
\forall i = 0, 1, \ldots, n: \left.\frac{\partial f}{\partial x_i}\right|_{\bm{x}_0}
++++
Geometrically, a critical point is a place where the graph of
stem:[f] is horizontal (ie all tangent lines are horizontal).
Additionally, The rational for this definition can be obtained by
looking at the definition of differentiable function. Furthermore, while
we call stem:[\bm{x}_0] the critical point,
stem:[f(\bm{x}_0)] is called the critical value.

=== Maximums and minimums

stem:[\bm{x}_0 \in D] is said to be

* a _relative (local) maximum_ if open subset
stem:[\exists: B \subseteq D] such that
stem:[\forall \bm{x}\in B: f(\bm{x}_0) \geq f(\bm{x})].
* a _relative (local) minimum_ if open subset
stem:[\exists: B \subseteq D] such that
stem:[\forall \bm{x}\in B: f(\bm{x}_0) \leq f(\bm{x})].
* a _absolute (global) maximum_ if
stem:[\forall \bm{x}\in D: f(\bm{x}_0) \geq f(\bm{x})].
* a _absolute (global) minimum_ if
stem:[\forall \bm{x}\in D: f(\bm{x}_0) \leq f(\bm{x})].


.Location of local maxima and minima
[.theorem]
****
If stem:[f] is continuous and all first partial
derivatives exists, then the local maxima and minima of stem:[f]
occur either at the boundary of stem:[D] or at the critical
points.
****

.Extreme Value Theorem
[.theorem]
****
If stem:[R] is a closed region in stem:[\mathbb{R}^n],
which stem:[f] is continuous on, then stem:[\exists] at
least one point in stem:[R] where stem:[f] has an absolute
maximum value and another where stem:[f] has an absolute minimum
value.
****

=== Classification of stationary points

Let stem:[f] be a function fo two variables stem:[x] and
stem:[y] such that its first and second order partial derivatives
are continuous on an open disk containing the stationary point
stem:[(a, b)]. That is

[stem]
++++
f_x(a,b) = 0\quad\quad f_y(a,b) = 0
++++
Now define

[stem]
++++
p = f_{xx}(a,b)
    \quad\quad
q = f_{xx}(a,b)
    \quad\quad
r = f_{xy}(a,b)
++++
Then

* stem:[(a,b)] is a local minima if stem:[pq - r^2 > 0]
and stem:[p > 0] (or stem:[q > 0]).
* stem:[(a,b)] is a local maxima if stem:[pq - r^2 > 0]
and stem:[p < 0] (or stem:[q < 0]).
* stem:[(a,b)] is a saddle point if stem:[pq - r^2 > 0]
* If stem:[pq -r^2 = 0], no conclusion can be made

[NOTE]
Since the second order partial derivatives of stem:[f] are
continuous, stem:[f_{xy} = f_{yx}]

== Lagrange Multipliers

_Lagrange multipliers_ are a useful tool when trying to optimize (find
relative extrema) a function subject to one or more constraints.

Let stem:[f: D \rightarrow \mathbb{R}] be a continuously
differentiable function, such that
stem:[D \subseteq \mathbb{R}^n]. Then the relative extrema of
stem:[f] subject to the stem:[k] constraints
stem:[\varPhi_i(x_1, x_2, \ldots, x_n) = 0] for
stem:[i = 1, 2,\ldots, k] (which are also continuously
differentiable) are also relative extrema of the following auxiliary
function

[stem]
++++
F(x_1, x_2, \ldots, x_n, \lambda_1, \lambda_2, \ldots, \lambda_k) 
    = f + \lambda_1\varPhi_1 + \lambda_2\varPhi_2 + \cdots + \lambda_k\varPhi_k
++++
where stem:[\lambda_1, \lambda_2, \ldots, \lambda_k] (independent
of stem:[x_1, x_2, \ldots, x_n]) are called _Lagrange
multipliers_.

Therefore, finding the relative extrema of stem:[f] with
constraints stem:[\varPhi_1, \varPhi_2, \ldots, \varPhi_k]
reduces to finding solutions to

[stem]
++++
F_{x_1} = 0, \; 
    F_{x_2} = 0, \; 
    \ldots,\;
    F_{x_n} = 0
++++
and

[stem]
++++
F_{\lambda_1} = 0, \; 
    F_{\lambda_2} = 0, \; 
    \ldots,\;
    F_{\lambda_k} = 0
++++
Notice that these last stem:[k] equations are equivalent to the
constraints stem:[\varPhi_i(x_1, x_2, \ldots, x_n) = 0] (for
stem:[i = 1, 2,\ldots, k]).

=== Reasoning

Consider any point stem:[\bm{x}] which satisfies all constraints.
Then the set of directions allowed by all constraints is precisely the
space of directions perpendicular to the gradients of all constraints.
That is, if the space of allowable moves is stem:[A] at
stem:[\bm{x}] and the span of the constraints’ gradients is
stem:[S], then stem:[A = S^{\perp}]. Furthermore, by
considering the (constrained) points at which stem:[f] has an
extremum (ie doesn’t change), we see that the allowable directions must
be perpendicular to the gradient of stem:[f] otherwise, we could
increase/decrease stem:[f] by moving in that allowed direction.
Therefore, stem:[\nabla f(\bm{x}) \in A^\perp = S] and hence
there exists stem:[\lambda_1, \lambda_2, \ldots, \lambda_k] such
that

[stem]
++++
\nabla f(\bm{x}) = \sum_{i=1}^k \lambda_i \nabla \varPhi_i(\bm{x})
    \quad\Longleftrightarrow\quad
    \nabla f(\bm{x}) - \sum_{i=1}^k \lambda_i \nabla \varPhi_i(\bm{x}) = \bm{0}
++++
This condition along with the constraints are equivalent finding to
stationary points of the auxiliary function. Note that this condition is
necessary but not sufficient (ie not all solutions are indeed
solutions).

