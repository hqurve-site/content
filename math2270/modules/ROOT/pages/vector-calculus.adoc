= Vector Calculus

NOTE: This section mostly focuses on 3 dimensional euclidean vectors;
however, most of these concepts can be extended to other vector spaces.

== Basics

=== Vector and scalar fields

A _vector field_ associates a vector with each particular point while a
_scalar field_ associates a scalar value with each point. Basically,
both are just functions outputting vectors and scalars respectively. In
this section a _vector function_ would refer to a function outputting
vectors while a _scalar function_ would refer to a function outputting
scalars.

=== Limit definition

A vector function stem:[\bm{F}(u)] is said to have a limit value
stem:[\bm{P}] at point stem:[u_0] if for any
stem:[\varepsilon > 0], stem:[\exists \delta > 0] such
that

[stem]
++++
\left| u - u_0 \right| < \delta \implies \left| \bm{F}(u)- \bm{P} \right| < \varepsilon
++++
and is written as

[stem]
++++
\lim_{u\rightarrow u_0} \bm{F}(u) = \bm{P}
++++
where stem:[\left| \cdot \right|] is a norm (may differ for
domain and codomain). Note that this is basically the same
stem:[\varepsilon-\delta] definition and this observation is true
for most definitions including continuity and other properties.

=== Differential

If

[stem]
++++
\bm{F}(u) = F_1(u)\hat{i} + F_2(u)\hat{j} + F_3(u)\hat{k}
++++
then

[stem]
++++
\frac{\partial \bm{F}}{\partial t} 
    = \frac{\partial F_1}{\partial t}\hat{i}
    + \frac{\partial F_2}{\partial t}\hat{j}
    + \frac{\partial F_3}{\partial t}\hat{k}
++++
Actually, the total differential of stem:[\bm{F}] is just the
Jacobian matrix of stem:[\bm{F}] and these are just results of
the jacobian matrix.

=== Derivatives of products

Consider vector function stem:[\bm{A}] and stem:[\bm{B}],
and the scalar function stem:[\phi]. then

* stem:[\displaystyle\frac{\partial }{\partial t}\left( \phi \bm{A} \right) = \phi \frac{\partial \bm{A}}{\partial t} + \frac{\partial \phi}{\partial t}\bm{A}]
* stem:[\displaystyle\frac{\partial }{\partial t}\left( \bm{A}\bm{\cdot}\bm{B} \right) = \bm{A}\bm{\cdot}\frac{\partial \bm{B}}{\partial t} + \frac{\partial \bm{A}}{\partial t}\bm{\cdot}\bm{B}]
* stem:[\displaystyle\frac{\partial }{\partial t}\left( \bm{A}\times \bm{B} \right) = \bm{A}\times \frac{\partial \bm{B}}{\partial t} + \frac{\partial \bm{A}}{\partial t}\times\bm{B}]

Notice that they are just the usual product rule just with different
operators. In fact, the product rule holds for all bilinear operators.
However, be careful with operators which are not commutative (such as
the cross product).

=== Integrals

Integrals, like derivatives, are computed component-wise. That is if

[stem]
++++
\bm{F}(u) = F_1(u)\hat{i} + F_2(u)\hat{j} + F_3(u)\hat{k}
++++

[stem]
++++
\int_C\bm{F}(t)dt = \int_C F_1(t) dt\hat{i} + \int_C F_2(t) dt\hat{j} + \int_C F_3(t) dt\hat{k}
++++

== Parameterized curves

Consider the curve stem:[\bm{r}(t)] where
stem:[t\in \mathbb{R}].

=== Arc length

The arc length of stem:[\bm{r}], stem:[s], from a point
stem:[a] is given by

[stem]
++++
s(t)= \int_a^t \sqrt{
            \left( \frac{dx}{du} \right)^2
            +\left( \frac{dy}{du} \right)^2
            +\left( \frac{dz}{du} \right)^2
        } du
        = \int_a^t \left| \bm{r}'(u) \right| du
++++

=== Tangent and smooth

Furthermore, the curve is called _smooth_ on interval stem:[I] if
stem:[\bm{r}'] is continuous and
stem:[\bm{r}' \neq \bm{0}] on stem:[I]. Then the length of
unit tangent vector is given by

[stem]
++++
\bm{T}(t)  = \frac{\bm{r}'(t)}{\left| \bm{r}'(t) \right|}
++++
and indicates the direction of stem:[\bm{r}].

=== Curvature

The curvature at a given point is a measure of how quickly the curve
changes direction and it is given by

[stem]
++++
\kappa = \left| \frac{d\bm{T}}{ds} \right|
++++
and can be simplified using the chain rule as

[stem]
++++
\kappa = \left| \frac{d\bm{T}}{ds} \right| = \left| \frac{d\bm{T}/dt}{ds/dt} \right| = \frac{\left| \bm{T}'(t) \right|}{\left| \bm{r}'(t) \right|}
++++
since stem:[\frac{ds}{dt} = \left| \bm{r}'(t) \right|].
Additionally, it can be written as

[stem]
++++
\kappa = \frac{\left| \bm{r}'(t) \times \bm{r}''(t) \right|}{\left| \bm{r}'(t) \right|^3}
++++
by rewriting stem:[\left| \bm{T}'(t) \right|] using the fact that
stem:[\left| \bm{f}(t) \right| = c \implies \bm{f}(t) \bm{\cdot}\bm{f}'(t) = 0].

=== Normal and Binormal vectors

The normal and binormal vectors are unit vectors which are both
perpendicular to the curve at a point in addition to being perpendicular
to each other. The _unit normal_ is defined using
stem:[\bm{T}(t)] as

[stem]
++++
\bm{N}(t) = \frac{\bm{T}'(t)}{\left| \bm{T}'(t) \right|}
++++
since stem:[\left| \bm{T}(t) \right| =1] and hence
stem:[\bm{T}(t) \bm{\cdot}\bm{T}'(t) = 0]. The _binormal vector_
is simply defined as

[stem]
++++
\bm{B}(t) = \bm{T}(t) \times \bm{N}(t)
++++
which is already a unit vector since stem:[\bm{T}] and
stem:[\bm{N}] are orthogonal unit vectors.

=== Notable planes

The plane determined by the normal and binormal vectors
stem:[\bm{N}] and stem:[\bm{B}] at a point stem:[P]
is called the _normal plane_ and consists of all vectors orthogonal to
the curve at stem:[P]. Whilst the plane determined by the vectors
stem:[\bm{T}] and stem:[\bm{N}] is called the _osculating
plane_ of the curve at stem:[P] and it is the plane that comes
closest to containing part of the curve near stem:[P].

The circle that lies in the osculating plane at stem:[P], has the
same tangent as the curve at stem:[P], lies on the concave side
of the curve (toward which stem:[\bm{N}] points) and has radius
stem:[\frac{1}{\kappa}] is called the _osculating circle_ and is
the circle which best describes how the curve behaves near
stem:[P].

== Direction cosines

Direction cosines are a simple way to define the direction of a vector
(in a symmetric manner). Consider the points
stem:[\bm{P}_0= (x_0, y_0, z_0)] and stem:[\bm{P}=(x,y,z)]
in stem:[\mathbb{R}^3] then the direction cosines are defined as

[stem]
++++
\cos(\alpha) = \frac{x-x_0}{\left| \bm{P_0P} \right|}
    ,\quad
    \cos(\beta) = \frac{y-y_0}{\left| \bm{P_0P} \right|}
    ,\quad
    \cos(\gamma) = \frac{z-z_0}{\left| \bm{P_0P} \right|}
++++
where stem:[\alpha, \beta, \gamma \in [0, \pi\]]

[stem]
++++
\left| \bm{P_0P} \right| = s = \sqrt{(x-x_0)^2 + (y-y_0)^2 + (z-z_0)^2}
++++
Then stem:[\cos^2(\alpha) + \cos^2(\beta) + \cos^2(\gamma) = 1].
Therefore, using the chain rule, the derivative of function a
stem:[f] at stem:[\bm{P}_0] in direction
stem:[\bm{P_0P}] is given by

[stem]
++++
\begin{aligned}
    \frac{\partial f}{\partial s} 
    &= \frac{\partial f}{\partial x}\frac{\partial x}{\partial s}
        + \frac{\partial f}{\partial y}\frac{\partial y}{\partial s}
        + \frac{\partial f}{\partial z}\frac{\partial z}{\partial s}
        \\
    &= \frac{\partial f}{\partial x}\cos(\alpha)
        + \frac{\partial f}{\partial y}\cos(\beta)
        + \frac{\partial f}{\partial z}\cos(\gamma)\end{aligned}
++++
which is called the directional derivative of stem:[f] in
direction stem:[\alpha], stem:[\beta],
stem:[\gamma].

REMARK: Really, I don’t understand why this is done since this is just a special
case of the chain rule and can be computed just as easily without
considering angles and just using unit vectors instead. The case for
direction cosines is worsened even further since they are not even
independent. Furthermore, two angles are not sufficient since there are
two solutions for the sum of squares formula.

