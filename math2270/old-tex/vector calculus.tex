\section{Vector Calculus}
Note that this section mostly focuses on 3 dimensional euclidean vectors however, most of
these concepts can be extended to other vector spaces.

\subsection{Basics}
\subsubsection*{Vector and scalar fields}
A \emph{vector field} associates a vector with each particular point while a \emph{scalar field}
associates a scalar value with each point. Basically, both are just functions outputting
vectors and scalars respectively. In this section a \emph{vector function} would
refer to a function outputting vectors while a \emph{scalar function} would refer to 
a function outputting scalars.

\subsubsection*{Limit definition}
A vector function $\vec{F}(u)$ is said to have a limit value $\vec{P}$ at point $u_0$ if
for any $\varepsilon > 0$, $\exists \delta > 0$ such that 
\[
    \abs{u - u_0} < \delta \implies \abs{\vec{F}(u)- \vec{P}} < \varepsilon
\]
and is written as 
\[
    \lim_{u\rightarrow u_0} \vec{F}(u) = \vec{P}
\]
where $\abs{\cdot}$ is a norm (may differ for domain and codomain). Note that this is 
basically the same $\varepsilon-\delta$ definition and this observation is true for most definitions
including continuity and other properties.

\subsubsection*{Differential}
If 
\[
    \vec{F}(u) = F_1(u)\hat{i} + F_2(u)\hat{j} + F_3(u)\hat{k}
\]
then
\[
    \pLeibniz{\vec{F}}{t} 
    = \pLeibniz{F_1}{t}\hat{i}
    + \pLeibniz{F_2}{t}\hat{j}
    + \pLeibniz{F_3}{t}\hat{k}
\]
Actually, the total differential of $\vec{F}$ is just the Jacobian matrix of $\vec{F}$ and these
are just results of the jacobian matrix.

\subsubsection*{Derivatives of products}
Consider vector function $\vec{A}$ and $\vec{B}$, and the scalar function $\phi$. then
\begin{itemize}
    \item $\displaystyle\pLeibniz{}{t}\paren{\phi \vec{A}} = \phi \pLeibniz{\vec{A}}{t} + \pLeibniz{\phi}{t}\vec{A}$
    \item $\displaystyle\pLeibniz{}{t}\paren{\vec{A}\dotp \vec{B}} = \vec{A}\dotp \pLeibniz{\vec{B}}{t} + \pLeibniz{\vec{A}}{t}\dotp\vec{B}$
    \item $\displaystyle\pLeibniz{}{t}\paren{\vec{A}\times \vec{B}} = \vec{A}\times \pLeibniz{\vec{B}}{t} + \pLeibniz{\vec{A}}{t}\times\vec{B}$
\end{itemize}
Notice that they are just the usual product rule just with different operators. In fact, the product rule
holds for all bilinear operators. However, be careful with operators which are not commutative (such as the cross product).

\subsubsection*{Integrals}
Integrals, like derivatives, are computed component-wise. That is if
\[
    \vec{F}(u) = F_1(u)\hat{i} + F_2(u)\hat{j} + F_3(u)\hat{k}
\]
\[
    \int_C\vec{F}(t)dt = \int_C F_1(t) dt\hat{i} + \int_C F_2(t) dt\hat{j} + \int_C F_3(t) dt\hat{k}
\]

\subsection{Parameterized curves}%James stewart chapter 13.3
Consider the curve $\vec{r}(t)$ where $t\in \mathbb{R}$.

\subsubsection*{Arc length}
The arc length of $\vec{r}$, $s$, from a point $a$ is given by 
\[
    s(t)= \int_a^t \sqrt{
            \paren{\frac{dx}{du}}^2
            +\paren{\frac{dy}{du}}^2
            +\paren{\frac{dz}{du}}^2
        } du
        = \int_a^t \abs{\vec{r}'(u)} du
\]

\subsubsection*{Tangent and smooth}
Furthermore, the curve is called \emph{smooth} on interval $I$ if $\vec{r}'$ is continuous and $\vec{r}' \neq \vec{0}$
on $I$. Then the length of  unit tangent vector is given by 
\[
    \vec{T}(t)  = \frac{\vec{r}'(t)}{\abs{\vec{r}'(t)}}
\]
and indicates the direction of $\vec{r}$.

\subsubsection*{Curvature}
The curvature at a given point is a measure of how quickly the curve changes direction and it is
given by
\[
    \kappa = \abs{\frac{d\vec{T}}{ds}}
\]
and can be simplified using the chain rule as 
\[
    \kappa = \abs{\frac{d\vec{T}}{ds}} = \abs{\frac{d\vec{T}/dt}{ds/dt}} = \frac{\abs{\vec{T}'(t)}}{\abs{\vec{r}'(t)}}
\]
since $\frac{ds}{dt} = \abs{\vec{r}'(t)}$. Additionally, it can be written as
\[
    \kappa = \frac{\abs{\vec{r}'(t) \times \vec{r}''(t)}}{\abs{\vec{r}'(t)}^3}
\]
by rewriting $\abs{\vec{T}'(t)}$ using the fact that $\abs{\vec{f}(t)} = c \implies \vec{f}(t) \dotp \vec{f}'(t) = 0$.

\subsubsection*{Normal and Binormal vectors}
The normal and binormal vectors are unit vectors which are both perpendicular to the curve at a point in addition
to being perpendicular to each other. The \emph{unit normal} is defined using $\vec{T}(t)$ as
\[
    \vec{N}(t) = \frac{\vec{T}'(t)}{\abs{\vec{T}'(t)}}
\]
since $\abs{\vec{T}(t)} =1$ and hence $\vec{T}(t) \dotp \vec{T}'(t) = 0$. The \emph{binormal vector} is simply defined as
\[
    \vec{B}(t) = \vec{T}(t) \times \vec{N}(t)
\]
which is already a unit vector since $\vec{T}$ and $\vec{N}$ are orthogonal unit vectors.

\subsubsection*{Notable planes}
The plane determined by the normal and binormal vectors $\vec{N}$ and $\vec{B}$ at a point $P$ 
is called the \emph{normal plane} and consists of all vectors orthogonal to the curve at $P$. Whilst the 
plane determined by the vectors $\vec{T}$ and $\vec{N}$ is called the \emph{osculating plane} of the curve at $P$
and it is the plane that comes closest to containing part of the curve near $P$.

The circle that lies in the osculating plane at $P$, has the same tangent as the curve at $P$, lies on the concave 
side of the curve (toward which $\vec{N}$ points) and has radius $\frac{1}{\kappa}$ is called the \emph{osculating circle}
and is the circle which best describes how the curve behaves near $P$.

% \subsubsection*{Velocity and acceleration}
% Consider the curve $\vec{r}(u)$ based on a parameter which is itself a function of time.
% When $u$ is taken as the arc length $s$ from a fixed point on the curve, then
% \[
%     \frac{d\vec{r}}{ds} = \hat{t}
% \]
% where $\hat{t}$ is the unit vector in the direction tangent to the curve and
% \[
%     \text{velocity},\, \vec{v} 
%     = \frac{d\vec{r}}{dt} 
%     = \frac{d\vec{r}}{ds} \paren{\frac{ds}{dt}}
%     = \frac{ds}{dt} \hat{t}
% \]
% which yields the typical ``speed and direction''. Similarly, the acceleration can
% computed as
% \[
%     \text{acceleration}, \, \vec{a} = \frac{d\vec{v}}{dt} = \frac{d^2\vec{r}}{dt^2}
% \]

\subsection{Direction cosines}
Direction cosines are a simple way to define the direction of a vector (in a symmetric manner).
Consider the points $\vec{P}_0= (x_0, y_0, z_0)$ and $\vec{P}=(x,y,z)$ in $\mathbb{R}^3$ then the direction cosines are 
defined as
\[
    \cos(\alpha) = \frac{x-x_0}{\abs{\vec{P_0P}}}
    ,\quad
    \cos(\beta) = \frac{y-y_0}{\abs{\vec{P_0P}}}
    ,\quad
    \cos(\gamma) = \frac{z-z_0}{\abs{\vec{P_0P}}}
\]
where $\alpha, \beta, \gamma \in [0, \pi]$
\[
    \abs{\vec{P_0P}} = s = \sqrt{(x-x_0)^2 + (y-y_0)^2 + (z-z_0)^2}
\]
Then $\cos^2(\alpha) + \cos^2(\beta) + \cos^2(\gamma) = 1$. Therefore, using the chain rule,
the derivative of function a $f$ at $\vec{P}_0$ in direction $\vec{P_0P}$ is given by
\begin{align*}
    \pLeibniz{f}{s} 
    &= \pLeibniz{f}{x}\pLeibniz{x}{s}
        + \pLeibniz{f}{y}\pLeibniz{y}{s}
        + \pLeibniz{f}{z}\pLeibniz{z}{s}
        \\
    &= \pLeibniz{f}{x}\cos(\alpha)
        + \pLeibniz{f}{y}\cos(\beta)
        + \pLeibniz{f}{z}\cos(\gamma)
\end{align*}
which is called the directional derivative of $f$ in direction $\alpha$, $\beta$, $\gamma$.
\paragraph*{Remark} Really, I don't understand why this is done since this is just a special
case of the chain rule and can be computed just as easily without considering angles and just using
unit vectors instead. The case for direction cosines is worsened even further since they are not 
even independent. Furthermore, two angles are not sufficient since there are two solutions for
the sum of squares formula.

