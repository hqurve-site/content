\section{Differentiation}
For a function of two or more variables, the variable must be specified for differentiation of 
the function. This leads to the concept of partial derivatives.

\subsection{Partial derivative}
The \emph{partial derivative} of $f(x, y)$ with respect to $x$ is
$$\pLeibniz{f}{x} = D_x f = f_x(x, y) = \lim_{\Delta x\rightarrow 0} \frac{f(x + \Delta x, y) - f(x, y)}{\Delta x}$$
Things to notice
\begin{itemize}
    \item The partial derivative of $f(x, y)$ with respect to $y$ is defined similarly
    \item If this limit doesn't exist, we say that the partial derivative does not exist
    \item The partial derivative for functions of more than two variables are defined similarly
    \item This formula is very similar to the case for only one variable
    \item The partial derivative of $f(x, y)$ at the point $(x_0, y_0)$ is denoted 
            $\left. \pLeibniz{f}{x} \right|_{(x_0, y_0)}$ or $f_x(x_0, y_0)$
\end{itemize}

\subsection{Differential}
The \emph{increment} of $f(x, y)$ at $(x_0, y_0)$ denoted by $\Delta f(x_0, y_0)$ is defined as
$$\Delta f(x_0, y_0) = f(x_0, \Delta x, y_0, \Delta y) - f(x_0, y_0)$$
Furthermore, $f$ is \emph{differentiable} if and only if the increment of $f$ can be written as
$$\Delta f(x_0, y_0) = 
    \left( \pLeibniz{f}{x} (x_0, y_0) \right)\Delta x
    + \left( \pLeibniz{f}{y} (x_0, y_0) \right)\Delta y
    +  \Delta x\, \epsilon_1 +  \Delta y\, \epsilon_2
$$
where $\epsilon_1$ and $\epsilon_2$ are both functions of $\Delta x$ and $\Delta y$ such that 
$$
    \lim_{(\Delta x, \Delta y)\rightarrow (0, 0)} \epsilon_1(\Delta x, \Delta y)
    = 0 =
    \lim_{(\Delta x, \Delta y)\rightarrow (0, 0)} \epsilon_2(\Delta x, \Delta y)
$$
Notice that the existence of the partial derivatives of $f$ is not sufficient for $f$ to be 
differentiable. Therefore, more work must be done. We will now show ways to prove and disprove that a function
is differentiable.

\noindent
The above is sometimes called the fundamental increment lemma.

\subsubsection*{Theorem: If $f$ is differentiable then $f$ is continuous}
We will show that this is a sufficient condition however not necessary. That is $f$ cannot be proven to be differentiable
just by showing that it is continuous.
\begin{proof} This proof will be for two variables however, a similar argument works in general.

    \noindent
    Since $f$ is differentiable, there exists functions $\epsilon_1, \epsilon_2$ of $\Delta x$ and $\Delta y$ such that
    $$
        \Delta f(x_0, y_0) = 
        \left( \pLeibniz{f}{x} (x_0, y_0) \right)\Delta x
        + \left( \pLeibniz{f}{y} (x_0, y_0) \right)\Delta y
        +  \Delta x\, \epsilon_1 +  \Delta y\, \epsilon_2
    $$
    and $\lim_{(\Delta x, \Delta y)\rightarrow (0, 0)} \epsilon_1(\Delta x, \Delta y)
    = 0 =
    \lim_{(\Delta x, \Delta y)\rightarrow (0, 0)} \epsilon_2(\Delta x, \Delta y)$.

    \noindent
    Therefore,
    $$
        \lim_{(\Delta x, \Delta y)\rightarrow (0, 0)} f(x_0 + \Delta x, y_0 + \Delta y) - f(x_0, y_0)
        =
        \lim_{(\Delta x, \Delta y)\rightarrow (0, 0)} \Delta f(x_0, y_0) 
        = 0
    $$
    and by defining $x = x_0 + \Delta x$ and $y = y_0 + \Delta y$ we note that as $(\Delta x, \Delta y)\rightarrow (0, 0)$, $(x, y)\rightarrow (x_0, y_0)$.
    Therefore, 
    $$
        \lim_{(\Delta x, \Delta y)\rightarrow (0, 0)} f(x_0 + \Delta x, y_0+ \Delta y) - f(x_0, y_0) =
        \lim_{(x, y)\rightarrow (x_0, y_0)} f(x, y) - f(x_0, y_0) = 0
    $$
    Hence, the result follows.
\end{proof}
\noindent
This theorem can be used to show that a function is not differentiable through the contrapositive.

\subsubsection*{Theorem: If all the partial derivatives of $f$ are continuous then $f$ is continuously differentiable}
Note that this continuity need only persist on an interval about $(x_0, y_0)$. (The proof of this is apparently very hard).

\subsubsection*{Total differential}
If $f$ is differentiable at $(x, y)$, then the total differential of $f$, denoted $df$, is defined as
$$df = \pLeibniz{f}{x} dx + \pLeibniz{f}{y} dy$$
Note that this formula arises from the fact that both $\epsilon_1$ and $\epsilon_2$ tend to $0$ as $(\Delta x, \Delta y)\rightarrow (0, 0)$
Also, note the general results
\begin{itemize}
    \item If $f(x_1, x_2, \ldots, x_n)$ is constant, then the total differential $df=0$.
    \item The expression $P(x, y) dx + Q(x, y) dy$ is the differential of $f$ iff $P_y = P_x$, in which
            case, $P(x, y) dx + Q(x, y) dy$ is referred to as an exact differential
\end{itemize}

\subsubsection*{General form in terms of vectors}
A function $f: D\rightarrow \mathbb{R}$, where $D \subseteq \mathbb{R}^n$, is said to be 
differentiable at the point $a \in D$ if there exists a linear function (ie a matrix) and a function
$\Phi$ from a deleted neighbourhood of the origin in $\mathbb{R}^n$ to $\mathbb{R}^n$ such that
\begin{enumerate}[label=\roman*)]
    \item $\lim_{\bm{h}\rightarrow\bm{0}} \Phi(\bm{h}) = \bm{0}$ 
    \item $f(\bm{a} + \bm{h}) = f(\bm{a}) + M(\bm{h}) + \Phi(\bm{h})\cdot\bm{h}$ for all non-zero vectors $\bm{h}$
            sufficiently close to $\bm{0} \in \mathbb{R}^n$.
\end{enumerate}
When this is the case, the linear function $M$ is called the derivative of $f$ at $\bm{a}$. 
Note $M$ is total differential.


\subsection{Chain Rule}
If $u$ is a differentiable function of $x$ and $y$ defined by $u = f(x, y)$ and $x=F(r, s)$ and $y = G(r, s)$ and 
$$
\pLeibniz{x}{r}, \quad \pLeibniz{x}{s}, \quad \pLeibniz{y}{r}, \quad \pLeibniz{y}{s}
$$
all exist, then $u$ is a function of $r$ and $s$ and the following holds
$$
\pLeibniz{u}{r} = \pLeibniz{u}{x}\pLeibniz{x}{r} + \pLeibniz{u}{y}\pLeibniz{y}{r}
$$
$$
\pLeibniz{u}{s} = \pLeibniz{u}{x}\pLeibniz{x}{s} + \pLeibniz{u}{y}\pLeibniz{y}{s}
$$

\subsubsection*{Generalization}
Given that $u$ is a differentiable function of the $n$ variables $x_1, x_2, \ldots, x_n$ and that each of these variables
is a function of the $m$ variables $y_1, y_2, \ldots, y_m$ and that each of the partial derivatives 
$\pLeibniz{x_i}{y_j}$ exist for $i = 1, 2, \ldots, n$ and $j = 1, 2, \ldots, m$. Then $u$ is a function of  $y_1, y_2, \ldots, y_m$
and 
$$
    \pLeibniz{u}{y_j} = \sum_{i=1}^n \pLeibniz{u}{x_i}\pLeibniz{x_i}{y_j}
$$
for all $j = 1, 2, \ldots, m$.

\subsection{Higher order partial derivatives}
Higher order derivatives are obtained just by repeatedly differentiating lower order ones. That is
$$D_{xy} f = D_y(D_x f) = f_{xy} = \frac{\partial^2 f}{\partial y \partial x} = \pLeibniz{}{y}\left(\pLeibniz{f}{x}\right)$$
which can simply be computed by using the previously established definitions.

\noindent
For example
$$f_{xy}(x_0, y_0) = \lim_{\Delta y \rightarrow 0} \frac{f_x(x_0, y_0 + \Delta y) - f_x(x_0, y_0)}{\Delta y}$$

\subsubsection*{Clairaut's Theorem}
In general $f_{xy} \neq f_{yx}$ however, if both are continuous at a point $(x_0, y_0)$ then $f_{xy}(x_0, y_0) = f_{yx}(x_0, y_0)$.


\subsection{Implicit function theorem}
If $F(x, y)$ is a continuously differentiable function defined on a disk containing $(x_0, y_0)$ 
where $F(x_0, y_0)$ = 0 and $F_y(x_0, y_0) \neq 0$, then there exists an open set $U \subset \mathbb{R}^2$
containing $(x_0, y_0)$
such that there is a unique continuously differentiable function $g: U \rightarrow \mathbb{R}$ where
$F(x, g(x)) = 0$ and who's derivative is given by 
$$\frac{d g}{dx} = - \frac{F_x}{F_y}$$
Note: 
\begin{itemize}
    \item We usually say that $y$ is a function of $x$ instead of there exists a function $g$.
    \item We get the above formula by applying the chain rule to $F(x, y) = 0$.
\end{itemize}

\subsubsection*{More than two variables}
Let $F: \mathbb{R}^{n+1} \rightarrow \mathbb{R}$ be a continuously differentiable function with coordinates 
$(\bm{x}, y) \in \mathbb{R}^n \times \mathbb{R}$. Then for any fixed point $(\bm{x_0}, y)$ with $F(\bm{x}, y) =0$
and $F_y(\bm{x}, y) \neq 0$ there exists an open set $U \subset \mathbb{R}^{n+1}$, containing $(\bm{x_0}, y)$, 
such that there exists a unique function $g: U \rightarrow \mathbb{R}$ where $F(\bm{x}, g(\bm{x})) = 0$ and whos 
partial derivatives are given by
$$\frac{d g}{dx_i} = - \left[\frac{\partial F}{\partial y}\right]^{-1} \frac{\partial F}{\partial x_i} = - \frac{F_{x_i}}{F_y}$$
Note again: 
\begin{itemize}
    \item We usually say that $y$ is a function of $\bm{x}$ instead of there exists a function $g$.
    \item We get the above formula by applying the chain rule to $F(\bm{x}, y) = 0$ (differentiating wrt $x_i$).
\end{itemize}