\section{Line Integrals}
\subsection{Definition}
The line integral of a vector field $\vec{F}$ over a curve $C$ is
\[
    \int_C \vec{F}\dotp \vec{dr}
    = \int_C F_1 dx_1 + F_2 dx_2 + \cdots + F_n dx_n
\]
\subsubsection*{Properties}
As with the normal integral, the line integral is both linear and additive over their region.
That is
\[
    \int_C (\alpha\vec{F}_1 + \beta\vec{F}_2)\dotp \vec{dr}
    = \alpha\int_C \vec{F}_1\dotp \vec{dr} + \beta\int_C \vec{F}_2\dotp \vec{dr}
\]
and if $C_1$ and $C_2$ are disjoint regions (intersection has measure $0$) and $C = C_1 \cup C_2$ then
\[
    \int_{C} \vec{F}\dotp \vec{dr}
    = \int_{C_1} \vec{F}\dotp \vec{dr} + \int_{C_2} \vec{F}\dotp \vec{dr}
\]
Furthermore, the curve in the reverse direction is $-C$ and its integral is negated. That is
\[
    \int_{-C} \vec{F}\dotp \vec{dr} = -\int_{C} \vec{F}\dotp \vec{dr}
\]
\subsubsection*{Closed curves}
If $C$ is a closed curve (endpoints are the same), the integral is instead
written as
\[
    \oint_C \vec{F} \dotp \vec{dr}
\]
Note that positive orientation in the plane refers to counterclockwise motion.

\subsubsection*{Parameterized computation}
One way to compute the line integral is to parameterize the curve with respect to some 
real number $t$. Then
\[
    \int_C \vec{F} \dotp \vec{dr}
    = \int_a^b \vec{F}(\vec{r}(t)) \dotp \nabla(\vec{r}(t)) dt
\]
where $\nabla \vec{r}(t)$ is the gradient.\\
Note that $\nabla (\vec{r}(t))$ is also sometimes written as $\vec{r}'(t)$

\subsection{Independence of path}
A line integral is said to be independent of path over region $D$ if 
\[
    \int_{C_1} \vec{F} \dotp \vec{dr} = \int_{C_2} \vec{F} \dotp \vec{dr}
\]
whenever $C_1$ and $C_2$ (in $D$) have the same end points. In other words, the integral only depends on its 
endpoints. This statement has various equivalents outlined below.

\subsubsection*{Closed Paths}
If the integral is independent of path, then the integral of a closed path is $0$. Consider any closed path 
composed of paths $C_1$ and $C_2$. Then 
\[
    \int_{C_1 \cup C_2} \vec{F} \dotp \vec{dr}
    = \int_{C_1} \vec{F} \dotp \vec{dr} + \int_{C_2} \vec{F} \dotp \vec{dr}
    = \int_{C_1} \vec{F} \dotp \vec{dr} - \int_{-C_2} \vec{F} \dotp \vec{dr}
    = 0
\]
since curves $C_1$ and $-C_2$ have the same endpoints. Conversely, if integral of all
closed paths is $0$ consider two curves $C_1$ and $-C_2$ with the same endpoints. Then $C_1 \cup C_2$
forms a closed path and by the above equation, the integral is independent of path.

\subsubsection*{Conservative Fields}
Consider the smooth curve $C$ defined by $\vec{r}(t)$ from $a \leq t \leq b$. Then if $\vec{F} = \nabla \phi$ (conservative)
\[
    \int_C \vec{F} \dotp \vec{dr} 
    = \int_a^b \nabla \phi(\vec{r}(t))  \dotp \vec{r}'(t) dt
    = \int_a^b \frac{d}{dt} \phi(\vec{r}(t)) dt
    =\phi (\vec{r}(b)) - \phi (\vec{r}(a))
\]
by the chain rule (multivariable one) and fundamental theorem of calculus. Note that the curve can also be 
piecewise smooth; that is not smooth over region with measure 0. Additionally, the converse is also 
true under the added condition that $D$ is open and connected. To show this fact, consider the function
\[
    \phi(\vec{x}) = \int_{\vec{x}_0}^{\vec{x}} \vec{F} \dotp \vec{dr}
\]
where $\vec{x}_0$ is an arbitrary point in $D$. Now all that is required is to show that the component
$F_i$ is equal to $\pLeibniz{\phi}{x_i}$. Since $D$ is open, there exists a ball around $\vec{x}$;
choose a point $\vec{x}_1$ in this ball with all components excluding $x_i$ equal to that of $\vec{x}$.
Then
\[
    \phi(\vec{x}) = \int_{\vec{x}_0}^{\vec{x}_1} \vec{F} \dotp \vec{dr} + \int_{\vec{x}_1}^{\vec{x}} \vec{F} \dotp \vec{dr}
\]
Notice that the first integral is independent of $x_i$. Also, consider the straight line path from $\vec{x}_1$ to $\vec{x}$
then all components but $x_i$ in the second integral are constant; so $\vec{dr}$ has zeros for all components except $x_i$. Then
\[
    \pLeibniz{\phi(\vec{x})}{x_i} = 0 + \pLeibniz{}{x_i}\int_{\vec{x}_1}^{\vec{x}} F_i\, dx_i = F_i(\vec{x})
\]
The above procedure can be repeated for all components and therefore $\vec{F}$ is conservative.

\subsection{Greens Theorem}
Greens theorem relates the integral of a curved curve $C$ in the plane $\mathbb{R}^2$ to the region, $D$, which it encloses by the following equation
\[
    \oint_C P\, dx + Q\, dy = \iint_D \pLeibniz{Q}{x} - \pLeibniz{P}{y}\, dA
\]
where $P$ and $Q$ are continuously differentiable in $D$.
Or alternatively,
\[
    \oint_C \vec{F} \dotp \vec{dr} = \iint_D \nabla \times \vec{F}\, dA = \iint_D \vec{d}(\vec{F}\dotp \vec{dx})
\]

\subsubsection*{Proof for simple regions}
If $D$ is can be written as both a type $I$ and type $II$ region, then
\begin{alignat*}{1}
    & D = \{(x,y) | a\leq x \leq b, f_1(x) \leq y \leq f_2(x)\}\\
    & D = \{(x,y) | c\leq y \leq d, g_1(y) \leq x \leq g_2(y)\}
\end{alignat*}
\paragraph*{Part 1}
\begin{align*}
    \iint_D \pLeibniz{P}{y}\,dA 
    &= \int_a^b \int_{f_1(x)}^{f_2(x)} \pLeibniz{P}{y} \, dy\, dx\\
    &= \int_a^b P(x, f_2(x)) - P(x, f_1(x)) \, dx\\
    &= \int_{-C} P \, dx\\
    &= -\int_C P \, dx
\end{align*}
The send to last equivalence follows by considering the cycle 
\[
    (a, f_1(a)) 
    \rightarrow (b, f_1(b)) 
    \rightarrow (b, f_2(b)) 
    \rightarrow (a, f_2(a)) 
    \rightarrow (a, f_1(a))
\]
Note that integral along the path where $a$ and $b$ are constant is $0$.

\paragraph*{Part 2}
\begin{align*}
    \iint_D \pLeibniz{Q}{x}\,dA 
    &= \int_c^d \int_{g_1(y)}^{g_2(y)} \pLeibniz{Q}{x} \, dx\, dy\\
    &= \int_c^d Q(y, g_2(y)) - Q(y, g_1(y)) \, dy\\
    &= \int_C Q \, dy
\end{align*}
The last equivalence follows by considering the cycle
\[
    (c, g_1(c)) 
    \rightarrow (c, g_2(c)) 
    \rightarrow (d, g_2(d)) 
    \rightarrow (d, g_1(d)) 
    \rightarrow (c, g_1(c))  
\]

\paragraph*{Conclusion}
By combining the two above parts, the result is obtained.

\subsubsection*{Extension to finite union of simple regions}
Consider the region $D$ which can be split into two regions which greens theorem holds. 
Let the regions be $D_1$ and $D_2$ with curves boundary  $C_1 \cup C_3$ and $C_2 \cup -C_3$
respectfully where $C_3$ is the curve along which $D_1$ and $D_2$ meet. Then
\begin{align*}
    \int_C \vec{F} \dotp \vec{dr}
    &= \int_{C_1\cup C_2} \vec{F} \dotp \vec{dr}\\
    &= \int_{C_1\cup C_2} \vec{F} \dotp \vec{dr} + \int_{C_3} \vec{F} \dotp \vec{dr} + \int_{-C_3} \vec{F} \dotp \vec{dr}\\
    &= \int_{C_1\cup C_3} \vec{F} \dotp \vec{dr} + \int_{C_2 \cup -C_3} \vec{F} \dotp \vec{dr}\\
    &= \iint_{D_1} \nabla \times \vec{F}\, dA + \iint_{D_2} \nabla \times \vec{F}\, dA\\
    &= \iint_{D} \nabla \times \vec{F}\, dA
\end{align*}