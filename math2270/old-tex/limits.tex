\section{Limits}
% 2020-09-18
For the sake of brevity we will only consider functions of two variables. 
However, the following can easily be extended for any number of variables.

\subsection{Epsilon delta definition}
Let $f(x, y)$ be defined in a deleted $\delta$-neighbourhood of $(x_0, y_0)$.
We say that $L$ is the limit of $f(x, y)$ as $x$ approaches $x_0$ and $y$ approaches $y_0$
and write
$$\lim_{(x, y) \rightarrow (x_0, y_0)} f(x, y) = L$$
if $\forall \varepsilon > 0$, $\exists \delta > 0$ such that 
$$ 0 < |(x, y) - (x_0, y_0)| < \delta \implies |f(x,y) - L | < \varepsilon $$

\subsubsection*{Definition of the neighbourhood}
Note that the neighbourhood used doesn't matter. In fact both deleted circular and
rectangular neighbourhoods are valid. 
That is we can define $0 < |(x, y) - (x_0, y_0)| < \delta$ as either
\begin{itemize}
    \item circular: $0 < \sqrt{(x-x_0)^2 + (y-y_0)^2} <\delta$
    \item rectangular: $ 0 < |x - x_0| < \delta$ and $0 < |y-y_0| < \delta$
\end{itemize}
Additionally, we can notice that the circular $\delta$-neighbourhood is a subset of the rectangular $\delta$-neighbourhood.

\paragraph*{Worked example: } Prove that $\lim_{(x, y)\rightarrow (1, 1)} (x^2 + 2y^2) = 3$
\begin{proof} required to show that for $\varepsilon>0$, $\exists \delta > 0$ such that
$$0 < |x-1|< \delta \text{ and } 0 < |y-1| < \delta \implies |x^2 + 2y^2 - 3| < \varepsilon$$
Suppose we already have such $\delta$
\begin{align*}
    |x^2 + 2y^2 - 3| 
    &= |x^2 -1 + 2y^2 - 2|\\
    &\leq |x^2 - 1| + 2|y^2-1| \quad= |x - 1||x+1| + 2|y-1||y+1|\\
    &< \delta|x + 1| + 2\delta|y+1| \quad= \delta|x-1 + 2|  + 2 \delta|y-1+2|\\
    &\leq\delta(|x-1| + 2) + 2\delta(|y-1| + 2) \quad= \delta (|x-1| + 2|y-1| +6)\\
    &< \delta (3\delta + 6)
\end{align*}
Now it would be nice if $\delta$ was also at most $1$ (note another number could have been used).\\
So lets assume $\delta \leq 1$. This gives
$$|x^2 + 2y^2 - 3| < \delta (3\delta + 6) \leq \delta (3 + 6) = 9\delta = \varepsilon$$
Therefore for any $\varepsilon > 0$, $\exists \delta = \min(1, \frac{\varepsilon}{1}) > 0 $ such that for
$$0 < |x-1| < \delta \text{ and } 0 < |y-1| < \delta \implies |x^2 + 2y^2 - 3| < \varepsilon$$
\end{proof}

\subsection{Path taken for the limit}
Sometimes, before we can prove the value of a limit, we need to determine whether the limit exists in addition
to finding its value (possible value). In order to do this, we examine the approach taken to the limit.

Similar to the directed limits for single variable functions, the limit of a multivariable function 
exists if the limit of all approaches exists and are equal. However, there exists an infinite number of 
possible approaches thereby making it tedious to prove the limit this way. Despite this, considering 
multiple approaches is not useless as we can use the information gained to disprove the existence
of a limit or find its possible value. In order to do we use the following theorem.

\subsubsection*{Lemma}
If 
$
    \lim_{t\rightarrow a} g(t) = x_0 
$
and
$
    \lim_{t\rightarrow a} h(t) = y_0
$
and
$\lim_{(x, y) \rightarrow (x_0, y_0)} f(x, y) = L$
then
$$
    \lim_{t\rightarrow a} f(g(t), h(t)) = L
$$

\begin{proof}
    Consider $\varepsilon > 0$. Therefore, $\exists \delta_1 > 0$ such that 
    $$ 0<| x - x_0|< \delta_1 \text{ and } 0 < |y-y_0| < \delta_1 \implies |f(x, y) - L| < \varepsilon$$
    Additionally, since $\delta_1 > 0$, $\exists \delta_2, \delta_3 > 0$ such that
    $$ 
        (0 < |t - a| < \delta_2 \implies |g(t) - x_0| < \delta_1)
        \text{ and }
        (0 < |t - a| < \delta_3 \implies |h(t) - y_0| < \delta_1)
    $$
    Now, we can choose $\delta = \min(\delta_2, \delta_3)$ such that 
    \begin{align*}
        0 < |t- a| < \delta
        &\implies |g(t) - x_0| < \delta_1 \text{ and } |h(t) - y_0 | < \delta_1\\
        &\implies |f(g(t), h(t)) - L| < \varepsilon
    \end{align*}
    Therefore
    $$
        \lim_{t\rightarrow a} f(g(t), h(t)) = L
    $$
\end{proof}
Note that the above proof only holds if $(g(t),h(t))$ is not equal to $(x_0, y_0)$ within a certain neighbourhood.

\subsubsection*{Usage}
Using this information, we can easily compare two paths to a certain limit and prove that it does not exist (via contradiction) or find a possible value.

\subsubsection*{Example 1} 
Given that $f(x, y) = \frac{x^2y}{x^3+y^3}$,
find $\lim_{(x, y)\rightarrow(0, 0)} f(x,y)$ if it exists.
\begin{proof}
Suppose the limit exists. Then via the above lemma,
$$\lim_{(x, y)\rightarrow(0, 0)} f(x, y) = \lim_{t\rightarrow 0 } f(t, 0) = \lim_{t \rightarrow 0} \frac{0}{t^3} = 0$$
Also
$$\lim_{(x, y)\rightarrow(0, 0)} f(x, y) = \lim_{t\rightarrow 0 } f(t,t) = \lim_{t \rightarrow 0} \frac{t^3}{2t^3} = \frac{1}{2}$$
This is a contradiction, hence the limit does not exist.
\end{proof}

\subsubsection*{Example 2}
Given that $f(x, y) = \frac{3x^2y}{x^2+y^2}$,
find $\lim_{(x, y)\rightarrow(0, 0)} f(x,y)$ if it exists.

Investigation\\
Let $a, b \in \mathbb{R}$. Suppose $\lim_{(x, y)\rightarrow(0, 0)} f(x,y)$ exists. Then
\begin{align*}
    \lim_{(x, y)\rightarrow(0, 0)} f(x,y) 
    &= \lim_{t \rightarrow 0} f(at, bt)\\
    &= \lim_{t \rightarrow 0} \frac{3a^2b t^3}{(a^2 + b^2) t^2}\\
    &= \lim_{t \rightarrow 0} \frac{3a^2b}{a^2 + b^2}t\\
    &= 0
\end{align*}
This is convincing evidence that the limit is equal to $0$. But we need to formally prove it.

\begin{proof} required to show that for $\varepsilon>0$, $\exists \delta > 0$ such that
$$0 < |x| < \delta \text{ and } 0 < |y| < \delta \implies |\frac{3x^2y}{x^2+y^2}| < \varepsilon$$
Suppose we already have such $\delta$
\begin{align*}
    |\frac{3x^2y}{x^2+y^2}| 
    &= \frac{3|x|^2|y|}{|x|^2 + |y|^2}\\
    &\leq \frac{3|x|^2|y|}{2|x||y|} \text{by AM-GM}\\
    &= \frac{3}{2}|x|\\
    &< \frac{3}{2} \delta = \varepsilon
\end{align*}
Therefore for any $\varepsilon > 0$, $\exists \delta = \frac{2}{3}\varepsilon > 0 $ such that for
$$0 < |x| < \delta \text{ and } 0 < |y| < \delta \implies |\frac{3x^2y}{x^2+y^2}| < \varepsilon$$
\end{proof}


\subsection{Using polar coordinates to determine a limit}
Based on the nature of our function $f$, it is sometimes easier to use polar coordinates to evaluate or prove a limit. This is because
$$
    \lim_{(x, y)\rightarrow (x_0, y_0)} f(x, y) = L \iff \lim_{(r, \theta) \rightarrow (r_0, \theta_0)} f(r\cos\theta, r\sin\theta) = L
$$
where $x_0 = r\cos\theta_0$ and $y_0 = r\sin\theta_0$.
Note for the case of $x_0 = y_0 = 0$, $\theta_0$ can take any value, an is usually left arbitrary.

\subsubsection*{Proof of this property}
I am not going to prove this for sake of my current sanity. But I believe that the general form of this is as follows:
Given metric spaces $A$ and $B$, the limit points $a_0\in A$ and $y_0 \in B$ and the bijection $T: A\rightarrow B$ such that
\begin{itemize}
    \item $T(a_0) = b_0$
    \item $T^{-1}(b_0) = a_0$
    \item $T$ is continuous at $a_0$
    \item $T^{-1}$ is continuous at $b_0$
    % \item There exists a deleted $\delta$-neighbourhood about $a_0$ such that $T(a) \neq b_0$
    % \item There exists a deleted $\delta$-neighbourhood about $b_0$ such that $T(b) \neq a_0$
    % removed above since T is a bijection
\end{itemize}
Then 
$$\lim_{a\rightarrow a_0} f(a) = L \iff \lim_{b\rightarrow b_0} f(T^{-1}(b)) = L$$

\paragraph*{Proof of forward direction}
\begin{proof}
    Since the $\lim_{a\rightarrow a_0} f(a) = L$, consider $\varepsilon > 0$, then $\exists \delta_1 > 0$ such that
    $$ 0 < |a - a_0| < \delta_1 \implies |f(a) - L| < \varepsilon$$
    Also, since $T^{-1}$ is continuous at $b_0$, $\exists \delta_2 > 0$ such that
    $$ 0 < |b - b_0| < \delta_2 \implies |T^{-1}(b) - a_0| = |T^{-1}(b) - T^{-1}(b_0)| < \delta_1$$
    Also, since $T$ is a bijection: 
    $$ 0 < |b-b_0| \implies 0 < |T^{-1}(b) - T^{-1}(b_0)|$$
    Finally, let $\delta = \delta_2 > 0$. Then
    \begin{align*}
        0 < |b - b_0| < \delta
        &\implies 0 < |T^{-1}(b) - T^{-1}(b_0)| < \delta_1\\
        &\implies 0 < |T^{-1}(b) - a_0| < \delta_1\\
        &\implies |f(T^{-1}(b)) - L| < \varepsilon
    \end{align*}
\end{proof}
\noindent
Note that the reverse direction is the exact same steps since the conditions are symmetric.\\
Also, I ended up doing it :P.\\
Also, I don't know if the conditions I placed are either necessary or sufficient, but the seem to get it done.

\subsubsection*{Sufficient $T$ for this property}
\[T: \mathbb{R}^2 \rightarrow \mathbb{R}^+ \times [0, 2\pi) \cup \{(0, 0)\}\]
\[\arctan: \mathbb{R} \rightarrow [0, 2\pi)\]
\begin{align*}
    sign(x) &= \begin{cases}
        0 \quad &\text{if}\, x = 0\\
        1 \quad &\text{if}\, x > 0\\
        -1 \quad &\text{if}\, x < 0
    \end{cases}\\
    T(x, y) &= \begin{cases}
        (0, 0) \quad &\text{if}\, x = y= 0\\
        (|y|, \frac{\pi}{2}\text{sign}(y)) \quad &\text{if}\, x = 0\\
        (\sqrt{x^2 + y^2}, \arctan\frac{y}{x}) \quad &\text{otherwise}
    \end{cases}
\end{align*}



% 2020-09-23




\subsection{Iterated Limits}
Yet another way to disprove the existence of limits is through the use of iterated limits.
We will only consider the case of two variables, but it is generalizable.

\subsubsection*{Statement}
If 
$$\lim_{(x, y)\rightarrow(x_0, y_0)} f(x, y) = L$$
then
$$
    \lim_{x\rightarrow x_0}\left[\lim_{y\rightarrow y_0} f(x, y)\right]
    = L =
    \lim_{y\rightarrow y_0}\left[\lim_{x\rightarrow x_0} f(x, y)\right]
$$

\paragraph*{Note: } the contrapositive of this statement is what is really useful when disproving the existence of limits.

\paragraph*{Worked example: } Prove that $lim_{(x, y)\rightarrow (0, 0)} \frac{x-y}{x+y}$ does not exist.
\begin{proof}
    Note that 
    $$
        \lim_{x\rightarrow 0}\left[\lim_{y\rightarrow 0} \frac{x-y}{x+y}\right]
        =
        \lim_{x\rightarrow 0} \frac{x}{x}
        = 1
    $$
    however, 
    $$
        \lim_{y\rightarrow 0}\left[\lim_{x\rightarrow 0} \frac{x-y}{x+y}\right]
        =
        \lim_{x\rightarrow 0} \frac{-y}{y}
        = -1
    $$
    Therefore, since
    $$
        \lim_{x\rightarrow 0}\left[\lim_{y\rightarrow 0} \frac{x-y}{x+y}\right]
        \neq
        \lim_{y\rightarrow 0}\left[\lim_{x\rightarrow 0} \frac{x-y}{x+y}\right]
    $$
    The $\lim_{(x, y)\rightarrow(x_0, y_0)} \frac{x-y}{x+y}$ does not exist.
\end{proof}