\section{Stationary points and Optimization}


\subsection{Stationary points}
Let $f: D \rightarrow \mathbb{R}$ be a function, such that $D \subseteq \mathbb{R}^n$. 
A \emph{critical (stationary or level) point} of $f$ is a point $\bm{x}_0$ at which
all first partial derivatives vanish, ie
$$\forall i = 0, 1, \ldots, n: \left.\frac{\partial f}{\partial x_i}\right|_{\bm{x}_0}$$
Geometrically, a critical point is a place where the graph of $f$ is horizontal (ie all tangent lines
are horizontal). Additionally, The rational for this definition can be obtained by looking at the definition of
differentiable function. Furthermore, while we call $\bm{x}_0$ the critical point, $f(\bm{x}_0)$ is called the critical value.

\subsubsection*{Maximums and minimums}
$\bm{x}_0 \in D$ is said to be
\begin{itemize}
    \item a \emph{relative (local) maximum} if open subset $\exists: B \subseteq D$ such that $\forall \bm{x}\in B: f(\bm{x}_0) \geq f(\bm{x})$.
    \item a \emph{relative (local) minimum} if open subset $\exists: B \subseteq D$ such that $\forall \bm{x}\in B: f(\bm{x}_0) \leq f(\bm{x})$.
    \item a \emph{absolute (global) maximum} if $\forall \bm{x}\in D: f(\bm{x}_0) \geq f(\bm{x})$.
    \item a \emph{absolute (global) minimum} if $\forall \bm{x}\in D: f(\bm{x}_0) \leq f(\bm{x})$.
\end{itemize}


\subsubsection*{Theorem of local maxima and minima}
If $f$ is continuous and and all all first partial derivatives exists, then the local maxima
and minima of $f$ occur either at the boundary of $D$ or at the critical points.

\subsubsection*{Extreme Value Theorem}
If $R$ is a closed region in $\mathbb{R}^n$, which $f$ is continuous on, then $\exists$ at least one
point in $R$ where $f$ has an absolute maximum value and another where $f$ has an absolute minimum value.

\subsubsection*{Classification of stationary points}
Let $f$ be a function fo two variables $x$ and $y$ such that its first and second order partial 
derivatives are continuous on an open disk containing the stationary point $(a, b)$. That is
$$f_x(a,b) = 0\quad\quad f_y(a,b) = 0$$
Now define
$$
p = f_{xx}(a,b)
    \quad\quad
q = f_{xx}(a,b)
    \quad\quad
r = f_{xy}(a,b)
$$
Then
\begin{itemize}
    \item $(a,b)$ is a local minima if $pq - r^2 > 0$ and $p > 0$ (or $q > 0$).
    \item $(a,b)$ is a local maxima if $pq - r^2 > 0$ and $p < 0$ (or $q < 0$).
    \item $(a,b)$ is a saddle point if $pq - r^2 > 0$
    \item If $pq -r^2 = 0$, no conclusion can be made
\end{itemize}
\paragraph*{Note:} Since the second order partial derivatives of $f$ are continuous, $f_{xy} = f_{yx}$



\subsection{Lagrange Multipliers}
\emph{Lagrange multipliers} are a useful tool when trying to optimize (find relative extrema) a function
subject to one or more constraints.

\noindent
Let $f: D \rightarrow \mathbb{R}$ be a continuously differentiable function, such that $D \subseteq \mathbb{R}^n$. 
Then the relative extrema of $f$ subject to the $k$ constraints $\varPhi_i(x_1, x_2, \ldots, x_n) = 0$ for 
$i = 1, 2,\ldots, k$ (which are also continuously differentiable) are also relative extrema of the following 
auxiliary function
$$
    F(x_1, x_2, \ldots, x_n, \lambda_1, \lambda_2, \ldots, \lambda_k) 
    = f + \lambda_1\varPhi_1 + \lambda_2\varPhi_2 + \cdots + \lambda_k\varPhi_k
$$
where $\lambda_1, \lambda_2, \ldots, \lambda_k$ (independent of $x_1, x_2, \ldots, x_n$) are called 
\emph{Lagrange multipliers}.

Therefore, finding the relative extrema of $f$ with constraints $\varPhi_1, \varPhi_2, \ldots, \varPhi_k$
reduces to finding solutions to
$$
    F_{x_1} = 0, \; 
    F_{x_2} = 0, \; 
    \ldots,\;
    F_{x_n} = 0 
$$
and 
$$
    F_{\lambda_1} = 0, \; 
    F_{\lambda_2} = 0, \; 
    \ldots,\;
    F_{\lambda_k} = 0 
$$
Notice that these last $k$ equations are equivalent to the constraints $\varPhi_i(x_1, x_2, \ldots, x_n) = 0$ (for $i = 1, 2,\ldots, k$).

\subsubsection*{Reasoning}
Consider any point $\bm{x}$ which satisfies all constraints. Then the set of directions allowed by all constraints
is precisely the space of directions perpendicular to the gradients of all constraints. That is, if the space
of allowable moves is $A$ at $\bm{x}$ and the span of the constraints' gradients is $S$, then $A = S^{\perp}$.
Furthermore, by considering the (constrained) points at which $f$ has an extremum (ie doesn't change),
we see that the allowable directions must be perpendicular to the gradient of $f$ otherwise,
we could increase/decrease $f$ by moving in that allowed direction. Therefore, $\nabla f(\bm{x}) \in A^\perp = S$
and hence there exists $\lambda_1, \lambda_2, \ldots, \lambda_k$ such that
$$
    \nabla f(\bm{x}) = \sum_{i=1}^k \lambda_i \nabla \varPhi_i(\bm{x})
    \quad\Longleftrightarrow\quad
    \nabla f(\bm{x}) - \sum_{i=1}^k \lambda_i \nabla \varPhi_i(\bm{x}) = \bm{0}
$$
This condition along with the constraints are equivalent finding to stationary points of the auxiliary function.
Note that this condition is necessary but not sufficient (ie not all solutions are indeed solutions).