= Special Transformations

Here we discuss two special conformal mappings

== Powers
The transformation 
[stem]
++++
w = z^n
++++
is conformal at all points except stem:[z=0]

== Exponential
The transformation
[stem]
++++
w = e^z
++++
is conformal. Furthermore, it maps

* Lines stem:[\Re(z) = r] to geometric circles centered at the origin with radius stem:[r]
* Lines stem:[\Im(z) = \theta] to rays from the origin with angle stem:[\theta] with the real axis.
