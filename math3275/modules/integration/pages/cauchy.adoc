= Cauchy's Integral Theorems

[REMARK]
====
Lo siento a otra mathematiciones

THOUGHT: My condolences to people who speak spanish
====

Throughout this section, all closed contours are simply closed Jordan arcs unless otherwise stated.
Also, we would denote the interior of a closed contour stem:[C], defined by the Jordan curve theorem, as stem:[I(C)].

== Cauchy-Goursat Integral Theorem
Let stem:[C] be a closed contour and stem:[f(z)] be analytic on stem:[C \cup I(C)] then
[stem]
++++
\oint_C f(z) \ dz = 0
++++
image::cauchy-theorem.png[width=50%]

This theorem was initially formulated by Cauchy with the additional restriction
that stem:[f'(z)] was continuous. However, Goursat was later able to obtain the
same result without requiring this restriction.

.Proof with differential continuity
[%collapsible]
====
Let stem:[f(z) = u + iv]. Then
[stem]
++++
\oint_C f(z) \ dz = \oint_C (u+iv)(dx + i dy) = \oint_C u dx - v dy + i\oint_C v dx + u dy
++++
Then, by xref:math2270::line-integrals.adoc#_greens_theorem[Green's theorem] since stem:[f'(z)] is continuous
and hence the partial derivatives of stem:[u] and stem:[v] are also continuous, we get that
[stem]
++++
\oint_C f(z) \ dz = \iint_{I(C)} (-v_x - u_y) dA + i \iint_{I(C)} (u_x - v_y) dA = 0
++++
with additional help from the xref:analytic:cr-equations.adoc[].
====



=== Indepedence of path
If stem:[I(C)] contains two paths stem:[C_1] and stem:[C_2] with the same endpoints (and orientation),
[stem]
++++
\int_{C_1} f(z) \ dz = \int_{C_2} f(z) \ dz
++++
image::independent-path.png[width=50%]

.Proof
[%collapsible]
====
Notice that stem:[C_1 \cup (-C_2)] form a closed contour and since they lie in stem:[I(C)],
stem:[f(z)] is also analytic in stem:[(C_1 \cup (-C_2)) \cup I(C_1 \cup (-C_2))]
and hence
[stem]
++++
0 = \oint_{C_1 \cup (-C_2)} f(z) \ dz = \oint_{C_1} f(z) \ dz - \oint_{C_2} f(z) \ dz
++++
and we are done.
====

== Multiply Connected regions
Let stem:[C] and stem:[C_1, \ldots C_n] be closed contours such that stem:[I(C)] contains all stem:[C_i]
and each of the stem:[C_i] are disjoint. Then if stem:[f(z)] is analytic
in the region between them,
[stem]
++++
\oint_C f(z) \ dz = \oint_{C_1} f(z) \ dz + \oint_{C_2} f(z) \ dz + \cdots + \oint_{C_n} f(z) \ dz
++++

image::cauchy-theorem-multiple.png[width=50%]

.Proof
[%collapsible]
====
We can the result by creating a cross cuts from stem:[C] to
the stem:[C_i].

image::cauchy-theorem-multiple-cuts.png[width=50%]

Hence obtaining the following integral
[stem]
++++
\begin{aligned}
0 
&= \oint_{C \cup (-B_1) \cup (-C_1) \cup (B_1) \cup \cdots (B_1) \cup (-C_n) \cup (B_n)} f(z) \ dz
\\&= \oint_{C} f(z) \ dz - \oint_{C_1} f(z) \ dz - \cdots - \oint_{C_n} f(z) \ dz
\end{aligned}
++++
since the new path encloses the same region and the cross cuts cancel each of themselves out.
Hence, the result follows.
====

TIP: Using this result when stem:[n=1] allows us to "`reshape`" the contour over which we need
to integrate.



== Integral Formula
Let stem:[C] be a closed contour and stem:[f(z)] be analytic on stem:[C \cup I(C)] 
and stem:[a \in I(C)]. Then
[stem]
++++
f(a) = \frac{1}{2\pi i}\oint_C \frac{f(z)}{z-a} \ dz
++++
image::cauchy-integral.png[width=50%]

.Proof
[%collapsible]
====
Firstly, let stem:[\varepsilon > 0]. Then, since stem:[f] is analytic,
it is continuous at stem:[z = a] and hence, there exists
stem:[\delta > 0] such that
[stem]
++++
\forall z: |z - a| < \delta \implies |f(z) - f(a)| < \varepsilon
++++
Then let stem:[0 < \rho < \delta] be such that the circle stem:[C_1], defined by stem:[|z-a| = \rho],
lies in stem:[I(C)].

image::cauchy-integral-small.png[width=50%]

Then since stem:[\frac{f(z)}{z-a}] has only possibly has a pole at stem:[z = a], it is analytic on
the area between stem:[C] and stem:[C_1]. Then
[stem]
++++
\oint_{C} \frac{f(z)}{z-a} \ dz  = \oint_{C_1} \frac{f(z)}{z-a} \ dz = \oint_{C_1} \frac{f(z) - f(a)}{z-a} \ dz + \oint_{C_1} \frac{f(a)}{z-a} \ dz
++++
We would now focus on each of the integrals separately. Then, 
by using the parameterization stem:[z-a = \rho e^{i\theta}]
[stem]
++++
\oint_{C_1} \frac{f(z) - f(a)}{z-a} \ dz
= \int_0^{2\pi} \frac{f(a + \rho e^{i\theta}) - f(a)}{\rho e^{i\theta}} i\rho e^{i\theta} \ d\theta
= i\int_0^{2\pi} f(a + \rho e^{i\theta}) - f(a) \ d\theta
++++
Then we get that
[stem]
++++
\begin{aligned}
\left|\oint_{C_1} \frac{f(z) - f(a)}{z-a} \ dz\right|
&= \left| \int_0^{2\pi} f(a + \rho e^{i\theta}) - f(a) \ d\theta \right|
\\&\leq \int_0^{2\pi} |f(a + \rho e^{i\theta}) - f(a)| \ d\theta
\\&\leq \int_0^{2\pi} \varepsilon \ d\theta
\\&= 2\pi \varepsilon
\end{aligned}
++++
Therefore the integral must be zero since it lies in every circle centered at the origin.
Hence,
[stem]
++++
\oint_{C} \frac{f(z)}{z-a} \ dz
= \oint_{C_1} \frac{f(a)}{z-a} \ dz
= \int_0^{2\pi} \frac{f(a)}{\rho e^{i\pi}} i\rho e^{i\theta} \ d\theta
= i f(a)\int_0^{2\pi} \ d\theta
= 2\pi i f(a)
++++
and we are done.

====

TIP: This formula can be combined with the multiple connected
region theorem to simplify integrals.

=== Cauchy's multiple integral formula
If stem:[f(z)] is analytic in-between contours stem:[C] and stem:[D] and stem:[a] lies in that region,
[stem]
++++
f(a) = \frac{1}{2\pi i}\oint_{C} \frac{f(z)}{z-a} \ dz - \frac{1}{2\pi i}\oint_{D} \frac{f(z)}{z-a} \ dz
++++
image::cauchy-integral-multiple.png[width=50%]

NOTE: This is a corollary of the integral formula along with the multiple region theorem.

=== Gauss Mean Value Theorem
If stem:[f(z)] is analytic in some domain stem:[D] and
the circular region stem:[|z - a| \leq \rho] is contained in stem:[D],
then
[stem]
++++
f(a) = \frac{1}{2\pi} \int_0^{2\pi} f(a + \rho e^{i\theta}) \ d\theta
++++
This follows immediately from a change of variables stem:[z = \rho e^{i\theta}] on the integral
theorem.

=== Posisson's Integral Formula
Let stem:[f(z)] be analytic in stem:[C \cup I(C)] where
stem:[C] is the circle stem:[|z| = R]. Then if
stem:[r < R] and stem:[z = re^{i\theta}] then
[stem]
++++
f(re^{i\theta}) = \frac{1}{2\pi} \int_0^{2\pi} \frac{R^2 - r}{R^2 - 2Rr\cos(\theta-\phi) + r^2} f(Re^{i\phi}) \ d\phi
++++

.Proof
[%collapsible]
====
Let stem:[a = re^{i\theta}]. Then by the integral theorem
[stem]
++++
f(re^{i\theta})
= \frac{1}{2\pi i }\oint_C \frac{f(z)}{z-re^{i\theta}} \ dz
++++
Also, since stem:[\frac{R^2}{\overbar{a}}] is the inverse point of stem:[a],
it lies outside of stem:[C] and hence
[stem]
++++
0 
= \oint_{C} \frac{f(z)}{z-\frac{R^2}{\overbar{a}}} \ dz
= \oint_{C} \frac{f(z)}{z-\frac{R^2}{r}e^{i\theta}} \ dz
++++
since it remains analytic since it has no poles. Therefore
[stem]
++++
\begin{aligned}
f(re^{i\theta})
&= \frac{1}{2\pi i }\oint_C \frac{f(z)}{z-re^{i\theta}} \ dz - \frac{1}{2\pi i}\oint_{C} \frac{f(z)}{z-\frac{R^2}{r}e^{i\theta}} \ dz
\\&= \frac{1}{2\pi i }\oint_C \frac{f(z)}{z-re^{i\theta}} - \frac{f(z)}{z-\frac{R^2}{r}e^{i\theta}} \ dz
\\&= \frac{1}{2\pi i }\int_0^{2\pi} f(Re^{i\phi})
    \frac{
        Re^{i\phi}-\frac{R^2}{r}e^{i\theta} - Re^{i\phi}+re^{i\theta}
    }{
        (Re^{i\phi}-re^{i\theta})\left(Re^{i\phi}-\frac{R^2}{r}e^{i\theta}\right)
    } iRe^{i\phi} d\phi
\\&= \frac{1}{2\pi}\int_0^{2\pi} f(Re^{i\phi})
    \frac{
        re^{i\theta} - \frac{R^2}{r}e^{i\theta}
    }{
        (Re^{i\phi}-re^{i\theta})\left(Re^{i\phi}-\frac{R^2}{r}e^{i\theta}\right)
    } Re^{i\phi} d\phi
\\&= \frac{1}{2\pi}\int_0^{2\pi} f(Re^{i\phi})
    \frac{
        r^2 - R^2
    }{
        (Re^{i\phi}-re^{i\theta})(re^{-i\theta}-Re^{-i\phi})
    } d\phi
\\&= \frac{1}{2\pi}\int_0^{2\pi} f(Re^{i\phi})
    \frac{
        r^2 - R^2
    }{
        Rre^{i(\phi-\theta)} - R^2 - r^2 + Rre^{i(\theta-\phi)}
    } d\phi
\\&= \frac{1}{2\pi}\int_0^{2\pi} f(Re^{i\phi})
    \frac{
        r^2 - R^2
    }{
        2Rr\cos(\theta - \phi) - R^2 - r^2
    } d\phi
\\&= \frac{1}{2\pi}\int_0^{2\pi} f(Re^{i\phi})
    \frac{
        R^2 - r^2
    }{
        R^2 - 2Rr\cos(\theta - \phi) + r^2
    } d\phi
\end{aligned}
++++
====

== Derivatives
If stem:[f(z)] is analytic on stem:[C \cup I(C)] then
[stem]
++++
f'(a) = \frac{1}{2\pi i }\oint_C \frac{f(z)}{(z-a)^2} \ dz
++++
and in general
[stem]
++++
f^{(n)}(a) = \frac{n!}{2\pi i }\oint_C \frac{f(z)}{(z-a)^{n+1}} \ dz
++++
Both of these are a result of from the xref:#_lemma_1[following lemma]
since xref:#_integral_formula[Cauchy's integral formula] holds and stem:[f(z)]
is analytic and hence bounded on stem:[C].

=== Lemma 1
Let stem:[C] be a closed contour on which stem:[g(z)] is bounded.
Also let
[stem]
++++
f(z) = \oint_C \frac{g(w)}{(w - z)^n} \ dw
++++
for all stem:[z \in I(C)] for some stem:[n \in \mathbb{Z}^+]. Then,
stem:[f(z)] is analytic on stem:[I(C)] and
[stem]
++++
f'(z) = n \oint_C \frac{g(w)}{(w-z)^{n+1}} \ dw
++++
for all points stem:[z \in I(C)]. This then implies that stem:[f]
is infinitely differentiable.

.Proof
[%collapsible]
====
Firstly, focus on some stem:[z=a \in I(C)] and let stem:[d > 0] be such that 
stem:[|z-a| > d] for all stem:[z \in C].
Then let stem:[\varepsilon > 0] and stem:[0<|h| < \delta] where stem:[0<\delta < \frac{d}{2}]

image::cauchy-integral-lemma-1.png[width=50%]

Then, we get that
[stem]
++++
\frac{f(a + h) - f(a)}{h} - \oint_C \frac{ng(w)}{(w-a)^{n+1}} \ dw
= \oint_C g(w)h\frac{\mu_n(w,h,a)}{(w-a -h)^n(w-a)^{n+1}} \ dw
++++
where stem:[\mu_n] is some multinomial in stem:[w], stem:[h] and stem:[a].

.When stem:[n=1]
[%collapsible]
=====
Because of the manipulations done when stem:[n \geq 2], we would conduct
this case separately.
[stem]
++++
\begin{aligned}
&\frac{f(a + h) - f(a)}{h} - \oint_C \frac{g(w)}{(w-a)^{2}} \ dw
\\&= \frac{1}{h}\left[ \oint_{C} \frac{g(w)}{(w-a -h)} \ dw - \oint_C \frac{g(w)}{(w-a)} \ dw \right] - \oint_C \frac{g(w)}{(w-a)^{2}} \ dw
\\&= \frac{1}{h}\oint_C g(w)\frac{(w-a) - (w-a-h)}{(w-a -h)(w-a)} \ dw - \oint_C \frac{g(w)}{(w-a)^{2}} \ dw
\\&= \oint_C g(w)\frac{1}{(w-a -h)(w-a)} \ dw - \oint_C \frac{g(w)}{(w-a)^{2}} \ dw
\\&= \oint_C g(w)\frac{(w-a) - (w-a-h)}{(w-a -h)(w-a)^2} \ dw
\\&= \oint_C g(w)\frac{h}{(w-a -h)(w-a)^2} \ dw
\\&= \oint_C g(w)h\frac{\mu_1(w,h,a)}{(w-a -h)(w-a)^{2}} \ dw
\end{aligned}
++++
=====
.When stem:[n \geq 2]
[%collapsible]
=====
[stem]
++++
\begin{aligned}
&\frac{f(a + h) - f(a)}{h} - \oint_C \frac{ng(w)}{(w-a)^{n+1}} \ dw
\\&= \frac{1}{h}\left[ \oint_{C} \frac{g(w)}{(w-a -h)^n} \ dw - \oint_C \frac{g(w)}{(w-a)^n} \ dw \right] - \oint_C \frac{ng(w)}{(w-a)^{n+1}} \ dw
\\&= \frac{1}{h}\oint_C g(w)\frac{(w-a)^n - (w-a-h)^n}{(w-a -h)^n(w-a)^n} \ dw - \oint_C \frac{ng(w)}{(w-a)^{n+1}} \ dw
\\&= \oint_C g(w)\frac{\frac{1}{h}(w-a)[(w-a)^n - (w-a-h)^n] - n(w-a-h)^n}{(w-a -h)^n(w-a)^{n+1}} \ dw
\\&= \oint_C g(w)\frac{\frac{1}{h}(w-a)^{n+1} - \frac{1}{h}(w-a)(w-a-h)^n - n(w-a-h)^n}{(w-a -h)^n(w-a)^{n+1}} \ dw
\\&= \oint_C g(w)\frac{\frac{1}{h}(w-a)^{n+1} - \left[\frac{1}{h}(w-a) +n\right](w-a-h)^n}{(w-a -h)^n(w-a)^{n+1}} \ dw
\\&= \oint_C g(w)\frac{\frac{1}{h}(w-a)^{n+1} - \left[\frac{1}{h}(w-a) +n\right]\sum_{k=0}^n\binom{n}{k}(-h)^k(w-a)^{n-k}}{(w-a -h)^n(w-a)^{n+1}} \ dw
\\&= \oint_C g(w)\frac{\frac{1}{h}(w-a)^{n+1} - \left[\frac{1}{h}(w-a) +n\right](w-a)^n - \left[\frac{1}{h}(w-a) +n\right]\sum_{k=1}^n\binom{n}{k}(-h)^k(w-a)^{n-k}}{(w-a -h)^n(w-a)^{n+1}} \ dw
\\&= \oint_C g(w)\frac{-n(w-a)^n - \left[\frac{1}{h}(w-a) +n\right]\sum_{k=1}^n\binom{n}{k}(-h)^k(w-a)^{n-k}}{(w-a -h)^n(w-a)^{n+1}} \ dw
\\&= \oint_C g(w)\frac{-n(w-a)^n - \left[\frac{1}{h}(w-a) +n\right]n(-h)(w-a)^{n-1} - \left[\frac{1}{h}(w-a) +n\right]\sum_{k=2}^n\binom{n}{k}(-h)^k(w-a)^{n-k}}{(w-a -h)^n(w-a)^{n+1}} \ dw
\\&= \oint_C g(w)\frac{n^2h(w-a)^{n-1} - \left[\frac{1}{h}(w-a) +n\right]\sum_{k=2}^n\binom{n}{k}(-h)^k(w-a)^{n-k}}{(w-a -h)^n(w-a)^{n+1}} \ dw
\\&= \oint_C g(w)h\frac{n^2(w-a)^{n-1} - \left[(w-a) +nh\right]\sum_{k=2}^n\binom{n}{k}(-h)^{k-2}(w-a)^{n-k}}{(w-a -h)^n(w-a)^{n+1}} \ dw
\\&= \oint_C g(w)h\frac{\mu_n(w,h,a)}{(w-a -h)^n(w-a)^{n+1}} \ dw
\end{aligned}
++++
=====
Then we see that,
[stem]
++++
|w-a-h| \geq |w-a| - |h| > d - \frac{d}{2} = \frac{d}{2}
++++
Also, let stem:[|g(w)| < M] and stem:[|\mu_n(w,h,a)| < N_n]. Then,
[stem]
++++
\begin{aligned}
&\left|\frac{f(a + h) - f(a)}{h} - \oint_C \frac{ng(w)}{(w-a)^{n+1}} \ dw\right|
\\&= \left|\oint_C g(w)h\frac{\mu_n(w,h,a)}{(w-a -h)^n(w-a)^{n+1}} \ dw\right|
\\&\leq \oint_C |g(w)||h|\frac{|\mu_n(w,h,a)|}{|w-a -h|^n|w-a|^{n+1}} \ |dw|
\\&\leq \oint_C |h|\frac{MN_n}{\left(\frac{d}{2}\right)^n d^{n+1}} \ |dw|
\\&= \frac{2^nMN_n}{d^{2n+1}}|h|\oint_C \ |dw|
\\&= \frac{2^nMN_n L}{d^{2n+1}}|h|
\\&= \frac{2^nMN_n L}{d^{2n+1}}\delta
\end{aligned}
++++
where stem:[L] is the length of stem:[C]. Then, we could have fixed stem:[\delta]
to be a value such that the above was less than stem:[\varepsilon] and hence we get that
[stem]
++++
f'(a) = \lim_{h\to 0} \frac{f(a + h) - f(a)}{h} = \oint_C \frac{ng(w)}{(w-a)^{n+1}} \ dw
++++
====


=== Liouville's Theorem: Bounded entire functions
If stem:[f(z)] is a bounded entire function, then stem:[f(z)] is constant.

.Proof
[%collapsible]
====
Focus on some stem:[z = a] and let stem:[r > 0] and stem:[C] be a circle
of radius stem:[r] centered at stem:[a]. Then,

[stem]
++++
f'(a) = \frac{1}{2\pi i}\oint_C \frac{f(z)}{(z-a)^2} \ dz
++++
Let stem:[|f(z)| < M], then
[stem]
++++
|f'(a)|
= \frac{1}{2\pi}\left|\oint_C \frac{f(z)}{(z-a)^2} \ dz\right|
\leq frac{1}{2\pi}\oint_C \frac{|f(z)|}{|z-a|^2} \ |dz|
\leq frac{1}{2\pi}\oint_C \frac{M}{r^2} \ |dz|
= frac{1}{2\pi} \frac{2\pi r M }{r^2}
= \frac{M}{r}
++++
and hence for each stem:[\varepsilon > 0], stem:[\exists r = \frac{M}{\varepsilon}]
such that stem:[|f'(a)| < \varepsilon]. Therefore,
stem:[f'(a) = 0] and hence stem:[f(z)] is constant.
====

== Morera's Theorem
Let stem:[f(z)] be defined on domain stem:[D]. Then if

* stem:[f(z)] is continuous inside all closed contours stem:[C] inside stem:[D]
* stem:[\oint_C f(z) \ dz = 0] for all simple closed curves stem:[C] inside stem:[D].

Then, stem:[f(z)] is analytic in stem:[D].

NOTE: The second condition can equivalently stated as the integral is
independent of path inside stem:[D].


[REMARK]
====
I'm not totally sure, but I do believe that this page has already proven this
result. Notice that all the above proofs (barring the first) only required that
the integral of a closed curve as stem:[0] and the function was continuous. Note
that the requirement of continuity was only used by the integral formula. Then
since the derivative formula holds, we get that stem:[f(z)] must be analytic.

{empty}... I must be missing something
====
