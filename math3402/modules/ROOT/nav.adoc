* Running notes
** xref:running-notes/2021-09-13.adoc[]
** xref:running-notes/2021-09-14.adoc[]
* xref:intro.adoc[]
* xref:first-order-linear.adoc[]
* xref:fourier-series/index.adoc[]
** xref:fourier-series/non-2pi.adoc[]
* xref:second-order-linear/index.adoc[]
** Special Equations
*** xref:second-order-linear/wave-1d.adoc[]
*** xref:second-order-linear/heat-1d.adoc[]
*** xref:second-order-linear/laplace-2d.adoc[]
