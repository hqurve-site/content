= Probability Moments

Consider sequence
[stem]
++++
p_0, p_1, p_2, \ldots 
++++

NOTE: Typically these would be the probabilities of a discrete
distribution.

== Moments
We have several interesting moments

=== Ordinary moments
[stem]
++++
m_k = \sum_{j=0}^\infty j^k p_j
= 0^k p_0 + 1^k p_1 + 2^k p_2 + 3^k p_3 + \cdots
++++
where stem:[0^0 = 1] so that stem:[m_0 = \sum_{j=0}^\infty p_j].

=== Factorial moments
[stem]
++++
(m)_k = \sum_{j=0}^\infty (j)_k p_j
++++
where stem:[(j)_k] is the falling factorial. That is
[stem]
++++
(j)_k = \frac{j!}{(j-k)!} = (j)(j-1)(j-2)\cdots(j-k+1)
++++
Also, by using xref:stirling.adoc[stirling numbers], we get that
[stem]
++++
m_k = \sum_{i=1}^k S(k, i)(m)_i
\quad\text{and}\quad
(m)_k = \sum_{i=1}^k s(k, i)m_i
++++

.Proof
[%collapsible]
====
[stem]
++++
\begin{aligned}
m_k
&= \sum_{j=0}^\infty j^k p_j
\\&= \sum_{j=0}^\infty \sum_{i=1}^k S(k, i)(j)_i p_j
\\&= \sum_{i=1}^k S(k, i)\sum_{j=0}^\infty (j)_i p_j
\\&= \sum_{i=1}^k S(k, i)(m)_i
\end{aligned}
++++
====

=== Binomial Moments
[stem]
++++
B_k = \sum_{j=k}^\infty \binom{j}{k} p_j = \sum_{j=k}^\infty \frac{(j)_k}{k!}p_j
++++


== Generating Functions

=== Ordinary Generating function
[stem]
++++
P(t) = \sum_{k=0}^\infty p_k t^k
++++

=== Moment generating functions

Exponential::
stem:[m(t) = \exp(mt) = \sum_{k=0}^\infty m_k \frac{t^k}{k!} = P(e^t)]

Factorial::
stem:[\exp(m)t = \sum_{k=0}^\infty (m)_k \frac{t^k}{k!} = P(1+t)]

Binomial::
stem:[B(t) = \sum_{k=0}^\infty B_k t^k = P(1+t)]

=== Cumulants
We define the cumulant (exponential) generating function to be
[stem]
++++
L(t) = \ln m(t)
= \sum_{k=1}^\infty \lambda_k \frac{t^k}{k!}
= \lambda_1 t + \lambda_2 \frac{t^2}{2!} + \lambda_3 \frac{t^3}{3!} + \cdots
++++
where stem:[\lambda_k] is the stem:[k]'th cumulant.

NOTE: We omit stem:[\lambda_0] since we would
usually expect that stem:[m_0 = m(0) = 1].
