= Factor Analysis

Suppose we have conduct a survey using a questionnaire.
Factor analysis allows us to answer the following questions

. How many *factors* or underlying quantities does the questionnaire measure?
. Which questions measure which factors?

.Psychometric Survey
====
A psychometric survey for prospective employees
may may want to measure multiple factors, such as honesty,
creativity and confidence.
Instead of using a single question to measure each of these factors,
the questionnaire will use multiple, possibly overlapping questions per factor.
Factor analysis will allow the employers to determine how well the
questionnaire was designed and allow them to analyze
how well each of the questions measure the underlying variables.
====

.Data reanalysis
====
A shipping company needs to know when their ships need maintenance.
To do this they use a large set of quantitative and qualitative measurements 
which are obtained from onboard diagnostic systems and crew reports.
For example, the average engine temperature may be used as an indicator
of when the engine needs servicing.
However, environmental temperatures many significantly distort
the inference based on engine temperature alone.
Factor analysis will allow us to determine what other measurements may be used
as an auxiliary to engine temperature and the significance of each of these
measurements.
====

Note that in the above examples, factor analysis does not assign
names to the factors.
The names and meanings of each of the factors must be reverse engineered
based on the data and the prior information.


The typical steps of factor analysis are as follows

. Collect data
. Extract the initial factors (perhaps via PCA)
. Choose the number of factors to retain
. Choose an estimation method
. Rotate and interpret factors
. Decide if changes need to be made to the model (eg dropping/including items).
  If any changes are made, repeat steps 4 and 5
. Construct scales/scores and use them in further analysis (eg as independent variables in a regression
model).

There are also two types of factor analysis

Exploratory:: This is done when we do not know the factor structure.
Confirmatory:: This is done when we want to test an expected factor structure.

== The Orthogonal Factor Model
Suppose we have observable random vector stem:[\vec{X} \in \mathbb{R}^p]
and underlying common factors stem:[\vec{F}\in \mathbb{R}^m]
and specific factors stem:[\vec{\varepsilon} \in \mathbb{R}^p].
Then the *orthogonal factor model* is
[stem]
++++
\begin{array}{ccccc cccccc}
X_1 -\mu_1 &=& l_{11}F_1 &+& l_{12}F_2 &+& \cdots &+& l_{1m}F_{m} &+& \varepsilon_1\\
X_2 -\mu_2 &=& l_{21}F_1 &+& l_{22}F_2 &+& \cdots &+& l_{2m}F_{m} &+& \varepsilon_2\\
&\vdots &\\
X_p -\mu_p &=& l_{p1}F_1 &+& l_{p2}F_2 &+& \cdots &+& l_{pm}F_{m} &+& \varepsilon_p\\
\end{array}
++++
or equivalently
[stem]
++++
\vec{X} - \vec{\mu} = \mat{L}\vec{F} + \vec{\varepsilon}
++++
where
stem:[\mat{L}] is is the matrix of *factor loadings* (constant)
and stem:[\vec{\mu}] is the mean vector (also constant)
and the following conditions are satisfied

* stem:[\vec{F}] and stem:[\vec{\varepsilon}] are independent
* stem:[E[\vec{F}\]=\vec{0}] and stem:[Cov[\vec{F}\] = \mat{I}]
* stem:[E[\vec{\varepsilon}\] = \vec{0}] and stem:[Cov[\vec{\varepsilon}\]=\mat{\Psi}]
where stem:[\mat{\Psi}] is a diagonal matrix.

NOTE: If stem:[Cov[\vec{F}\]] is not a diagonal matrix,
the model is instead a *oblique factor model*

Note that in this model, the common and specific factors are both
unobservable.

=== Covariance structure

Factor analysis aims to recover the common factors by estimating the loadings.
In order to estimate the loadings, note the covariance structure
of the observable data stem:[\vec{X}].

[.proposition]
****
Let stem:[\vec{X}] be a random vector which follows the orthogonal factor
model. Then

. stem:[Cov[\vec{X}\] = \mat{\Sigma} = \mat{L}\mat{L}^T + \mat{\Psi}].
That is
+
--
[stem]
++++
\begin{aligned}
Var[X_i] &= l_{i1}^2 + \cdots l_{im}^2 + \psi_i
\\
Cov[X_i, X_k] &= l_{i1}l_{k1} + \cdots l_{im}l_{km}
\end{aligned}
++++
--
. stem:[Cov[\vec{X}, \vec{F}\] = \mat{L}].
That is
+
--
[stem]
++++
Cov[X_i, F_j] = l_{ij}
++++
--
****

Therefore, we can use the sample covariance matrix in order to estimate
the parameters of the factor model.

Note that the covariance structure of the model can be simplified as
[stem]
++++
Var[X_i] = h_i^2 + \psi_i
++++
where stem:[h_i^2 = l_{i1}^2 + \cdots l_{im}^2] is the *communality* and
measures the amount of variance of stem:[X_i] explained by the common factors.
Sometimes the stem:[\psi_i] is referred to as the *uniqueness*.

* If the stem:[X_i] is *informative* of the factors, the communality is high.
* If the stem:[X_j] is *not informative* of the factors, the uniqueness is high.

=== Model fitting
The following are common methods which are used
to determine the parameters of the model:

* principal component analysis
* principal factor/axis
* maximum likelihood estimation

A benefit of principal component analysis is that it also
informs us how many factors the model should use.

=== Factor Rotation
Note that if stem:[\mat{L}] is a loading matrix and
stem:[\mat{T}] is an orthogonal matrix (stem:[\mat{T}\mat{T}^T = \mat{I}]),
then both stem:[\mat{L}] and stem:[\mat{L}^*=\mat{T}\mat{L}]
yield the same covariance structure of stem:[\vec{X}].
Therefore the choice of stem:[\mat{L}] is not unique.

Since the choice of stem:[\mat{L}] is not unique and does not affect the fit
of the model, we perform *factor rotation* to increase
the *ease of interpretation*. 
Common criteria are

Varimax::
Most often the *varimax criterion* is used for factor rotation.
This criterion maximizes the variables of the squared loadings
for each factor by making some of then as large as possible and the rest
as small (close to zero) as possible).

Quartimax::
The *quartimax criterion* maximizes
the variance of the squared loadings on each variable.
It tends to produce factors with high loadings for all variables.

== Implementing Factor Analysis

[source, R]
----
# data with observations as rows and variables as columns
data <- ..

# perform factor analysis
factanal(data, factors=3, rotation = "varimax")
----

TIP: See https://www.geo.fu-berlin.de/en/v/soga-r/Advances-statistics/Multivariate-approaches/Factor-Analysis/A-Simple-Example-of-Factor-Analysis-in-R/index.html
