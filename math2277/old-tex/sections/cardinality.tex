\section{Cardinality}
The cardinality of a set $X$ is denoted $|X|$ and is a measure of the size of $X$.

\subsection{Set equivalence}
Two sets $A$ and $B$ are said to be equivalent if there exists a bijection
from $A$ to $B$. This is written as 
\[
    |A| = |B| \quad\text{or}\quad A \sim B
\]
Also, note that set equivalence is a equivalence relation since
\begin{itemize}
    \item the identity function is bijective
    \item for each bijective function, there is a unique inverse function that is also a bijection.
    \item function composition of two bijective functions is a bijection.
\end{itemize}
As such, we can arrange sets into equivalence classes according to their cardinality
which will be useful.

\subsection{Finite sets}
We denote $J_n$ to be the set of elements $\{1, 2, \ldots, n\}$
where $n \in \mathbb{N}$. Then, we call a set finite if it is either 
equivalent to $J_n$ for some $n$ or if it is the empty set. Otherwise, 
we call the set infinite.

\subsection{Cardinal numbers}
The cardinal number of a set is the value assigned to its cardinality. If 
\begin{itemize}
    \item the set is empty, its cardinal number is $0$.
    \item if it is equivalent to $J_n$, its cardinal number is $n$.
    \item if it is infinite, the cardinal number is called transfinite.
\end{itemize}

\subsection{Ordering of cardinal numbers}
We can induce an order on all sets (more specifically their equivalence classes or cardinalities)
by defining the following relation.
We say that $|A| \leq |B|$ iff $\exists$ an injection from $A$ to $B$.
Then, `$\leq$' exhibits the necessary properties to induce an ordering
\begin{itemize}
    \item reflexive: since the identity function is injective
    \item transitive: since the functional composition of two injective functions is also injective
    \item antisymmetric: by the Cantor-Schr\"oder-Bernstein theorem, the existence of a injection
            from $A$ to $B$ and vice-versa ensures that there is a bijection from $A$ to $B$
            and hence $|A| = |B|$.
\end{itemize}

\subsubsection{Cantor-Schr\"oder-Bernstein theorem}
The Cantor-Schr\"oder-Bernstein theorem asserts that if $f:A\to B$
and $g:B\to A$ are both injective, then their exists a bijective function
from $A$ to $B$.

The proof given below is attributed to Julius K\"onig however I changed (and added) a few bits to make it easier to understand
% (plus the generation of functions are slightly different but equivalent)
; a translation of the proof can be found at \url{https://en.wikipedia.org/wiki/Schr%C3%B6der%E2%80%93Bernstein_theorem}. 
Alternatively, if the axiom of choice holds (in your system), the proof is quite simple as the existence of the injection $g:B\to A$
implies that there is a surjection from $A$ to $B$.

\begin{proofi}Recall that functions are just relations that exhibit the following two
    properties (where $h: A \to B$)
    \begin{itemize}
        \item Existence: $\forall a \in A: \exists b \in B: (a, b) \in h$
        \item Uniqueness: $\forall a\in A: \forall b_1, b_2 \in B: (a, b_1) \in h \;\land\; (a,b_2) \in h \implies b_1=b_2$.
    \end{itemize}
    Now, for injective functions their inverse relations necessarily exhibit
    the uniqueness while the existence property may or may not hold. That is, the inverse of an element
    in the codomain of an injective function either does not exist or is unique.

    Next, we will say that $A$ and $B$ are disjoint sets. If they arent, we can simply adjoin an element to $A$ and $B$
    to produce two corresponding sets $A'$ and $B'$ (for example we could have $A\times\{0\}$ and $B\times\{1\}$).
    Then, consider an elements $a \in A$ and $b \in B$. Then, we can produce the following \emph{chain}
    \[
        \cdots \to f^{-1}(g^{-1}(a)) \to g^{-1}(a) \to a \to f(a) \to g(f(a)) \cdots
    \]
    Then, notice that since $f, g, f^{-1}, g^{-1}$ all produce unique elements, $a$ belongs
    to exactly one chain. That is, if $a$ belongs to two chains (for example that produced by $a$ and $g(f(a))$),
    then sequences to the left and right of $a$ are identical. Similarly, each $b$ occurs in exactly
    one chain. Therefore, the chains partition the (disjoint) union of $A$ and $B$ and it suffices
    to produce bijections within the chains. 
    
    \paragraph*{Note} the direction of the bijections do not matter as the inverse of a bijection is also a bijection.
    
    Now, a chain may have one of four states and hence the bijection would be produced accordingly
    \begin{itemize}
        \item Its rightmost element is in $A$. Then, immediately, the bijection would be $f$.
        \item Its rightmost element is in $B$. Then, the bijection would be $g$.
        \item The chain forms a cycle. Then both $f$ or $g$ form bijections. 
        \item The chain is doubly infinite. Then both $f$ and $g$ form bijections.
    \end{itemize}

    
\end{proofi}