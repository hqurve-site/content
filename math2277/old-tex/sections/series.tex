\section{Infinite Series}
Let $\{a_n\}$ be a sequence of real numbers. Then, we define the sequence 
$\{s_n\}$ of \emph{parital sums} by
\[
    s_n = \sum_{k=1}^n a_k
\]
Then, we say that the \emph{infinite series} $\sum_{n=1}^\infty$ is convergent if 
$\{s_n\}$ is convergent and we define
\[
    s = \sum_{n=1}^\infty \coloneq \lim_{n\to \infty} \sum_{k=1}^n a_k = \lim_{n\to \infty} s_n
\]
where $s$ is the \emph{sum of the series}. If $\sum_{n=1}^\infty a_n$ is not convergent,
we say that the series is \emph{divergent}.

\paragraph*{Note on starting index} Although we initially defined 
the infinite series as starting from index $n=1$, we can start at any
integer as it would only fintely affect the series. That is, if a series 
is convergent, it remains convergent if finitely many terms are added or removed.

\subsubsection{Linearity}
The infinite sum is a linear operator, that is if $\sum_{n=1}^\infty a_n = a$
and $\sum_{n=1}^\infty b_n = b$ then
\[
    \sum_{n=1}^\infty (a_n + b_n) = a + b
\]
and 
\[
    \sum_{n=1}^\infty ka_n = ka 
\]
where $k \in \mathbb{R}$.

\subsubsection{Cauchy criterion for convergence}
Let $\{a_n\}$ be a sequence of real numbers and consider the sequence of partial sums
$\{s_n\}$. Then, recall that a real sequence is convergent iff it is cauchy convergent.
Then, $\sum_{n=1}^\infty a_n$ converges iff
\[
    \forall \varepsilon > 0: \exists N \in \mathbb{N}: \forall m, n \in \mathbb{N}:
    n > m > N \implies |s_n - s_m| = |a_{m+1} + \cdots + a_{n}| < \varepsilon
\]

\subsubsection{Absolute Convergence}
Let $\{a_n\}$ be a sequence of real numbers. Then, the series $\sum_{n=1}^\infty a_n$ is said 
to be \emph{absolutely convergent} if the series $\sum_{n=1}^\infty |a_n|$ converges. 
Then, by the triangle inequality, 
\[
    \forall \varepsilon > 0: \exists N \in \mathbb{N}: \forall m, n \in \mathbb{N}:
    n > m > N \implies |a_{m+1} + \cdots + a_n| \leq |a_{m+1}| +\cdots + |a_{n}| < \varepsilon
\]
we see that absolute convergence implies convergence; that is, absolute convergence
is a special kind of convergence. Additionally, we call convergent series which 
are not absolutely convergent, \emph{conditionally convergent}. 

\paragraph*{Side note:} I believe that the term `conditional' is used\textsuperscript{[citiation needed]} 
since the value to which a series consisting of the same terms terms
depends on the arrangement of said terms. In fact by the Riemann series theorem, the terms 
of a conditionally convergent sequence can be rearranged in such a manner, to converge to any real number,
positive or negative infinity, or diverge. This is unlike absolutely convergent series which 
converge to the same value regardless of the order of its terms. From this, we see that 
an absolutely convergent sequence is simply the sum of a set while a conditionally convergent sequence
is dependent on its sequence of partial sums.

\paragraph*{Side note 2:} I just remembered how integrals (and integral transforms) are defined,
they require absolute convergence. Usually it is phrased as the integral of the negatives
and positives, both converge and the integral of the function itself is simply the difference. 
In a similar manner, we could consider an absolutely convergent series as the difference
of the series(es) of positive and negative terms provided that they both converge.

\subsection{Tests for convergence/divergence}

\subsubsection{Limit of terms is zero if convergent}
Let $\{a_n\}$ be a real sequence such that $\sum_{n=1}^\infty a_n$ converges. 
Then immediately, by the cauchy criterion, $\{a_n\}$ must tend to zero by taking $m = n-1$.

\subsubsection{Comparision test}
Let $\{a_n\}$ and $\{b_n\}$ be two non-negative sequences where $\forall n \in \mathbb{N}: a_n \leq b_n$. Then
\begin{itemize}
    \item If $\sum_{n=1}^\infty b_n$ is convergent, then $\sum_{n=1}^\infty a_n$ is also convergent.
    \item If $\sum_{n=1}^\infty a_n$ is divergent, then $\sum_{n=1}^\infty b_n$ is also divergent.
\end{itemize}
\begin{proofi} 
    Firstly, note that the second statement is simply the contrapositive of the first, so only the first statement
    will be proven.

    Let $\{s_n\}$ and $\{t_n\}$ be the sequence of partial sums of $\{a_n\}$
    and $\{b_n\}$ respectively. Then notice that both $\{s_n\}$ and $\{t_n\}$ are 
    monotonically increasing and $\forall n \in \mathbb{N}: s_n \leq t_n$. Then, 
    if $\{t_n\}$ (or equivalently $\sum_{n=1}^\infty b_n$) converges, then by the monotone convergence
    theorem, there exists an upper bound $M$ for $\{t_n\}$. This upper bound must also
    be an upper bound for $\{s_n\}$ and the by monotone convergence theorem we are done.
\end{proofi}

\subsubsection{Limit comparision test}
Let $\{a_n\}$ and $\{b_n\}$ be two positive sequences such that $\{a_n / b_n\}$
converges to a positive number. Then, either both $\sum_{n=1}^\infty a_n$ 
and $\sum_{n=1}^\infty b_n$ converge or both diverge.
\begin{proofi}
    Note that since $\{a_n / b_n\}$ converges to a positive value, the sequence
    of recipricals $\{b_n / a_n\}$ also converge to a positve value. Then,
    we only need to show that $\sum_{n=1}^\infty a_n$ converges implies
    that $\sum_{n=1}^\infty b_n$ also converges.

    Consider arbitary $\varepsilon > 0$ and let $\lim_{n\to \infty} \frac{b_n}{a_n} = L$.
    Then, consider a yet to determined constants $\varepsilon_1, \varepsilon_2 > 0$ and note that
    \[
        \exists N_1 \in \mathbb{N}: \forall n \in \mathbb{N}: 
        n > N_1
        \implies \frac{b_n}{a_n} \in N(L, \varepsilon_1)
        \implies b_n < a_n(L +\varepsilon_1)
    \]
    Next, by the cauchy converge criterion, 
    \[
        \exists N_2 \in \mathbb{N}: \forall m, n \in \mathbb{N}: 
        n > m > N_2
        \implies \sum_{k=m}^n a_k < \varepsilon_2
    \]
    Now, let $N = \min(N_1, N_2)$ and we get that
    \[
        \forall m, n \in \mathbb{N}:
        n > m > N 
        \implies \sum_{k=m}^n b_k < \sum_{k=n}^m a_k (L + \varepsilon_1)
        < (L + \varepsilon_1)\varepsilon_2
    \]
    All that remains is to set $\varepsilon_1$ and $\varepsilon_2$ such that 
    $(L +\varepsilon_1)\varepsilon_2 \leq \varepsilon$. Note that this choice is arbitary
    but $\varepsilon_1 = 1$ and $\varepsilon_2 = \frac{\varepsilon}{L + 1}$
    would work.
\end{proofi}
\paragraph*{Note 1:} After doing some online research, I noted that I could have stopped immeditately at 
$b_n < a_n(L + \varepsilon_1)$ and applied the comparision test.

\paragraph*{Note 2:} I am yet to find another source for this, but the restrictions of this test are too strict.
All that is required is that $\{a_n/b_n\}$ is bounded between two positive numbers (for all $n$ greater than some 
$N \in \mathbb{N}$, there is no need
for the the sequence to converge. Additionally, using this version of the test, the statement is equivalent to 
normal comparision test (albiet with finite constants which do not affect the outcome).

\subsubsection{Ratio test}
Let $\{a_n\}$ be a sequence of non-zero terms and
\[
    L = \lim_{n \to \infty} \left| \frac{a_{n+1}}{a_n} \right|
\]
Then, if 
\begin{itemize}
    \item $L < 1$, $\sum_{n=1}^\infty a_n$ converges absolutely.
    \item $L > 1$, $\sum_{n=1}^\infty a_n$ diverges.
    \item $L = 1$, the test gives no information about the convergence or divergence of $\sum_{n=1}^\infty a_n$.
\end{itemize}
\begin{proofi}
    In a similar manner to note 2 of the limit comparision test, we will be using bounds instead of limits.
    The wikipedia article states it in terms of limit inferior and limit superior, but the methods used here
    are equivalent. There is no need to find the actual infimum or supremum of the sequence, but all that is 
    required is the existence of bounds which satisfy the requirements. That is, all that is required is 
    to find a $N \in \mathbb{N}$ such that for all $n > N$, $\left|\frac{a_{n+1}}{a_n}\right|$ lies above or 
    below the required boundary. That being said, in order
    to show that the equivalent case of $L=1$, the actual supremum/infimum must be used.
    
    Consider the sequence $\{|a_{n+1}/a_{n}|\}$ with limit inferior $r$ and limit supremum $s$. Then
    \begin{itemize}
        \item $s < 1$ (or equivalent when $L < 1$). Then, let $v = \frac{1+s}{2} < 1$, and since
            $\varepsilon = v - s > 0$, $\exists N \in \mathbb{N}$ such that
            \[
                \forall n \in \mathbb{N}: n > N \implies \left|\frac{a_{n+1}}{a_n}\right| < v \implies |a_{n+1}| < v|a_n|
            \]
            Now, by induction
            \[
                \forall n \in \mathbb{N}: n > N \implies |a_{n+1}| \leq v |a_{n+1 - 1}| \leq v^2 |a_{n+1 - 2}| \leq \cdots \leq v^{n-N}|a_{N+1}|
            \]
            Note that I left in the case of equality for when $n = N+1$.
            Now, consider the series,
            \begin{align*}
                \sum_{k=1}^\infty |a_k| 
                &= \sum_{k=1}^N |a_k| + \sum_{k=N+1}^\infty |a_{k}|
              \\&= \sum_{k=1}^N |a_k| + \sum_{k=N}^\infty |a_{k+1}|
              \\&\leq \sum_{k=1}^N |a_k| + \sum_{k=N}^\infty |a_{N+1}|v^{n-N}
              \\&= \sum_{k=1}^N |a_k| + |a_{N+1}|\sum_{k=0}^\infty v^k
            \end{align*}
            Now, since $v < 1$, the geometric series converges and by the comparision test, the original sequence is 
            absolutely convergent.
        \item $r > 1$ (or equivalently when $L > 1$). Then, by letting $u = \frac{r+1}{2} > 1$,
            since $\varepsilon = r - u > 0$, $\exists N \in \mathbb{N}$ such that
            \[
                \forall n \in \mathbb{N}: n > N \implies \left|\frac{a_{n+1}}{a_n}\right| > u \implies |a_{n+1}| > u|a_n|
            \]
            and by a similar calculations, we would get that 
            \[
                \forall n \in \mathbb{N}: n > N \implies |a_{n+1}| \geq u^{n-N} |a_{N+1}|
            \]
            now since $a_{N+1} \neq 0$ and $u > 1$, it is clear that $\lim_{n\to \infty}|a_{n+1}| = \infty$.
            Therefore, $\lim_{n\to \infty} a_{n+1} \neq 0$  and in fact either tends to $\pm \infty$
            or doesnt exist. Then the series does not converge.
        \item For the case when $L = 1$, the equivalent cases are $r = 1$ and $s = 1$. I am yet to 
            come up with examples which do not produce a limit, but only produce the relevant
            limit superior or limit inferior. Below are three examples,
            \begin{itemize}
                \item $\sum_{n=1}^\infty 1$ diverges to infinity.
                \item $\sum_{n=1}^\infty (-1)^n$ diverges.
                \item $\sum_{n=1}^\infty \frac{(-1)^n}{n}$ converges conditionally
                \item $\sum_{n=1}^\infty \frac{1}{n^2}$ converges absolutely.
            \end{itemize}
            Note that in general, any rational function would produce a ratio limit of $1$.
    \end{itemize}
\end{proofi}
If not evident, the ratio test works by approximating the sequence $\{a_n\}$ by an exponential one $\{\beta L^n\}$.

\subsubsection{n'th root test}
Let $\{a_n\}$ be a real sequence and 
\[
    L = \lim_{n\to \infty} |a_n|^{\frac{1}{n}}
    \lim_{n\to \infty} \sqrt[n]{|a_n|}
\]
Then if
\begin{itemize}
    \item $L < 1$, the series $\sum_{n = 1}^\infty$ converges absolutely.
    \item $L > 1$, the series $\sum_{n = 1}^\infty$ diverges.
    \item $L=1$, no information could be obtained about the series.
\end{itemize}
Furthermore, all conclusions yeilded by the ratio test are also yeilded by the n'th root test (but not vice-versa).
\begin{proofi}
    Similar to the ratio test, the limit suprerior and limit inferior can be used instead of just the limit.
    Let $r$ and $s$ be the limit inferior and limit suprerior of $\{|a_n|^{\frac{1}{n}}|\}$ respectively. Then
    \begin{itemize}
        \item If $s < 1$, let $v = \frac{1+s}{2}$. Then, $\varepsilon = v-s > 0$ and $\exists N \in \mathbb{N}$ such that 
            \[
                \forall n \in \mathbb{N}: n > N \implies |a_{n}|^{\frac{1}{n}} < v \implies |a_n| < v^n
            \]
            Then, by the comparision test, $\sum_{n=N+1}^\infty |a_n|$ is convergent. Hence,
            the original series is absolutely convergent.
        \item If $r > 1$, let $u = \frac{1+s}{2}$. Then, $\varepsilon = r-u > 0$ and $\exists N \in \mathbb{N}$ such that 
            \[
                \forall n \in \mathbb{N}: n > N \implies |a_{n}|^{\frac{1}{n}} > u \implies |a_n| > u^n
            \]
            Then, the $\lim_{n\to \infty} |a_n| = \infty$ and hence $\{a_n\}$ does not converge
            to $0$. Therefore, the oriiginal series is divergent
        \item If $L=1$. The same examples given in the ratio test can be used here.
    \end{itemize}
    To see why the ratio test is weaker, we first show that conclusions in the ratio test
    imply conclusions for the n'th root test. Only the limit superior would be shown
    but the same arguement can be used for the limit inferior.

    Let $L = \lim_{n\to \infty} \sup \left|\frac{a_{n+1}}{a_n}\right|$ where $L < 1$
    and consider $\varepsilon > 0$ such that $L + \varepsilon < 1$. Then from the proof for the 
    limit comparision test, $\exists N \in \mathbb{N}$ such that
    \[
        \forall n \in \mathbb{N}: n > N \implies
        |a_{n+1}| < |a_{N+1}|(L +\varepsilon)^{N-n}
    \]
    and by reindexing
    \[
        \forall k \in \mathbb{N}: |a_{k+N+1}| < |a_{N+1}|(L + \varepsilon)^k 
        \implies |a_{k+N+1}|^{\frac{1}{k}} < |a_{N+1}|^{\frac{1}{k}}(L + \varepsilon)
    \]
    Note that the limit of $|a_{N+1}|^{\frac{1}{k}}$ is $1$ and hence the limit supremum is at most $L + \varepsilon$
    which is less than $1$. Hence, the condition for the n'th root test is satisifed.

    See example 2.10 in Lay's book to see where the ratio test is inconclusive while the root test works.
    Additionally, another example is $a_n = 3^{-n +(-1)^n}$ which was obtained from 
    \url{https://math.stackexchange.com/q/1708595}.
\end{proofi}
\paragraph*{Remark} Theorem 2.8 of Lay's book lays out a more general condition that in fact
only the limit suprerior needs to exceed $1$ for the series to diverge. I am not going to modify my proof
here, see Lay's book for the statement and proof. Furthermore, the wikipedia article states that if the
limit suprerior decreases to $1$, the series is also divergent.

\subsubsection{Alternating series test}
Let $\{a_n\}$ be a non-negative sequence of montonically decreasing terms which tend to $0$. Then, the series
\[
    \sum_{n=1}^\infty (-1)^n a_n
\]
is convergent. Intuitively, this works since series always alternates upwards and downwards
getting closer and closer (due to the decrasing nature of the terms). This together with the fact 
the terms tending to $0$ causes the difference in partial sums to tend to $0$
and also causes the series to converge. Note that this is an outline of one
of the actual proofs.

To see a complete  proof, see the article at 
\url{https://en.wikipedia.org/wiki/Alternating_series_test} which also gives a counterexample
to show the importance of montonic condition. 

\subsubsection{Integral test}
Let $\{a_n\}$ be a sequence where there exists a non-negative (monotonically) decreasing function $f: \mathbb{R} \to \mathbb{R}$
such that $a_n = f(n)$. Then
\[
    \sum_{n=1}^\infty a_n \text{ converges}
    \iff
    \int_1^\infty f(x) \ dx \text{ converges}
\]
Note that the lower bounds of both operations need not be $1$.
\begin{proofi}
    Firstly, note that 
    \[
        \forall n \in \mathbb{N}: f(n) \geq \int_n^{n+1} f(x) \ dx \geq f(n+1) \geq 0
    \]
    Since $n < x < n+1 \implies f(n) \geq f(x) \geq f(n+1)$ and the preservation
    of inequalities using integrals. Next, notice that the following relationship,
    \[
        \int_1^\infty f(x) \ dx = \sum_{n=1}^\infty \int_n^{n+1} f(x) \ dx
    \]
    which is the sum of the sequence $\left\{b_n =\int_n^{n+1} f(x) \ dx\right\}$.
    Then, we will show both directions seperately.
    \begin{itemize}
        \item if $\sum_{n=1}^\infty f(n)$ is convergent, then by the comparision test,
            the sum of $\left\{b_n\right\}$ converges. That is the integral converges.
        \item if the intergral converges, the sume of $\{b_n\}$ converges and by the 
            comparision test, the sum of $\{f(n+1)\}$ converges. Therefore,
            since this series only differs by one term, the sum of $\{f(n)\}$ 
            also converges.
    \end{itemize}
    From the above inequalities, it is possible to base bounds on both the 
    integral and summation.
\end{proofi}

\subsection{Some common series}
\subsubsection{Harmonic series}
The \emph{harmonic series} is defined by 
\[
    \sum_{n=1}^\infty \frac1{n}
\]
is divergent. This can be shown using the cauchy onvergence
criterion and partial sums $S_{n}$ and $S_{2n}$ which will have a 
difference greater than $n\frac{1}{2n} = \frac{1}{2}$.

\subsubsection{p-series}
A p-series is defined by 
\[
    \sum_{n=1}^\infty \frac{1}{n^p}
\]
which converges iff $p > 1$ where $p \in \mathbb{R}$. This can be easily shown using the intergal test for
when $p \neq 1$ and the harmonic series for when $p=1$.

\subsubsection{Geometric series}
A geometric series is one of the form
\[
    \sum_{n=1}^\infty ar^n
\]
which converges iff $|r| < 1$ where $a, r \in \mathbb{R}$.
This can be easily shown by writing the partial sum as $\frac{a(r^{n+1} - 1)}{r-1}$
when $|r| \neq 1$.

\subsection{Power series}
Let $\{a_n\}$ be a sequence of real numbers and $x_0 \in \mathbb{R}$. The series
\[
    \sum_{n=0}^\infty a_n(x-x_0)^n
\]
is called a \emph{power series} and $a_n$ is alled the $n$\textsuperscript{th}
\emph{coefficient} of the series. 

\subsubsection{Radius of convergence using ratio test}
Let $\{a_n\}$ be a sequence of real numbers and $x_0 \in \mathbb{R}$, 
by the ratio test, the series converges absolutely if
\[
    \lim_{n\to \infty} \left|\frac{a_{n+1}(x-x_0)^{n+1}}{a_n(x-x^0)^n}\right|
    = |x-x_0|\lim_{n\to \infty} \left|\frac{a_{n+1}}{a_n}\right|
    < 1
    \iff
    R \coloneq \lim_{n\to \infty}\left|\frac{a_n}{a_{n+1}}\right| > |x-x_0|
\]
Note that this gives no information about the convergence at the endpoints. Also,
we have two cases to consider, if $R = \infty$, this is because the original limit tends to $0$,
hence the power series is absolutely convergent everywhere. On the otherhand,
if $R = 0$, this is because the original limit diverged, hence the power series is 
only convergent at $x=x_0$ (trivially) and its value is equal to $a_0$.

\subsubsection{Radius of convergence using n'th root test}
Let $\{a_n\}$ be a sequence of real numbers and $x_0 \in \mathbb{R}$, 
by the ratio test, the series converges absolutely if
\[
    \lim_{n\to \infty} \sqrt[n]{|a_n(x-x_0)^n|}
    = |x-x_0|\lim_{n\to \infty} \sqrt[n]{|a_n|} < 1
    \iff
    R \coloneq \lim_{n\to \infty} \sqrt[n]{1/|a_n|} > |x - x_0|
\]
and analogous considerations (to those using the ratio test) are given for the values of $R$. 
Additionally, recall that n'th root test gives more information than the ratio test,
hence in general, the radius of convergence using the n'th root test would be wider.
