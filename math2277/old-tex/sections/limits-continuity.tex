\section{Limits and Continuity}

\subsection{Limits}
Let $A \subseteq \mathbb{R}$, $f\colon A \to \mathbb{R}$ and suppose that $a \in A'$.
Then, we say that $f(x)$ approaches the limit $L \in \mathbb{R}$ as $x$ approaches $a$
iff
\begin{align*}
    \forall \varepsilon > 0: \exists \delta > 0: \forall x \in A: 0 < |x-a| < \delta \implies |f(x) - L| < \varepsilon
\end{align*}
and is written symbolically as $\lim_{x \to a} f(x) = L$. 
Additionally, since $|(f(x) - L) - 0|=|f(x) - L| = ||f(x) - L| - 0|$, we see that 
\[
    \lim_{x\to a} f(x) = L \iff \lim_{x \to a} f(x) - L = 0 \iff \lim_{x \to a} |f(x) - L| = 0
\]
Note, that the above
definition could be written equivalently as
\[
    \lim_{x \to a} f(x) = L \iff 
    \forall \varepsilon >0: \exists \delta > 0: \forall x \in N^*(a, \delta): f(x) \in N(L, \varepsilon)
\]
which immediately reveals why we require that $a$ be a limit point of $A$.



\subsubsection{Uniqueness of the limit}
Let $A \subseteq \mathbb{R}$ and $f\colon A \to \mathbb{R}$ and $a \in A'$, then
\[
    \lim_{x \to a} f(x) = L_1 \text{ and } \lim_{x \to a} f(x) = L_2 
    \implies L_1 = L_2
\]
or in other words, the limit is well defined or the limit is unique if it exists.
\begin{proofi}
    Suppose $\lim_{x \to a} f(x) = L_1$ and $\lim_{x \to a} f(x) = L_2$ and consider
    $\varepsilon > 0$, then since $\frac{\varepsilon}{2} > 0$, $\exists \delta_1, \delta_2$
    such that 
    \[
        x \in N^*(x, \delta_1) \implies |f(x) -L_1| < \tfrac{\varepsilon}{2}
    \]
    \[
        x \in N^*(x, \delta_2) \implies |f(x) -L_2| < \tfrac{\varepsilon}{2}
    \]
    Then, by letting $\delta = \min(\delta_1, \delta_2)$ we get that $N^*(x, \delta) \subseteq N^*(x, \delta_1)$
    and $N^*(x, \delta_2)$ hence
    \[
        x \in N^*(\delta) 
        \implies |L_1 - L_2| 
            \leq |f(x) - L_1| + |f(x) - L_2| 
            < \tfrac{\varepsilon}{2} + \tfrac{\varepsilon}{2} = \varepsilon
    \]
    Then, since $\varepsilon$ was arbitrary, $L_1 - L_2 = 0$ and we are done.
\end{proofi}
\subsubsection{Nice properties}
Let $A \subseteq \mathbb{R}$ and $f, g\colon A \to \mathbb{R}$ and $a \in A'$ such that 
\[
    \lim_{x \to a} f(x) = L \text{ and } \lim_{x\to a} g(x) = M
\]
then
\begin{enumerate}[label=\roman*)]
    \begin{multicols}{2}
        \item $\lim_{x \to a} (f(x) + g(x)) = L + M$
        \item $\lim_{x \to a} kf(x) = kL$ where $k \in \mathbb{R}$
        \item $\lim_{x \to a} f(x)g(x) = LM$
        \item $\lim_{x \to a} \tfrac{1}{f(x)} = \tfrac{1}{L}$ if $L \neq 0$
    \end{multicols}
\end{enumerate}
\begin{proofi}
    The proof of the first three are omitted as I refuse to rewrite a proof I did numerous times in Math1151 (calc 2).

    For the forth one one, consider $\epsilon > 0$, 
    Notice that since $L \neq 0$, $\tfrac{|L|}{2} > 0$ and then $\exists \delta_1 > 0$ such that 
    \[
        \forall x \in A: x \in N^*(a, \delta_1) 
        \implies f(x) \in N\paren{L, \frac{|L|}{2}}
        \implies |f(x)| > \frac{|L|}{2} > 0
        \implies \frac{1}{|f(x)|} < \frac{2}{|L|}
    \]
    also, since $\frac{|L|^2\epsilon}{2} > 0$, $\exists \delta_2 > 0$ such that
    \[
        \forall x \in A: x \in N^*(a, \delta_1)
        \implies |f(x) - f(a)| < \frac{|L|^2\epsilon}{2}
    \]
    now, let $\delta = \min(\delta_1, \delta_2) > 0$ and
    \[
        \forall x \in A: x \in N^*(a, \delta) \implies 
        \abs{\frac{1}{(f(a))} - \frac{1}{L}} 
        = \frac{|f(x) - L|}{|f(x)| |L|}
        < \paren{\frac{|L|^2\epsilon}{2}}\paren{\frac{2}{|L|}} \paren{\frac{1}{|L|}}
        = \epsilon
    \]
    and we are done.
\end{proofi}


\subsubsection{Preservation of inequalities}
Let $A \subseteq \mathbb{R}$ and $f, g\colon A \to \mathbb{R}$ and $a \in A'$. Then
if $f(x) \leq g(x)$ $\forall x \in A$ and limits of $f$ and $g$ both exists at $a$,
\[
    \lim_{x \to a} f(x) \leq \lim_{x \to a} g(x)
\]
That is, the limit preserves inequalities. 

\paragraph*{Note} strict inequalities are not preserved as equality may occur. For
    example consider $f(x) = \frac{x}{2}$ and $g(x) = x$ on $(0, \infty)$ with limit point $0$.
\begin{proofi}
    Instead of proving this fact directly, we would consider a function $h:A \to \mathbb{R}$
    such that $h(x) \geq 0\ \forall x \in A$ and $\lim_{x \to a} f(x)$ exists and is equal to $L$.
    Now, if $L < 0$, we get that 
    \[
        \exists \varepsilon = -L > 0: \forall \delta > 0: \exists x \in N^*(x, \delta): |f(x) -L| = f(x) - L \geq -L =\varepsilon
    \]
    Note that the above statement is the negation of the limit definition and hence 
    $L \not< 0$.

    Now, back to our initial statement, if $f(x) \leq g(x)$, then $g(x) - f(x) \geq 0$.
    It then follows that 
    \[
        \lim_{x \to a}(g(x) - f(x)) \geq 0 \implies \lim_{x \to a} f(x) \leq \lim_{x \to a} g(x)
    \]
    and we are done.
\end{proofi}
\subsubsection{Squeeze/Sandwich Theorem}
Let $A \subseteq \mathbb{R}$ and $f, g, h\colon A \to \mathbb{R}$ and $a \in A'$.
Then, if $f(x) \leq g(x) \leq h(x)\ \forall x \in A$ and 
$\lim_{x \to a} f(x) = \lim_{x\to a} h(x) = L$ then 
\[
    \lim_{x \to a} g(x) = L
\]
\begin{proofi}
    Despite all of its hype, its proof is quite simple. 
    \begin{align*}
        &\lim_{x \to a} f(x) = \lim_{x\to a} h(x) = L\\
        &\implies \forall \varepsilon > 0: \exists \delta_1, \delta_2 > 0: 
            \brack{\forall x \in N^*(x, \delta_1): f(x) \in N(L, \varepsilon)}
            \ \land \
            \brack{\forall x \in N^*(x, \delta_2): h(x) \in N(L, \varepsilon)}
            \\
        &\implies \forall \varepsilon > 0: \exists \delta = \min(\delta_1, \delta_2) > 0: 
            \brack{\forall x \in N^*(x, \delta): f(x), h(x) \in N(L, \varepsilon)}
            \\
        &\implies \forall \varepsilon > 0: \exists \delta = \min(\delta_1, \delta_2) > 0: 
            \forall x \in N^*(x, \delta): g(x) \in N(L, \varepsilon)
    \end{align*}
    since $N(L, \varepsilon)$ is an interval and hence connected.
\end{proofi}

\subsubsection{Nice theorem}
Let $A \subseteq \mathbb{R}$ and $f, g \colon A \to \mathbb{R}$ and $a \in A'$.
Then, if $f$ is bounded and $\lim_{x\to a} g(x) = 0$, $\lim_{x\to a} f(x)g(x) = 0$.

\begin{proofi}
    Since $f$ is bounded, $\exists M > 0$ such that $-M$ is a lower bound and $M$
    is an upper bound for $f(A)$. Furthermore, $M \geq |f(x)| \ \forall x \in A$.
    
    Now, let $\varepsilon > 0$, then $\frac{\varepsilon}{M} > 0$ and $\exists \delta > 0$
    such that 
    \[
        \forall x \in A: x \in N^*(a, \delta) 
        \implies |g(x)| < \frac{\varepsilon}{M}
        \implies |f(x)g(x)| \leq M|g(x)| < \varepsilon
    \]
    and we are done.
\end{proofi}

\subsection{Left and Right sided limits}
Let $A \subseteq \mathbb{R}$ and $f\colon A \to \mathbb{R}$. Then, we define the left
and right hand limits as follows.
\begin{itemize}
    \item Let $a \in (A \cap (-\infty, a))'$ then $L \in \mathbb{R}$ is the \emph{left-hand limit}
            of $f$ at $a$ iff
            \[
                \forall \varepsilon > 0: \exists \varepsilon > 0:  \forall x \in A: -\delta < x-a < 0 \implies |f(x) - L| < \varepsilon
            \]
            and we write $f(x-) = \lim_{x\to a^-} f(x) = L$
    \item Let $a \in (A \cap (a, \infty))'$ then $L \in \mathbb{R}$ is the \emph{right-hand limit}
            of $f$ at $a$ iff
            \[
                \forall \varepsilon > 0: \exists \varepsilon > 0:  \forall x \in A: 0 < x -a < \delta \implies |f(x) - L| < \varepsilon
            \]
            and we write $f(x+) = \lim_{x\to a^+} f(x) = L$
\end{itemize}
Note, both of these one-sided limits could be considered be considered as limits of 
the function $f$ with a constrained domain. That is, if we let
$g\colon (A \cap (-\infty, a))\to \mathbb{R}$ and $h\colon (A\cap (a, \infty)) \to \mathbb{R}$
\[
    \lim_{x \to a} g(x) = f(a-) 
    \quad\text{and}\quad
    \lim_{x \to a} h(x) = f(a+) 
\]
granted that they exist. As a result, all the properties for standard limits also hold for one-sided limits.

\subsubsection{Relation to general limit}
Let $A \subseteq \mathbb{R}$ and $f\colon A \to \mathbb{R}$ and $a \in (A \cap (a, \infty))' \cap (A \cap (-\infty, a))'$.
Then
\[
    \lim_{x \to a}f (x) = L \iff f(a+) = f(a-) = L
\]
\begin{proofi}
    The forward direction is quite simple. Let $\varepsilon > 0$ then, $\exists \delta > 0$
    such that
    \begin{align*}
        &\forall x \in A: x \in N^*(a, \delta) \implies |f(x) - L| < \varepsilon\\
        &\implies \forall x \in A: (- \delta < x-a < 0 \ \lor \ 0 < x-a < \delta) \implies |f(x) - L| < \varepsilon\\
        &\implies \forall x \in A: \brack{ - \delta < x-a < 0 \implies |f(x) - L| < \varepsilon} \ \land \ \brack{0 < x-a < \delta \implies |f(x) - L| < \varepsilon}\\
        &\implies \brack{\forall x \in A:  - \delta < x-a < 0 \implies |f(x) - L| < \varepsilon}\\
        &\quad\quad\ \land \  \brack{\forall x \in A: 0 < x-a < \delta \implies |f(x) - L| < \varepsilon}
    \end{align*}
    and hence $f(a+) = f(a-) = L$.

    Now, consider the reverse direction. Let $\varepsilon > 0$ then $\exists \delta_1, \delta_2 > 0$
    such that
    \[
        \forall x \in A: - \delta_1 < x-a < 0 \implies |f(x) - L| < \varepsilon
    \]
    and
    \[
        \forall x \in A: 0 < x-a < \delta_2 \implies |f(x) - L| < \varepsilon
    \]
    Then, let $\delta = \min(\delta_1, \delta_2)$ and consider arbitrary $x \in A$ then
    \begin{align*}
        x \in N^*(a, \delta) 
        &\implies 0 < x -a < \delta \ \lor \ -\delta < x-a < 0\\
        &\implies 0 < x -a < \delta_1 \ \lor \ -\delta_2 < x-a < 0\\
        &\implies |f(x) - L|< \varepsilon \ \lor \ |f(x) - L| < \varepsilon\\
        &\implies |f(x) - L|< \varepsilon
    \end{align*}
    and hence $\lim_{x \to a} f(x) = L$.
\end{proofi}

\subsection{Continuity}
Let $A \subseteq \mathbb{R}$ and $f\colon A \to \mathbb{R}$ and $a \in A$. Then $f$ is \emph{continuous}
at $a$  if 
\[
    \forall \varepsilon > 0: \exists \delta > 0: \forall x \in A: x \in N(a, \delta) \implies f(x) \in N(f(a), \varepsilon)
\]
Note that the $a$ need not be a limit point, it can even be a isolated.

\subsubsection{Isolated points and relation to limits}
Let $A \subseteq \mathbb{R}$ and $f\colon A \to \mathbb{R}$ and $a \in A$.
Then, we have one of the following
\begin{enumerate}[label=\roman*)]
    \item If $a$ is an isolated point, $f$ is immediately continuous at $a$
    \item If $a \in A'$, $f$ is continuous at $a$ iff $\lim_{x\to a} f(x) = f(a)$
\end{enumerate}
\begin{proofi}
    Each part will be proven separately
    \paragraph*{Part (i)} If $a$ is an isolated point, $\exists \delta > 0$ 
    such that $\forall x \in A: x \notin N^*(a, \delta)$ and hence
    \[
        \forall x \in A: x \in N(a, \delta) \implies x = a \implies |f(x) - f(a)| = 0 < \varepsilon,\ \forall \varepsilon > 0
    \]
    \paragraph*{Part (ii)}Let $a \in A'$
    \begin{align*}
        &f \text{ is continuous at } a\\
        &\iff \forall \varepsilon > 0: \exists \delta > 0: x \in N(a, \delta) \implies f(x) \in N(f(a), \varepsilon)\\
        &\iff \forall \varepsilon > 0: \exists \delta > 0: x \in N^*(a, \delta) \implies f(x) \in N(f(a), \varepsilon)\\
        &\iff \lim_{x \to a} f(x) = f(a)
    \end{align*}
    Note that the second reverse implication is true since $x = a \implies f(x) = f(a) \in N(f(a), \varepsilon)$.
\end{proofi}

\subsubsection{Nice properties}
Let $A \subseteq \mathbb{R}$ and $f, g\colon A \to \mathbb{R}$ and $a \in A$.
If $f$ and $g$ are continuous at $a$, then
\begin{multicols}{2}
\begin{enumerate}[label=\roman*)]
    \item $f+ g$ is continuous at $a$.
    \item $kf$ is continuous at $a$ where $k \in \mathbb{R}$.
    \item $fg$ (multiplication) is continuous at $a$.
    \item $\frac{f}{g}$ is continuous at $a$ if $g(a) \neq 0$.
\end{enumerate}
\end{multicols}
\begin{proofi}
    The proof of these are omitted because I refuse to rewrite proofs I did numerous times in Math1151 (calc 2).
    Also, the proof if these properties are very similar to the analogous properties of limits.
\end{proofi}

\subsubsection{Polynomials and Rational functions}
A \emph{rational function} is a function $R$ which can be written as the ratio
of two polynomial functions. That is, if $P$ and $Q$ are polynomials, then
we define the rational function $R$ as
\[
    R(x) = \frac{P(x)}{Q(x)}
\]
with domain being the set of points where $Q(x) \neq 0$. From this 
definition, it is clear that polynomial functions are also rationals
with $Q$ being the constant function $1$.

Then, we have the following theorems
\begin{enumerate}[label=\roman*)]
    \item Every polynomial is continuous on $\mathbb{R}$.
    \item Every rational function is continuous on its domain.
\end{enumerate}
\begin{proofi}
    Firstly, note that the constant function $f(x) = 1$ and $f(x) = x$ are both 
    continuous on $\mathbb{R}$. Then, the result follows from the nice properties above.
\end{proofi}

\subsubsection{Composition of functions}
Let $A, B \subseteq \mathbb{R}$ and $f\colon A\to \mathbb{R}$ and
$g\colon B \to \mathbb{R}$ and $f(A) \subseteq B$.
Then, if $f$ is continuous at $a \in A$ and $g$ is continuous at
$f(a) \in B$, then $g \circ f\colon A\to \mathbb{R}$ is continuous 
at $a$.

\begin{proofi}
    Consider $\epsilon > 0$, then $\exists \delta_1 > 0$ such that 
    \[
        \forall y \in B: y \in N(f(a), \delta_1) \implies g(y) \in N(g(f(a)), \epsilon)
    \]
    and since $\delta_1 > 0$, $\exists \delta > 0$ such that 
    \[
        \forall x \in A: x \in N(a, \delta) \implies f(x) \in N(f(a), \delta_1) \implies g(f(x)) \in N(g(f(a)), \epsilon)
    \]
\end{proofi}

\subsubsection{Equivalent definition for continuity}
Let $A \subseteq \mathbb{R}$ and $f\colon A \to \mathbb{R}$. Then
$f$ is continuous iff $\forall$ open sets $G \subseteq \mathbb{R}$,
$\exists$ an open set $H$ such that $H \cap A = f^{-1}(G)$.

As a result, if $A$ is a open set, the pre-image of any open set $G$,
with respect to $f$, is also open.

\begin{proofi}
    Firstly consider an arbitrary $a \in f^{-1}(G)$. Then since $G$
    is open and $f(a) \in G$, $\exists \varepsilon_a > 0$ such that 
    $N(f(a) , \varepsilon_a) \subseteq G$. Next since $G$ is continuous,
    $\exists \delta_a > 0$ such that $f(A \cap N(a, \delta_a)) \subseteq N(f(a), \varepsilon_a) \subseteq G$.
    We now define the following family
    \[
        \mathcal{A} = \cbrack{N(a, \delta_a): a \in f^{-1}(G)}
    \]
    and $H = \bigcup_{a \in f^{-1}(G)} N(a, \delta_a)$. Then, since each neighbourhood is open,
    $H$ is also open. Also, note that $f^{-1}(G) \subseteq H \cap A$ and
    \[
        H \cap A = \bigcup_{a \in f^{-1}(G)} A \cap N(a, \delta_a) \subseteq f^{-1}(G)
    \]
    since the image of each $A \cap N(a, \delta_a)$ is a subset of $G$.
    Therefore, we have found a open set $H$ satisfying $H \cap A = f^{-1}(G)$.

    Conversely, consider an arbitrary $a \in A$ and $\varepsilon > 0$ 
    and $G = N(f(a), \varepsilon)$. Then $\exists H \subseteq \mathbb{R}$
    that is open and $H \cap A = f^{-1}(G)$. Then notice that $a \in H$
    since $a \in f^{-1}(G)$ and hence $\exists \delta> 0$ such that 
    $N(a, \delta) \subseteq H$ and hence
    \[
        \forall x \in A: x \in N(a,\delta) \implies x \in H \implies f(x) \in G = N(f(a), \varepsilon)
    \]
    and we are done.
\end{proofi}

\subsection{Uniform continuity}
Let $A \subseteq \mathbb{R}$ and $f \colon A \to \mathbb{R}$.
Then $f$ is \emph{uniformly continuous} if 
\[
    \forall \varepsilon > 0: \exists \delta> 0: \forall x, y \in A: |x - y| < \delta \implies |f(x) - f(y)| < \varepsilon
\]
Note that uniform continuity implies continuity by fixing $y$ however, the converse of this statement is
not necessarily true.

\subsubsection{Compact sets}
Let $A \subseteq \mathbb{R}$ and $f\colon A\to \mathbb{R}$.
Then if $A$ is compact and $f$ is continuous, $f$ is uniformly 
continuous on $A$.
\begin{proofi}
    Look at Theorem 4.6 in Lay's book.
\end{proofi}

\subsection{Discontinuities}
Let $A \subseteq \mathbb{R}$ and $f\colon A\to \mathbb{R}$ and
$a \in A$. Then, if $f$ is not continuous at $a$, it is said that 
$f$ is \emph{discontinuous} at $a$ or that $f$ has a \emph{discontinuity} at $a$.
Note that in this case $a \in A'$, as $f$ is continuous at all isolated points,
and hence we can speak of the limit of $f$ as $x \to a$. 

Then, since $a \in A'$ and $f$ is continuous iff $\lim_{x \to a} f(x) = f(a)$, we
classify discontinuities as follows
\begin{itemize}
    \item Removable discontinuity: $\lim_{x \to a} f(x)$ exists but is not equal to $f(a)$.
    \item Essential discontinuity: $\lim_{x\to a} f(x)$ does not exist. In this case, if $a$ is
            approachable from both the left and right, there are a further two possibilities
        \begin{itemize}
            \item Jump discontinuity: both $f(a-)$ and $f(a+)$ exist but are not equal.
            \item Essential discontinuity?: one of $f(a-)$ of $f(a+)$ does not exist.
        \end{itemize}
\end{itemize}

\subsection{Monotonicity}
Let $A \subseteq \mathbb{R}$ and $f\colon A\to \mathbb{R}$, then
$f$ is
\begin{enumerate}
    \item \emph{(monotonically) increasing} if 
        \[
            \forall x,y \in A: x < y \implies f(x) \leq f(y)
        \]
        further, $f$ is \emph{strictly increasing} if 
        \[
            \forall x,y \in A: x < y \implies f(x) < f(y)
        \]
    \item \emph{(monotonically) decreasing} if 
        \[
            \forall x,y \in A: x < y \implies f(x) \geq f(y)
        \]
        further, $f$ is \emph{strictly decreasing} if 
        \[
            \forall x,y \in A: x < y \implies f(x) > f(y)
        \]
\end{enumerate}
Furthermore, if $f$ is either increasing or decreasing, its called a \emph{monotonic (or monotone)} function
and if $f$ is either strictly increasing or strictly decreasing, its called a 
\emph{strictly monotonic (or strictly monotone)} function.

\subsubsection{Limits at interior points}
Let $A \subseteq \mathbb{R}$ and $f\colon A\to \mathbb{R}$ be a monotonic
function and $a \in \mathbb{R}$. Then if $a \in (A\cap (-\infty, a))'$, $f(a+)$ exists
and if $a \cap (A \cap (a, \infty))$ and $f(a+)$ exists. Specifically
\begin{itemize}
    \item If $f$ is increasing,
        \[ f(a-) = \sup\{f(x) \in \mathbb{R}: x \in A \text{ and } x < a\} = \sup(f(A \cap (-\infty, a))) \]
        and
        \[ f(a+) = \inf\{f(x) \in \mathbb{R}: x \in A \text{ and } x > a\} = \inf(f(A \cap (a, \infty))) \]
    \item If $f$ is decreasing,
        \[ f(a-) = \inf\{f(x) \in \mathbb{R}: x \in A \text{ and } x < a\} = \inf(f(A \cap (-\infty, a))) \]
        and
        \[ f(a+) = \sup\{f(x) \in \mathbb{R}: x \in A \text{ and } x > a\} = \sup(f(A \cap (a, \infty))) \]
\end{itemize}
This immediately implies that if $f$ is discontinuous at $a \in A$,
it must a jump discontinuity and $a$ must be approachable from both the left and right.
\begin{proofi}
    We will only prove the result when $a \in (A\cap (-\infty, a))'$ and $f$ is increasing
    as the other cases are quite similar. Also, note that this proof assumes that $\sup(f(A \cap (-\infty, a)))$
    or more specifically, $f(A\cap (-\infty, a))$ has an upper bound
    which can be confirmed if $(A \cap [a,\infty)) \neq \varnothing$.

    Firstly, let $u = \sup(f(A \cap (-\infty, a)))$ and consider arbitrary $\varepsilon > 0$, then
    by the characterization of supremum, $\exists f(x_1) \in f(A \cap (-\infty, a))$ such 
    that $f(x) > u - \varepsilon$. We then let $\delta = a - x_1 > 0$ since $x_1 \in (-\infty, a)$.
    Now, by the monotonicity of $f$ and since $u$ is an upper bound for $f(A \cap (-\infty, a))$,
    \begin{align*}
        \forall x \in (A \cap (-\infty, a)): 
        x > x_1 
        &\implies u \geq f(x) \geq f(x_1) > u - \varepsilon\\
        &\implies 0 \geq f(x) -u > - \varepsilon\\
        &\implies |f(x) - u| < \varepsilon
    \end{align*}
    Furthermore,
    \[
        \forall x \in A: -\delta = x_1 - a < x-a < 0 \implies |f(x) - u| < \varepsilon
    \]
    and we are done since the choice of $\varepsilon$ was arbitrary.
\end{proofi}

\subsubsection{Countability of discontinuities}
Let $A \subseteq \mathbb{R}$ and $f\colon A\to \mathbb{R}$ be a monotonic
function. Then, $f$ has countably many discontinuities.

\begin{proofi}
    We will prove that the number of discontinuities in $(a, b) \cap A$ is 
    countable and it would follow that the total number of discontinuities in $A$
    is countable since $A$ can be written as the union of countably many 
    such sets $(a, b) \cap A$.
\end{proofi} 