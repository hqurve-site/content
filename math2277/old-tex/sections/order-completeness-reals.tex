\section{Order completeness of reals}
\subsection{Fields}
A set $\mathbb{F}$ together with two binary operations $+$ and $\cdot$
on $\mathbb{F}$ form a field $(\mathbb{F}, + , \cdot)$
if the following properties hold
\begin{enumerate}[label=\roman*)]
    \newcommand{\field}{\mathbb{F}}
    \item $\forall x, y \in \field: x + y \in \field$ and $x \cdot y \in \field$ (closure of $\field$ under $+$ and $\cdot$).
    \item $+$ and $\cdot$ are both commutative. That is $\forall x, y \in \field: x + y = y + x$ and $x \cdot y = y \cdot x$.
    \item $+$ and $\cdot$ are both associative. That is $\forall x, y, z \in \field: (x+y)+z = x+(y+z)$ and $(x\cdot y)\cdot z = x \cdot (y \cdot z)$.
    \item $\cdot$ distributes over $+$. That is $\forall x, y, z \in \field: x \cdot(x + z) = (x \cdot y) + (x \cdot z)$.
    \item $\exists\, 0, 1 \in \field$ which are the identities under $+$ and $\cdot$ (respectively) in $\field$.
    \item $\forall x \in \field: \exists (-x) \in \field: x + (-x) = 0$.
    \item $\forall x \in \field - \{0\}: \exists x^{-1} \in \field: x \cdot x^{-1} = 1$.
\end{enumerate}
The above definition can be summarized as follows
\begin{itemize}
    \item $(\mathbb{F}, +)$ forms an abelian group where $0$ is identity element.
    \item $(\mathbb{F} - \{0\}, \cdot)$ forms an abelian group where $1$ is identity element.
    \item $\cdot$ distributes over $+$.
\end{itemize}
Note however, that the second point requires proof of closure of $\cdot$ over $\mathbb{F} - \{0\}$ (this
is proven below).

\subsubsection{Convenience convensions}
Note that for convenience we use the following conventions
% \begin{multicols}{2}
    \begin{itemize}
        \item $\mathbb{F}$ used in place of $(\mathbb{F}, + , \cdot)$ when the operations are understood.
        \item In general, we call $+$ field addition and $\cdot$ field multiplication even if they are not the standard operations.
        \item Let $n \in \mathbb{N}$. Then, we write/define
        \begin{multicols}{2}
            \begin{itemize}[label=$\ast$]
                \item $x-y = x + (-y)$.
                \item $xy = x\cdot y$.
                \item $\frac{x}{y} = x\cdot y^{-1}$ where $y \neq 0$.
                \item $(n+1)x = nx + x$
                \item $n = n(1)$. That is, the naturals are imbedded in the field.
                \item $(-n)x = n(-x)$
                \item $x^{n+1} = x^nx$
                \item $x^0 = 1$ where $x \neq 0$
                \item $x^{-n} = (x^{-1})^n$ where $x \neq 0$
            \end{itemize}    
        \end{multicols}
        
    \end{itemize}
% \end{multicols}

\subsubsection{Immediate results}
Let $x,y , z\in \mathbb{F}$, then
\begin{enumerate}[label=\roman*)]
    \item $x = y \implies x + z = y+ z$
    \item $x = y \implies xz = yz$
    \item $0x = 0$
    \item $xy = 0 \implies x = 0 \text{ or } y =0$
    \item $-x = (-1)x$
    \item $(-x)(-y) = xy$
\end{enumerate}
\paragraph*{Note: } I do not know how to prove the first two facts (or if it is even possible).

\begin{proofi}Regardless, here is a proof of the other ones
    \paragraph*{Part (iii)} Since $0$ is the additive identity
        \[
            0 = 0+ 0 \implies 0x = (0+0)x = 0x +0x
        \]
        Then by the uniqueness of the identity in the group $(\mathbb{F}, +)$,
        $0 = 0x$.
    \paragraph*{Part (iv)} Assume BWOC that $xy = 0$ and $x \neq 0$ and $y \neq 0$. Then,
        \[
            1 
            = \paren{x \cdot \frac{1}{x}}\paren{y \cdot \frac{1}{y}}
            = \paren{xy}\paren{ \frac{1}{x} \cdot \frac{1}{y}}
            = 0\paren{ \frac{1}{x} \cdot \frac{1}{y}}
            = 0
        \]
        This is clearly not true since it would imply that $1x = 0x = 0 \neq x$. Note: this
        actually is okay only when $\mathbb{F}$ is the trivial field $\{0\}$ but the result
        still holds.
    \paragraph*{Part (v)}
        \[
            x + (-1)x = 1x + (-1)x = (1-1)x = 0x = 0
        \]
        Then by the uniqueness of the inverse in the group $(\mathbb{F}, +)$, $(-1)x = -x$.
    \paragraph*{Part (vi)}
        By part (v) and the associativity and commutativity of $\cdot$ in $\mathbb{F}$ all
            we need to show is that $(-1)(-1) = 1$. However, again by part (v)
            and since the inverse of the inverse is the element in a group (specifically $(\mathbb{F}, +)$)
            \[
                (-1)(-1) = -(-1) = 1
            \]
\end{proofi}

\subsection{Ordered field}
A field $\mathbb{F}$ is called an ordered field with respect to the relation $<$
if $\forall x, y, z \in \mathbb{F}$, the following hold
\begin{enumerate}[label=\roman*)]
    \item Law of trichotomy: exactly one of the following hold.
        \[
            x < y, \quad x =y, \quad y < x
        \]
    \item Transitivity of $<$
    \item Monotonicity of addition: $x < y \implies x + z < y +z $
    \item Monotonicity of multiplication: $x < y$ and $0 < z$ $\implies xz < yz$
\end{enumerate}

\subsubsection{Convenience convensions}
Note that for convenience we use the following conventions
\begin{itemize}
    \item $x < y$ is equivalent to $y > x$.
    \item We say $x$ is \emph{negative} if $x < 0$ and \emph{positive} if $x > 0$. 
            Similar conventions hold for `greater than' and `less than'.
    \item We write $x$ is \emph{greater than or equal} to $y$ as $x \geq y$. Again, a 
            similar convention is used for `less than or equal to'.
    \item We say that $x$ is \emph{nonnegative} if $x \geq 0$.
\end{itemize}


\subsubsection{Immediate results}
Let $w, x,y , z\in \mathbb{F}$, then
\begin{enumerate}[label=\roman*)]
    \item $w < x$ and $y < z$ $\implies w + y < x + z$.
    \item $x >0$, $w < x$ and $0 < y < z$ $\implies wy < xz$.
    \item $x < y$ and $z < 0$ $\implies xz > yz$.
    \item $x \neq 0$ $\implies$ $x^2 > 0$. In particular $1 > 0$.
    \item $n \in \mathbb{N} \implies n(1) > 0$
    \item $0 < x < y$ $\implies$ $0 < \frac{1}{y} < \frac{1}{x}$.
    \item $xy > 0$ $\implies $ either $x$ and $y$  are both positive or both negative.
\end{enumerate}

\begin{proofi}This is mostly a sketch of the proof.
    \paragraph*{Part (i)} Since $w < x$, then $w + y < x + y$ and since $y < z$, $x+y < x + z$.
        The result follows by transitivity.
    \paragraph*{Part (ii)}
        Since $y > 0$, $wy < xy$ and since $x > 0$, $xy < yz$.
        The result follows by transitivity.
    \paragraph*{Part (iii)}
        Since $z < 0$, $0 = z-z < -z$ and hence $-zx < -zy$ and hence the result follows.
    \paragraph*{Part (iv)}
        There are two cases. When $x > 0$, the result follows by part (ii). Otherwise, when 
        $x < 0$ then by part (iii), $xx = x^2 > 0$.
    \paragraph*{Part (v)}
        Since $1 = 1^2$, $1 > 0$. Also, if $n > 0$, $n+1 > 0+ 1 > 0$. Then the result follows inductively.
    \paragraph*{Part (vi)}
        Since $x > 0$, $\frac{1}{x} > 0$ (otherwise $1 = x\frac{1}{x} < 0$) and similarly
        $\frac{1}{y} > 0$. Therefore, $1 = x\frac{1}{x} < \frac{y}{x}$ which 
        implies that $\frac{1}{y} < \frac{1}{y}\frac{y}{x} = \frac{1}{x}$.
    \paragraph*{Part (vii)}
        Note that if $x$ or $y$ are zero, $xy = 0$. So, consider $x > 0$,
        then $y$ must be greater than $0$ otherwise $xy < 0$. On the other hand,
        if $x < 0$, then $y < 0$ otherwise $xy < 0$.
\end{proofi}

\subsubsection{Equality with zero}
Let $\mathbb{F}$ be an ordered field and $x \in \mathbb{F}$. Then if $x \geq 0$
and $x \leq \varepsilon$ $\forall \epsilon > 0$, $x = 0$.

\begin{proofi}
    BWOC, suppose that $x > 0$. Then, since $2>0$, $\frac{1}{2} > 0$ and $\epsilon = \frac{x}{2} >0$.
    However, $2x > x$ implies that $\epsilon = \frac{x}{2} < x$. This is a contradiction and the result holds.
\end{proofi}

\subsection{Bounds}
Let $A \subset \mathbb{F}$ where $A \neq \varnothing$. Then
\begin{itemize}
    \item $u \in \mathbb{F}$ is called an \emph{upper bound} of $A$ if $u \geq a$ $\forall a \in A$. If such a $u$ exists
            then $A$ is said to be \emph{bounded above}.
    \item $l \in \mathbb{F}$ is called a \emph{lower bound} of $A$ if $l \leq a$ $\forall a \in A$. If such a $l$ exists
            then $A$ is said to be \emph{bounded below}.
    \item $A$ is said to be \emph{bounded} if it is both bounded above and bounded below.
\end{itemize}

\subsubsection{Supremum and Infimum}
\begin{itemize}
    \item An element $u \in \mathbb{F}$ is called the \emph{supremum} (or least upper bound) of $A$ if $u$
        is an upper bound of $A$ and $u \leq u'$ forall other upper bounds $u'$ of $A$.
        In this case we write $u = \sup A$. 
    \item An element $l \in \mathbb{F}$ is called the \emph{infimum} (or greatest lower bound) of $A$ if $l$
        is an upper bound of $A$ and $l \geq l'$ forall other upper bounds $l'$ of $A$.
        In this case we write $l = \inf A$. 
\end{itemize}
Then, we have the following theorem
\[
    l = \inf A \iff -l = \sup (-A)\quad\text{where } -A = \{-a: a \in A\}
\]
\begin{proofi}
    For the sake of brevity, we will let $UB(X)$ and $LB(X)$ be the set 
    of upper and lower bounds of $X$ (respectively). Then
    \[
        LB(X) = - UB(-X)
    \]
    This is true since
    \begin{align*}
        l \in LB(X)
        &\iff \forall x \in X: x \geq l\\
        &\iff \forall x \in X: -x \leq -l\\
        &\iff \forall x \in (-X): x \leq -l\\
        &\iff -l \in UB(-X)\\
        &\iff l \in - UB(-X)
    \end{align*}
    Additionally, notice that
    \begin{align*}
        &\forall\, l' \in LB(X): l \geq l'\\
        &\iff \forall\, l' \in -UB(-X): l \geq l'\\
        &\iff \forall\, -l' \in UB(-X): l \geq l'\\
        &\iff \forall\, l' \in UB(-X): l \geq -l'\\
        &\iff \forall\, l' \in UB(-X): -l \leq l'
    \end{align*}
    Therefore, by combining these two results
    \begin{align*}
        l = \sup A
        &\iff l \in LB(A) \;\land\; \forall\, l' \in LB(A): l \geq l'\\
        &\iff l \in -UB(-A) \;\land\; \forall\, l' \in UB(-A): -l \leq l'\\
        &\iff -l \in UB(-A) \;\land\; \forall\, u' \in UB(-A): -l \leq u'\\
        &\iff -l = \sup (-A)
    \end{align*}
\end{proofi}

\subsubsection{Characterization of supremum and infimum}
The following statements are equivalent
\begin{enumerate}[label=\roman*)]
    \item $u = \sup A$
    \item $u$ is an upper bound of $A$ and $\forall \varepsilon > 0: \exists x \in A: x > u-\varepsilon$
\end{enumerate}
And in a similar manner the following statements are equivalent
\begin{enumerate}[label=\roman*)]
    \setcounter{enumi}{2}
    \item $l = \inf A$
    \item $l$ is a lower bound of $A$ and $\forall \varepsilon > 0: \exists x \in A: x < u+\varepsilon$
\end{enumerate}
Because of the relationship between suprema and infima, $(iii)\iff (iv)$ follows from $(i) \iff (ii)$
as follows
\begin{align*}
    l = \inf A
    &\iff -l = \sup(-A)\\
    &\iff -l \in UB(-A) \text{ and }\forall \varepsilon > 0: \exists x \in (-A): x > -l-\varepsilon\\
    &\iff l \in LB(A) \text{ and } \forall \varepsilon > 0: \exists x \in A: -x > -l-\varepsilon\\
    &\iff l \in LB(A) \text{ and } \forall \varepsilon > 0: \exists x \in A: x < l +\varepsilon
\end{align*}
As for the first part
\begin{proofi}
    We will prove $(i) \implies (ii)$ by contrapositive. The contrapositive of $(ii)$ is
    \[
        u \text{ is not an upper bound of $A$ or} \exists \varepsilon > 0: \forall x \in A: x \leq u - \varepsilon
    \]
    Then, if $u$ is not an upper bound of $A$, it is clearly not the supremum of $A$. On the other hand,
    if $\exists \epsilon > 0: \forall x \in A: x \leq u - \varepsilon$, $u -\varepsilon$ is an upper
    bound of $A$ and since $u-\varepsilon < u$, $u$ is not the supremum of $A$. Therefore,
    $(i) \implies (ii)$.

    Now, we will prove that $(ii) \implies (i)$. Suppose that $u'$ is also an upper bound
    then, if $u' < u$, $u - u' > 0$. By letting $\varepsilon = u - u'$ we get that 
    $\exists x \in A$ such that $x > u - \varepsilon = u - (u-u') = u'$ and hence $u'$
    is not an upper bound. This is a contradiction and hence $u' \geq u$ and it follows
    that $u = \sup A$.

\end{proofi}

\subsubsection{Maxima and Minima}
Let $A \subseteq \mathbb{F}$ then
\begin{itemize}
    \item An element $u \in A$ is called the \emph{maximum} of $A$ if $u \geq a$ $\forall a \in A$.
            In this case we write $u = \max A$.
    \item An element $l \in A$ is called the \emph{minimum} of $A$ if $l \leq a$ $\forall a \in A$.
            In this case we write $l = \min A$.
\end{itemize}
Furthermore, the following two sets of equivalences hold
\[
    \max A \text{ exists}\iff \sup A = \max A \iff \sup A \in A
\]
\[
    \min A \text{ exists}\iff \inf A = \min A \iff \inf A \in A
\]
\begin{proofi}
    Proof omitted because obvious.
\end{proofi}

\subsubsection{Sum of suprema and infima}
Let $A, B \subseteq \mathbb{F}$ then
\begin{itemize}
    \item $\sup A, \sup B \in \mathbb{F} \implies \sup(A + B) = \sup A + \sup B$
    \item $\inf A, \inf B \in \mathbb{F} \implies \inf(A + B) = \inf A + \inf B$
\end{itemize}
\begin{proofi}
    Again, the second statement follows from the first since $\inf A = - \sup(-A)$.
    Now, notice that $\sup A + \sup B$ is an upper bound for $A + B$ since
    \[
        x \in A + B \implies x = a + b \leq \sup A + \sup B
    \]
    Now let $\varepsilon > 0$ then $\frac{\varepsilon}{2} > 0$ and
    \[
        a \in A  \implies a > \sup A - \frac{\varepsilon}{2} 
        \quad \text{and}\quad
        b \in B  \implies b > \sup B - \frac{\varepsilon}{2}
    \]
    then
    \[
        x \in A + B \implies 
        x = a + b  > 
            \sup A - \frac{\varepsilon}{2} + \sup A - \frac{\varepsilon}{2} 
            = (\sup A + \sup B) - \varepsilon
    \]
    and by the characterization by suprema, the result follows.
\end{proofi}

\subsubsection{Nice theorem}
Let $A, B \subseteq \mathbb{F}$ such that $\sup A$ and $\inf B$ exists in $\mathbb{F}$
Then
\[
    \forall a \in A: \forall b \in B: a \leq b
    \iff
    \sup A \leq \inf B
\]
\begin{proofi}
    We use $A - B = \{a -b : a \in A, b\in B\}$. Then
    \begin{align*}
        &\forall a \in A: \forall b \in B: a \leq B\\
        &\iff \forall a \in A: \forall b \in B: a -b \leq 0 \\
        &\iff \forall x \in (A -B): x \leq 0 \\
        &\iff 0 \text{ is an upper bound for } A -B\\
        &\iff \sup(A - B ) \leq 0\\
        &\iff \sup(A  + (- B) ) \leq 0\\
        &\iff \sup(A) + \sup (- B) \leq 0\\
        &\iff \sup(A) \leq -\sup (- B) = \inf(B)
    \end{align*}
\end{proofi}

\subsubsection{Additional nice results}
Let $x \in \mathbb{F}$ and $A \subseteq \mathbb{F}$ such that $\sup A$ and $\inf A$
exists in $\mathbb{F}$ (where applicable). Then
\begin{multicols}{2}
    \begin{enumerate}[label=\roman*)]
        \item $\sup(x + A) = x + \sup A$
        \item If $x > 0$, then $\sup(xA) = x \sup(A)$
        \item If $x < 0$, then $\sup(xA) = x \inf(A)$
        \item $\inf(x + A) = x + \inf A$
        \item If $x > 0$, then $\inf(xA) = x \inf(A)$
        \item If $x < 0$, then $\inf(xA) = x \sup(A)$
    \end{enumerate}
\end{multicols}
Note that $(i)$ and $(iv)$ follow from the sum of suprema and infima.
Additionally, $(iii)$, $(v)$ and $(vi)$ will follow from $(ii)$ using
the correspondence between infima and suprema.

\begin{proofi}
    As said above, only a proof of $(ii)$ will be given.

    Let $x > 0$ and $\sup A \in \mathbb{F}$ then immediately,
    $x\sup A$ is an upper bound for $\sup(xA)$ since
    \[
        \forall a \in A: a \leq \sup A
        \iff
        \forall a' = xa \in xA: a' = xa \leq x\sup A
    \]
    by the monotonicity of multiplication. Now, consider $\varepsilon > 0$
    then since $x > 0$, $\frac{\varepsilon}{x} > 0$. Then 
    by the characterization of suprema
    \[
        \frac{\varepsilon}{x} > 0 \implies \exists a \in A: a > \sup A - \frac{\varepsilon}{x}
    \]
    then $xa > x\sup A - \varepsilon$ and since $xa \in xA$
    \[
        \exists a' = xa \in xA: a' > x\sup A - \varepsilon
    \]
    and hence the result follows.

\end{proofi}

\subsection{Completeness}
An ordered field $\mathbb{F}$ is \emph{complete}
if every
\begin{itemize}
    \item $A \subseteq \mathbb{F}$ is bounded above $\implies \sup(A) \in \mathbb{F}$.
    \item $A \subseteq \mathbb{F}$ is bounded below $\implies \inf(A) \in \mathbb{F}$.
\end{itemize}
In, this section we will focus on the completeness of $\mathbb{R}$ which
is complete either by construction or axiomatically. In either case, the following 
theorem holds
\begin{center}
    \begin{minipage}{0.8\linewidth}
        There exists a unique ordered field $\mathbb{R}$ with the least upper bound
        property such that $\mathbb{Q} \subset \mathbb{R}$.
    \end{minipage}
\end{center}
The existence of this field is guaranteed by Dedekind cuts (one possible construction)
however for uniqueness, I do not know.

\subsubsection{Archimedean Property in Reals}
The set $\mathbb{N}$ of natural numbers is unbounded above in $\mathbb{R}$. The proof
of is straightforward by contradiction. Regardless the following statements are equivalent
\begin{enumerate}[label=\roman*)]
    \item The Archimedean property holds.
    \item $\forall z \in \mathbb{R}: \exists n \in \mathbb{N}: n > z$.
    \item $\forall x \in \mathbb{R}^+, y \in \mathbb{R}: \exists n \in \mathbb{N}: nx > y$.
    \item $\forall x \in \mathbb{R}^+: \exists n \in \mathbb{N}: 0 < \tfrac{1}{n} < x$.
\end{enumerate}
\begin{proofi}
    Note that $(i)$ is logically equivalent to $(ii)$ by the definition of upper bounds (or rather the negation).
    Part $(iii)$ follows from and implies $(ii)$ since $\tfrac{y}{x} \in \mathbb{R}$.
    Finally, $(iv)$ follows from $(iii)$ by using $y = 1$ and implies $(iv)$ since $\tfrac{1}{x} \in \mathbb{R}^+$
    and all non-positive, values of $z$ (iii) trivially holds.
\end{proofi}

\subsubsection{Denisty of the rationals in the reals}
If $x , y \in \mathbb{R}$ such that $x < y$, then $\exists r \in \mathbb{Q}$
such that $x < r < y$. It also follows that there are infinitely many such $r$.

\begin{proofi}
    The proof of this can be found under theorem 3.13 in Lay's book.
\end{proofi}

\paragraph*{Corollary} It follows that $\exists w \in \mathbb{Q}^c$ such that $x < w < y$
by letting $x\to \tfrac{x}{\sqrt{2}}$ and $y \to \tfrac{y}{\sqrt{2}}$ and taking $w = r\sqrt{2}$.
Therefore the irrationals are also dense in $\mathbb{R}$.