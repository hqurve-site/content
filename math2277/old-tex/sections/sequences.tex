\section{Sequences}

A sequence $\{x_n\}$ of real numbers is simply a function $f: \mathbb{N} \to \mathbb{R}$
where $f(n) = x_n$.

\subsection{Convergence}
Let $\{x_n\}$ be a sequence. Then $\{x_n\}$ \emph{converges} 
converges iff $\exists x \in \mathbb{N}$ such that
\[
    \forall \varepsilon > 0: \exists N \in \mathbb{N}:
    \forall n \in \mathbb{N}: n > N \implies |x_n - x| < \varepsilon
\]
Then, we call $x$ the \emph{limit of the sequence} $\{x_n\}$ and write
\[
    \lim_{n \to \infty} x_n = x
    \text{ or }
    x_n \to x \text{ as }  n \to \infty
\]
If $\{x_n\}$ does not converge, we say that it \emph{diverges}

\paragraph*{Note: } many of the proofs of properties that are also exhibited by function limits
are very similar to those for sequences. Hence, most proofs would be omitted due to their similarity
unless otherwise stated.

\subsubsection{Uniqueness of the limit}
Let $\{x_n\}$ be a sequence, then, if it converges, its limit is unique.

\subsubsection{Nice Properties}
Let $\{x_n\}$ and $\{y_n\}$ be two convergent sequences such that 
\[
    \lim_{n\to \infty}x_n = x
    \text{ and }
    \lim_{n \to \infty} y_n = y
\]
\begin{multicols}{2}
    \begin{enumerate}[label=\roman*)]
       \item $\lim_{n \to \infty} (x_n + y_n) = x + y$
       \item $\lim_{n \to \infty} (x_n y_n) = x y$
       \item $\lim_{n \to \infty} (kx_n) = kx$ where $k \in \mathbb{R}$
       \item $\lim_{n \to \infty} \frac{x_{n}}{y_{n}} = \frac{x}{y} $ if $y \neq 0$.
    \end{enumerate}
\end{multicols}

\subsubsection{Boundedness}
Let $\{x_n\}$ be a bounded sequence, then $\{x_n\}$ is bounded.
\begin{proofi}
    This proof is basically identical to that provided by Dr Job.

    Firstly, take $\varepsilon = 1$ (any number could work really). Then,
    $\exists N \in \mathbb{N}$ such that 
    \[
        \forall n \in \mathbb{N}: n > N \implies |x_n - x| < \varepsilon 
        \implies |x_n| = |x_n -x + x| \leq |x_n -x | + |x| < |x| + \varepsilon
    \]
    Then, let $M$ be defined as
    \[
        M = \max \{|x_1|, |x_2|, \ldots, |x_N|, |x|+\varepsilon\}
    \]
    Then, $M > 0$ and exists since the set is finite and 
    \[
        \forall n \in \mathbb{N}: |x_n| \leq M
    \]
    and hence $\{x_n\}$ is bounded.
\end{proofi}

\subsubsection{Order preservation}
Let $\{x_n\}$ and $\{y_n\}$ be two convergent sequences with limits $x$ and $y$. Then if 
$\exists N \in \mathbb{N}$ such that $\forall n > N$: $x_n \leq y_n$, $x \leq y$. 
As a corollary, the squeeze/sandwich theorem applies for sequences as well.

\subsection{Sequential definition of continuity}
Let $A \subseteq \mathbb{R}$ and $f \colon A \to \mathbb{R}$ and $a \in A \cap A'$.
Then, the following statements are equivalent
\begin{enumerate}
    \item $f$ is continuous at $a$.
    \item if $\{x_n\}$ is a sequence which converge to $a$, then $\{f(x_n)\}$ is a sequence that converges to $f(a)$.
\end{enumerate}
\begin{proofi}
    Firstly, let $f$ be continuous at $a$ and $\{x_n\}$ be a sequence that converges to $a$
    and consider arbitary $\varepsilon > 0$. Then by continuity $\exists \delta > 0$ such that 
    \[
        \forall x \in A: |x - a| < \delta \implies |f(x) - f(a) | <\varepsilon
    \]
    Next, since $\{x_n\}$ converges to $a$, $\exists N \in \mathbb{N}$ such that 
    \[
        \forall n \in \mathbb{N}: n > N \implies|x_n - a| < \delta \implies |f(x_n) - f(a)| < \varepsilon
    \]
    Therefore, $\{f(x_n)\}$ converges to $f(a)$.
    
    Conversely, suppose that $f$ is not continuous at $a$, we want to show that $\exists$ a
    sequence $\{x_n\}$ such that $\{f(x_n)\}$ does not converge to $f(a)$. We will construct
    such a sequence. Note that since $f$ is discontinuous at $a$,
    \[
        \exists \varepsilon > 0: \forall \delta > 0: \exists x \in A: |x -a| < \delta \ \land\ |f(x) - f(a)| \geq \varepsilon 
    \]
    We define $x_n \in \{x: |x - a| < \tfrac{1}{n} \ \land\ |f(x) - f(a)| \geq \varepsilon\}$.
    Then, notice that such a set is non-empty and by the axiom of choice we can
    generate a sequence of $\{x_n\}$. Notice that such a sequence would necessarily
    converge to $a$ since the $x_n$ is contained in smaller and smaller intervals.
    However, notice that $\{f(x_n)\}$ does not converge to $f(a)$, so we are done.
\end{proofi}
\paragraph*{Note: } 
In hindsite, there was no need for me to use the axiom of choice since all that was required
was the existence of such a set and not the actual construction. I should have just
considered an arbitrary such sequence and shown the necessary properties. Also,
since the set of such sequences is non-empty we would have satisfied existence.

\subsection{Monotone Convergence Theorem}
Let $\{x_n\}$ be a bounded monotone sequence, then $\{x_n\}$ converges.

\begin{proofi}
    This proof is basically the same as that in the notes. WLOG, we suppose that 
    $\{x_n\}$ is monotonically increasing. And we let $E = \{x_n: n \in \mathbb{N}\}$
    then we will show that $\lim_{n \to \infty} x_n = \sup(E)$.

    Let $\varepsilon > 0$ and by the characterization of supremum, $\exists x_N \in E$
    such that $x_N > \sup(E) - \varepsilon$. Then, since the sequence is monotonically 
    increasing
    \[
        \forall n \in \mathbb{N}: n > N 
        \implies \sup(E) - \varepsilon < x_N \leq x_n \leq \sup(E) 
        \implies |x_n - \sup(E)| < \varepsilon
    \]
    Then, we are done
\end{proofi}

\subsection{Cauchy Sequences}
Let $\{x_n\}$ be a sequence, then $\{x_n\}$ is called a \emph{Cauchy sequence}
iff
\[
    \forall \varepsilon > 0: \exists N \in \mathbb{N}:
    \forall m, n \in \mathbb{N}:
    m, n > N \implies |x_n - x_m| < \varepsilon
\]

\subsubsection{Cauchy convergence criterion}
Let $\{x_n\}$ be a real sequence, then
\[
    \{x_n\} \text{ is convergent}
    \iff 
    \{x_n\} \text{ is Cauchy}
\]

\subsection{Subsequences}
Let $\{x_n\}$ be a real sequence, and $\{n_k\}$ be a sequence
of strictly increasing natural numbers. Then $\{x_{n_k}\}$ is
called a \emph{subsequence} of $\{x_n\}$.

\subsubsection{Convergence of subsequences}
Let $\{x_n\}$ be a real sequence, then $\{x_n\}$ converges
iff all of its subsequences converge to the same value.

\subsubsection{Bolzano-Weierstrass Theorem for sequences}
Let $\{x_n\}$ be a bounded sequence, then $\{x_n\}$ has at least
one convergent subsequence.
\paragraph*{Note:} This theorem is equivalent to the Bolzano-Weierstrass theorem
for limit points.


\subsection{Sequential Compactness}
Let $A \subseteq \mathbb{R}$, then $A$ is called \emph{sequentially compact}
iff every sequence in $A$ has a convergent subsequence whose limit belongs
to $A$.

\subsection{Useful result}
Let $A \subseteq \mathbb{R}$, then $A$ is sequentially compact
iff $A$ is closed and bounded.
