\section{Countability}
As its name implies, the countability of a set refers to the ability to count the elements
in that set. More specifically, the ability to sequence all of the elements such that 
each element appears in the list. There are two cases in which a set $S$ is countable
\begin{itemize}
    \item $S$ is finite. That is, empty or equivalent to $J_n$ for some $n$.
    \item $S$ is equivalent to $\mathbb{N}$.
\end{itemize}
The latter definition is due to the fact that a \emph{sequence} is simply a bijection from 
$\mathbb{N}$ to $S$ as $\mathbb{N}$ is defined as a sequence of element. That is
$\mathbb{N}$ has
\begin{itemize}
    \item a first element $0 = \varnothing \in \mathbb{N}$
    \item a successor function $s$ such that $\forall n \in \mathbb{N}: s(n) = n \cup \{n\} \in \mathbb{N}$
\end{itemize}
In the case that $|S| = |\mathbb{N}|$ we call $S$ \emph{denumerable} 
and represent its cardinality by $\aleph_0$.

\subsection{Some theorems including countable sets}
\begin{enumerate}[label=\roman*)]
    \item Every subset of a finite set is finite.
    \item The union of two finite sets is finite.
    \item Every subset of a denumerable set is countable.
    \item An infinite subset of a denumerable set is denumerable.
    \item The union of a finite and denumerable set is denumerable.
    \item The union of denumerable sets is denumerable.
\end{enumerate}
\begin{proofi}
    Note that these proofs will not take into consideration empty sets as the results
    follow immediately.
    \paragraph*{Part (i) } If $S \sim J_n$ and $A \subseteq S$ then WLOG (since permutations are bijections)
    we can let $A$ consist of the first $m$ elements of $S$. Then clearly immediately, $A\sim J_m$.
    
    \paragraph*{Part (ii)} Let $A \sim J_n$ and $B \sim J_m$, then we can split $A \cup B$ into
    the disjoint sets $A\backslash B$, $B \backslash A$ and $A \cap B$ and instead only consider the 
    case when $A$ and $B$ are disjoint. In this case, let $f\colon A \to J_n$ and and $g\colon B \to J_m$
    be our bijective functions. Then, we can define $h\colon A\cup B \to J_{m+n}$ as $h(x) = f(x)$ if $x \in A$ 
    and $h(x) = g(x) + n$ if $x\in B$. Then $h$ is bijective and $A\cup B \sim J_{m+n}$.

    \paragraph*{Part (iii)} Let $A \sim \mathbb{N}$ and $B \subseteq A$. Then, we can label
    the elements of $A$ as $x_1, x_2, \ldots$ according to the bijection from $\mathbb{N}$ to $A$.
    Then, a subset of $B$ is uniquely defined by a possibly finite strictly increasing sequence 
    $i_1, i_2, \ldots$ of elements in $\mathbb{N}$.
    If the sequence is finite, there is a maximum element $i_n$ and hence $B \equiv J_n$.
    On the other hand, if the sequence is infinite our bijection from $h\colon\mathbb{N}\to B$
    is defined by $h(k) = x_{i_k}$.

    \paragraph*{Part (iv)} The proof of this was captured in part (iii) above.
    
    \paragraph*{Part (v)} Let $A \sim J_n$ and $B \sim \mathbb{N}$. If $A$ and $B$ are not disjoint we will split $A \cup B$
    into disjoint sets and reapply this theorem (note that the intersection will be finite).
    Now that $A$ and $B$ are disjoint, let $f\colon A \to J_n$ and $g\colon B \to \mathbb{N}$ then define
    $h\colon A \cup B \to \mathbb{N}$ as $h(x) = f(x)$ if $x \in A$ and $h(x) = g(x) + n$ if $x \in B$.
    Then, $h$ is our bijection from $A \cup B$ to $\mathbb{N}$.

    \paragraph*{Part (vi)} Let $A \sim B \sim \mathbb{N}$. Then, we will only consider the case
    when they are disjoint since the sets can be split into disjoint parts. Let $f\colon A \to \mathbb{N}$
    and $g\colon B \to \mathbb{N}$ be our bijective functions and define $h\colon A \cup B \to \mathbb{N}$
    as $h(x) = 2f(x)-1$ if $x \in A$ and $h(x) = 2g(x)$ if $x \in B$. Then,
    $h$ is out bijection from $A \cup B$ to $\mathbb{N}$.
\end{proofi}

\subsection{Equivalent definitions countability}
Let $A$ be a set, then following definitions are equivalent
\begin{enumerate}[label=\roman*)]
    \item $A$ is countable.
    \item $\exists$ an injection from $A$ to $\mathbb{N}$.
    \item $\exists$ an surjection from $\mathbb{N}$ to $A$.
\end{enumerate}
The proof outlined below ignores the possibility that $A$ may be empty.
\begin{proofi}
    Recall that a set is countable if its empty, equivalent to $J_n$ or equivalent to $\mathbb{N}$.
    Then immediately, $(i)\implies (ii) \,\land\, (iii)$.

    \paragraph*{$(ii) \to (i)$: } 
    Let $f\colon A\to \mathbb{N}$ be our injective function. Then, $f(A) \subseteq \mathbb{N}$
    and hence $f(A)$ is countable. Then, since $f$ is a bijection from $A$ to $f(A) \subseteq \mathbb{N}$,
    $A$ must be countable.

    \paragraph*{$(iii) \to (i)$: }
    Let $f\colon \mathbb{N} \to B$ be our surjective function. Then, by definition, for all $b \in B$ there exists an
    element $i_b \in \mathbb{N}$ such that $f(i_b) = b$. Let $i_b$ be the minimal such element
    and $I$ be the set of all such minimal $i_b$. Therefore the function
    $g\colon B\to I$ defined by $g(b) = i_b$ is a bijection from $B$ to the countable set $I$ and hence $B$ is countable.
\end{proofi}
Note that since (ii) is more concise than our original definition, we will prefer to use it in proofs.
Additionally, since equivalence requires a bijection, $\mathbb{N}$ in the above statements can be replaced by any denumerable set.

\subsection{Minimality of denumerable sets}
Let $S$ be an infinite set. Then, $S$ contains a subset which is denumerable.
Below is an outline of the proof and note that this proof requires the axiom of choice (obviously).
\begin{proofi}
    Let $s_1$ be an element in $S$. Then, since $S$ is infinite, $S\backslash \{s_1\}$
    is also infinite and there exists an element $s_2$ in it. We can this process
    infinitely since $S$ is infinite and will not ``run out of elements.''
    Then, we define our subset as $D = \{s_1, s_2, \ldots\}$ and
    bijective function $f\colon \mathbb{N} \to D$ as $f(n) = s_n$. Clearly, this function is
    bijective and since $D \subseteq S$, the result follows.
\end{proofi}
Therefore, for all infinite sets $S$, $|\mathbb{N}| = |D| \leq |S|$ and 
$\aleph_0$ is the minimal transfinite cardinal number.

\subsection{Countability of cartesian product of countable sets}
Let $A$ and $B$ be countable sets. Then, we can define an injection from
$A \times B$ to $\mathbb{N} \times \mathbb{N}$ so we will only show that 
$\mathbb{N} \times \mathbb{N}$ is countable.
\begin{proofi}
    Let $f\colon \mathbb{N} \times \mathbb{N} \to \mathbb{N}$ be defined as 
    $f(m, n) = 2^m 3^n$. Then $f$ is injective since
    \[
        f(m_1, n_1) = f(m_2, n_2) 
        \iff 2^{m_1}3^{n_1} = 2^{m_2}3^{n_2}
        \iff 2^{m_1 - m_2} = 3^{n_2 - n_1}
    \]
    Then since the only power of $2$ that is a power of $3$ is $1$ and hence the result follows.
\end{proofi}

\subsection{Countable union of countable sets}
Let the indexed family of sets $\mathcal{A}$ indexed by $I$ be a countable set of countable sets. That is 
$|I| \leq |\mathbb{N}|$ and $\forall i \in I: |A_i| \leq |\mathbb{N}|$.
Then
\[
    S = \bigcup_{i \in I} A_i
\]
is countable.
\begin{proofi}
    Firstly, we let $f\colon \mathbb{N} \to I$ be our surjective function since $I$
    is countable. Also, for each $i \in I$, we defined $g_i\colon \mathbb{N} \to A_i$
    be our surjective function from each set in $\mathcal{A}$ (since all are countable).
    Finally, we define $h\colon \mathbb{N} \times \mathbb{N} \to S$ as
    \[
        h(n, m) = g_{f(n)}(m)
    \]
    To show that $h$ is surjective, take $x \in S$. Then, by the definition of
    the union $\exists i \in I$ such that $x \in A_i$. Next, since
    $f$ is surjective, $\exists n \in \mathbb{N}: f(n) = i$ and 
    and finally since $g_i$ is surjective $\exists m \in \mathbb{N}: g_i(m) = x$.
    Then
    \[
        h(n, m) = g_{f(n)}(m) = g_i(m) = x
    \]
    Therefore, $S$ is countable.
\end{proofi}