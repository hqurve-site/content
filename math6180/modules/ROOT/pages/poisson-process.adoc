= Poisson Processes

TIP: Poisson processes are examples of xref:ctmc.adoc[].
In particular, it is a pure birth process.

Consider a collection of random variables stem:[\{X(t): t\geq 0\}], which we will refer to as a *process*.
Typically we may think of stem:[t] as time and stem:[X(t)] as the evolution of some real world
process; however, this need not be necessary.
We define an *increment* of the process is the difference in the process at two times
stem:[s] and stem:[t] (stem:[s < t]). That is stem:[X(t) - X(s)].

Stationary increments:: A process is said to have *stationary* increments
if the distribution of stem:[X(t) - X(s)] s a function of stem:[t-s] only.
So stem:[X(t_1) - X(s_1)] and stem:[X(t_2) - X(s_2)] have the same distribution
if stem:[t_1 - s_1 = t_2 - s_2].

Independent increments:: A process is said to have *independent* increments
if the increments for two disjoint intervals are independent.
That is for stem:[s_1 < t_1 < s_2 < t_2], stem:[X(t_1) - X(s_1) \perp X(t_2) - X(s_2)].

A *Poisson Process* with rate stem:[\lambda] is a collection of discrete random variables (with continuous time)
stem:[\{N(t): t \geq 0\}]
which satisfy the several properties

[.lowerroman]
. stem:[N(0) =0]
. The increments of the process are *stationary* and *independent*
. For stem:[t > 0], stem:[N(t) \sim Poisson(\lambda t)]

From this definition, we can get a few properties

* The Poisson process is non-decreasing (with probability 1).
To see this, note that
+
--
[stem]
++++
N(t)-N(s) \sim N(t-s)-N(0) = N(t-s) \sim Poisson(\lambda(t-s))
++++
Note that we have to say with probability stem:[1] since it is possible that our sample space
allows for negative increments; but they occur with probability zero.
--

* stem:[N(t+s) - N(t) \sim Poisson(\lambda s)]. For this reason, the Poisson process
is a *counting process*.

Also, note that stem:[\{N(t) : t\geq 0\}] can be viewed as a continuous time markov
chain with states stem:[\{0,1,2,3,\ldots\}], constant rate stem:[\nu_n = \lambda]
and transition probabilities of stem:[P_{i,i+1} = 1].
Note that the constant rate stem:[\nu_n = \lambda] is a result of the interarrival
times being iid stem:[Exp(\lambda)] random variables (see below).

== Properties

Consider the Poisson process stem:[\{N(t): t \geq 0\}] with rate stem:[\lambda].
Since stem:[N(t)] is a monotonic step function in stem:[t] (with probability 1),
we can define stem:[S_1, S_2, \ldots] to be the times at which steps
occur. That is for stem:[t < S_n], stem:[P(N(t) < n) = 1] and
for stem:[t > S_n], stem:[P(N(t) \geq n) = 1].
These are call the *event times* of the Poisson process.
We can also define *interarrival times* to be the times between events.
That is

* stem:[T_1 = S_1]
* stem:[T_n = S_n - S_{n-1}] for stem:[n \geq 2]

.Distribution of increments
[.proposition]
****
The interarrival times stem:[T_1, T_2 \ldots ] of a Poisson
process stem:[\{N(t): t\geq 0\}] with rate stem:[\lambda]
are iid stem:[Exponential(\lambda)]
random variables.

TIP: Recall that stem:[\{X_i\}_{i\in I}] is a collection
of linearly independent random variables iff
each finite subcollection is linearly independent


[.proof%collapsible]
====
To prove that the stem:[T_i] are independent, we prove that each collection
of stem:[\{T_i\}_{i=1}^n] is independent for each stem:[n\geq 1].
To do this, we can simply find the joint pdf.

Note that
[stem]
++++
\begin{aligned}
P\left(T_{n+1} > t_{n+1}\ |\ T_1=t_1,\ \ldots T_n=t_n\right)
&= P(N(t_{n+1} + t_n + \cdots + t_1) - N(t_n + \cdots + t_1) = 0)
\\&= P(N(t_{n+1}) = 0)
\quad\text{by stationary increments}
\\&= e^{-\lambda t_{n+1}}
\end{aligned}
++++
So, the conditional pdf of stem:[T_{n+1}\ |\ T_1=t_1,\ \ldots T_n=t_n]
is
[stem]
++++
f_{T_{n+1}\ |\ T_1=t_1,\ \ldots T_n=t_n}(t_{n+1}) = \lambda e^{-\lambda t_{n+1}}
++++
and hence the joint pdf is given by
[stem]
++++
f_{T_1, \ldots T_n}(t_1, \ldots t_n)
= f_{T_1}(t_1) \prod_{i=1}^{n-1} f_{T_{i+1}\ |\ T_1=t_1,\ \ldots T_i=t_i}(t_{i+1})
= \prod_{i=1}^n \lambda e^{-\lambda t_{i}}
++++
This is the joint pdf of stem:[n] iid stem:[Exponential(\lambda)] random variables as desired.
====
****

.Distribution of stem:[n]'th event
[.corollary]
****
Let stem:[\{N(t) : t \geq 0\}] be a Poisson process with rate stem:[\lambda].
Then, the time of the stem:[n]'th event, stem:[S_n], is a stem:[Gamma(n,\lambda)] random variable.

CAUTION: This is the marginal distribution.
****

=== Conditional distribution

If we know that stem:[n] events occur in stem:[[0, t\]] (ie stem:[N(t) = n]),
we may also want to know in what the distribution of the events were.
It happens that the events occur uniformly in the stem:[[0,t\]] interval.
I find this to be quite surprising

.Conditional distribution of events
[.theorem]
****
Let stem:[\{N(t) : t \geq 0\}] be a Poisson process with rate stem:[\lambda].
Then, the joint conditional density of stem:[S_1, \ldots S_n | N(t)=n],

[stem]
++++
f_{S_1, S_2, \ldots S_n\ |\ N(t) = n}(s_1, \ldots s_n) = \begin{cases}
\frac{n!}{t^n} , \quad&\text{if } 0 < t_1 < \cdots < t_n < t\\
0 , \quad&\text{otherwise}
\end{cases}
++++

TODO: Proof

// [.proof%collapsible]
// ====
// We would find the conditional distribution function.
// Let stem:[0 < t_1 < \cdots < t_n < t]
// [stem]
// ++++
// \begin{aligned}
// &P(S_1 < t_1, \ t_1 < S_2 <)
// \end{aligned}
// ++++
// ====

****



== Alternative definition of Poisson process
Let stem:[\{N(t): t\geq 0\}] be a continuous time stochastic process.
Then stem:[N] is a Poisson process with rate stem:[\lambda > 0] iff

[.lowerroman]
. stem:[N(0) = 0]
. stem:[N] has stationary and independent increments
. The following three hold (as stem:[h \to 0])
.. stem:[P(N(h) = 0) = 1-\lambda h + o(h)]
.. stem:[P(N(h) = 1) = \lambda h + o(h)]
.. stem:[P(N(h) \geq 2) = o(h)]

TIP: The symbol stem:[o(h)] is an instance of
https://en.wikipedia.org/wiki/Big_O_notation#Little-o_notation[Landau's little-o notation].
Note stem:[f(h) = o(g(h))] (as stem:[h\to a]) iff
stem:[\lim_{h \to a} \frac{f(h)}{g(h)} = 0] where stem:[a] is a limit point. That is stem:[a \in \mathbb{R} \cup \{\pm \infty\}].


Note that the only difference between this definition and the original definition is the third property.
So, we need to show that both third properties are equivalent (under assumption of the first two properties).

=== Proof of forward direction
First assume that stem:[N(t) \sim Poisson(\lambda t)].
Then,

[.loweralpha]
. stem:[P(N(h) = 0) = 1-\lambda h + o(h)] since
+
[stem]
++++
\lim_{h \to 0} \frac{P(N(h) = 0) - (1-\lambda h)}{h}
= \lim_{h\to 0} \frac{e^{-\lambda h} + \lambda h}{h}
= \lim_{h\to 0} \frac{-\lambda e^{-\lambda h} + \lambda}{1}
= 0
++++

. stem:[P(N(h) =1) = \lambda h + o(h)] since
+
[stem]
++++
\lim_{h \to 0} \frac{P(N(h) = 1) - \lambda h}{h}
= \lim_{h\to 0} \frac{e^{-\lambda h} (\lambda h) - \lambda h}{h}
= \lambda \lim_{h\to 0} (e^{-\lambda h} - 1)
= 0
++++

. stem:[P(N(h) \geq 2) = o(h)]
+
[stem]
++++
\begin{aligned}
\lim_{h \to 0} \frac{P(N(h) \geq 2)}{h}
&= \lim_{h \to 0} \frac{1-P(N(h) =0)-P(N(h) =1)}{h}
\\&= \lim_{h \to 0} \frac{1-e^{-\lambda h} - e^{-\lambda h}(\lambda h)}{h}
\\&= \lim_{h \to 0} \frac{e^{-\lambda h}(\lambda^2 h)}{1}
\\&= 0
\end{aligned}
++++

Therefore the first definition of a Poisson process implies the second.

=== Proof of reverse direction
Next, assume that the second definition holds true.
Note that the proof of the reverse direction was not given in class.
But just prior, we did a proof of a Poisson approximation to the Binomial
by finding of stem:[P(X_n = k)] as stem:[n\to \infty], where stem:[X \sim Binomial\left(n, \frac{t\lambda}{n}\right)].
So, it may make sense to prove that stem:[N(t) \sim Poisson(\lambda t)] be a similar manner.
But instead, we can use the moment generating function approach.
We want to find the moment generating function of stem:[N(t)] for some fixed stem:[t].
We expect it to be stem:[e^{\lambda t (e^{s}-1)}].

From the stationary and independent increments, we have that
for stem:[n\in \mathbb{Z}^+] and stem:[s \in \mathbb{R}]
[stem]
++++
\begin{aligned}
E\left[s e^{N(t)}\right]
= E\left[e^{s\sum_{k=1}^n N\left(\frac{k}{n}t\right) - N\left(\frac{k-1}{n}\right)}\right]
= \prod_{k=1}^n E\left[e^{sN\left(\frac{k}{n}t\right) - sN\left(\frac{k-1}{n}\right)}\right]
= E\left[e^{sN\left(\frac{t}{n}\right)}\right]^n
\end{aligned}
++++
Next, if stem:[h = \frac{t}{n}] and restrict that stem:[s \leq 0] (to ensure convergence), we have that
[stem]
++++
\begin{aligned}
&E\left[e^{s N(h)}\right]
= \sum_{k=0}^\infty e^{sk} P(N(h) = k)
= P(N(h) = 0) + e^{s}P(N(h) = 1) + \sum_{k=2}^\infty e^{sk} P(N(h) = k)
\\&\implies
\begin{aligned}[t]
&P(N(h) = 0) + e^{s}P(N(h) = 1) + \sum_{k=2}^\infty (0) P(N(h) = k)
\\&\quad\leq E\left[e^{s N(h)}\right]
\\&\quad \leq
P(N(h) = 0) + e^{s}P(N(h) = 1) + \sum_{k=2}^\infty e^{2s} P(N(h) = k)
\end{aligned}
\\&\implies
\begin{aligned}[t]
&P(N(h) = 0) + e^{s}P(N(h) = 1)
\\&\quad\leq E\left[e^{s N(h)}\right]
\\&\quad \leq
P(N(h) = 0) + e^{s}P(N(h) = 1) + e^{2s}P(N(h) \geq 2)
\end{aligned}
\\&\implies
\begin{aligned}[t]
&P(N(t/n) = 0) + e^{s}P(N(t/n) = 1)
\\&\quad\leq E\left[e^{s N(t/n)}\right]
\\&\quad \leq
P(N(t/n) = 0) + e^{s}P(N(t/n) = 1) + e^{2s}P(N(t/n) \geq 2)
\end{aligned}
\end{aligned}
++++
Also,
as stem:[n \to \infty]

* stem:[P(N(t/n) = 0) + e^{s}P(N(t/n) = 1) = 1 + \frac{\lambda t (e^s - 1)}{n} + o\left(\frac{1}{n}\right)]
* stem:[P(N(t/n) = 0) + e^{s}P(N(t/n) = 1) + e^{2s}P(N(t/n) \geq 2) = 1 + \frac{\lambda t (e^s - 1)}{n} + o\left(\frac{1}{n}\right)]

So,
[stem]
++++
E\left[e^{sN(t)}\right] = E\left[e^{sN\left(\frac{t}{n}\right)}\right]^n
= \left[1 + \frac{\lambda t (e^s - 1)}{n} + o\left(\frac{1}{n}\right)\right]^n
++++
Then, for each stem:[\varepsilon > 0], we can find
sufficiently large stem:[n] such that that the term inside the brackets differs from stem:[1 + \frac{\lambda t (e^s - 1)}{n}]
by at most stem:[\frac{\varepsilon}{n}].
Therefore, for each epsilon stem:[\varepsilon], we can find sufficiently large stem:[n] such that
[stem]
++++
\left[1 + \frac{\lambda t (e^s - 1) - \varepsilon}{n}\right]^n
\leq E\left[e^{sN(t)}\right] \leq
\left[1 + \frac{\lambda t (e^s - 1) + \varepsilon}{n}\right]^n
++++
and as stem:[n\to \infty], we have that
[stem]
++++
e^{\lambda t(e^s-1) - \varepsilon}
\leq E\left[e^{sN(t)}\right] \leq
e^{\lambda t(e^s-1) + \varepsilon}
++++
Therefore,
[stem]
++++
 E\left[e^{sN(t)}\right] = 
e^{\lambda t(e^s-1)}
++++
This is precisely the moment generating function of a stem:[Poisson(\lambda t)] random variable and hence we have proven the desired result.

== Cool properties


[.proposition]
****
Let stem:[\{N(t) : t \geq 0\}] be a Poisson process with rate stem:[\lambda].
Then,
[stem]
++++
N(t) \xrightarrow[]{a.s.} \infty
++++
That is
[stem]
++++
P\left(\lim_{t \to \infty} N(t) = \infty\right) = 1
++++

[.proof%collapsible]
====
Consider arbitrary stem:[n\in \mathbb{Z}^+] and stem:[t\geq 0].
Then,
[stem]
++++
\begin{aligned}
P(N(t) \geq n)
&\geq P\left(N\left(\frac{k}{n}t\right) - N\left(\frac{k-1}{n}t\right) \geq 1 \text{ for } k = 1, \ldots n\right)
\\&= \prod_{k=1}^n P\left(N\left(\frac{k}{n}t\right) - N\left(\frac{k-1}{n}t\right) \geq 1\right) \quad\text{by independence of increments}
\\&= P\left(N\left(\frac{t}{n}\right) \geq 1\right)^n
\\&= (1-e^{-\lambda t})^n
\end{aligned}
++++
So,
[stem]
++++
1 \leq P(N(t) \geq n \quad\text{for some } t \geq 0) \geq \lim_{t \to \infty} P(N(t) \geq n) \geq \lim_{t\to \infty} (1-e^{-\lambda t})^n
= 1
++++
since for stem:[t_1 \leq t_2], stem:[N(t_1) \geq n \implies N(t_2) \geq n].
Therefore, we have that
[stem]
++++
P\left(\lim_{t \to \infty} N(t) = \infty\right)
= P\left(\forall n \geq 1: \exists t \geq 0: N(t) \geq n\right)
= 1
++++
since for each stem:[n \geq 1], stem:[P\left(\lim_{t\to\infty} N(t) \geq n\right) = 1]

====
****


[.proposition]
****
Let stem:[\{N_1(t): t\geq 0\}] and stem:[\{N_2(t): t\geq 0\}] be independent Poisson processes with
rates stem:[\lambda_1] and stem:[\lambda_2] respectively.
Then, their superposition
[stem]
++++
\{N_1(t) + N_2(t): t\geq 0\}
++++
is also a Poisson process with rate stem:[\lambda_1 + \lambda_2].

NOTE: The proof of this follows from the fact that the sum of two independent poisson random variables is also poisson,
but with the combined mean.
****


== Generalizations

=== Multiple event types

TIP: [.line-through]#This is a generalization of a theorem by Ross (1996) which was only two types.#
After reading the Ross text, this is the exact same theorem. The notes, that I was using, only focused on the special case where there are two
types.

TIP: This can also be viewed as a *thinning* of the original process since each stem:[N_i] counts only a subset of the events.

Consider a poisson process stem:[\{N(t): t\geq 0\}]
with rate stem:[\lambda].
Now, suppose that event may be classified as one of stem:[i \in I]
which occur with probabilities stem:[p_i(t)] respectively
where at each stem:[t \geq 0], stem:[\sum_{i\in I}p_i(t) = 1].
So, we may be interested in the separate processes
stem:[\{N_i(t): t \geq 0\}] which count the number of occurrences of event type stem:[i \in I].


[.theorem]
****
For some fixed stem:[t \geq 0], the variables stem:[N_i(t)] are independent
poisson random variables with respective means stem:[\lambda P_i t]
where
[stem]
++++
P_i = \frac{1}{t} \int_0^t p_i(s) \ ds
++++

[.proof%collapsible]
====
We will find
[stem]
++++
P(N_i(t) = n_i: i \in I)
++++
for some fixed stem:[\{n_i\}_{i\in I} \in \mathbb{N}^I].
Let stem:[n = \sum_{i\in I} n_i].
Let stem:[0< T_1 < \cdots < T_n < t] be the times of the events.
Then,
[stem]
++++
\begin{aligned}
&P(N_i(t) = n_i: i \in I)
\\&= P(N(t) = n) P(N_i(t) = n_i: i \in I \ | \ N(t) = n)
\\&= P(N(t) = n) \int_{0 < s_1 < \cdots s_n < t} P\left(\begin{aligned}&N_i(t) = n_i: i \in I \\&| \ N(t) = n, \ T_1 = s_1, \ldots T_n=s_n\end{aligned}\right)
    f_{T_1,\ldots T_n| N(t) = n}(s_1, \ldots s_n) \ dt
\\&\hspace{4em}\text{by conditioning on } T_1, \ldots T_n
\\&= \frac{e^{-\lambda t} (\lambda t)^n}{n!} \int_{0 < s_1 < \cdots s_n < t} P\left(\begin{aligned}&N_i(t) = n_i: i \in I \\&| \ N(t) = n, \ T_1 = s_1, \ldots T_n=s_n\end{aligned}\right)
    \frac{n!}{t^n} \ dt
\\&= e^{-\lambda t} \lambda^n \int_{0 < s_1 < \cdots s_n < t} P\left(\begin{aligned}&N_i(t) = n_i: i \in I \\&| \ N(t) = n, \ T_1 = s_1, \ldots T_n=s_n\end{aligned}\right) \ dt
\\&= e^{-\lambda t} \lambda^n \int_{0 < s_1 < \cdots s_n < t} \sum_{\begin{array}{c}\bigcup_{i\in I} J_i = \{s_1,\ldots s_n\}\\|J_i| = n_i\end{array}}P\left(
    \text{events of class } i \text{ occur at } J_i \right) \ dt
\\&= e^{-\lambda t} \lambda^n \int_{0 < s_1 < \cdots s_n < t} \sum_{\begin{array}{c}\bigcup_{i\in I} J_i = \{s_1,\ldots s_n\}\\|J_i| = n_i\end{array}}P\left(
    \text{events of class } i \text{ occur at } J_i \right) \ dt
\\&= e^{-\lambda t} \lambda^n \int_{0 < s_1 < \cdots s_n < t} \sum_{\begin{array}{c}\bigcup_{i\in I} J_i = \{s_1,\ldots s_n\}\\|J_i| = n_i\end{array}}
    \prod_{i \in I} \prod_{j \in J_i} p_i(s_j) \ dt
\\&= e^{-\lambda t} \lambda^n \frac{1}{n!}\int_{s_1,\ldots s_n \in [0,t]} \sum_{\begin{array}{c}\bigcup_{i\in I} J_i = \{s_1,\ldots s_n\}\\|J_i| = n_i\end{array}}
    \prod_{i \in I} \prod_{j \in J_i} p_i(s_j) \ dt
    \quad\text{by symmetry of the }s_j
\\&= e^{-\lambda t} \lambda^n \frac{1}{n!}\int_{\begin{array}{c}s_{ij} \in [0,t]\\i \in I, \ j \in 1,\ldots n_i\end{array}} \binom{n}{n_i: i \in I} \prod_{i \in I}\prod_{j=1}^{n_i} p_{i}(s_{ij}) \ dt
    \quad\text{by assigning the }s_j
\\&= e^{-\lambda t} \lambda^n \frac{1}{n!} \binom{n}{n_i: i \in I} \prod_{i \in I} \left(\int_0^t  p_{i}(s) \ ds\right)^{n_i}
\\&= e^{-\lambda t} \lambda^n \frac{1}{\prod_{i \in I} n_i!} \prod_{i \in I} t^{n_i}\left(\frac{1}{t}\int_0^t  p_{i}(s) \ ds\right)^{n_i}
\\&= e^{-\lambda t} \lambda^n \prod_{i \in I} \frac{(\lambda t)^{n_i}}{n_i!} P_i^{n_i}
\\&= e^{-\lambda \sum_{i \in I} P_i t} \prod_{i \in I} \frac{(\lambda t)^{n_i}}{n_i!} P_i^{n_i}\quad\text{since } \sum_{i\in I}P_i = 1
\\&=  \prod_{i \in I} \frac{e^{-\lambda P_i t }(\lambda P_i t)^{n_i}}{n_i!}
\end{aligned}
++++
Therefore, we have the desired result
====
****

=== Non-homogeneous Poisson process

A Markov process stem:[\{N(t) : t \geq 0\}] is a *non-homogeneous* Poisson
process with intensity stem:[\lambda(t)] (where stem:[\lambda: \mathbb{R}^+ \to \mathbb{R}^+]) if

. stem:[N(0) = 0]
. stem:[\{N(t): t\geq 0\}] has independent increments
. For stem:[0 \leq s < t], the random variable stem:[N(t) - N(s)]
is Poisson with mean stem:[\int_s^t \lambda(u) \ du].
That is,
+
[stem]
++++
P(N(t) - N(s) = k) = \frac{e^{-\int_s^t \lambda(u) \ du} \left(\int_s^t \lambda(u) \ du\right)^k}{k!}
,\quad k = 0,1,2,\ldots
++++

=== Compound Poisson Process
Let stem:[\{N(t): t\geq 0\}] be a poisson process and stem:[\{Y_i: i \in \mathbb{Z}^+\}]
be a sequence of iid random variables.
Then, we can define a *compound poisson process* stem:[\{X(t): t\geq 0\}]
as
[stem]
++++
X(t) = \sum_{i=1}^{N(t)} Y_i
++++
