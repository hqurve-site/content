= Continuous Distributions

== Uniform
A stem:[Uniform[a,b\]] distribution has the following pdf on support stem:[[a,b\]]
[stem]
++++
f(x) = \frac{1}{b-a}
++++
and
[stem]
++++
E[X] = \frac{a+b}{2},\quad\text{and}\quad Var[X] = \frac{(b-a)^2}{12}
++++
and its MGF is given by
[stem]
++++
E[e^{tX}] = \int_a^b e^{tx}\frac{1}{b-a} \ dx
= \begin{cases}
\frac{e^{tb}-e^{ta}}{t(b-a)},\quad &\text{if } t\neq 0\\
1,\quad&\text{if } t= 0\\
\end{cases}
++++

== Exponential
The stem:[Exponential(\lambda)] distribution is often used to model times and is useful in this course because of the memoryless property (see below).
The support is given by stem:[\lambda > 0] and stem:[x \geq 0]
[stem]
++++
f(\lambda) = \lambda e^{-\lambda x}
++++
[stem]
++++
E[X] = \frac{1}{\lambda},\quad\text{and}\quad Var[X] = \frac{1}{\lambda^2}
++++
[stem]
++++
E[e^{tX}] = \int_0^\infty \lambda e^{(t-\lambda) x} \ dx
= \frac{\lambda}{\lambda-t}
++++
NOTE: The MGF is only defined when stem:[t < \lambda].

=== Memoryless property
The memoryless property states if stem:[X\sim Exp(\lambda)]
[stem]
++++
P(X > s+t | X > s) = P(X > t)
++++
TIP: This is clear to see by substituting the definition of conditional probability and the pdf and simplifying 

=== Minimum
Let stem:[X_1, \ldots X_n] be independent exponential random variables with stem:[X_i \sim \exp(\lambda_i)].
Then

* stem:[\min(X_1, \ldots X_n) \sim \exp\left(\sum_{i=1}^n \lambda_i\right)]
* stem:[P(\min(X_1, \ldots X_n) = X_i) = \frac{\lambda_i}{\sum_{j=1}^n \lambda_j}]


[.proof%collapsible]
====
For the first property
[stem]
++++
\begin{aligned}
P(\min(X_1, \ldots X_n) > t)
&= P(X_1 > t,\ X_2 >t, \ \ldots,\ X_n > t)
\\&= \prod_{i=1}^n P(X_i> t) \quad\text{since the } X_i \text{ are independent}
\\&= \prod_{i=1}^n e^{-\lambda_i t}
\\&= e^{-\sum{i=1}^n \lambda_i t}
\end{aligned}
++++
Hence stem:[\min(X_1, \ldots X_n) \sim \exp\left(\sum_{i=1}^n \lambda_i\right)].

Next,
[stem]
++++
\begin{aligned}
P(X_i = \min(X_1, \ldots X_n))
&= P(X_i < X_j \text{ for } j \neq i)
\\&= \int_0^\infty P(X_i < X_j \text{ for } j \neq i \ | \ X_i = t)\lambda_i e^{-\lambda_i t} \ dt
    \quad\text{ by conditioning on } X_i
\\&= \int_0^\infty P(t < X_j \text{ for } j \neq i)\lambda_i e^{-\lambda_i t} \ dt
\\&= \int_0^\infty\lambda_i e^{-\lambda_i t} \prod_{j \neq i} e^{-\lambda_j t} \ dt
\\&= \int_0^\infty\lambda_i \prod_{j =1}^n e^{-\lambda_j t} \ dt
\\&= \frac{\lambda_i}{\sum_{i=1}^n \lambda_i} \int_0^\infty \left(\sum_{i=1}^n \lambda_i\right)
        e^{-\left(\sum_{i=1}^n \lambda_i\right)t }  \ dt
\\&= \frac{\lambda_i}{\sum_{i=1}^n \lambda_i}
\end{aligned}
++++
as desired
====

== Gamma 
The stem:[Gamma(\alpha, \beta)] distribution is useful as it can be though of as the sum of exponential distributions.
The support is given by stem:[\alpha, \beta, x > 0]
[stem]
++++
f(x) = \frac{\beta^\alpha}{\Gamma(\alpha)}x^{\alpha-1}e^{-\beta x}
++++
[stem]
++++
E[X] = \frac{\alpha}{\beta},\quad\text{and}\quad Var[X] = \frac{\alpha}{\beta^2}
++++
[stem]
++++
\begin{aligned}
E[e^{tX}]
&= \int_0^\infty e^{tx}\frac{\beta^\alpha}{\Gamma(\alpha)}x^{\alpha-1}e^{-\beta x} \ dx
\\&= \int_0^\infty \frac{\beta^\alpha}{\Gamma(\alpha)}x^{\alpha-1}e^{(t-\beta) x} \ dx
\\&= \left(\frac{\beta}{\beta-t}\right)^\alpha\int_0^\infty \frac{(\beta-t)^\alpha}{\Gamma(\alpha)}x^{\alpha-1}e^{-(\beta-t) x} \ dx
\\&= \left(\frac{\beta}{\beta-t}\right)^\alpha
\end{aligned}
++++

== Normal
The stem:[Normal(\mu, \sigma^2)] distribution is often the default distribution of choice as a result of the central limit theorem.
It's support is all real numbers and its pdf is given by
[stem]
++++
f(x) = \frac{1}{\sqrt{2\pi\sigma^2}}\exp\left(-\frac{(x-\mu)^2}{2\sigma^2}\right)
++++
[stem]
++++
E[X] = \mu,\quad\text{and}\quad Var[X] = \sigma^2
++++
[stem]
++++
\begin{aligned}
E[e^{tX}]
&= \int_{-\infty}^{\infty} \frac{1}{\sqrt{2\pi\sigma^2}}\exp\left(-\frac{(x-\mu)^2}{2\sigma^2}\right) \exp(tx) \ dx
\\&= \int_{-\infty}^{\infty} \frac{1}{\sqrt{2\pi\sigma^2}}\exp\left(-\frac{(x-\mu)^2}{2\sigma^2}\right) \exp(tx) \ dx
\\&= \int_{-\infty}^{\infty} \frac{1}{\sqrt{2\pi\sigma^2}}\exp\left(-\frac{(x-\mu)^2-2tx\sigma^2}{2\sigma^2}\right) \ dx
\\&= \int_{-\infty}^{\infty} \frac{1}{\sqrt{2\pi\sigma^2}}\exp\left(-\frac{(x-\mu)^2-2t(x-\mu)\sigma^2 - 2t\mu\sigma^2}{2\sigma^2}\right) \ dx
\\&= \int_{-\infty}^{\infty} \frac{1}{\sqrt{2\pi\sigma^2}}\exp\left(-\frac{(x-\mu - t\sigma^2)^2- (t\sigma^2)^2 - 2t\mu\sigma^2}{2\sigma^2}\right) \ dx
\\&= \exp\left(\frac{(t\sigma^2)^2 + 2t\mu\sigma^2}{2\sigma^2}\right)\int_{-\infty}^{\infty} \frac{1}{\sqrt{2\pi\sigma^2}}\exp\left(-\frac{(x-\mu - t\sigma^2)^2}{2\sigma^2}\right) \ dx
\\&= \exp\left(\frac{(t\sigma^2)^2 + 2t\mu\sigma^2}{2\sigma^2}\right)
\\&= \exp\left(\frac{t^2\sigma^2 + 2t\mu}{2}\right)
\end{aligned}
++++

=== Central Limit Theorem

IMPORTANT: This is an informal statement of the theorem.
Please see xref:math3278:moment-generating-functions:theorems.adoc[Math3278 -- Moment Generating functions] 
for a more formal statement.

Let stem:[X_1,\ldots X_n] be a random sample from a population with finite
variance and finite mean.
Then,
[stem]
++++
\overbar{X}_n \approx N\left(\mu, \frac{\sigma^2}{n}\right)
++++
This approximation holds better when stem:[n] is large (usually if stem:[n\geq 30]).
