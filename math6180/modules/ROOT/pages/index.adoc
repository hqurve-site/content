= Math 6180 - Applied Probability Theory

This course is taught by Dr. Isaac Dialsingh
and is the same as STAT 6100 in everything but name and weighting.

== Table of contents
include::../nav.adoc[]

== Materials

* Introduction to Probability Models by Sheldon Ross
* An introduction to stochastic processes with applications to biology by Linda J. S. Allen
