= Todo


== Kuratowski
(Kuratowski) for any subset stem:[A], the operations of closure and complement can produce at most
14 distinct sets. To show this, one needs to show that for any set stem:[B],
stem:[\interior(\closure(\interior(\closure(B)))) = \interior(\closure(B))] and consider
the graph of operations.

The 14 is achievable with the for example, the following set
[stem]
++++
A = \{0\} \cup (1,2) \cup \left(2,\frac{5}{2}\right) \cup \left(\left(\frac{5}{2}, 3\right]\cap \mathbb{Q}\right)
++++
The key ideas of this example include

* stem:[A] and stem:[A^c] have isolated points which would be removed after applying interior
* the part with stem:[\mathbb{Q}] appears in both stem:[\closure(A)] and stem:[\closure(X-A)]

== Continuation
If stem:[g] is continuous on stem:[A], then any stem:[\overbar{g}: \closure(A)\to Y]
which is continuous and agrees with stem:[g] on stem:[A] is unique.

This is useful when continuing solutions from stem:[\mathbb{Q}] to stem:[\mathbb{R}].

== Topological groups
A group which satisfies the stem:[T_1] axiom such that composition and inversion are continuous.

In such a case, any subgroup stem:[H \leq G] is also a topological group (where the topology is the subspace topology on stem:[H]).
Also, stem:[\closure(H)] is a topological group (note you need to prove that stem:[\closure(H)] is a group).

If stem:[\alpha \in G], stem:[x \to \alpha x] and stem:[x\to x\alpha] are homeomorphisms (Note they are not homomorphisms).
This makes stem:[G] a homogeneous space since for any fixed stem:[x,y\in G] there is a homeomorphism which maps stem:[G] to itself
and carries stem:[x] to stem:[y].
