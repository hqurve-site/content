= Fisher Information

Suppose a population exhibits some parameter stem:[\theta]. Then, for each sample
we take, there possibly exists some information about stem:[\theta] which
allows us to estimate it. Additionally, with larger samples should contain
more information about stem:[\theta] allowing for better estimates.
_Fisher information_ is one measure of the amount of information a sample contains
about a parameter.

Firstly, lets define the fisher information for a single observation. Suppose that
stem:[X] comes from a distribution with pdf stem:[f] which depends on stem:[\theta].
Then, we define stem:[\lambda(x, \theta) = \log(f(x, \theta))] and 
[stem]
++++
I(\theta) = E(\lambda'(X, \theta)^2)
++++
where stem:[\lambda^{(n)} = \frac{\partial^n \lambda}{\partial \theta^n}].

Furthermore if stem:[\vec{X} = (X_1, \ldots X_n)] is an iid random sample,
we define
[stem]
++++
I_n(\theta) = E(\lambda_n'(\vec{X}, \theta)^2) = \sum_{i=1}^n E(\lambda'(X_i, \theta)^2) = nI(\theta)
++++
.Proof
[%collapsible]
====
[stem]
++++
\begin{aligned}
I_n(\theta)
&= E(\lambda_n'(\vec{X}, \theta)^2)
\\&= E\left(\left( \frac{\partial }{\partial \theta}\ln(f(\vec{X}, \theta)) \right)^2\right)
\\&= E\left(\left( \frac{\partial }{\partial \theta}\ln\left(\prod_{i=1}^n f(X_i, \theta)\right) \right)^2\right)
\\&= E\left(\left( \frac{\partial }{\partial \theta}\sum_{i=1}^n\ln\left( f(X_i, \theta)\right) \right)^2\right)
\\&= E\left(\left( \sum_{i=1}^n \frac{\partial }{\partial \theta}\ln\left( f(X_i, \theta)\right) \right)^2\right)
\\&= E\left(\left( \sum_{i=1}^n \lambda'(X_i, \theta) \right)^2\right)
\\&= E\left( \sum_{i\neq j} \lambda'(X_i, \theta) \lambda'(X_j, \theta) + \sum_{i=1}^n \lambda'(X_i, \theta)^2\right)
\\&= \sum_{i\neq j} E(\lambda'(X_i, \theta)) E(\lambda'(X_j, \theta)) + \sum_{i=1}^n E(\lambda'(X_i, \theta)^2)
\\&= \sum_{i=1}^n E(\lambda'(X_i, \theta)^2)
\\&= nI(\theta)
\end{aligned}
++++
We show that stem:[E(\lambda'(X, \theta)) = 0] xref:#_expectation_of_first_derivative_log_likelihood[here].
====

== Equivalent formulations for fisher information
If our pdf of our variable is "`nice`", we can show that
the following are equivalent formulations for fisher information
of a random variable
[stem]
++++
I(\theta)  = Var(\lambda'(X, \theta)) = -E(\lambda'' (X, \theta))
++++
.Proof
[%collapsible]
====
Notice that
[stem]
++++
I(\theta) = E(\lambda'(X, \theta)^2) - E(\lambda'(X, \theta))^2 = Var(\lambda'(X, \theta))
++++
because of xref:#_expectation_of_first_derivative_log_likelihood[this result].
Then, for the second equality,
[stem]
++++
\begin{aligned}
- E(\lambda'' (X, \theta))
&= E\left(\frac{f(X, \theta)f''(X, \theta) - f'(X, \theta)^2}{f(X, \theta)^2}\right)
\\&= E\left(\lambda'(X, \theta)^2 - \frac{f''(X, \theta)}{f(X, \theta)}\right)
\\&= I(\theta) - E\left(\frac{f''(X, \theta)}{f(X, \theta)}\right)
\\&= I(\theta) - \int_{\mathbb{R}} \frac{f''(x, \theta)}{f(x, \theta)} f(x, \theta) \ dx
\\&= I(\theta) - \int_{\mathbb{R}} f''(x, \theta) \ dx
\\&= I(\theta) - \int_{\mathbb{R}} \frac{\partial^2}{\partial \theta^2}f(x, \theta) \ dx
\\&= I(\theta) - \frac{\partial^2}{\partial \theta^2}\int_{\mathbb{R}} f(x, \theta) \ dx
\\&= I(\theta) - \frac{\partial^2}{\partial \theta^2} 1
\\&= I(\theta)
\end{aligned}
++++
====
Furthermore, for a random sample,
[stem]
++++
I_n(\theta) = \sum_{i=1}^n Var(\lambda'(X_i, \theta)) = -\sum_{i=1}^n E(\lambda'' (X_i, \theta))
++++

[#_expectation_of_first_derivative_log_likelihood]
=== Expected value of differential of log likelihood
[stem]
++++
E(\lambda'(X, \theta)) = 0
++++

.Proof
[%collapsible]
====
[stem]
++++
\begin{aligned}
E(\lambda'(X, \theta))
&= E\left(\frac{f'(X, \theta)}{f(X, \theta)}\right)
\\&= \int_{\mathbb{R}} \frac{f'(x, \theta)}{f(x, \theta)} f(x, \theta)\ dx
\\&= \int_{\mathbb{R}} f'(x, \theta)\ dx
\\&= \int_{\mathbb{R}}\frac{\partial }{\partial \theta} f(x, \theta)\ dx
\\&= \frac{\partial }{\partial \theta} \int_{\mathbb{R}} f(x, \theta)\ dx
\\&= \frac{\partial }{\partial \theta} 1
\\&= 0
\end{aligned}
++++
====

== Information Inequality
Let stem:[\vec{X} = (X_1, \ldots X_n)] comprise a random sample
of a distribution with pdf stem:[f(X_i, \theta)] and let stem:[T]
be an unbiased estimator for stem:[m(\theta)]. Then,
[stem]
++++
Var(T) \geq \frac{m'(\theta)^2}{nI(\theta)}
++++
.Proof
[%collapsible]
====
This result follows the
xref:math3273:inner-product-space:index.adoc#_cauchy_schwartz_inequality[cauchy-schwartz inequaility]
since the set of random variables together with the covariance function forms an inner product
space. [xref:math2274::joint-random-variables.adoc#_covariance_as_an_inner_product[see here]].
Then, if we consider random variables stem:[T] and stem:[\lambda'(\vec{X}, \theta)], by the 
cauchy-schwartz inequality, it follows that
[stem]
++++
\begin{aligned}
&Var(T)Var(\lambda'(\vec{X}, \theta)) \geq Cov(T, \lambda'(\vec{X}, \theta))^2
\\& \implies Var(T) 
    \geq \frac{Cov(T, \lambda'(\vec{X}, \theta))^2}{Var(\lambda'(\vec{X}, \theta))}
= \frac{Cov(T, \lambda'(\vec{X}, \theta))^2}{nI(\theta)}
\end{aligned}
++++
Therefore, it would suffice for us to show that
[stem]
++++
Cov(T, \lambda'(\vec{X}, \theta))
= E(T\lambda'(\vec{X}, \theta)) - E(T)E(\lambda'(\vec{X},\theta))
= e(t\lambda'(\vec{x}, \theta))
= m'(\theta)
++++
since stem:[E(\lambda'(\vec{X}, \theta)) = 0]. [xref:#_expectation_of_first_derivative_log_likelihood[see here]]

Now, since stem:[T] is a statistic from stem:[\vec{X}], we can write it in its function form
stem:[T(\vec{X})] to make it clear that the joint distribution can be formulated just in terms of stem:[x].
Then,
[stem]
++++
\begin{aligned}
E(T\lambda'(\vec{X}, \theta))
&= \int_{\mathbb{R}^n}T(\vec{x})\frac{f'(\vec{x}, \theta)}{f(\vec{x}, \theta)}f(\vec{x}, \theta) \ d\vec{x}
\\&= \int_{\mathbb{R}^n}T(\vec{x})f'(\vec{x}, \theta) \ d\vec{x}
\\&= \frac{\partial }{\partial \theta}\int_{\mathbb{R}^n}T(\vec{x})f(\vec{x}, \theta) \ d\vec{x}
\\&= \frac{\partial }{\partial \theta}E(T(\vec{X}))
\\&= \frac{\partial }{\partial \theta} m(\theta)
\\&= m'(\theta)
\end{aligned}
++++
and we are done.
====

In particular, when stem:[m(\theta) = \theta], we obtain
[stem]
++++
Var(T) \geq \frac{1}{nI(\theta)}
++++
which is the _Cramer-Rao Lower bound_. Therefore, we aim to find an estimator
which attains this lower bound, in which case, they would be one of the best
unbiased estimators for stem:[\theta]. If we attain such an estimator, 
we call it a _minimum variance, unbiased estimator (MVUE) for stem:[\theta]_ 

CAUTION: This lower bound is only for unbiased estimators of stem:[\theta]. That is, there may exists
biased estimators with smaller variance. For example, stem:[T=5] has variance stem:[0].

== Rao-Blackwell Theorem
Suppose stem:[\vec{X} = (X_1, \ldots X_n)] comprise a sample of a distribution with parameter
stem:[\theta]. Then, if stem:[T] is an unbiased estimator for stem:[m(\theta)] and stem:[U]
is sufficient for stem:[\theta], we can define a random variable stem:[W = E(T| U)]
such that

* stem:[W] is sufficient for stem:[m(\theta)]
* stem:[W] is an unbiased estimator for stem:[m(\theta)]
* stem:[Var(W) \leq Var(T)]

Furthermore, stem:[W = T] if stem:[T] is sufficient for stem:[m(\theta)] (hence, this construction
is idempotent). Therefore, there
is no non-practical reason to not use a sufficient estimator for stem:[\theta].

WARNING: The proof of the first result is missing. Until then, please do not use it.

.Proof
[%collapsible]
====
By the law of
xref:math3278::conditional-expectation.adoc#_law_of_iterated_expectation[iterated expectation].
[stem]
++++
E(W) = E_U(E(T|U)) = E(T) = m(\theta)
++++
and by using the law of 
xref:math3278::conditional-expectation.adoc#_law_of_iterated_variance[iterated variance].
[stem]
++++
\begin{aligned}
&Var(T) = Var_U(E(T|U)) + E_U(Var(T|U)) = Var(W) +  E_U(Var(T|U))
||& \implies Var(W) = Var(T) - E_U(Var(T|U)) \leq Var(T)
\end{aligned}
++++
Now, notice that stem:[Var(T|U) \geq 0] and hence its expected value must also
be non-negative. Furthermore, notice that stem:[Var(T|U) = 0] iff stem:[U]
completely determines stem:[T]. One instance of this is when stem:[U]
determines stem:[\theta] which in turn determines stem:[m(\theta)] which determines
stem:[T]. That is, we get equality when stem:[T] is determined by stem:[m(\theta)].
====

REMARK: Non-practical. Very-important that we note that computing stem:[W]
is itself a challenge.

== Lehmann-scheffe Theorem
If stem:[U] is complete sufficient statistic for stem:[\theta]
and stem:[W=g(U)] is an unbiased estimator for stem:[m(\theta)],
then W is the unique minimal variance unbiased estimator for stem:[m(\theta)]

=== Corollary: Rao-Blackwellization yeilds unique MVUE
If stem:[U] is a complete sufficient statistic for stem:[\theta]
and stem:[T] is an unbiased estimator for stem:[m(\theta)]
then stem:[W = E(T|U)] is the unique MVUE for stem:[m(\theta)].
