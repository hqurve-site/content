= Properties of Statistics


== Sufficiency
Let stem:[T] be a statistic based on the sample stem:[X_1, \ldots X_n] from a population with parameter stem:[\theta].
Then stem:[T] is _sufficient_ for stem:[\theta] if it contains all the information from the sample which is
necessary to estimate stem:[\theta]. That is, the distribution of stem:[X_1, \ldots X_n] given
some specified value of stem:[T] does not depend on stem:[\theta].

IMPORTANT: stem:[T] itself is not an estimator for stem:[\theta] but rather contains all the information necessary
to estimate stem:[\theta]. In other words, any estimator for stem:[\theta] can be constructed using stem:[T]
instead of the sample itself. Then immediately, if stem:[T] is sufficient, any bijective mapping applied to stem:[T]
is also sufficient. Additionally, it is trivially true that the sample itself is sufficient for estimating the parameter.

=== Fisher's factorization Criterion
REMARK: Yes, xref:math2276::block-designs#_fishers_inequality[that] Fisher.

____
A statistic stem:[\vec{T}] is sufficient for stem:[\vec{\theta}] iff the joint pdf of stem:[X_1, \ldots X_n]
can be written as
[stem]
++++
f(\vec{x}; \vec\theta) = u(\vec{x})v(\vec{T};\vec{\theta})
++++
____

For understanding, stem:[v] can be seen as the joint pdf of stem:[\vec{T}] and stem:[u] is the conditional
pdf of stem:[\vec{x}] given stem:[\vec{T}]. (Note that stem:[u] and stem:[v] need not be these functions,
but if they are, this works).


=== Exponential Family of Distributions
Let stem:[X] be a random variable with pdf stem:[f] which depends on parameters stem:[\vec{\theta}].
Then stem:[X] belongs to the _exponential family_ of distributions if 
[stem]
++++
f(x; \vec{\theta}) = a(\vec{\theta})b(x)e^{\vec{c}(\vec{\theta})\cdot \vec{d}(x)}
++++
where stem:[c] and stem:[d] are functions which output vectors with the same length as stem:[\theta].

CAUTION: stem:[x] is a single random variable.

Then, if we have a random sample of stem:[\{X_i\}], we can immediately see that the joint pdf is
[stem]
++++
\begin{aligned}
f(\vec{x};\vec{\theta})
&= \prod_{i=1}^n a(\vec\theta)b(x_i)e^{\vec{c}(\vec{\theta}) \cdot \vec{d}(x_i)}
\\&=  a(\vec\theta)^n \left(\prod_{i=1}^n b(x_i)\right) \exp\left\{\sum_{i=1}^n\vec{c}(\vec{\theta}) \cdot \vec{d}(x_i)\right\}
\\&=  \left(\prod_{i=1}^n b(x_i)\right) a(\vec\theta)^n \exp\left\{\vec{c}(\vec{\theta})\cdot \sum_{i=1}^n \vec{d}(x_i)\right\}
\end{aligned}
++++
Then, by comparing by the Fisher factorization criterion, we see that if we let stem:[T = \sum_{i=1}^n \vec{d}(x_i)],
we obtain a sufficient vector of statistics for stem:[\vec\theta].


==== Maximum likelihood estimator
TIP: See more xref:point-estimators:construction#_maximum_likelihood_estimator[here].

By taking the logarithm of the above function, we get that our log
like-likelihood function is
[stem]
++++
L(\vec{x}; \vec{\theta}) = \ln\left(\prod_{i=1}^n b(x_i)\right) + n\ln(a(\vec{\theta})) + \vec{c}(\vec{\theta})\cdot \vec{T}
++++
And we see that stem:[\vec{T}] is indeed sufficient since all the information needed to estimate stem:[\vec{\theta}] is given by stem:[\vec{T}].
And if we take derivatives, we see that we need to solve
[stem]
++++
\frac{na'(\vec{\theta})}{a(\vec{\theta})} + \vec{c}'(\vec{\theta})\cdot\vec{T} = 0
++++
where stem:[\vec{c}'] is the element-wise derivative since we are just using it to avoid summation formulae.

==== Examples
The following distributions are exponential

[#_exp_bernoulli]
Bernoulli::
stem:[
f(x; p)
= p^x(1-p)^{1-x}
= (1-p)e^{x\ln(p) -x \ln(1-p)}
= (1-p)e^{x(\ln(p) - \ln(1-p))}
]
+
Therefore a sufficient statistic for stem:[p] is stem:[\sum X_i]


[#_exp_binomial]
Binomial (with stem:[n] known)::
stem:[
f(x; p)
= \binom{n}{x} p^{x}(1-p)^{n-x}
= \binom{n}{x} (1-p)^n e^{x\ln(p) - x\ln(1-p)}
= \binom{n}{x} (1-p)^n e^{x(\ln(p) - \ln(1-p))}
]
+
Therefore a sufficient statistic for stem:[p] is stem:[\sum X_i]

[#_exp_multinomial]
Multinomial (with stem:[n] and stem:[m] known):: {empty}
+
[stem]
++++
\begin{aligned}
&f(x_1, \ldots x_m; a_1,\ldots a_m)
\\&= \binom{n}{x_1, \ldots x_m} \prod_{i=1}^m a_i^{x_i}
\\&= \binom{n}{x_1, \ldots x_{m-1}, \left(n-\sum_{i=1}^{m-1} x_i\right) } \left(\prod_{i=1}^{m-1} a_i^{x_i}\right) \left(1-\sum_{i=1}^{m-1} a_i\right)^{n-\sum_{i=1}^{m-1} x_i}
\\&= \binom{n}{x_1, \ldots x_{m-1}, \left(n-\sum_{i=1}^{m-1} x_i\right)} \exp\left\{\sum_{i=1}^{m-1} x_i\ln(a_i) - \sum_{i=1}^{m-1}x_i\ln\left(1-\sum_{i=1}^{m-1} a_i\right) \right\} \left(1-\sum_{i=1}^{m-1} a_i\right)^n
\\&= \binom{n}{x_1, \ldots x_{m-1}, \left(n-\sum_{i=1}^{m-1} x_i\right)}\left(1-\sum_{i=1}^{m-1} a_i\right)^n \exp\left\{\sum_{i=1}^{m-1} x_i\left(\ln(a_i) - \ln\left(1-\sum_{j=1}^{m-1} a_j\right)\right) \right\} 
\end{aligned}
++++
+
Therefore a sufficient statistic for stem:[(a_1, \ldots a_{m-1})] is stem:[\left(\sum X_{1j}, \sum X_{2j}, \ldots \sum X_{(m-1)j}\right)] where stem:[X_{ij}]
is the stem:[j]'th sample of the stem:[i]'th type.

[#_exp_poisson]
Poisson::
stem:[
f(x; \lambda)
= \frac{e^{-\lambda} \lambda^x}{x!}
= \frac{1}{x!}e^{-\lambda} e^{x\ln(\lambda)}
]
+
Therefore a sufficient statistic for stem:[\lambda] is stem:[\sum X_i]

[#_exp_normal]
Normal::
stem:[
f(x; \mu, \sigma^2)
= \frac{1}{\sqrt{2\pi\sigma^2}}e^{\frac{-(x-\mu)^2}{2\sigma^2}}
= \frac{1}{\sqrt{2\pi\sigma^2}}\exp\left\{\frac{-\mu^2}{2\sigma^2}\right\} \exp\left\{-\frac{1}{2\sigma^2}x^2 + \frac{\mu}{\sigma^2}x\right\}
]
+
Therefore a sufficient statistic for stem:[(\mu, \sigma^2)] is stem:[\left(\sum X_i, \sum X_i^2\right)].

[#_exp_gamma]
Gamma::
stem:[
f(x; \alpha, \beta)
= \frac{1}{\beta^\alpha \Gamma(\alpha)}x^{\alpha-1} e^{\frac{-x}{\beta}}
= \frac{1}{\beta^\alpha \Gamma(\alpha)} \exp\left\{\ln(x)(\alpha-1) - x\frac{1}{\beta}\right\}
]
+
Therefore a sufficient statistic for stem:[(\alpha, \beta)] is stem:[\left(\sum \ln X_i, \sum X_i\right)] then by using
logarithm rules, we see that an equivalent sufficient statistic is stem:[\left(\prod X_i, \sum X_i\right)]

[#_exp_beta]
Beta::
stem:[
f(x; \alpha, \beta)
= \frac{\Gamma(\alpha + \beta)}{\Gamma(\alpha)\Gamma(\beta)} x^{\alpha-1}(1-x)^{\beta - 1}
= \frac{\Gamma(\alpha + \beta)}{\Gamma(\alpha)\Gamma(\beta)} \exp\left\{(\alpha-1)\ln(x) + (\beta-1)\ln(1-x)\right\}
]
+
Therefore a sufficient statistic for stem:[(\alpha, \beta)] is stem:[\left(\sum \ln X_i, \sum \ln(1-X_i)\right)] then by using
logarithm rules, we see that an equivalent sufficient statistic is stem:[\left(\prod X_i, \prod (1-X_i)\right)]


REMARK: I am not including the support into the above functions. But we could 
simply multiply stem:[b(x)] by stem:[\mathbb{1}_I(x)] where stem:[I] is the support.

== Ancillary
Let stem:[T] be a statistic based on the sample stem:[X_1, \ldots X_n] from a population with
parameter stem:[\theta]. Then, stem:[T] is an _ancillary statistic_ if the distribution of stem:[T]
does not depend on stem:[\theta].

Therefore, if stem:[S] is sufficient for stem:[\theta], the distribution of stem:[T] given
a value for stem:[S] is an ancillary statistic.


== Complete
Let stem:[T] be a statistic based on the sample stem:[X_1, \ldots X_n] from a population with
parameter stem:[\theta]. Then, stem:[T] is a _complete statistic_ if for any function, stem:[h]
[stem]
++++
[\forall \theta:  E(h(T)) = 0] \implies h(t) = 0
++++

=== Basu's theorem on complete sufficient statistics and ancillary statistics
If stem:[U] is a complete sufficient statistic for stem:[\theta] and stem:[W]
is ancillary for stem:[\theta], then stem:[U] and stem:[W] are independent.


