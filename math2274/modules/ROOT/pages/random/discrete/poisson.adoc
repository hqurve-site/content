= Poisson Distribution

A _poisson experiment_ consists of counting the number of occurrences of
an event within a fixed space (and or time), given some average rate of
occurrence stem:[\lambda]. Let stem:[X] be the number of
events that occur in a specified unit of space and or time. Then

[stem]
++++
X \sim Poisson(\lambda)
++++
if the following hold

* The number of events in two disjoint intervals are independent
* The probability of an event occurring in a small interval is
proportional to the length of the interval.

Furthermore, the probability function of stem:[X] is

[stem]
++++
f(x) = P(X = x) = \frac{e^{-\lambda}\lambda^x}{x!}
++++
for stem:[x = 0, 1, 2, \ldots]

== Moments

[stem]
++++
\begin{aligned}
    E[X^r]
    &= \sum_{x =0}^{\infty} x^r\frac{e^{-\lambda}\lambda^x}{x!}\\
    &= \sum_{x =1}^{\infty} x^r\frac{e^{-\lambda}\lambda^x}{x!}\\
    &= \lambda \sum_{x =1}^{\infty} x^{r-1}\frac{ e^{-\lambda} \lambda^{x-1}}{(x-1)!}\\
    &= \lambda \sum_{x =0}^{\infty} (x+1)^{r-1}\frac{ e^{-\lambda} \lambda^{x}}{x!}\\
    &= \lambda E[(X+1)^{r-1}]\end{aligned}
++++

== Expectation

[stem]
++++
E[X] = E[X^1] = \lambda E[(X+1)^0] = \lambda
++++

== Variance

[stem]
++++
E[X^2] = \lambda E[(X+1)^1] = \lambda (E[X] + 1) = \lambda(\lambda + 1)
++++
Therefore

[stem]
++++
Var[X] = E[X^2] - E[X]^2 = \lambda(\lambda + 1) - \lambda^2 = \lambda
++++

== Moment Generating Function

[stem]
++++
\begin{aligned}
    M_X(t)
    &= E[e^{Xt}]\\
    &= \sum_{x=0}^\infty e^{xt} \frac{e^{-\lambda}\lambda^x}{x!}\\
    &= e^{-\lambda}\sum_{x=0}^\infty \frac{(\lambda e^t)^x}{x!}\\
    &= e^{-\lambda}e^{\lambda e^t}\\
    &= \exp\left( \lambda e^t - \lambda \right)\end{aligned}
++++


