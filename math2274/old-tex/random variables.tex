%2020-09-30
\section{Random Variables}
In order to easier deal with data from an experiment, the outcomes are transformed based on
various properties into either categories or numerical values. Furthermore, these categories
can be encoded as numerical values which are usually a subset of the real numbers. This 
transformation or mapping is referred to as \emph{random variable}; that is, a random
variable is a function which maps the outcomes of a statistical experiment onto the set 
of real numbers (usually a subset).

\paragraph*{Side note} Apparently a random variable really is a function which maps to
any measurable set, not necessarily the reals. However, for this course, it will only be
mapped to the reals.

\subsection{Types of random variables}
The approach used to analyze random variables is typically based on the range of the random variable.
Typically, if the range is countable, ie either finite or countably infinite, the random variable is referred 
to as \emph{discrete}. Otherwise, if the range is uncountably infinite, it is called \emph{continuous}.
However, this classification is not strictly correct since there are random variables which
can be considered both discrete and continuous. Nevertheless, both types will be discussed simultaneously.

\subsection{Probability mass/density function}
Let $X$ be a random variable. 

\subsubsection{Discrete Random Variable}
If $X$ is discrete, it has a probability (mass) function, $f$, defined by
\[f(x) = P(X = x)\]
and hence has the properties that
\begin{enumerate}[label=\roman*)]
    \item $0 \leq f(x) \leq 1$ forall $x$ in the range of $X$.
    \item $\displaystyle\sum_x f(x) = 1$
\end{enumerate}

\subsubsection{Continuous Random Variable}
If $X$ is continuous, it has a probability density function, $f$, such that for all $a$ and $b$
\[P(a < X \leq b) = \int_a^b f(t) dt\]
Notice that there is little concern whether or not either of the end points are strict 
since the endpoints of an integral can be considered as limits. Furthermore, 
$f$ has the following properties
\begin{enumerate}[label=\roman*)]
    \item $f(x) \geq 0 \; \forall x \in \mathbb{R}$
    \item $\int_{-\infty}^{\infty} f(x) dx = 1$
\end{enumerate}
Notice that unlike in the discrete case, the probability density function is not a probability.





\subsection{Probability distribution function}
Let $X$ be a random variable, then the probability distribution function (or cumulative distribution function), 
$F$, completely defines $X$ where $F(x)$ is defined as
\[F(x) = P(X \leq x)\]

\subsubsection{Properties}
If $F$ is a valid pdf, it has the following properties
\begin{enumerate}[label=\roman*)]
    \item $0 \leq F(x) \leq 1 \quad \forall x\in \mathbb{R}$ 
    \item $F(x)$ is monotonic increasing. That is 
            $$ \forall\, x, y\in \mathbb{R}: x < y \implies F(x) \leq F(y)$$
    \item $\displaystyle\lim_{x\rightarrow -\infty} F(x) = 0$ and $\displaystyle\lim_{x\rightarrow +\infty} F(x) = 1$
    \item $F(x)$ is right continuous . That is
            $$\forall a\in \mathbb{R}: F(a) = \lim_{x\rightarrow a^+} F(x)$$
\end{enumerate}
Notice that all these properties can be established using the axioms of probability.

\subsubsection{Computation}
The probability distribution function is computed slightly differently for the discrete
and continuous cases as follows
\begin{multicols}{2}
    \centering
    \noindent
    \[F(x) = \sum_{t \leq x} f(t)\]
    Discrete Random Variable

    \noindent
    \[F(x) = \int_{-\infty}^x f(t) dt\]
    Continuous Random Variable
\end{multicols}

\subsubsection{Note about bounds for continuous random variables}
It is easily seen that for a continuous random variable, $P(X = x) = 0$ for all $x \in \mathbb{R}$.
Therefore, the probabilities of open and closed intervals are the same. That is
\[
    P(a < X \leq b) = P(a \leq X < b) = P(a \leq X \leq b) = P(a < X < b) 
\]

\subsection{Expectation and Variance}
Let $X$ be a random variable then its \emph{expected value} or \emph{mean}
, $E[X]$ or $\mu_X$, is defined as
\begin{multicols}{2}\centering
    \noindent
    \[E[X] = \sum_{t} tf(t)\]
    Discrete Random Variable

    \noindent
    \[E[X] = \int_{-\infty}^\infty tf(t) dt\]
    Continuous Random Variable
\end{multicols}

The expected value of a random variable $X$ can be thought of as a `typical value' of $X$
and is a sort of weighted average. Notice, that the expected value doesn't
always exist or is finite.

\subsubsection{Equivalent formulation for random variables with integer values}
If $X$ is a discrete random variable taking integer values, then its expectation can be written as 
\begin{align*}
    E[X] 
    &= \sum_{x=-\infty}^\infty xP (X = x)\\
    &= \sum_{x=1}^\infty xP (X = x) + \sum_{x=1}^{\infty} (-x) P (X = -x)\\
    &= \sum_{x=1}^\infty \sum_{k=1}^x P (X = x) - \sum_{x=1}^{\infty} \sum_{k=1}^{x} P (X = -x)\\
    &= \sum_{k=1}^\infty \sum_{x=k}^\infty P (X = x) - \sum_{k=1}^{\infty} \sum_{x=k}^{\infty} P (X = -x)\\
    &= \sum_{k=1}^\infty P(X \geq k) - \sum_{k=1}^{\infty} P(X \leq -k)\\
    &= \sum_{k=1}^\infty \brack{P(X \geq k) - P(X \leq -k)}
\end{align*}
\paragraph*{Note} an analogous formulation holds for continuous random variables using almost the same steps.

\subsubsection{Expectation of a function}
If $g$ is a real-valued function, then the expectation of $g(X)$ is 
\begin{multicols}{2}\centering
    \noindent
    \[E[g(X)] = \sum_{t} g(t)f(t)\]
    Discrete Random Variable
    
    \noindent
    \[E[g(X)] = \int_{-\infty}^\infty g(t)f(t) dt\]
    Continuous Random Variable
\end{multicols}

This is useful when the a transformation of the values of a random variable is required.
Note that expectation of a function can be thought of function composition of $g$ with $X$ since $X$ is itself a function.
Furthermore, $g(X)$ is indeed, in its own right, a random variable.
\paragraph*{Remark:} Apparently, this result is not merely a definition but is infact a theorem called
``The Law of the Unconscious Statistician'' and arises from the fact that $g(X)$ is a random variable.
The law is not hard to prove but I will leave it out.

\subsubsection{Linearity of the expectation}
The expectation is indeed a linear transformation from the function space
$\{g:\mathbb{R} \rightarrow \mathbb{R}\}$ to the reals, $\mathbb{R}$. That is,
the expectation satisfies the following properties
\begin{enumerate}
    \item $E[g(X) + h(X)] = E[g(X)] + E[h(X)]$
    \item $E[a\, g(X)] = aE[g(X)]$
\end{enumerate}
These properties follow from the fact that summation/integral is also a linear transformation. Additionally, 
since the total probability of the sample space is $1$ (which is a constant function), then 
\[E[1] =  1\]
Together these three properties help simplify the computation of complicated expectations.




\subsubsection{Moments}
The $r$'th moment of $X$ is given by
\[\mu_r = E[X^r]\]
this will be useful later.

\subsubsection{Fair games}
A game, whose winnings follow the random variable $X$, is \emph{fair} if 
\[E[X] = c\]
where $c$ is the cost to play a game.

\subsubsection{Variance}
If $X$ has a finite mean and second moment
then the \emph{variance} of $X$ denoted $Var[X]$ is given by
\[\sigma^2 = Var[X] = E[(X - \mu)^2]\]
and can be simplified as follows
\begin{align*}
    Var[X] 
    &= E[(X - \mu)^2]\\
    &= E[X^2 - 2\mu X + \mu^2]\\
    &= E[X^2] + E[-2\mu X] + E[\mu^2]\\
    &= E[X^2] -2\mu E[X] + \mu^2\\
    &= E[X^2] - E[X]^2
\end{align*}
Note that the latter formula is much simpler to use in practice. However, the first
supplies intuition as to what the variance means.

In fact, the variance is a measure of how ``spread-out'' the range of $X$ is, or how much
$X$ deviates from the mean.

\paragraph*{Note} Unlike the expectation, variance is not a linear operator. Nevertheless, it
exhibits the following Properties
\begin{enumerate}[label=\roman*)]
    \item $Var[a] = E[(a - E[a])^2] = E[0] = 0$
    \item $Var[X + a] = E[(X +a - E[X + a])^2] = E[(X - E[X])^2] = Var[X]$
    \item $Var[aX] = E[(aX)^2] - E[aX]^2 = a^2E[X] - a^2E[X]^2 = a^2Var[X]$
\end{enumerate}
Note that the previous properties were stated in terms of a basic random variable, $X$, with no function applied. But since 
$g(X)$ is also a random variable, as previously discussed, the previous properties also apply to it.



\subsection{Percentiles}
The $p$th percentile, $\theta_p$, of $X$ is defined by 
\[F(\theta_p) = P(X \leq \theta_p) = \frac{p}{100}\]
where $p \in [0, 100] \subset \mathbb{R}$. The percentile can be thought of a sort
of inverse for the distribution function (although multiplying by $100$).

\paragraph*{Median} The median of $X$ is defined as the $50$th percentile. Note, that for symmetric
distributions (distributions symmetric about their mean), the median is equal to the mean.

\subsection{Transformation of random variables}
This is pretty basic and I don't really want to go through it. Basically it amounts to finding the range and pre-image (not necessarily inverse)
of the transformed variable.

\subsubsection{Continuous case}
If $X$ and $Y$ are continuous random variables such that $Y = g(X)$
where $g$ is bijective and differentiable (both almost everywhere), then the probability
density function of $Y$ is 
\[
    f(g^{-1}(y)) |J_{g^{-1}}|
\]
where $f$ is the probability density function of $X$ and $J_{g^{-1}}$ is
the jacobian of transformation $g^{-1}$.
\paragraph*{Remark} I'm pretty sure this follows directly from the fundamental theorem of calculus.
