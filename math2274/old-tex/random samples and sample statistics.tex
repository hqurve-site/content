\section{Random Samples and Sample Statistics}

\subsection{Preface}
Sampling is a technique used to obtain information about a population by
extrapolating results from a subset of the population. It is important to 
ensure that gathered information is accurate or better \emph{representative}
of the entire population and hence a \emph{random sample} must be taken.
From such a sample, the information obtained can be condensed into
\emph{sample statistics} which are then used to gather information
about the population.

\subsubsection{Assumptions for this section}
The theory of sampling is complex and hard to understand without
first understanding ideal cases. Hence, in this section, the sample
would be taken an infinite population otherwise, the random 
variables from the sample would not be independent despite being 
identically distributed. 

The explanation for this is quite simple. Although the each of the distributions
are random, the information about the population as a whole is fixed.
Therefore, knowledge of one random variable affects possible knowledge
about another random variable. That is, the probability of the conditional
is not equal to the probability of the unconditional.


\subsubsection{Random Sample from a distribution}
A \emph{random sample} of size $n$ from a population with distribution $F$
is a sequence $\{X_i\}$ (or $n$-tuple) of independent and identically
distributed random variables such that $X_i \sim F$.

\subsection{Sample Statistic}
A \emph{sample statistic} of a sample with size $n$ is simply
a function $T$ (or transformation) of the $\{X_i\}$. Note that 
$T$ is itself a random variable and is used to summarize information about
the sample and its distribution is referred to as a \emph{sampling distribution}
as it is the distribution of the function $T$ applied to each of the samples. 

\paragraph*{Note} there are no restrictions on the 
function $T$ and hence the information obtained from such a transformation
is sometimes useless but are regardless valid.

\subsubsection{Unbiased Estimators}
A sample statistic, $\beta^*$, of a population statistic, $\beta$, 
is called \emph{unbiased} if the expectation of the sample statistic is the 
population statistic. That is
\[
    E[\beta^*] = \beta
\]

\subsection{Sample mean}
The average of a sample of $n$ samples $\{X_i\}$ is called the \emph{sample mean}
and is defined as 
\[
    \bar{X}_n = \frac{1}{n} \sum_{i=1}^n X_i
\]

\subsubsection{Mean}
Let the population mean be $\mu$, then
\begin{align*}
    E\brack{\bar{X}}
    = \frac{1}{n}E\brack{\sum_{i=1}^n X_i}
    = \frac{1}{n}\sum_{i=1}^nE\brack{ X_i}
    = \frac{1}{n}\sum_{i=1}^n \mu
    = \frac{1}{n} n \mu
    =\mu
\end{align*}
Therefore $\bar{X}$ is an unbiased estimator for the population mean.
\subsubsection{Variance}
Let the population mean and variance be $\mu$ and $\sigma^2$ respectively,
then
\begin{align*}
    Var\brack{\bar{X}}
    = \frac{1}{n^2}Var\brack{\sum_{i=1}^n X_i}
    = \frac{1}{n^2}\brack{\sum_{i=1}^n Var\brack{ X_i} + \sum_{i < j} Cov\brack{X_i, X_j}}
    = \frac{1}{n^2}\sum_{i=1}^n \sigma^2
    % = \frac{1}{n^2}n \sigma^2
    = \frac{\sigma^2}{n}
\end{align*}
Therefore, as the sample size increases, $\bar{X}$ becomes a better and better estimator
for the population mean.

\subsection{Sample Variance}
The \emph{sample variance} of $n$ samples $\{X_i\}$ is defined as 
\[
    S^2_n = \frac{1}{n-1}\sum_{i=1}^n{\paren{X_i - \bar{X}_n}^2}
\]
where $\bar{X}_n$ is the sample mean. Additionally, the above expression
can be simplified as follows
\begin{align*}
    S^2_n 
    &= \frac{1}{n-1}\sum_{i=1}^n\paren{X_i - \bar{X}_n}^2\\
    &= \frac{1}{n-1}\sum_{i=1}^n\brack{X_i^2 - 2X_i\bar{X}_n + \bar{X}_n^2}\\
    &= \frac{1}{n-1}\brack{\sum_{i=1}^n X_i^2 - 2\bar{X}_n\sum_{i=1}^n X_i + n\bar{X}_n^2}\\
    &= \frac{1}{n-1}\brack{\sum_{i=1}^n X_i^2 - 2n\bar{X}_n^2 + n\bar{X}_n^2}\\
    &= \frac{1}{n-1}\brack{\sum_{i=1}^n X_i^2 - n\bar{X}_n^2}
\end{align*}
\paragraph*{Note} the sample variance is not simply the variance of the sample
as the divisor of $(n-1)$ is used instead of $n$. This is to ensure
that the sample variance is an unbiased estimate for the population
variance.

\subsubsection{Mean}
Let the population mean and variance be $\mu$ and $\sigma^2$ respectively,
then
\begin{align*}
    E\brack{S^2_n}
    &= \frac{1}{n-1} E\brack{\sum_{i=1}^n X_i^2 - n\bar{X}_n^2}\\
    &= \frac{1}{n-1} \brack{\sum_{i=1}^n E\brack{ X_i^2} - nE\brack{\bar{X}_n^2}}\\
    &= \frac{1}{n-1} \brack{\sum_{i=1}^n (\sigma^2 + \mu^2) - n\paren{\frac{\sigma^2}{n} + \mu^2}}\\
    &= \frac{1}{n-1} \brack{n(\sigma^2 + \mu^2) - \paren{\sigma^2 + n\mu^2}}\\
    &= \frac{1}{n-1} (n-1)\sigma^2\\
    &= \sigma^2
\end{align*}
Therefore, $S^2_n$ is an unbiased estimator for the population variance.


\subsection{Sample mean and variance from normal distribution}
If each of the $\{X_i\}$ of the sample are independent and identically distributed
with distribution $N(\mu, \sigma^2)$, then
\[
    \bar{X}_n \sim N\paren{\mu, \frac{\sigma^2}{n}}
\]
and 
\[
    \frac{(n-1)S^2_n}{\sigma^2} \sim \chi^2_{n-1}
\]
The first of these two facts are clear to see however the later is has not yet been 
discussed and requires the use of MGFs to easily prove. Furthermore, the
two sample statistics are independent, that is
\[S^2_n \perp \bar{X}_n\]
\paragraph*{Remark} I have no idea why this is true or how to easily prove it at this point.

\subsubsection{Variance of sample variance}
In this spacial case, the variance of $S_n^2$ can be written as
\[
    Var\brack{S_n^2} = \frac{2\sigma^4}{n-1}
\]
since we know that $Var[\chi^2_{n-1}] = 2(n-1)$.


\subsection{Central Limit theorem}
The \emph{central limit theorem} (CLT) allows for the approximation of the distribution
of the sample mean of independent and identically distributed random variables. The theorem
is stated below.

\begin{center}
\begin{minipage}{0.8\linewidth}
    Let $\{X_i\}_1^n$ be a sequence of iid random variables with finite mean $\mu$
    and variance $\sigma^2$. Let $\bar{X}_n = \frac{1}{n}\sum_{i=1}^n X_i$, then
    \[
        \frac{\bar{X}_n - \mu}{\sigma / \sqrt{n}} \xrightarrow{d} Z
        \quad\quad\text{where } Z \sim N(0,1)
    \]
    That is, $\frac{\bar{X}_n - \mu}{\sigma / \sqrt{n}}$ converges in distribution to the 
    standard normal.
\end{minipage}
\end{center}
It should be noted that the CLT does not require that the random variables have a normal distribution,
in fact, its main appeal is that it does not have any constraints on the type of distribution of
each of the $X_i$.

\subsubsection{Corollaries}
Instead of stating that $\frac{\bar{X}_n - \mu}{\sigma / \sqrt{n}} \xrightarrow{d} Z$,
the central limit theorem applied as to prove that
\[\bar{X}_n \approx N\paren{\mu, \frac{\sigma^2}{n}}\]
or that
\[\sum_{i=1}^n X_i \approx N\paren{n\mu, n\sigma^2}\]
for ``sufficiently large $n$.''

\subsubsection{Remark on Binomial approximation to normal}
The binomial approximation to the normal relies on the CLT. As the binomial
distribution is just the sum of idd Bernoulli RVs, it is clear that for sufficiently
large $n$,
\[
    X \approx N\paren{np, np(1-p)}
\]
where $X \sim Binomial(n, p)$.

\subsubsection{Convergence in distribution}
Let $\{Y_i\}$ be a infinite sequence of random variables where each of the $Y_i$
have probability function $F_i$.
Let $Y$ be a random variable with probability function $F$, 
then $\{Y_i\}$ converges to distribution to $Y$ iff
\[
    \lim_{n\to \infty} F_i(x) = F(x)
\]
for all $x \in \mathbb{R}$ at which $F$ is continuous.