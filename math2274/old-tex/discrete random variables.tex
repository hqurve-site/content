%2020-09-30
%2020-10-02
%2020-10-07
\section{Discrete Random Variables}
Let $X$ be a discrete random variable. Then 
\begin{enumerate}[label=(\alph*)]
    \item The probability function (or probability mass function), $f$, of $X$ is the function
            $$f(x) = P(X = x) \; \text{ for each $x$ in the range of $X$}$$
    \item The distribution function (or cumulative distribution function), $F$, of $X$ is the function
        
        \begin{align*}
            F(x) &= P(X \leq x) \; \text{ for each $x$ in the range of $X$}\\
                 &= \sum_{t \leq x} f(t)
        \end{align*}
\end{enumerate}

\subsection{Properties of the probability and distribution functions}
\paragraph*{Probability function}
\begin{enumerate}[label=\roman*)]
    \item $0 \leq f(x) \leq 1$ forall $x$ in the range of $X$.
    \item $\displaystyle\sum_x f(x) = 1$
\end{enumerate}
\paragraph*{Distribution function}
\begin{enumerate}[label=\roman*)]
    \item $0 \leq F(x) \leq 1 \quad \forall x\in \mathbb{R}$ 
    \item $F(x)$ is monotonic increasing. That is 
            $$ \forall\, x, y\in \mathbb{R}: x < y \implies F(x) \leq F(y)$$
    \item $\displaystyle\lim_{x\rightarrow -\infty} F(x) = 0$ and $\displaystyle\lim_{x\rightarrow +\infty} F(x) = 1$
    \item $F(x)$ is right continuous . That is
            $$\forall a\in \mathbb{R}: F(a) = \lim_{x\rightarrow a^+} F(x)$$
\end{enumerate}
Notice that all these properties can be established using the axioms of probability.


\subsection{Expected value}
Let $X$ be a discrete random variable with pf. $f$. Then, the \emph{expected value} or \emph{mean} of $X$ is
$$E[X] = \mu_X = \sum_{x} xf(x)$$
The expected value can be thought of as a `typical value' of $X$ and is a sort of weighted average. Also, note that
the expectation need not exist. In this case, it is said that the expectation does not exist or is infinite.

\subsubsection{Expectation of a function}
If $g$ is a real-valued function then the expectation of $g(X)$ is
$$E[g(X)] = \sum_{x} g(x)f(x)$$
This is useful when the a transformation of the values of a random variable is required.
Note that expectation of a function can be thought of function composition of $g$ with $X$ since $X$ is itself a function.
Furthermore, $g(X)$ is indeed, in its own right, a random variable.

\subsubsection{Properties}
The expectation can be though of a linear transformation from a function space $\{g:\mathbb{R} \rightarrow \mathbb{R}\}$
to the reals, $\mathbb{R}$. Meaning that the expectation satisfies the following
\begin{enumerate}
    \item $E[g(X) + h(X)] = E[g(X)] + E[h(X)]$
    \item $E[a\, g(X)] = aE[g(X)]$
\end{enumerate}
These properties follow from the fact that summation is also a linear transformation. Additionally, since
the total probability of the sample space is $1$ (which is a constant function), then 
$$E[1] = \sum_{x} f(x) = 1$$
Together these three properties help simplify the computation of complicated expectations.

\subsubsection{Moments}
The $r$'th moment of $X$ is given by
$$\mu_r = E[X^r]$$
this will be useful later.

\subsubsection{Fair games}
A game, whose winnings follow the random variable $X$, is \emph{fair} if 
$$E[X] = c$$
where $c$ is the cost to play a game.

\subsection{Variance}
Let $X$ be a discrete random variable with pf $f$ with finite mean and second moment. 
Then the \emph{variance} of $X$ denoted $Var[X]$ is given by
$$\sigma^2 = Var[X] = E[(X - \mu)^2] = \sum_{x} (x - \mu)^2f(x)$$
and can be simplified as follows
\begin{align*}
    Var[X] 
    &= E[(X - \mu)^2]\\
    &= E[X^2 - 2\mu X + \mu^2]\\
    &= E[X^2] + E[-2\mu X] + E[\mu^2]\\
    &= E[X^2] -2\mu E[X] + \mu^2\\
    &= E[X^2] - E[X]^2
\end{align*}
Note that the later formula is much simpler to use in practice. However, the first
supplies intuition as to what the variance means.

In fact, the variance is a measure of how ``spread-out'' the range of $X$ is, or how much
$X$ deviates from the mean.

\subsubsection{Variance of a function}
Likewise to the expectation, the variance of mappings of $X$ through function $g$ can be calculated as follows
$$Var[g(X)] = E[(g(X) - E[g(X)])^2] = \sum_{x} (g(x) - E[g(X)])^2f(x)$$
Note that the nested expectation resolves to a constant real value. Additionally, it can be expressed as 
$$Var[g(X)] = E[g(X)^2] - E[g(X)]^2 = \sum_{x} g(x)^2f(x) - \left(\sum_x g(x)f(x) \right)^2$$

\subsubsection{Properties}
Unlike the expectation however, variance is not a linear map. Despite this, there are several nice properties
\begin{enumerate}[label=\roman*)]
    \item $Var[a] = E[(a - E[a])^2] = E[0] = 0$
    \item $Var[X + a] = E[(X +a - E[X + a])^2] = E[(X - E[X])^2] = Var[X]$
    \item $Var[aX] = E[(aX)^2] - E[aX]^2 = a^2E[X] - a^2E[X]^2 = a^2Var[X]$
\end{enumerate}
Note that the previous properties were stated in terms of a basic random variable, $X$, with no function applied. But since 
$g(X)$ is also a random, as previously discussed, the previous properties also apply to it.
