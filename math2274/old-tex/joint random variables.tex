\section{Joint Random Variables}

\subsection{Joint probability mass/density function}
Let $X$ and $Y$ be two random variables.

\subsubsection{Discrete Random Variables}
If $X$ and $Y$ are discrete, their joint probability (mass) function, 
$f_{XY}$, is defined by 
\[
    f_{XY}(x, y) = P(X = x \text{ and } Y =y)
\]
This joint probability function shares the same properties of a single random
variables. That is 
\begin{enumerate}[label=\roman*)]
    \item $0 \leq f_{XY}(x, y) \leq 1$ $\forall (x, y)$.
    \item $\displaystyle\sum_x\sum_y f_{XY}(x,y) = 1$ 
\end{enumerate}

\subsubsection{Continuous Random Variables}
If $X$ and $Y$ are continuous, their joint probability density function is
the function, $f_XY$, such that for all regions $E \subseteq \mathbb{R}^2$
\[
    P[(X, Y) \in E] = \iint_E f_{XY}(x,y) dA
\]
Similarly, this joint probability density function shares the same properties as
the a single random variable. That is
\begin{enumerate}[label=\roman*)]
    \item $f_{XY}(x,y) \geq 0 \; \forall (x, y) \in \mathbb{R}^2$
    \item $\iint_{\mathbb{R}^2} f_{XY}(x, y)\, dA = 1$
\end{enumerate}

\subsection{Probability Distribution Function}
Let $X$ and $Y$ be random variables. Then their joint probability distribution
function, $F_{XY}$, is defined by
\[
    F_{XY}(x, y) = P(X \leq x \text{ and } Y \leq y)
\]
and completely defined the joint distribution of $X$ and $Y$. \\
Notice, this function follows similar properties to those identified in the single variable
case.

\subsection{Marginal Distribution}
The marginal distribution of $X$ is strictly the distribution of $X$
without taking into account any of the other variables. The marginal
distribution is defined as follows
\begin{multicols}{2}
    \centering
    \noindent
    \[f_X(x) = \sum_y f_{XY}(x,y)\]
    Discrete Random Variables

    \noindent
    \[f_X(x) = \int_{\mathbb{R}} f_{XY}(x, y)\, dy\]
    Continuous Random Variables
\end{multicols}
\noindent
Note that the marginal distribution of $Y$ is defined analogously.


\subsection{Conditional Distribution}
The conditional distribution $X$ given $Y=y$ is the distribution
of $X$ given that $Y$ is already known. It is defined by 
\[
    f_{X|Y=y}(x) = \frac{f_{XY}(x,y)}{f_Y(y)}
\]
The formulation is the same in both discrete and continuous cases 
however note that they resolve to probability mass and density 
functions respectively.

\subsection{Independence}
Two random variables $X$ and $Y$ are independent if and only if
\[
    f_{XY} = f_X(x) f_Y(y) \forall(x,y)
\]
Note that pairwise independent does not imply mutual independence.

\subsection{Expectation}
The expectation of a function $g(X, Y)$ is defined as 
\begin{multicols}{2}
    \noindent\centering
    \[E[g(X, Y) = \sum_x\sum_y g(x, y) f_{XY}(x, y)]\]
    Discrete Random Variables
    
    \noindent
    \[
        E[g(X, Y)] = \iint_{\mathbb{R}^2} g(x, y) f_{XY}(x,y) dA
    \]
    Continuous Random Variables
\end{multicols}
\noindent
Similarly to the single variable case, the expectation is a linear 
transformation where $E[1] = 1$.


\subsubsection{Products of independent variables}
If $X \perp Y$ then 
\[
    E[XY] = E[X]E[Y]
\]
In fact this holds for any functions of the random variables. That is
\[
    X\perp Y \implies E[g(X)h(Y)] = E[g(X)]E[h(Y)]
\]
for any two functions $g$ and $h$.

\subsection{Extension to more than 2 random variables}
The above formulations and definitions can be extended to more
than $2$ random variables. The language used describing them were done
in such a way to make this easily apparent.

\subsection{Covariance}
The covariance of two random variables $X$ and $Y$ is a measure of the linear relationship
between $X$ and $Y$. It is defined by
\[
    Cov[X, Y] = \sigma_{XY} = E[(X- E[X])(Y-E[Y])]
\]
Futhermore, it can be simplified as follows
\begin{align*}
    Cov[X, Y] 
    &= E[(X- E[X])(Y-E[Y])]\\
    &= E[XY - E[X]Y - E[Y]X + E[X]E[Y])]\\
    &= E[XY] - E[X]E[Y] - E[Y]E[X] + E[X]E[Y]\\
    &= E[XY] - E[X]E[Y]
\end{align*}
The latter formula is much simpler to used in practice when compared to the definition.

\paragraph*{Note} The covariance (as hinted by its name) is a sort of extension to the concept of variance and
has the following two properties
\begin{enumerate}[label=\roman*)]
    \item $Cov[X, a] = E[(X - E[X])(a - E[a])] = 0$
    \item $\begin{aligned}[t]
            Cov[X + a, Y + b] 
            &= E[(X + a - E[X + a])(Y + b - E[X + b])]\\ 
            &= E[(X- E[X])(Y-E[Y])]\\ 
            &= Cov[X, Y]
        \end{aligned}$
\end{enumerate}

\subsubsection{Covariance as an inner product}
The covariance can be viewed as an inner product over the vector space of random variables. That is, it 
exhibits the following properties
\begin{enumerate}[label=\roman*)]
    \item $Cov[X, X] = Var[X] \geq 0$
    \item $Cov[X, X] = 0 \implies$ $X$ is almost always constant. ie $P(X = E[X]) =1$
    \item $Cov[X, Y] = E[(X - E[X])(Y - E[Y])] = Cov[Y, X]$
    \item $\begin{aligned}[t]
            Cov[aX, Y] 
            &= E[(aX - E[aX])(Y-E[Y])] \\
            &= a E[(X- E[X])(Y - E[Y])]\\ 
            &= aCov[X, Y]
        \end{aligned}$
    \item $\begin{aligned}[t]
            Cov[X + Y, Z] 
            &= E[(X + Y - E[X + Y])(Z - E[Z])]\\ 
            &= E[(X- E[X])(Z - E[Z]) + (Y- E[Y])(Z - E[Z])]\\ 
            &= Cov[X, Z] + Cov[Y, Z]
        \end{aligned}$
\end{enumerate}
Therefore, the covariance exhibits the Cauchy-Schwartz inequality below
\[
    \abs{Cov[X, Y]} \leq \sqrt{Var[X]}\sqrt{Var[Y]} = \sigma_X \sigma_Y
\]
and is additionally bilinear

\subsubsection{Covariance of independent variables}
If $X \perp Y$ then
\[
    Cov[X, Y] = E[XY] - E[X]E[Y] = 0
\]

\subsection{Linear combinations of Random Variables}
If $\{X_i\}$ are a set of jointly distributed random variables, a linear combination of them
is also a random variable. That is,
\[
    X = a_1 X_1 + a_2 X_2 + \cdots + a_n X_n
\]
is a random variable. Then, its expectation and variance are 
easily obtainable and have values of 
\[
    E[X] = a_1 E[X_1] + a_2 E[X_2] + \cdots a_n E[X_n]
\]
\[
    Var[X] = \sum_{i=1}^n a_i^2 Var[X_i] + 2\sum_{i < j} a_ia_j Cov[X_i, X_j]
\]
which follow immediately from the linearity and bilinearity of the expectation
and covariance. 

\subsubsection{Normally distributed random variables}
If each of the $X_i$ are normally distributed (not necessarily independent), then $X$ is also
normally distributed as
\[
    X \sim N(\mu, \sigma^2)
\]
where $\mu = E[X]$ and $\sigma^2 = Var[X]$.