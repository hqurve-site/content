\subsection{Negative Binomial}
A \emph{negative binomial} experiment is a statistical experiment consisting of independent bernoulli trials, each
with probability of success $p$, repeated until $r$ success are obtained. Let $X$ be the number
of trials required to obtain $r$ successes each with probability $p$. Then
\[X \sim NegativeBinomial(r, p)\]
Furthermore, the probability function of $X$ is
\begin{align*}
    f(x) 
    &= P(X = x) \\
    &= P(\text{$x$'th trial is a success and there are $r-1$ successes in the first $x-1$ trials})\\
    &= P(\text{$x$'th trial is a success}) P(\text{exactly $r-1$ success in the first $x-1$ trials})\\
    &= p \binom{x-1}{r-1}p^{r-1}(1-p)^{(x-1)-(r-1)}\\
    &= \binom{x-1}{r-1}p^{r}(1-p)^{x-r}
\end{align*}
for $x = r, r+1, \ldots$. 

Alternatively, a negative binomial distribution is the sum of $r$ independent random variables with a
geometric distribution. This is intuitive since $i$'th of these experiments consists of the the trials
following the $(i-1)$'th success up to the $i$th success. That is the $i$'th sub-experiment counts
the number of trials until the next ($i$'th) success is obtained.
\paragraph*{Notice} In a way, this is the `inverse' of the binomial distribution since the 
number of success are fixed instead of the number of trials.
\paragraph*{Note} Some books define this very differently, but this definition would be the one used in this course.

\subsubsection{Validity of probability function}
% Let
% \[
%     S_r(t) = \sum_{x=0}^\infty \binom{r+x}{r} t^{r+x+1}
% \]
% Then, 
% \begin{align*}
%     S_r(t) 
%     &= t^2 \sum_{x=0}^\infty \binom{r+x}{r} t^{r+x-1}\\
%     &= \frac{t^2}{r} \frac{d}{dt}\brack{\sum_{x=0}^\infty \binom{r+x-1}{r-1} t^{r+x}}\\
%     &= \frac{t^2}{r} \frac{d}{dt}S_{r-1}'(t)
% \end{align*}
% Since $S_0(t) = \frac{t}{1-t}$, we claim that $S_r(t) = \paren{\frac{t}{1-t}}^{r+1}$. Then, by the previous result,
% \[
%     S_r(t) 
%     = \frac{t^2}{r} \frac{d}{dt}S_{r-1}'(t)
%     = \frac{t^2}{r} \frac{d}{dt}\paren{\frac{t}{1-t}}^{r}
%     = \frac{t^2}{r} \cdot r \cdot \frac{1}{(1-t)^2} \paren{\frac{t}{1-t}}^{r-1}
%     =\paren{\frac{t}{1-t}}^{r+1}
% \]
% Alternatively, this formula can be derived as the number of ways to sum $r+1$ positive integers to $r+x+1$
% since $\paren{\frac{t}{1-t}}^{r+1} = \paren{t + t^2 + t^3 + \cdots}^{r+1}$. Either way, it is now clear that
% the probability function forms a valid probability space since
% \begin{align*}
%     \sum_{x=r}^\infty \binom{x-1}{r-1}p^{r}(1-p)^{x-r}
%     &=\sum_{x=0}^\infty \binom{x+r-1}{r-1}p^{r}(1-p)^{x}\\
%     &=\frac{p^{r}}{(1-p)^r}\sum_{x=0}^\infty \binom{x+r-1}{r-1}(1-p)^{x+r}\\
%     &=\frac{p^{r}}{(1-p)^r}S_{r-1}(1-p)\\
%     &=\frac{p^{r}}{(1-p)^r}\paren{\frac{1-p}{1-(1-p)}}^{r}\\
%     &=1
% \end{align*}
Let 
\[
    S_r(t) = \sum_{x=0}^\infty \binom{r+x}{r}t^x
\]
Then
\begin{align*}
    S_r(t)
    &= \sum_{x=0}^\infty \binom{r+x}{r}t^x\\
    &= \frac{1}{t^{r-1}}\sum_{x=0}^\infty \binom{r+x}{r}t^{x+r-1}\\
    &= \frac{1}{rt^{r-1}}\frac{d}{dt}\brack{\sum_{x=0}^\infty \binom{r+x-1}{r-1}t^{x+r}}\\
    &= \frac{1}{rt^{r-1}}\frac{d}{dt}\brack{t^rS_{r-1}(t)}
\end{align*}
Notice that $S_0(t) = \frac{1}{1-t}$ and hence we claim that $S_r(t) = \frac{1}{(1-t)^{r+1}}$ whenever $|t| < 1$. Then
\[
    S_r(t) 
    = \frac{1}{rt^{r-1}}\frac{d}{dt}\brack{t^rS_{r-1}(t)}
    = \frac{1}{rt^{r-1}}\frac{d}{dt}\paren{\frac{t}{1-t}}^r
    = \frac{1}{rt^{r-1}}\cdot r \cdot \frac{1}{(1-t)^2} \cdot\paren{\frac{t}{1-t}}^{r-1}
    = \frac{1}{(1-t)^{r+1}}
\]
Alternatively, this formula can be derived as the number of ways to sum $r+1$ non-negative integers to $x$
since $\frac{1}{(1-t)^{r+1}} = \paren{1 + t+  t^2 + t^3 + \cdots}^{r+1}$. Either way, it is now clear that
the probability function forms a valid probability space since
\[
    \sum_{x=r}^\infty \binom{x-1}{r-1}p^{r}(1-p)^{x-r}
    =\sum_{x=0}^\infty \binom{x+r-1}{r-1}p^{r}(1-p)^{x}
    =p^{r} S_{r-1}(1-p)
    =1
\]
\subsubsection{Moment}
\begin{align*}
    E[X^k] 
    &= \sum_{x=r}^{\infty} x^k\binom{x-1}{r-1}p^{r}(1-p)^{x-r}\\
    &= r\sum_{x=r}^{\infty} x^{k-1}\binom{x}{r}p^{r}(1-p)^{x-r}\\
    &= \frac{r}{p}\sum_{x=r}^{\infty} x^{k-1}\binom{x + 1 - 1}{r + 1 - 1}p^{r + 1}(1-p)^{(x+1)-(r+1)}\\
    &= \frac{r}{p}\sum_{x=r+1}^{\infty} (x-1)^{k-1}\binom{x - 1}{r + 1 - 1}p^{r + 1}(1-p)^{x-(r+1)}\\
    &= \frac{r}{p} E[(Y-1)^{k-1}]
\end{align*}
where $Y \sim NegativeBinomial(r+1, p)$
\subsubsection{Expectation}
\[E[X] = \frac{r}{p} E[(Y-1)^0] = \frac{r}{p}\]

\subsubsection{Variance}
\[
    E[X^2] 
    = \frac{r}{p} E[(Y-1)^1] 
    = \frac{r}{p}\left[\frac{r+1}{p} - 1\right]
    = \frac{r^2 + r - rp}{p^2}
\]
Therefore
\[Var[X] = E[X^2] - E[X]^2 = \frac{r^2 + r - rp}{p^2} - \frac{r^2}{p^2} = \frac{r(1-p)}{p^2}\]

\subsubsection{Moment Generating Function}
\begin{align*}
    M_X(t) 
    &= E[e^{Xt}]\\
    &= \sum_{x=r}^{\infty} \binom{x-1}{r-1}e^{xt} p^{r}(1-p)^{x-r}\\
    &= \sum_{x=0}^{\infty} \binom{x+r-1}{r-1}e^{xt+rt} p^{r}(1-p)^{x}\\
    &= p^re^{rt}\sum_{x=0}^{\infty} \binom{x+r-1}{r-1}\paren{(1-p)e^t}^{x}\\
    &= \paren{pe^t}^{r}S_{r-1}\paren{(1-p)e^t}\\
    &= \frac{\paren{pe^t}^{r}}{\paren{1-(1-p)e^t}^r}\\
    &= \paren{\frac{pe^t}{1-(1-p)e^t}}^r
\end{align*}
for $(1-p)e^t < 1 \Longleftrightarrow t < \ln\paren{\frac{1}{1-p}}$. Also, notice that the moment generating
function shows that the negative binomial can be seen as the sum of $r$ independent geometric distributions.
