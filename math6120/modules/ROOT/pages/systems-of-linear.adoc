= Systems of Linear Differential Equations

Consider the following system of first order differential equations
[stem]
++++
\begin{aligned}
x_1'(t) &= a_{11}(t)x_1(t) + a_{12}(t)x_2(t) + \cdots a_{1n}(t)x_n(t) + b_1(t)\\
x_2'(t) &= a_{21}(t)x_1(t) + a_{22}(t)x_2(t) + \cdots a_{2n}(t)x_n(t) + b_2(t)\\
&\vdots\\
x_n'(t) &= a_{n1}(t)x_1(t) + a_{n2}(t)x_2(t) + \cdots a_{nn}(t)x_n(t) + b_n(t)\\
\end{aligned}
++++
where the stem:[a_{ij}] and stem:[b_i] are continuous. Then, the above may be more compactly
written as
[stem]
++++
X'(t) = A(t) X(t) + B(t)
++++
where
[stem]
++++
X(t) = \begin{bmatrix}
x_1(t) \\ x_2(t) \\ \vdots \\ x_n(t)
\end{bmatrix}
,\quad
A(t) = \begin{bmatrix}
a_{11}(t) & a_{12}(t) &\cdots & a_{1n}(t)\\
a_{21}(t) & a_{22}(t) &\cdots & a_{2n}(t)\\
\vdots &  & \ddots & \\
a_{n1}(t) & a_{n2}(t) &\cdots & a_{nn}(t)\\
\end{bmatrix}
,\quad
B(t) = \begin{bmatrix}
b_1(t) \\ b_2(t) \\ \vdots \\ b_n(t)
\end{bmatrix}
++++

Terminology

Perturbed::
When in the above form, stem:[B] is called the _perturbed term_ and if stem:[B\neq 0],
the system is called _perturbed_ (or _non-homoegenous_).
However if stem:[B=0], the system is called _unperturbed_ (or _homoegenous_).

Autonomous::
If stem:[A(t)] is constant, the system is called _autonomous_.


.Existence and Uniqueness
[.theorem]
****
If stem:[A] and stem:[B] are continuous on stem:[I] then the following IVP
has a unqiue solution
[stem]
++++
X(t) = X_0 + \int_{t_0}^t A(s)X(s) + B(s) \ ds
++++

[.proof%collapsible]
====
From the non-local existence theorem for systems of differential equations,
we only need to show that
[stem]
++++
\vec{f}(t, X) = A(t)X + B(t)
++++
is Lipschitz. 
However, this is clear to see since
[stem]
++++
\left\|\vec{f}(t, X) - \vec{f}(t, Y)\right\|
= \left\|A(t)X - A(t)Y\right\|
\leq \|A(t)\| \ \|X-Y\|
++++
Since stem:[A] is continuous in closed interval stem:[I], stem:[A]  must be bounded
by some stem:[K] and hence the stem:[\vec{f}] is Lipschitz.

Therefore, by the non-local existence theorem, the IVP has a unique solution.
====
****

== Homoegenous
In this section, we consider the homogeneous system of linear differential equations.

.Test for linear independence
[.lemma]
****
Suppose stem:[X_1, \ldots X_n] are solutions of the homogeneous system
such that
[stem]
++++
X_i(t_0) = E_i
++++
are linearly independent for some stem:[t_0]. Then, the stem:[X_1, \ldots X_n] are
also linearly independent.

[.proof%collapsible]
====
Simply take a linear combination of stem:[X_i] and evaluate at stem:[t_0].
By the linear independence of the stem:[E_i], the coefficients must be all zero.
====
****

.Characterization of linearly independent solutions
[.lemma]
****
Suppose that stem:[X_1, \ldots X_n] are linearly independent solutions of the homogeneous
system.
Then for each stem:[t_0], the stem:[X_i(t_0)] are linearly independent.

[.proof%collapsible]
====
Consider some stem:[t_0] and stem:[c_1, \ldots c_n] such that
[stem]
++++
c_1X_1(t_0) + c_2X_2(t_0) + \cdots + c_nX_n(t_0) = 0
++++
Then, by the superposition principle, stem:[Y = c_1X_1 + \cdots c_nX_n]
is also a solution and has stem:[Y(t_0) = 0].
Then, by existence and uniqueness, we have that stem:[Y] is the zero solution.
Therefore
[stem]
++++
c_1X_1 + \cdots c_nX_n = Y = 0
++++
and by linear independence of the stem:[X_i], stem:[c_1=\cdots =c_n = 0].
====
****

.Existence of linearly independent solutions
[.lemma]
****
Consider the homogeneous system with stem:[n] equations.
Then, there exists stem:[X_1, \ldots X_n] which are linearly independent
solutions of the homogeneous system.


[.proof%collapsible]
====
Let each stem:[X_i] be the solution of the following IVP
[stem]
++++
X'(t) = A(t)X(t), \quad X(t_0) = E_i
++++
for some fixed stem:[t_0] where the stem:[E_i] are the standard basis vectors.
Then, by the existence theorem, each of these stem:[X_i] exist.
Furthermore, since there stem:[E_i] are linearly independent,
the stem:[X_i] are also linearly independent.
====
****

.Basis of solutions
[.theorem]
****
Consider the homogeneous system with stem:[n] equations.
If stem:[X_1, \ldots X_n] are linearly independent solutions,
then stem:[X] is a solution iff its it a linear combination of the stem:[X_i].
Therefore, the solution set the homogeneous system is a vector space
with stem:[n] dimensions.

[.proof%collapsible]
====
Let stem:[S] be the set of solutions of the homogeneous system.
Then, by the superposition principle, stem:[S] is a subspace and is itself a vector
space.

We already know that the stem:[X_1, \ldots X_n] are linearly independent.
We only need to show that they span stem:[S].
Suppose that stem:[Y] is in stem:[S] and consider arbitrary stem:[t_0].
Then, by the linear independence of the stem:[X_i], the stem:[X_i(t_0)] are also linearly
independent and form a basis for stem:[\mathbb{R}^n].
Therefore, there exists stem:[c_1, \ldots c_n] such that
[stem]
++++
Y(t_0) = c_1X_1(t_0) + \cdots + c_nX_n(t_0) 
++++
Since stem:[Y] and stem:[c_1X_1 + \cdots c_nX_n] are solutions to the following IVP
[stem]
++++
X'(t) = A(t)X(t), \quad X(t_0) = c_1X_1(t_0) + \cdots + c_nX_n(t_0)
++++
by uniqueness of solution, stem:[Y = c_1X_1 + \cdots c_nX_n] and stem:[S]
is the span of the stem:[X_i].
Therefore we have shown that the stem:[X_i] are a basis for stem:[S]
and the result follows.
====
****

Using the above theorem, we can therefore speak of the _fundamental matrix_.
If stem:[\vec{\phi}_1, \ldots \vec{\phi}_n] are stem:[n] linearly independent solutions
of the homogeneous system, then any solution is the linear combination of these vectors.
Therefore, it is useful to write the stem:[\vec{\phi}_i] as columns of a matrix.
We call such a matrix a _fundamental matrix_ since any solution may be written
as the product of this matrix and a constant vector.
[stem]
++++
\Phi(t) = \begin{bmatrix}
\vec{\phi}_1 & \vec{\phi}_2 & \cdots & \vec{\phi}_n
\end{bmatrix}
= \begin{bmatrix}
\phi_{11} & \phi_{12} & \cdots & \phi_{1n}\\
\phi_{21} & \phi_{22} & \cdots & \phi_{2n}\\
\vdots & & \ddots & \\
\phi_{n1} & \phi_{n2} & \cdots & \phi_{nn}\\
\end{bmatrix}
++++
Note that stem:[\Phi] also satisfies the homogeneous system since
each of its columns satisfy the system.

Solution matrix:: If the stem:[\vec{\phi}_1, \ldots \vec{\phi}_n] are not linearly independent, we simply
call the matrix the _solution matrix_.

Standard fundamental matrix:: If stem:[\Phi(t_0) = E] (the identity matrix),
stem:[\Phi] is called the _standard fundamental matrix_.

We also define the _Wronskian_ as
[stem]
++++
W(t) = \det \Phi(t)
++++
Although this may seem different from the definition of Wronskian in
xref:linear.adoc[], they are the same when we convert the higher order differential
equation to a system.

.Abel's Identity
[.theorem]
****
Let stem:[\Phi] be a solution matrix of the homogeneous system
[stem]
++++
X'(t) = A(t)X(t)
++++
on interval stem:[I] containing stem:[t_0].
Then
[stem]
++++
\det\Phi(t) = \det \Phi(t_0) \exp\left[\int_{t_0}^t \tr A(s) \ ds\right ]
++++
where stem:[\tr A(s) = \sum_{i=1}^n a_{ii}(s)] is the trace of stem:[A(s)].


[.proof%collapsible]
====
As shown in xref:appendix:vector-functions.adoc#_determinants[],
[stem]
++++
\frac{d}{dt} \det \Phi(t) = 
\begin{vmatrix}
\phi_{11}' & \phi_{12}' & \cdots & \phi_{1n}'\\
\phi_{21} & \phi_{22} & \cdots & \phi_{2n}\\
\vdots & & \ddots & \\
\phi_{n1} & \phi_{n2} & \cdots & \phi_{nn}\\
\end{vmatrix}
+
\begin{vmatrix}
\phi_{11} & \phi_{12} & \cdots & \phi_{1n}\\
\phi_{21}' & \phi_{22}' & \cdots & \phi_{2n}'\\
\vdots & & \ddots & \\
\phi_{n1} & \phi_{n2} & \cdots & \phi_{nn}\\
\end{vmatrix}
+\cdots+
\begin{vmatrix}
\phi_{11} & \phi_{12} & \cdots & \phi_{1n}\\
\phi_{21} & \phi_{22} & \cdots & \phi_{2n}\\
\vdots & & \ddots & \\
\phi_{n1}' & \phi_{n2}' & \cdots & \phi_{nn}'\\
\end{vmatrix}
++++
Focus on stem:[\phi_{ij}]. Since stem:[\vec{\phi}_j] is a solution of the homogeneous
system, we have that
[stem]
++++
\vec{\phi}_j' = A(s)\vec{\phi}_j
\implies \phi_{ij}' = \sum_{k=1}^n a_{ik}\phi_{kj}
++++
Therefore
[stem]
++++
\begin{vmatrix}
\phi_{11} & \phi_{12} & \cdots & \phi_{1n}\\
\vdots & & \ddots & \\
\phi_{i1}' & \phi_{i2}' & \cdots & \phi_{in}' \\
\vdots & & \ddots & \\
\phi_{n1} & \phi_{n2} & \cdots & \phi_{nn}\\
\end{vmatrix}
=
\begin{vmatrix}
\phi_{11} & \phi_{12} & \cdots & \phi_{1n}\\
\vdots & & \ddots & \\
\sum_{k=1}^n a_{ik}\phi_{k1} & \sum_{k=1}^n a_{ik}\phi_{k2} & \cdots & \sum_{k=1}^n a_{ik}\phi_{kn} \\
\vdots & & \ddots & \\
\phi_{n1} & \phi_{n2} & \cdots & \phi_{nn}\\
\end{vmatrix}
=
\begin{vmatrix}
\phi_{11} & \phi_{12} & \cdots & \phi_{1n}\\
\vdots & & \ddots & \\
a_{ii}\phi_{i1} & a_{ii}\phi_{i2} & \cdots & a_{ii}\phi_{in} \\
\vdots & & \ddots & \\
\phi_{n1} & \phi_{n2} & \cdots & \phi_{nn}\\
\end{vmatrix}
= a_{ii}\det\Phi(t)
++++
and hence
[stem]
++++
\frac{d}{dt}\det\Phi(t)
= \sum_{i=1}^n a_{ii} \det \Phi(t)
= \left(\tr A(t)\right)\left(\det\Phi(t)\right)
++++
After solving the above differential equation, we obtain the desired result
====
****

[.corollary]
****
A solution matrix stem:[\Phi] of a homogeneous system is a fundamental matrix
iff stem:[\det \Phi(t) \neq 0] on stem:[I].
****

Note that the above corollary also tells us whether a given matrix is a solution matrix.
For example, consider the matrix
[stem]
++++
\begin{bmatrix}
f_1 & f_2\\
0 & 0
\end{bmatrix}
++++
where stem:[f_1] and stem:[f_2] are linearly independent.
This implies that the columns are also linearly independent. However,
the determinant of the above matrix is always zero.
This may seem like a contradiction to the above corollary, but rather it implies that
the premise of the theorem is not satisfied. That is, the above
matrix is not a solution matrix for any homogeneous system.

.Fundamental matrices form an orbit when acted on by the general linear group on the right
[.theorem]
****
Let stem:[S] be the set of fundamental matrices of the homogeneous system
[stem]
++++
X' = A(t)X
++++
Then, stem:[S \neq 0] and 

* If stem:[\Phi \in S], then for any invertible constant matrix stem:[C], stem:[\Phi C \in S]
* If stem:[\Phi_1, \Phi_2 \in S], there exists an invertible constant matrix
stem:[C] such that stem:[\Phi_1 = \Phi_2 C]


[.proof%collapsible]
====
First let stem:[\Phi \in S] and stem:[C] be an invertible constant matrix.
Then,

[stem]
++++
(\Phi C)' = \Phi' C = A \Phi C = A (\Phi C)
++++
and stem:[\det (\Phi C) = \det \Phi \det C \neq 0].
Therefore stem:[\Phi C \in S].

Next, suppose that stem:[\Phi_1, \Phi_2 \in S].
Since stem:[\det \Phi_1 \neq 0], we can define
[stem]
++++
\Psi = \Phi_1^{-1}\Phi_2
++++
Then,
[stem]
++++
A(t)\Phi_2(t) = \Phi_2'(t) = (\Phi_1(t) \Psi(t) )'
= \Phi_1(t)\Psi'(t) + \Phi_1'(t)\Psi(t)
= \Phi_1(t)\Psi'(t) + A(t)\Phi_1(t)\Psi(t)
= \Phi_1(t)\Psi'(t) + A(t)\Phi_2
++++
Therefore
[stem]
++++
\Phi_1(t)\Psi'(t) = 0 \implies \Psi'(t) = 0 \implies \Psi \text{ is constant}
++++
Therefore we get the desired result.
====
****

[.theorem]
****
If stem:[\Phi] is a standard fundamental matrix with stem:[t_0=0] of the following
autonomous system
[stem]
++++
X'(t) = AX(t)
++++
on interval stem:[I]
where stem:[A] is a constant matrix, then
[stem]
++++
\forall s,t \in I: \Phi(s+t) = \Phi(s)\Phi(t)
++++

[.proof%collapsible]
====
NOTE: In this proof stem:[t] is treated as a constant.

Let
[stem]
++++
Z_1(s) = \Phi(s+t)
,\quad\text{and}\quad
Z_2(s) = \Phi(s)\Phi(t)
++++
Then, both stem:[Z_1] and stem:[Z_2] satisfy the following IVP
[stem]
++++
Z' = AZ \quad\text{where } Z(0) = \Phi(t)
++++
since
[stem]
++++
\begin{aligned}
Z_1'(s) &= \frac{d}{ds}\Phi(s+t) = \Phi'(s+t) \frac{d}{ds}(s+t) = A\Phi(s+t)(1) = AZ_1(s)
\\
Z_2'(s) &= \Phi'(s)\Phi(t) = A\Phi(s)\Phi(t) = AZ_2(s)
\end{aligned}
++++
Then, by uniqueness, stem:[Z_1=Z_2] and we have the desired result.

NOTE: The matrix system has a unique solution since the columns may be treated individually
and each have a unique solution.
====
****

=== Autonomous Systems
Consider the system
[stem]
++++
X' = AX
++++
where stem:[A] is a constant matrix.
Then, a fundamental matrix for this system is simply stem:[\Phi(t) = e^{tA}]
(the matrix exponential).


[.proof%collapsible]
====
Notice that stem:[\Phi(t)] is indeed a solution matrix since
[stem]
++++
\Phi'(t)
= \frac{d}{dt} \left(E + \sum_{n=1}^\infty \frac{(tA)^n}{n!}\right)
= \frac{d}{dt} \left(tA + \sum_{n=2}^\infty \frac{t^nA^n}{n!}\right)
= A + \sum_{n=2}^\infty \frac{t^{n-1}A^{n}}{(n-1)!}
= A\left(E + \sum_{n=1}^\infty \frac{t^{n}A^{n}}{n!}\right)
= A\Phi(t)
++++
Also, since stem:[\det \Phi(0) = \det E = 1 \neq 0], stem:[\Phi(t)] is a fundamental matrix.
====

==== Non-constant
Note that in general, stem:[e^{\int A(s) \ ds}] is not a solution.
A counterexample provided by
https://math.stackexchange.com/a/2047[Tobias Kienzler on stack exchange] is
stem:[A(s) = \begin{bmatrix}\cos t & \sin t \\ \sin t & -\cos t\end{bmatrix}].
In this case
[stem]
++++
\Phi(t) = \exp\int A(s) \ ds = \begin{bmatrix}
\sin t \sinh t + \cosh t & - \cos t \sinh t\\
-\cos t \sinh t & -\sin t \sinh t + \cosh t\\
\end{bmatrix}
++++
but
[stem]
++++
\Phi'(t) = \begin{bmatrix}
\sin t\cosh t + \cos t\sinh t + \sinh t & -\cos t \cosh t + \sin t \sinh t\\
-\cos t\cosh t + \sin t \sinh t & -\sin t\cosh t - \cos t\sinh t + \sinh t\\
\end{bmatrix}
++++
and
[stem]
++++
A(t)\Phi(t) = \begin{bmatrix}
\cos t \cosh t & - \sinh t + \cosh t \sin x\\
\sinh t + \sin t \cosh t & -\cos t \cosh t
\end{bmatrix}
++++
The problem is that stem:[A] and its integral do not commute.
For this reason, stem:[\frac{d}{dt}\left(\int A(t) \ dt\right)^n \neq n A(t) \left(\int A(t) \ dt\right)^{n-1}]
and the system is not satisfied.
More explicitly if stem:[B(t) = \int A(t) \ dt]
[stem]
++++
\frac{d}{dt} B(t)^n
= B'(t)B(t)^{n-1} + B(t)B'(t) B(t)^{n-2} + \cdots B(t)^{n-1}B'(t)
++++
since stem:[B] and stem:[B'] do not commute, we do not get the desired result.

=== Finding the matrix exponential
Although the solution to the autonomous system is quite simple, there is hidden complexity.
Primarily, how do we find the matrix exponential.
When dealing with scalar valued functions, we usually leave the exponential stem:[e^{\lambda t}] as is since
it is not possible to meaningly simplify further.
However, it is possible to obtain simpler results for matrix exponentials.

Before applying any overly complex results, first see if the powers of stem:[A]
form a nice sequence such that the infinite series of the exponential is easy to compute.

The simplest example is that of the diagonal matrix, in this case
[stem]
++++
\exp\begin{bmatrix}
\lambda_1 t & & & \\
& \lambda_2 t & &\\
 && \ddots & \\
&&& \lambda_n t\\
\end{bmatrix}
= 
\begin{bmatrix}
e^{\lambda_1 t} & & & \\
& e^{\lambda_2 t} & &\\
 && \ddots & \\
&&& e^{\lambda_n t}\\
\end{bmatrix}
++++
Furthermore, if stem:[A = PDP^{-1}] for some diagonal matrix stem:[D] (ie stem:[A] is diagonalizable),
[stem]
++++
\exp At = P (\exp Dt) P^{-1}
++++
Therefore, if stem:[(\lambda_i, V_i)] are stem:[n] eigenpairs of stem:[A \in \mathbb{R}^{n\times n}], the general solution is 
[stem]
++++
c_1e^{\lambda_1 t} V_1 + c_2e^{\lambda_2 t} V_2
+ \cdots + c_ne^{\lambda_n t} V_n
++++
However, this is not always possible since the geometric multiplicity of and eigenvalue may
be less than the algebraic multiplicity.
In such a case, we must resort to _generalized eigenvectors_.

=== Generalized Eigenvectors
TIP: Please see https://en.wikipedia.org/wiki/Jordan_normal_form#Generalized_eigenvectors

A vector stem:[V] is called a _generalized eigenvector_ of stem:[A] with _rank_ stem:[i] and 
eigenvalue stem:[\lambda] if
[stem]
++++
(A-\lambda E)^{i}V = \vec{0}\quad\text{and}\quad (A-\lambda E)^{i-1}V \neq \vec{0}
++++
Using this definition, a regular eigenvector is a generalized eigenvector of rank stem:[1].

Now, by letting stem:[Y(t) = e^{-\lambda t}X(t)], we rewrite the system as 
[stem]
++++
Y'(t) = \frac{d}{dt}(e^{-\lambda t}X(t)) = e^{-\lambda t}X'(t) -\lambda e^{-\lambda t}X(t)
= e^{-\lambda t}\left(AX(t) - \lambda X(t)\right) = (A-\lambda E)Y(t)
++++
Then, if stem:[y_0, y_1, \ldots] form the coefficients of the taylor series expansion of stem:[Y(t)], we obtain that
[stem]
++++
y_i = (A-\lambda E)^i y_0
++++
Therefore, if stem:[y_0] is a generalized eigenvector of rank stem:[r], the taylor series is finite
and
[stem]
++++
Y(t) = \sum_{i=0}^r \frac{t^i}{i!}\left(A-\lambda E\right)^i\vec{y}_0
++++
and
[stem]
++++
X(t) = e^{\lambda t}\sum_{i=0}^r \frac{t^i}{i!}\left(A-\lambda E\right)^i\vec{y}_0
++++
is a solution to the original equation.

Therefore, the plan is to find all generalized eigenvectors of stem:[A]
and generate solutions using the above formula.


Note that it is not obvious that stem:[n] generalized eigenvectors exist.
However, the following lemma assures this.

.Existence of generalized eigenvectors
[.lemma]
****
Let stem:[A] be a matrix with eigenvalue stem:[\lambda] with algebraic multiplicity stem:[m].
Then, if the equation
[stem]
++++
(A-\lambda E)^{j} V = \vec{0}
++++
has stem:[k < m] linear independent solutions,
the equation
[stem]
++++
(A-\lambda E)^{j+1} V = \vec{0}
++++
has at least stem:[k+1] linearly independent solutions.
****

=== Putzar Algorithm
Putzer's algorithm provides a conceptually simple way for computing
the matrix exponential

[.theorem]
****
Let stem:[\lambda_1, \ldots \lambda_n] be the spectrum of eigenvectors
of matrix stem:[A].
Then
[stem]
++++
e^{At} = \sum_{k=0}^{n-1} p_{k+1}(t) M_k
++++
where
[stem]
++++
M_0 = E,
\quad
M_k = \prod_{i=1}^k(A - \lambda_i E)
++++
and stem:[\vec{p}] is a solution to
[stem]
++++
p'(t) = \begin{bmatrix}
\lambda_1 & 0 & 0 & \cdots & 0 & 0\\
1 & \lambda_2 & 0 & \cdots & 0 & 0\\
0 & 1 & \lambda_3 & \cdots & 0 & 0\\
\vdots & & & \ddots & \vdots & \vdots\\
0 & 0 & 0 & \cdots & 1 & \lambda_n
\end{bmatrix}p(t),
\quad
p(0) = \begin{bmatrix}
1 \\ 0 \\ \vdots \\ 0
\end{bmatrix}
++++

[NOTE]
====
The proof simply checks if the above form satisfies stem:[X'=AX] and stem:[X(0)=E].
Then, by uniqueness, stem:[X=e^{At}]. This proof also uses the Cayley-Hamilton theorem.

I would not add this proof until I find a good motivation for the above form.
I find the proof that it is a solution boring, what is more interesting is the development
of the solution.
====
****

== Non-homogeneous
The method of solving non-homogeneous linear systems of differential equations is quite similar to solving
non-homogeneous higher order linear differential equations.
We solve it in two stages: 

* find the general solution to the homogeneous system
* find a particular solution to the system

We have already spoken about solutions to the homogeneous system, so now we would consider
the non-homogeneous system.
We use variation of parameters.
That is, if stem:[\Phi(t)] is a fundamental matrix, we try stem:[\Phi(t)\vec{u}(t)] for some vector function stem:[\phi{u}].
We obtain the following lemma

.Particular solution
[.lemma]
****
If stem:[\Phi(t)] is a fundamental matrix for the homogeneous system,
the solution to the IVP 
[stem]
++++
X' = A(t)X + B(t)
,\quad
X(t_0)=0
++++
is given by
[stem]
++++
\psi_p(t) = \int_{t_0}^t \Phi(t)\Phi^{-1}(s) B(s) \ ds
++++

[.proof%collapsible]
====
If we let stem:[\psi_p(t) = \Phi(t)u(t)] and assume that it is a solution to the IVP, we obtain that
[stem]
++++
\psi_p'(t) = A(t)\psi_p(t)+B(t) = A(t)\Phi(t)u(t) + B(t)
++++
Also, by using the product rule, we obtain that
[stem]
++++
\psi_p'(t) = \Phi'(t)u(t) + \Phi(t)u'(t) = A(t)\Phi(t)u(t) + \Phi(t)u'(t)
++++
Therefore
[stem]
++++
\Phi(t)u'(t) = B(t) \implies u(t) = u(t_0) + \int_{t_0}^t \Phi^{-1}(s)B(s) \ ds = \int_{t_0}^t \Phi^{-1}(s)B(s) \ ds
++++
since stem:[u(t_0) = \Phi^{-1}(t_0)\psi_p(t_0) = \Phi^{-1}(t_0)\vec{0} = \vec{0}]
and we get the desired result.

Also, by construction of stem:[u] we have ensured that the desired form is indeed a solution of the IVP.

NOTE: If we specified that stem:[\psi_p(t_0) = X_0], we could have also determined a solution
to the general IVP.
====
****

.Complementary solution
[.lemma]
****
If stem:[\Phi(t)] is a fundamental matrix for the homogeneous system,
the solution to the IVP 
[stem]
++++
X' = A(t)X
,\quad
X(t_0)=X_0
++++
is given by
[stem]
++++
\psi_c(t) = \Phi(t)\Phi^{-1}(t_0) X_0
++++
See above lemma for proof.
****


[.theorem]
****
If stem:[\Phi(t)] is a fundamental matrix for the homogeneous system,
the solution to the IVP 
[stem]
++++
X' = A(t)X + B(t)
,\quad
X(t_0)=X_0
++++
is given by
[stem]
++++
\psi(t) = \psi_c(t) + \psi_p(t)
++++
where stem:[\psi_c] and stem:[\psi_p] are as defined in the previous two lemmas.
Concisely,
[stem]
++++
\psi(t) = \Phi(t)\Phi^{-1}(t_0) X_0 + \int_{t_0}^t\Phi(t)\Phi^{-1}(s) B(s)\ ds
++++
****

=== Periodic
In this section, we seek to determine when solutions of perturbed and non-perturbed systems
have period solutions.

NOTE: In this section, we assume that the coefficient matrix stem:[A(t)] is periodic with the period of interest.

[.theorem]
****
A solution of the following system
[stem]
++++
X'=A(t)X + B(t)
++++
is periodic with period stem:[\omega] iff
[stem]
++++
X(\omega) = X(0)
++++

[.proof%collapsible]
====
The forward statement is clearly true. Now, suppose that stem:[\phi] is a solution
to the homogeneous system and stem:[\phi(\omega) = \phi(0)=X_0].
Let stem:[\psi(t) = \phi(t+\omega)]
Then,
[stem]
++++
\forall t: \psi'(t) = \phi'(t+\omega) =  A(t+\omega)\phi(t+\omega) + B(t+\omega)= A(t)\phi(t+\omega) + B(t)= A(t)\psi(t) + B(t)
++++
Therefore both stem:[\phi] and stem:[\psi] are solutions to the below IVP
[stem]
++++
X' = A(t)X + B(t), \ X(0) = X_0
++++
and by uniqueness stem:[\phi(t) = \psi(t) = \phi(t+\omega)].
====
****

[.corollary]
****
Consider the below system
[stem]
++++
X' = AX
++++
where stem:[A] is a constant matrix.
Then the system has a non-trivial periodic solution with period stem:[\omega]
iff stem:[E-e^{A\omega}] is singular.

NOTE: We state non-trivial since the zero solution is periodic with all periods.

[.proof%collapsible]
====
From previous discussions, we know that the solution of the above system is given by
[stem]
++++
X(t) = e^{At}C
++++
for some constant vector stem:[C].
We want that there exists non-zero stem:[C] such that solution is period.

Then, from the above theorem, stem:[X] is periodic iff
[stem]
++++
C = X(0)= X(\omega) = e^{A\omega}C
++++
iff stem:[(E-e^{A\omega}) C = \vec{0}]. Therefore, there exists a non-trivial stem:[C]
iff stem:[E-e^{A\omega}] is singular.
====
****

[.corollary]
****
Consider the below system
[stem]
++++
X' = AX + B(t)
++++
where stem:[A] is a constant matrix.
Then, the above system has a unique periodic solution with period stem:[\omega]
iff stem:[E-e^{A\omega}] is non-singular.

NOTE: Stating anymore than this becomes a bit difficult since we are dealing with matrices.
But, it is possible to also determine conditions when the system has periodic solutions.
However, these conditions have to do with the column space of the particular solution.

[.proof%collapsible]
====
From previous discussions, we know that the solution to the above system is given by
[stem]
++++
X(t) = e^{At}C + \int_{0}^t e^{At}e^{-As} B(s) \ ds
++++
for some constant vector stem:[C].
This solution is periodic iff
[stem]
++++
C = X(0) = X(\omega) = e^{A\omega}C + \int_{0}^\omega e^{At}e^{-As} B(s) \ ds
++++
We may now consider the above as a system with variable stem:[C].
Our differential system has a unique solution of period stem:[\omega]
iff the above system has a unique solution for stem:[C].
This happens iff stem:[E-e^{A\omega}] is non-singular as the above system is equivalent to.
[stem]
++++
(E - e^{A\omega})C = \int_{0}^\omega e^{At}e^{-As} B(s) \ ds
++++
====
****

Now, let us look at the nature of fundamental matrices for periodic systems.
The following theorem allows us to determine the solution of a periodic system by knowing
only the solution over one period.

[.theorem]
****
If stem:[\Phi(t)] is a fundamental matrix of
[stem]
++++
X' = A(t)X
++++
then stem:[\Phi(t + \omega)] is also a fundamental matrix.
Furthermore, there exists a constant matrix stem:[D] and non-singular stem:[P(t)] such that
[stem]
++++
P(t+\omega) = P(t), \quad\text{and}\quad \Phi(t) = P(t)e^{tD}
++++
Also, by applying the transformation stem:[X = P(t)Z], we obtain
[stem]
++++
Z'=DZ
++++

[.proof%collapsible]
====
Just verify that stem:[\det \Phi(t+\omega) \neq 0] and stem:[\Phi(t+\omega)] is indeed a solution.

Next, since stem:[\Phi(t+w)] is a solution, there exists non-singular matrix
stem:[B] such that
[stem]
++++
\Phi(t+\omega) = \Phi(t)B
++++
Also, since stem:[B] is non-singular, there exists a matrix stem:[D] such that stem:[B = e^{wD}].
Note that the matrix logarithm exists iff the matrix is invertible
(see https://en.wikipedia.org/wiki/Logarithm_of_a_matrix#Existence ).
Now, let stem:[P(t) = \Phi(t)e^{-tD}].
Then,
[stem]
++++
P(t+\omega) = \Phi(t+\omega) e^{-(t+\omega)D} = \Phi(t)e^{wD}e^{-(t+\omega)D}
= \Phi(t)e^{-tD} = P(t)
++++
Therefore we have found stem:[P].
====
****

[.theorem]
****
Consider the following system
[stem]
++++
X' = A(t)X
++++
then, there exists stem:[D] such that for
any fundamental matrix stem:[\Phi(t)],
there exists non-singular stem:[Q] such that
[stem]
++++
\Phi(t+\omega) = \Phi(t)Qe^{\omega D}Q^{-1}
++++
Furthermore, if stem:[\Phi(0) = E], there exists a unique set of constants
stem:[\lambda_1, \ldots \lambda_n] and stem:[r_1, \ldots r_n] (called _eigen multipliers_
and _eigen exponents_ of stem:[A] respectively) such that
[stem]
++++
\det \Phi(\omega) = \lambda_1\lambda_2 \cdots\lambda_n = e^{(r_1+r_2+\cdots+r_n)\omega}
++++
Also the constants stem:[\lambda_1, \ldots \lambda_n] are the eigenvalues of stem:[e^{\omega D}]
and the constants stem:[r_1, \ldots r_n] are the eigenvalues of stem:[D]
and neither depend on the particular choice of stem:[D].

[.proof%collapsible]
====
Consider fundamental matrix stem:[\Psi(t)].
Then, since stem:[\Psi(t+\omega)] is also fundamental,
there exists non-singular stem:[C] such that stem:[\Psi(t+\omega) = \Psi(t)C].
Also, since stem:[C] is non-singular, there exists stem:[D] such that
stem:[C = e^{\omega D}].

Now, consider arbitrary fundamental matrix stem:[\Psi].
Then, there exists stem:[Q] such that stem:[\Phi(t)Q = \Psi(t)].
Therefore
[stem]
++++
\Phi(t+\omega) = \Psi(t +\omega)Q^{-1} = \Psi(t)e^{\omega D}Q^{-1} = \Phi(t)Qe^{\omega D}Q^{-1}
++++
Therefore, we have proven the first claim.

Note that if we had chosen a different stem:[\Psi(t)], we may have obtained a different
stem:[D], say stem:[D_1].
However, the eigenvalues of stem:[e^{\omega D_1}] and stem:[D_1] would be the same
since there exists stem:[Q_1] such that stem:[e^{\omega D_1} = Q_1e^{\omega D}Q_1^{-1} = e^{\omega Q_1DQ_1^{-1}}]
(ie stem:[D] is similar to stem:[D_1] and stem:[e^{D}] is similar to stem:[e^{D_1}]).

Now, let stem:[\lambda_1, \ldots \lambda_n] be the eigenvalues of stem:[e^{\omega D}]
and suppose that stem:[\Phi(0) = E].
Then, from the Jordan-canonical form of stem:[Qe^{e^\omega D}Q^{-1}], we have
that
[stem]
++++
\det \Phi(\omega) = \det (\Phi(0) Q e^{\omega D}Q^{-1}) =\det E \det Q \det(e^{\omega D}) \det Q^{-1}
= \det (e^{\omega D}) = \lambda_1\lambda_2\cdots\lambda_n
++++

Also, if we let stem:[r_1, \ldots r_n] be the eigenvalues of stem:[D], we have that
[stem]
++++
\det \Phi(\omega) = \det (e^{\omega D}) = e^{\omega \tr D} = e^{(r_1 + r_2 + \cdots +r_n)\omega}
++++
====
****
