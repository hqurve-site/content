= Systems of Differential Equations


In this page, we discuss systems of differential equations.
Knowledge of solving systems of differential equations is important
since we would later show that any higher order differential equation
can be converted to a system of first order differential equations.
Many of the previously discussed techniques which have a single variable and equation
translate nicely to systems of differential equations.

Before we can speak about a system of differential equations, we must
first understand vector-valued functions which are either of a single real
variable or multiple variables.
Please see xref:appendix:vector-functions.adoc[] for basic terminology for vector functions.


== Basics
Let stem:[I] be an interval of stem:[\mathbb{R}] and stem:[E \subseteq \mathbb{R}^n].
Let stem:[D = I \times E \subseteq \mathbb{R}^{n+1}]
Consider the system of differentiation equations of the form
[stem]
++++
\left.\begin{aligned}
x_1'(t) &= f_1(t, x_1(t), x_2(t),\ldots x_n(t))\\
x_2'(t) &= f_2(t, x_1(t), x_2(t),\ldots x_n(t))\\
&\vdots\\
x_n'(t) &= f_n(t, x_1(t), x_2(t),\ldots x_n(t))\\
\end{aligned}
\right\}
++++
Where each stem:[f] has domain stem:[D \subseteq \mathbb{R}^{n+1}]
This may be more compactly written as stem:[\vec{x}'(t) = \vec{f}(t, \vec{x}(t))].

Then, stem:[\vec{\phi} = (\phi_1, \phi_2, \ldots \phi_n): I \to \mathbb{R}^n] is called
a solution of this system iff

. stem:[\phi_1', \phi_2', \ldots \phi_n'] exist
. stem:[(t, \phi_1(t), \phi_2(t), \ldots \phi_n(t)) \in D] for each stem:[t \in I]
. stem:[\phi_i'(t) = f_i(t, \phi_1(t), \phi_2(t), \ldots, \phi_n(t))] for each stem:[t \in I]

This may be more compactly written as 

. stem:[\vec{\phi}'] exists
. stem:[(t, \vec{\phi}(t)) \in D] for each stem:[t \in I]
. stem:[\vec{\phi}'(t) = \vec{f}(t, \vec{\phi})] for each stem:[t \in I]

NOTE: if we have an IVP, we have the additional condition that stem:[\vec{\phi}(t_0) = \vec{x}_0]

=== Lipschitz condition
Let stem:[\vec{f}: D \to \mathbb{R}^n]. Then, we say that stem:[f] is Lipschitz continuous
with constant stem:[K > 0]  if
[stem]
++++
\forall (t, \vec{x}_1), (t, \vec{x}_2) \in D:
\|\vec{f}(t,\vec{x}) - \vec{f}(t, \vec{y})\| \leq K\|\vec{x} - \vec{y}\|
++++

.Sufficient condition for Lipschitz
[.proposition]
****
If stem:[\vec{f}_{x_i}] exists and is bounded in closed region stem:[R \subseteq D],
then stem:[\vec{f}] satisfies the Lipschitz condition in stem:[R] 
with constant given by
[stem]
++++
\sup_{(t, \vec{x}) \in R,\ i\in \{1, \ldots n\}} \|\vec{f}_{x_i}(t, \vec{x})\| = K
++++
****

== Existence and Uniqueness of solution

.Equivalent integral equation
[.lemma]
****
Let stem:[\vec{f}: D \to \mathbb{R}^n] be a continuous function.
The function stem:[\vec{\phi}] is a solution of the following
IVP on some interval stem:[I] 
[stem]
++++
\vec{x}'(t) = \vec{f}(t, \vec{x}(t)), \quad\vec{x}(t_0)= \vec{x}_0
++++

iff stem:[\vec{\phi}] is a solution of the integral equation
[stem]
++++
\vec{x}(t) = \vec{x}_0 + \int_{t_0}^t \vec{f}(s, \vec{x}(s)) \ ds
++++
****

Picards approximations are defined equivalently as in the univariate case
stem:[\vec{\phi}_0(t) = \vec{x}_0] and
[stem]
++++
\vec{\phi}_{n+1}(t)
= \vec{x}_0 + \int_{t_0}^t \vec{f}(s, \vec{\phi}_n(s)) \ ds
++++

.Picards local existence theorem for systems of equations
[.theorem]
****
. Let stem:[D \subseteq \mathbb{R}^{n+1}] be an open domain and let
stem:[\vec{f}: D \to \mathbb{R}^n] be a vector valued function satisfying the two requirements

.. stem:[\vec{f} \in C(D)]; ie stem:[\vec{f}] is continuous
.. stem:[\vec{f}] satisfies Lipschitz condition in stem:[D] wrt stem:[\vec{x}]

. Let stem:[(t_0, \vec{x}_0) \in D] be an interior point and stem:[a > 0] and stem:[b>0]
such that the closed rectangle
+
--
[stem]
++++
R = \left\{(t,\vec{x}) \ | \ |t-t_0| \leq a,\ \|\vec{x} - \vec{x}_0\| \leq b\right\} \subseteq D
++++
Let stem:[M>0] be such that stem:[\vec{f}] is bounded by stem:[M] in stem:[R]
and stem:[\alpha = \min\left(a, \frac{b}{M}\right)].
--

Then, there exists a unique solution stem:[\vec{\phi}] of the IVP
[stem]
++++
\vec{x}'(t) = f(t, \vec{x}), \quad \vec{x}(t_0) = \vec{x}_0
++++
on the interval stem:[|t-t_0| \leq \alpha]
****

.Non-local existence theorem
[.theorem]
****
Let stem:[\vec{f}] be a vector valued function which is satisfies
the Lipschitz condition on a strip stem:[S] of the form
[stem]
++++
S = \left\{(t, \vec{x}) \ | \ |t-t_0| \leq a, \ \|\vec{x}\| < \infty\right\}
++++
If stem:[(t_0, \vec{x}_0) \in S], then Picards successive approximations
exists as continuous functions in stem:[I = |t-t_0| \leq a]
and converge to a unique solution of the IVP.
****

.Global existence theorem
[.theorem]
****
Let stem:[\vec{f}] be a vector valued function which is defined
and continuous on stem:[\mathbb{R}^{n+1}].
If stem:[\vec{f}] satisfies Lipschitz condition on every strip stem:[S_a]
[stem]
++++
S_a = \left\{(t, \vec{x}) \ | \ |t-t_0| \leq a, \ \|\vec{x}\| < \infty\right\}
++++
then the IVP associated with stem:[\vec{f}] has a unique solution which exists
for all real stem:[t].
****

.Cauchy - Peano Existence Theorem
[.theorem]
****
Let stem:[\vec{f}] be continuous in the open set stem:[D \subseteq \mathbb{R}^{n+1}]
and stem:[t_0, \vec{x}_0 \in D]. Then, there exists at least one solution
of the following IVP in the neighbourhood of stem:[t_0]
[stem]
++++
\vec{x}' = \vec{f}(t, \vec{x}), \ \vec{x}(t_0) = \vec{x}_0
++++
****


TIP: Apparently, the proofs of each of these results are very similar to
the univariate case.

== Abstract versions

// === Existence theorems

.Abstract version of Picard's Existence and Uniqueness theorem
[.theorem]
****
Let stem:[\vec{f} \in C(D, \mathbb{R}^n)] and stem:[(t_0, \vec{x}_0) \in D].
Also let stem:[I = |t-t_0| \leq a] and stem:[\Omega = \{\vec{x} \in \mathbb{R}^n: \| \vec{x} - \vec{x}_0\| \leq b\}]
be such that stem:[\vec{f}] is Lipschitz (in the second argument) in stem:[V = I \times \Omega \subseteq D]
with Lipschitz constant stem:[K> 0].

Then, there exists stem:[\alpha > 0] for which there exists a unique solution of the integral
equation
[stem]
++++
\vec{x}(t) = \vec{x}_0 = \int_{t_0}^t f(s, \vec{x}(s)) \ ds
++++
with domain stem:[|t-t_0| \leq \alpha]. One such stem:[\alpha] is such that
[stem]
++++
\alpha \leq a, \quad \alpha \leq \frac{b}{M}, \quad \alpha < \frac{1}{K}
++++
where stem:[\vec{f}] is bounded by stem:[M] in stem:[V].

IMPORTANT: Note that stem:[\alpha] is _strictly less than_ stem:[\frac{1}{K}]


[.proof%collapsible]
====
In this proof, we use the Banach fixed-point theorem to show that there is a single fixed point
of the following function
[stem]
++++
T\vec{x}(t) = \vec{x}_0 + \int_{t_0}^t \vec{f}(t, \vec{x}(s)) \ ds
++++
where stem:[T: X \to X] and stem:[X = C(J, \Omega)] where stem:[J = \|t-t_0\|\leq \alpha]
and stem:[\alpha] is yet to be specified.
Note that stem:[\vec{x}(t)] is a solution of the integral equation iff it is a fixed point of stem:[T].
We need to prove that

* stem:[X] is a complete metric space
* stem:[T] is a self-map
* stem:[T] is a contraction; ie Lipschitz with stem:[K < 1]

Note that stem:[X] is a complete metric space since the domain is closed and bounded.
Also notice that stem:[T] is a self map since for each
continuous stem:[\vec{\phi}(t) \in C(X, \mathbb{R}^n)], stem:[T\phi] is continuous and
[stem]
++++
\|T\vec{\phi}(t)- \vec{x}_0\|
= \left\|\int_{t_0}^t \vec{f}(t, \vec{\phi}(s)) \ ds \right\|
\leq \left|\int_{t_0}^t \left\| \vec{f}(t, \vec{\phi}(s))\right\| \ ds\right|
\leq \left|\int_{t_0}^t M \ ds\right|
\leq M\alpha
++++
For this reason, we want that stem:[\alpha \leq \frac{b}{M}]. Under this condition,
stem:[T] is indeed a self-map.

Finally
[stem]
++++
\begin{aligned}
\|T\vec{\phi}(t) - T\vec{\psi}(t)\|
&= \left\|\int_{t_0}^t \vec{f}(s, \vec{\phi}(s)) - \vec{f}(s, \vec{\psi}(s)) \ ds \right\|
\\&\leq \left|\int_{t_0}^t \left\|\vec{f}(s, \vec{\phi}(s)) - \vec{f}(s, \vec{\psi}(s)) \right\| \ ds\right|
\\&\leq \left|\int_{t_0}^t K\left\|\vec{\phi}(s) - \vec{\psi}(s) \right\| \ ds\right|
\\&\leq K\left\|\vec{\phi} - \vec{\psi} \right\| |t-t_0| \quad\text{Note this is the supremum metric between the functions}
\\&\leq K\alpha\left\|\vec{\phi} - \vec{\psi} \right\|
\end{aligned}
++++
Therefore, stem:[\|T\vec{\phi} - T\vec{\psi}\| \leq K\alpha\left\|\vec{\phi} - \vec{\psi} \right\|].
Also stem:[T] is a contraction if stem:[\alpha < \frac{1}{K}].

Therefore, the conditions of Banach's fixed-point theorem hold and there exists a unique stem:[\vec{x}]
such that stem:[T\vec{x} = \vec{x}].
====
****

.Abstract version of non-existence theorem
[.theorem]
****
Suppose that stem:[\vec{f}(t, \vec{x})] satisfies the Lipschitz
condition on stem:[V = I \times \mathbb{R}^n] where
stem:[I = |t-t_0| \leq a].
Then, there exists a unique solution to the following
integral equation
[stem]
++++
\vec{x}(t) = \vec{x}_0 = \int_{t_0}^t f(s, \vec{x}(s)) \ ds
++++


[.proof%collapsible]
====
The same proof as the local existence theorem can be used.
However, we need to somehow avoid limiting the domain
when proving that stem:[T] is a contraction.

In the notes, they do this by using a different norm. Namely
stem:[\|\vec{x}\| = \sup_{t\in I}|\vec{x}(t)|e^{-N|t-t_0|}] for some stem:[N > 0] yet to be specified.
We are not going to prove that this forms a norm; but apparently it does
and the metric induced is complete.
Now, lets prove that stem:[T] is a contraction

[stem]
++++
\begin{aligned}
\|T\vec{\phi}(t) - T\vec{\psi}(t)\|e^{-N|t-t_0|}
&= \left\|\int_{t_0}^t \vec{f}(s, \vec{\phi}(s)) - \vec{f}(s, \vec{\psi}(s)) \ ds \right\|e^{-N|t-t_0|}
\\&\leq \left|\int_{t_0}^t \left\|\vec{f}(t, \vec{\phi}(s)) - \vec{f}(t, \vec{\psi}(s)) \right\| \ ds\right|e^{-N|t-t_0|}
\\&\leq \left|\int_{t_0}^t K\left\|\vec{\phi}(s) - \vec{\psi}(s) \right\| \ ds\right|e^{-N|t-t_0|}
\\&\leq \left|\int_{t_0}^t K\left\|\vec{\phi} - \vec{\psi} \right\|e^{N|s-t_0|} \ ds\right|e^{-N|t-t_0|}
\\&\leq \frac{K}{N}\left\|\vec{\phi} - \vec{\psi} \right\|\left(e^{N|t-t_0|} - 1\right)e^{-N|t-t_0|}
\\&\leq \frac{K}{N}\left\|\vec{\phi} - \vec{\psi} \right\|
\end{aligned}
++++
Then, by choosing stem:[N = 2K], we get a that stem:[T] is a contraction and the result follows.

NOTE: We could have also done a similar trick for Picard's local existence theorem to remove
the restriction that stem:[\alpha < \frac{1}{K}].
====
****

== Continuation and dependence on initial conditions
The following theorem allows us to have 

.Continuity based on initial conditions
[.theorem]
****
Let stem:[\vec{f}] be continuous and bounded by some stem:[M > 0]
and satisfy Lipschitz condition on connected set stem:[D].
And consider the ODE
[stem]
++++
\vec{x}'(t) = \vec{f}(t, \vec{x})
++++
with interval stem:[I = [a,b\]].

Then for each stem:[\varepsilon > 0], there exists a stem:[\delta > 0]
such that for each pair of 
solutions stem:[\vec{\phi}] and stem:[\vec{\phi}^*]
corresponding to initial conditions stem:[\vec{x}(t_0) = \vec{x}_0]
and stem:[\vec{x}(t_0^*) = \vec{x}_0^*].
[stem]
++++
|t_0 - t_0^*| < \delta \quad\text{and}\quad \|\vec{x}_0 - \vec{x}_0^*\| < \delta
++++
implies that
[stem]
++++
\|\vec{\phi} - \vec{\phi}^*\| < \varepsilon
++++


[.proof%collapsible]
====
NOTE: The proof is basically identical to the univariate case.

Consider the supremum norm. Then for each stem:[t \in I]

[stem]
++++
\begin{aligned}
\|\vec{\phi}(t) - \vec{\phi}^*(t)\|
&= \left\| (\vec{x}_0-\vec{x}_0^*)
+ \int_{t_0}^t \vec{f}(s, \vec{\phi}(s)) \ ds
- \int_{t_0^*}^t \vec{f}(s, \vec{\phi}^*(s)) \ ds
\right\|
\\&= \left\| (\vec{x}_0-\vec{x}_0^*)
+ \int_{t_0}^t \vec{f}(s, \vec{\phi}(s)) - \vec{f}(s, \vec{\phi}^*(s))\ ds
+ \int_{t_0}^{t_0^*} \vec{f}(s, \vec{\phi}(s)) \ ds
\right\|
\\&\leq \left\| \vec{x}_0-\vec{x}_0^*\right\|
+ \left|\int_{t_0}^t \left\|\vec{f}(s, \vec{\phi}(s)) - \vec{f}(s, \vec{\phi}^*(s))\right\|\ ds\right|
+ \left| \int_{t_0}^{t_0^*} \left\|\vec{f}(s, \vec{\phi}(s))\right\| \ ds\right|
\\&\leq \left\| \vec{x}_0-\vec{x}_0^*\right\|
+ \left|\int_{t_0}^t K\left\|\vec{\phi}(s) - \vec{\phi}^*(s)\right\|\ ds\right|
+ M| t_0 - t_0^*|
\\&\leq (M+1)\delta
+ \left|\int_{t_0}^t K\left\|\vec{\phi}(s) - \vec{\phi}^*(s)\right\|\ ds\right|
\end{aligned}
++++
By applying Gronwall's inequality, we get
[stem]
++++
\|\vec{\phi}(t) - \vec{\phi}^*(t)\|
\leq (M+1)\delta e^{\left|\int_{t_0}^t K \ ds\right|}
\leq (M+1)\delta e^{K|b-a|}
++++
Therefore we can choose sufficiently small stem:[\delta] such that that the above
is less than stem:[\varepsilon] and hence we get the desired result.
====
****

NOTE: I split the Approximation and Uniqueness theorem into two parts

.Approximation Theorem
[.theorem]
****
Let stem:[\vec{f}] and stem:[\vec{g}] be continuous functions defined in
a rectangle
[stem]
++++
R = \left\{(t, \vec{x}) \ \middle| \ |t-t_0| \leq a, \ \|\vec{x} - \vec{x}_0\| \leq b\right\}
++++
and stem:[\vec{f}] satisfy the Lipschitz condition with constant stem:[K].
Let stem:[\vec{\phi}] and stem:[\vec{\psi}] be solutions of
the following respective IVPs
[stem]
++++
\vec{x}'=\vec{f}(t, \vec{x}),\ \vec{x}(t_0) = \vec{x}_0^{(1)}
++++
[stem]
++++
\vec{x}'=\vec{g}(t, \vec{x}),\ \vec{x}(t_0) = \vec{x}_0^{(2)}
++++
on some interval stem:[J] containing stem:[t_0].
Let stem:[\varepsilon > 0] and stem:[\delta > 0] be such that

[stem]
++++
\forall (t,\vec{x}) \in R:
\|\vec{f}(t, \vec{x}) - \vec{g}(t, \vec{x})\| \leq \varepsilon
\quad\text{and}\quad
\|\vec{x}_0^{(1)} - \vec{x}_0^{(2)}\| \leq \delta
++++
Then,
[stem]
++++
\forall t \in J: \|\vec{\phi}(t) - \vec{\psi}(t)\|
\leq \delta e^{K|t-t_0|} + \frac{\varepsilon}{K}\left(e^{K|t-t_0|} - 1\right)
++++

[.proof%collapsible]
====
[stem]
++++
\begin{aligned}
\|\vec{\phi}(t) - \vec{\psi}(t)\|
&\leq \|\vec{x}_0^{(1)} - \vec{x}_0^{(2)}\|
+ \left\|\int_{t_0}^t \vec{f}(s, \vec{\phi}(s)) - \vec{g}(s, \vec{\psi}(s)) \ ds\right\|
\\&\leq \|\vec{x}_0^{(1)} - \vec{x}_0^{(2)}\|
+ \left\|\int_{t_0}^t \vec{f}(s, \vec{\phi}(s)) - \vec{f}(s, \vec{\psi}(s)) \ ds\right\|
+ \left\|\int_{t_0}^t \vec{f}(s, \vec{\psi}(s)) - \vec{g}(s, \vec{\psi}(s)) \ ds\right\|
\\&\leq \delta
+ \varepsilon|t-t_0|
+ K\left|\int_{t_0}^t \|\vec{\phi}(s) - \vec{\psi}(s)\| \ ds\right|
\end{aligned}
++++

By using the strategy as in xref:exercises:chapter2.adoc#continuation-and-dependence-question2[Lecture 4 Question 2],
we obtain the desired result.

TIP: In case the above link is unreachable, the idea is two cases based on the sign 
of stem:[t-t_0]. Then, let the right hand side be some function stem:[h(t)]. Then, conduct
the same process as in the proof of Gronwall's inequality.
====
****

This theorem may be used to approximate solutions to hard initial value problems
by providing an approximation for stem:[f] using stem:[g].

.Uniqueness
[.corollary]
****
Let stem:[\vec{f}] satisfy the Lipschitz condition in some rectangle stem:[R].
Then the following IVP has at most one solution on any interval containing stem:[t_0]
[stem]
++++
\vec{x} = \vec{f}(t, \vec{x}),\ \vec{x}(t_0) = \vec{x}_0
++++
****

== Higher order differential equations
Consider the following IVP
[stem]
++++
\begin{aligned}
&x^{(n)} = f(t, x, \ldots x^{(n-1)})
\\&x(t_0) = \alpha_1, \quad x'(t_0) = \alpha_2, \quad\ldots\quad x^{(n-1)} = \alpha_{n}
\end{aligned}
++++
We have not yet developed any techniques to solve this problem.
However, we can convert it to a system of differential equations by
defining stem:[\vec{\alpha}_0 = (\alpha_1, \ldots \alpha_n)] and stem:[\vec{x} = (x_1, x_2, \ldots x_n)] as follows
[stem]
++++
x_1 = x,\quad x_2 = x',\quad \ldots\quad x_{n} = x^{(n-1)}
++++
Consider the following system of differential equations
[stem]
++++
\begin{aligned}
x_1' &= x_2 \quad &x_1(t_0) &= \alpha_1\\
x_2' &= x_3 \quad &x_2(t_0) &= \alpha_2\\
&\vdots\\
x_{n-1}' &= x_{n} \quad &x_{n-1}(t_0) &= \alpha_{n-1}\\
x_n' &= f(t, x_1, x_2, \ldots x_n) \quad &x_n(t_0) &= \alpha_n\\
\end{aligned}
++++
Then, it is not hard to see that this system of equations is equivalent to our original IVP and
stem:[x] is a solution of the IVP iff stem:[\vec{x} = (x, x', \ldots x^{(n-1)}] is a solution of the above system.
Therefore, we can apply the theorems developed for systems of differential equations to higher order differential equations.
