= Basics


(I am not sure how much preface to put)

The following definitions are relevant to a 2d plane.

Connected:: A non-empty set stem:[S] is called _connected_ if any two points of stem:[S]
can be joined by a continuous curve which lies entirely in stem:[S]

Open:: A non-empty set stem:[S] is called _open_ if each point can be enclosed by
a circle which lies entirely in stem:[S].

Domain:: Open and connected

Boundary point:: A point stem:[p] is a _boundary point_ of domain stem:[D] if
every circle around stem:[p] contains points in stem:[D] and points which are not in stem:[D].

Closed domain:: A domain together with its boundary points

The set of real or complex-valued functions having stem:[k] continuous
derivatives on stem:[I] is denoted by stem:[C^k(I)]

== What is a differential equation?
Equations which express a relationship between variables and their derivatives are called differential equations.
We do not classify the following as differential equations

* stem:[\frac{d}{dx}(xy) = y + x \frac{dy}{dx}]. This is an identity
* stem:[\left.\frac{dy}{dx}\right|_x = y_{x+1}] since the left and right hand side are evaluated at different positions
* stem:[\frac{dy}{dx} = \int_0^x e^sy(s) \ ds]. Note that we can convert this to a differential equation using
    the fundamental theorem of calculus (part 1).

== Solutions
A function stem:[\phi(t)] is a called a solution of a differential equation
(on interval stem:[I]) of order stem:[n] iff

. stem:[\phi(t)] is stem:[n] times differentiable on stem:[I]
. stem:[\phi(t)] satisfies the equation for each stem:[t \in I]

== Forms

General/implicit form::
+
[stem]
++++
F(t, x, x', \ldots, x^{(n)}) = 0
++++
where stem:[n \geq 1]
and stem:[F] is a function from some subset of stem:[\mathbb{R}^{n+2}]
to stem:[\mathbb{R}].

Normal/explicit/canonical form::
+
[stem]
++++
x^{(n)} = f(t, x, \ldots x^{(n-1)})
++++
where stem:[n\geq 1]
and stem:[f] is a function from some subset of stem:[\mathbb{R}^{n+1}]
to stem:[\mathbb{R}].

Operator form::
+
[stem]
++++
L(x) = r(t)
++++
where stem:[L] is a operator on stem:[x].
In this context,
an operator is a function which maps from functions to functions.
Note that the right hand side consists only of the independent variable.
The left hand side may have stem:[t] in it.

CAUTION: I believe this only makes sense in the case of linear operators.


// == Classification

== Degree and Order

Order::
The order of a differential equation is the order
of the highest derivative present in the equation.

Degree::
The degree of a differential equation is the power of the highest derivative.

== Linear
A differential equation in operator form is called _linear_
if the operator is linear.
For our cases, there are two fundamental linear operators of interest

- Scalar multiplication: stem:[L(x) = p(t)x]
- Differentiation: stem:[L(x) = x']

Since linear operators are closed under linear combinations and composition,
we see that a linear differential equation of order stem:[n]
has the form
[stem]
++++
L(x) := \sum_{k=0}^n p_k(t) x^{(k)} = r(t)
++++
If stem:[r(t) = 0], then we call the linear DE _homogeneous_ 
otherwise we call it _non-homogeneous_.

== Separable
The differential equation stem:[x' = f(t, x)] is _separable_ if there exists
a continuous function stem:[h(t)] and a continuously differentiable function stem:[g(x) \neq 0]
such that
[stem]
++++
f(t, x) = h(t)g(x)
\quad\text{or}\quad
f(t,x) = \frac{h(t)}{g(x)}
++++
In the case stem:[h(t)=1], then the equation becomes stem:[x' = g(x)]
and is called an _autonomous equation_ otherwise it is called _non-autonomous_.

Such equations can be solved using the separation of variables technique.


== Exact
Consider the following differential equation
[stem]
++++
M(t,x) \ dt + N(t,x) \ dx = 0
++++
This DE is called _exact_ if there exists a function stem:[u(t,x)]
such that stem:[M\ dx + N\ dx = du]. That is, the differential equation is equivalent to
[stem]
++++
\frac{d}{dt} u (t,x) = 0
++++

In the case where stem:[M] and stem:[N] are continuous with continuous partial derivatives in
a given closed rectangle, a necessary and sufficient condition for the DE to be exact is
[stem]
++++
\frac{\partial M}{\partial x} = \frac{\partial N}{\partial t}
++++

[.proof%collapsible]
====
Clearly the forward direction is true by Clairaut's theorem.
So, now we take that 
stem:[\frac{\partial M}{\partial x} = \frac{\partial N}{\partial t}]
and we need to find stem:[u] such that stem:[u_t = M] and stem:[u_x=N].

Since stem:[M] is continuous, by the FTC (part 1),
there exists stem:[G(t,x)] such that stem:[G_t = M]
and stem:[G] has continuous second derivatives.
We may choose such a stem:[G(t,x) = \int_{t_0}^t M(s) \ ds].
Then,
[stem]
++++
\frac{\partial N}{\partial t}
= \frac{\partial M}{\partial x}
= \frac{\partial^2 G}{\partial x \partial t}
= \frac{\partial }{\partial t} \left(\frac{\partial G}{\partial x}\right)
\implies \frac{\partial }{\partial t}\left(N-\frac{\partial G}{\partial x}\right)  =0
++++
We may now take
[stem]
++++
u(t,x) = G(t,x) + \int_{x_0}^x N(t,s) - G_x(t,s) \ ds
++++
We may then verify that that the equation is exact since

[stem]
++++
\frac{\partial }{\partial t}u(t,x) 
= M(t,x) + \int_{x_0}^x \frac{\partial }{\partial t}\left(N(t,s)- G_x(t,s)\right) \ ds
= M(t,x) + 0
++++
[stem]
++++
\frac{\partial}{\partial x} u(t,x) = G_x(t,x) + N(t,x)-G_x(t,x) = N(t,x)
++++

[NOTE]
=====
We could have equivalently taken
[stem]
++++
u(t,x) = \int_{t_0}^t M(r,x) \ dr + \int_{x_0}^x N(t,s) \ ds
    - \int_{x_0}^x\int_{t_0}^t P(r,s) \ ds \ dr
++++
where
[stem]
++++
P(r,s) = M_x(r,s) = N_t(r,s)
++++
Defining stem:[u] like this is symmetric and is easier to compute.
It is also worth knowing that the order of integration does not matter in the third integral
since stem:[P] is continuous on a closed region which implies that it must be bounded and hence
we may use Fubini's theorem.
=====
[IMPORTANT]
The use of the Leibniz rule is valid since the partial derivatives of the functions being integrated are
continuous.
====

[REMARK]
====
In the notes, the following remark is made. In the proof, we showed that 
one valid stem:[u] has the form
[stem]
++++
u(t,x) = G(t,x) + \int_{x_0}^x N(t,s) - G_x(t,s) \ ds
++++
where stem:[G_t = M]. In this form the integrated term is independent of stem:[t] (has zero partial derivative).
Therefore, we may simply say that
[stem]
++++
u(t,x) = \int M(t,x) \ dt + \int \text{terms of }N\text{ without }t \ dx
++++
====

=== Conversion to Exact
We may force a differential equation to be exact by multiplying by an _integrating factor_
stem:[\mu(t,x)].
The resulting equation is now
[stem]
++++
\mu(t,x) M(t,x)\ dt + \mu(t,x) N(t,x) \ dx = 0
++++
Then, this DE is exact iff
[stem]
++++
M\mu_x + \mu M_x = (M\mu)_x = (N\mu_t) = N\mu_t + \mu N_t
++++
There are three special cases of interest

Function of t only::
+
[stem]
++++
\frac{\mu'}{\mu} = \frac{M_x - N_t}{N}
++++
Function of x only::
+
[stem]
++++
\frac{\mu'}{\mu} = \frac{N_t - M_x}{M}
++++
Function of xy only::
+
[stem]
++++
\frac{\mu'}{\mu} = \frac{N_t - M_x}{tM -xN}
++++
+
Note that if stem:[M= xf(tx)] and stem:[N=tg(tx)] with stem:[f\neq g],
then solving this yields the integrating factor
stem:[\mu(tx) = \frac{1}{tM -xN}]

In all three cases, we first ensure that the right hand side is of the correct form,
then solve for stem:[\mu].

==== Other Integrating factors

* stem:[\frac{1}{Mt + Nx}] if stem:[Mt + Nx \neq 0] and stem:[\frac{M}{N}] is a function of stem:[\frac{t}{x}]
+
[.proof%collapsible]
====
[stem]
++++
\frac{\partial}{\partial x}\frac{M}{Mt + Nx}
= \frac{1}{(Mt +Nx)^2}
\left(
M_x(Mt +Nx) - M(M_x t +N_x x + N)
\right)
= \frac{1}{(Mt +Nx)^2}
\left(
M_xNx  - MN_x x -NM
\right)
++++
[stem]
++++
\frac{\partial}{\partial t}\frac{N}{Mt + Nx}
= \frac{1}{(Mt +Nx)^2}
\left(
N_t(Mt+Nx) - N(M_tt + M + N_tx)
\right)
= \frac{1}{(Mt +Nx)^2}
\left(
N_tM t - NM_t t- NM
\right)
++++
Note both are equal iff
[stem]
++++
\begin{aligned}
&M_xNx - MN_x x  = N_tM t - NM_t t
\\&\iff \frac{M_x}{M}x - \frac{N_x}{N}x  = \frac{N_t}{N}t - \frac{M_t}{M}t
\\&\iff \begin{bmatrix}t&x\end{bmatrix} \cdot \begin{bmatrix}\frac{\partial}{\partial t} & \frac{\partial}{\partial x}\end{bmatrix}\ln\left(\frac{M}{N}\right)
    = \left(t \frac{\partial}{\partial t} + x \frac{\partial}{\partial x}\right)\ln\left(\frac{M}{N}\right) = 0
\\&\iff \exists \mu : \begin{bmatrix}\frac{\partial}{\partial t} & \frac{\partial}{\partial x}\end{bmatrix}\ln\left(\frac{M}{N}\right)
        = \begin{bmatrix}-\mu x & \mu t\end{bmatrix}
        \quad\text{where }\mu \text{ is a function}
\end{aligned}
++++
From a bit of trial and error, we expect that stem:[\ln\left(\frac{M}{N}\right)]
is a function of stem:[\frac{t}{x}]. The substitution stem:[t=ux] does not work on its own.
Instead notice that the differential is very similar to the chain rule. In particular,
if stem:[(t,x)] is a function of stem:[(u, v)], we get that for any function stem:[f(t,x)]
[stem]
++++
\frac{\partial f}{\partial v}
= \frac{\partial f}{\partial t}\frac{\partial t}{\partial v}
+ \frac{\partial f}{\partial x}\frac{\partial x}{\partial v}
++++
Therefore, we want stem:[\frac{\partial t}{\partial v} = t] and stem:[\frac{\partial x}{\partial v}=x].
The clear solution to this is stem:[t=ue^v] and stem:[x=\frac{1}{u}e^v].
However, with this, it is impossible to get stem:[x=0].
So, instead we use stem:[t=\sqrt{u}v] and stem:[x = \frac{v}{\sqrt{u}}].
Then, we get that
[stem]
++++
\begin{aligned}
&\text{Multiplication by the integrating factor yields an exact DE}
\\&\iff \left(t \frac{\partial}{\partial t} + x \frac{\partial}{\partial x}\right)\ln\left(\frac{M}{N}\right) = 0
\\&\iff \left(\frac{1}{v}\frac{\partial}{\partial v}\right)\ln\left(\frac{M}{N}\right) = 0
\\&\iff \ln\left(\frac{M}{N}\right) = f(u) = f\left(\frac{t}{x}\right)
\\&\iff \frac{M}{N} = g\left(\frac{t}{x}\right)
\end{aligned}
++++
Therefore, the integrating factor stem:[\frac{1}{Mx+Ny}] is valid if
stem:[\frac{M}{N}] is a function of stem:[\frac{t}{x}].

NOTE: Verification is not necessary, but it is nice to see that it works

To verify, we return to the necessary and sufficient condition for exact. We showed above
that the integrating factor causes the equation to be exact iff
[stem]
++++
M_xNx - MN_x x  = N_tM t - NM_t t
++++
By substituting stem:[M = gN] where stem:[g] is a function of stem:[\frac{t}{x}], we see that
[stem]
++++
M_xNx - MN_xx
= \left(-\frac{t}{x^2}g'N + gN_x\right)Nx - gNN_xx
= -\frac{t}{x}g'N^2
++++
and
[stem]
++++
N_tM t - NM_t t
= N_t gN t - N\left(\frac{1}{x}g'N + gN_t\right)t
= - \frac{t}{x} g'N^2
++++
Therefore, we have not made any mistakes
====

* stem:[t^\alpha x^\beta] for some stem:[\alpha] and stem:[\beta] if stem:[M] and stem:[N] are (multivariate) polynomials in stem:[t] and stem:[x].
+
[WARNING]
====
I do not know how correct this is. Consider stem:[M=1] and stem:[N=t^2+x^2].
Then,
[stem]
++++
\beta t^\alpha x^{\beta-1}
=
(t^\alpha x^\beta M)_x
= 
(t^\alpha x^\beta N)_t
=
(\alpha+2)t^{\alpha+1} x^\beta + \alpha t^{\alpha-1}x^\beta
++++
Then, at least one of the two terms on the right must be non-zero.
However, the powers of stem:[x] do not match.
====


== Boundary value problem and initial value problem
Differential equations may also have constraints. Two common sets of constraints are 
_initial conditions_ and _boundary conditions_.
A problem with one of these constraints is called 

Initial value problem (IVP) (or Cauchy's problem)::
if initial values are given. Usually of the form stem:[x(t_0) = x_0]


Boundary value problem (BVP)::
if the boundary conditions are given. Usually of the form stem:[x(t_0)=x_1]
and stem:[x(t_1)=x_1].
Note that this is a one dimensional example on an interval.
For higher dimensional spaces, the boundary consists of the topological boundary.


== Well posed
We say that a differential equation is _well posed_ if

. A solution exists satisfying the conditions
. There is a unique solution corresponding to each of the conditions
. Solutions depend continuously on the conditions.

REMARK: Perhaps the final condition is needed since any set of conditions measured in the real
world has some intrinsic error. If the solution is continuous on the conditions,
the error of the solution is dependent on the error of the conditions.



== Special classifications


=== Bernoulli
A Bernoulli equation is one of the form
[stem]
++++
x'+p(t) x = r(t)x^\alpha
++++
If stem:[\alpha = 0] or stem:[\alpha=1], this DE is linear otherwise it is non linear.
By setting stem:[v = x^{1-\alpha}] we can transform this into a linear DE as follows
[stem]
++++
\begin{aligned}
v'
= (1-\alpha)x^{-\alpha}x'
= (1-\alpha)x^{-\alpha}(rx^\alpha - px)
= (1-\alpha)(r - px^{1-\alpha})
= (1-\alpha)(r - pv)
\end{aligned}
++++

