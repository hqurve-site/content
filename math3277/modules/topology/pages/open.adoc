= Open Sets

Let stem:[A \subseteq X], then stem:[A] is _open_ iff all of its
elements are xref:interior.adoc[interior points]. That is
stem:[A \subseteq int(A)] which could be equivalently stated as
stem:[int(A) = A] since necessarily stem:[int(A) \subseteq A].

Then, immediately, we see that stem:[X] is open since all points
would necessarily be interior and stem:[\varnothing]
is also open since stem:[int(\varnothing) \subseteq \varnothing]
and hence stem:[int(\varnothing) = \varnothing].

== Equivalent definition
Let stem:[A \subseteq X], then the following are equivalent

. stem:[A] is open
. stem:[\partial A \subseteq X\backslash A]
That is stem:[A] does not contain its xref:interior.adoc[boundary]

.Proof
[%collapsible]
====
First suppose that stem:[A] is open then immediately,
we get that stem:[x\in \partial A \implies x \notin int(A) \implies x\notin A].

Conversely, suppose that stem:[\partial A \subseteq X\backslash A]. Then
[stem]
++++
x\in A \implies x \notin \partial A \implies x \notin (\partial A \cup ext(A))
\implies x\in int(A)
++++
since stem:[ext(A) \subseteq X\backslash A] and the interior,
partial and exterior of a set partition stem:[X]
 [xref:interior.adoc#_partitioning[read more]].

====

== Collections of open sets
The following two statements are true

. The union of any collection of open sets is also open
. The intersection of a finite collection of open sets is also open

.Proof of 1
[%collapsible]
====
Let the collection of open sets be stem:[\mathcal{U}].
Then
[stem]
++++
\begin{aligned}
x \in \bigcup_{U \in \mathcal{U}}  U
&\implies \exists U \in \mathcal{U}: x \in U = int(U)
\\&\implies \exists U \in \mathcal{U}: \exists r > 0: B(x,r) \subseteq U
\\&\implies \exists r> 0: \exists U \in \mathcal{U}: B(x,r) \subseteq U
\\&\implies \exists r> 0: B(x,r) \subseteq \bigcup_{U\in \mathcal{U}} U
\\&\implies x \in int\left(\bigcup_{U\in \mathcal{U}} U\right)
\end{aligned}
++++
====
.Proof of 2
[%collapsible]
====
Note that since the collection is finite, we only need to show that
the intersection of two open sets is also open and the result would
follow by repeated application.

Let stem:[A, B \subseteq X] be open sets. Then by
using xref:interior.adoc#_properties_of_interiors[property 1] we get that
[stem]
++++
int(A\cap B) = int(A) \cap int(B) = A\cap B
++++
and we are done.

For a counter example as to why this doesn't
hold for an arbitrary collection of sets
consider
[stem]
++++
\mathcal{U} = \left\{(-x,x) : x \in \mathbb{R}\right\}
++++
with the usual metric. Then the intersection of the
elements in stem:[\mathcal{U}] is stem:[\{0\}] which is not open.
====


