= Properties of Statistics

In this page, we let stem:[\chi] represent the joint sample space.
We study different properties of statistics.


The properties and their intuition are

[horizontal]
Sufficiency:: Does this statistic capture all the information necessary to estimate stem:[\theta]?
    For example, can we generate the MLE using this statistic
Ancillary:: This statistic captures no information about stem:[\theta].
    So, knowing this statistic does not give us a better idea of what stem:[\theta] may be.
Complete:: This statistic only captures information about stem:[\theta].

.Amount of information captured by each type of statistic
image::information-diagram.png[width=60%]


== Sufficiency
Let stem:[T(\vec{X})] be a statistic where stem:[\vec{X} = (X_1,\ldots X_n)] are iid
with a population parameter stem:[\theta].
Then, stem:[T(X)] is *sufficient* for stem:[\theta] if and only if
[stem]
++++
f_{\vec{X}}(\vec{x} \ | \ T(\vec{X}) = t, \theta) = h(\vec{x})
++++
That is, the above function does not depend on stem:[\theta].

.Equivalent definition for sufficiency
[.theorem]
****
Let stem:[f_{\vec{X}}(\vec{x} \ | \ \theta)] be the joint pdf/pmf of stem:[\vec{X}]
and let stem:[q(t \ |  \ \theta)] be the pdf/pmf of stem:[T(\vec{X})].
Then stem:[T(\vec{X})] is sufficient for stem:[\theta] if for stem:[\vec{x} \in \chi]
[stem]
++++
\frac{f_{\vec{X}}(\vec{x} \ |\ \theta)}{q(T(\vec{x}) \ | \ \theta)}
++++
is a constant function of stem:[\theta].

[.proof%collapsible]
====
NOTE: We prove the discrete case. The continuous case is identical.

We use the definition of sufficiency directly.
[stem]
++++
\begin{aligned}
P(\vec{X} = \vec{x} \ | \ T(\vec{X}) = t,\ \theta)
&= \frac{
    P(\vec{X} = \vec{x} , \ T(\vec{X}) = t,\ \theta)
}{
    P(T(\vec{X}) = t,\ \theta)
}
\\&= \begin{cases}
    \frac{
        P(\vec{X} = \vec{x},\ \theta)
    }{
        P(T(\vec{X}) = t,\ \theta)
    }
    &\quad\text{if } T(\vec{x}) = t\\
    0 & \quad\text{otherwise}
\end{cases}
\\&= \begin{cases}
    \frac{
        f_{\vec{X}}(\vec{x} \ | \ \theta)
    }{
        q(T(\vec{x}) \ | \ \theta)
    }
    &\quad\text{if } T(\vec{x}) = t\\
    0 & \quad\text{otherwise}
\end{cases}
\end{aligned}
++++
Therefore, we it is clear that 

stem:[P(\vec{X} = \vec{x} \ | \ T(\vec{X}) = t, \ \theta)] is constant with respect to stem:[\theta]
if and only if 
stem:[
\frac{
    f_{\vec{X}}(\vec{x} \ | \ \theta)
}{
    q(T(\vec{x}) \ | \ \theta)
}
] is constant with respect to stem:[\theta].
====
****


.Fisher factorization theorem
[.theorem]
****
Let stem:[f_{\vec{X}}(\vec{x} \ | \ \theta)] be the joint pdf/pmf of stem:[\vec{X}].
A statistic stem:[T(\vec{X})] is sufficient for stem:[\theta] if and only if
there exists functions stem:[g(t | \theta)] and stem:[h(\vec{x})]
such that
[stem]
++++
\forall \vec{x} \in \chi: \forall \theta \in \Omega:
f_{\vec{X}}(\vec{x} \ | \ \theta) = g(T(\vec{x}) \ | \ \theta)h(\vec{x})
++++

[.proof%collapsible]
====
NOTE: We prove the discrete case. The continuous case is identical.

For the forward direction, we can use the definition of sufficiency.
Suppose stem:[T(\vec{X})] is sufficient for stem:[\theta].
Let stem:[g(t \ | \ \theta) = P(T(\vec{X}) = t \ | \ \theta)]
and stem:[h(\vec{x}) = P(\vec{X} = \vec{x} \ | \ T(\vec{X}) = T(\vec{x}))].
Since stem:[T(\vec{X})] is sufficient for stem:[\theta], stem:[h(\vec{x})] is constant
with respect to stem:[\theta]. So, we have that
[stem]
++++
f(\vec{x} \ | \ \theta)
= P(\vec{X} = \vec{x}\ | \ \theta)
= P(\vec{X} = \vec{x}, \ T(\vec{X}) = T(\vec{x})\ | \ \theta)
= P(\vec{X} = \vec{x} \ | \ T(\vec{X}) = T(\vec{x}),\ \theta)P(T(\vec{X}) = T(\vec{x})\ | \ \theta)
= h(\vec{x})g(T(\vec{x}) \ | \ \theta)
++++
Hence, we have proven that such functions stem:[g] and stem:[h] exist.

Now, we prove the converse. Suppose there exists functions stem:[g(t \ | \ \theta)] and stem:[h(\vec{x})] satisfying the necessary
condition. We need to prove that stem:[T(\vec{X})] is sufficient.
To do this, we use the previous theorem.
We need to find an expression for the distribution of stem:[T(\vec{X})].
Let
[stem]
++++
A_t = \{\vec{x} \in \chi: \ T(\vec{x}) = t \}
++++
Then, the pmf of stem:[T(\vec{X})] is
[stem]
++++
q(t \ | \theta) = P(T(\vec{X}) = t \ | \ \theta)
= \sum_{\vec{y} \in A_t}f_{\vec{X}}(\vec{y} \ |\ \theta)
= \sum_{\vec{y} \in A_t}g(T(\vec{y}) \ | \ \theta)h(\vec{y})
= \sum_{\vec{y} \in A_t}g(t \ | \ \theta)h(\vec{y})
++++
So,
[stem]
++++
\frac{f_{\vec{X}}(\vec{x} \ | \ \theta)}{q(T(\vec{x}) \ | \theta)}
= \frac{g(T(\vec{x}) \ | \ \theta)h(\vec{x})}{\sum_{\vec{y} \in A_t}g(T(\vec{x}) \ | \ \theta)h(\vec{y})}
= \frac{h(\vec{x})}{\sum_{\vec{y} \in A_t}h(\vec{y})}
++++
which is clearly constant wrt to stem:[\theta]. This implies that stem:[T(\vec{X})] is sufficient for stem:[\theta].

====
****

=== Minimal Sufficiency
A sufficient statistic stem:[T(\vec{X})] is called a *minimal sufficient* statistic
if for any other sufficient statistic stem:[T'(\vec{X})],
stem:[T(\vec{X})] is a function of stem:[T'(\vec{X})].

[.theorem]
****
Let stem:[f_{\vec{X}}(\vec{x} \ | \ \theta)] be the joint pdf/pmf of random sample
stem:[\vec{X} \in \chi]. Suppose that there exists stem:[T(\vec{x})]
such that for all stem:[\vec{x}, \vec{y} \in \chi],
the ratio
[stem]
++++
\frac{f_{\vec{X}}(\vec{x} \ | \ \theta)}{f_{\vec{X}}(\vec{y} \ | \ \theta)}
++++
is constant as a function of stem:[\theta] if and only if stem:[T(\vec{x}) = T(\vec{y})].
Then, stem:[T(\vec{X})] is a minimal sufficient statistic.
****

.Different definitions for sufficiency for Bernoulli(p)
====
Let stem:[X_1,\ldots X_n \sim Ber(p)] distribution.
Then, we want to consider stem:[T(\vec{X}) = \sum_{i=1}^n X_i]

For the definition of sufficiency, we need the distribution of stem:[T(\vec{X})].
This is simple since we know that the sum of bernoulli random variables is binomial.
Therefore,
[stem]
++++
\begin{aligned}
f_{\vec{X}}(\vec{x}\ |\ T(\vec{X}) = t, p)
&= \frac{P(\vec{X} = \vec{x}, \ T(\vec{X}) = t\ | \ p)}{P(T(\vec{X}) = t \ | \ p)}
\\&= \begin{cases}
\frac{P(\vec{X} = \vec{x}, \ T(\vec{X}) = T(\vec{x})\ | \ p)}{P(T(\vec{X}) = T(\vec{x}) \ | \ p)}
,&\quad \text{if } T(\vec{x}) = t\\
0
,&\quad \text{if } T(\vec{x}) \neq t
\end{cases}
\\&= \begin{cases}
\frac{P(\vec{X} = \vec{x}\ | \ p)}{P(T(\vec{X}) = T(\vec{x}) \ | \ p)}
,&\quad \text{if } T(\vec{x}) = t\\
&\quad\text{since the space of }T(\vec{X}) =T(\vec{x})\text{ is a superset of } \vec{X} = \vec{x}
\\
0
,&\quad \text{if } T(\vec{x}) \neq t
\end{cases}
\\&= \begin{cases}
\frac{\prod_{i=1}^n p^{x_i}(1-p)^{1-x_i}}{\binom{n}{\sum_{i=1}^n x_i}p^{\sum_{i=1}^n x_i}(1-p)^{n-\sum_{i=1}^n x_i}}
,&\quad \text{if } T(\vec{x}) = t\\
\\
0
,&\quad \text{if } T(\vec{x}) \neq t
\end{cases}
\\&= \begin{cases}
\frac{1}{\binom{n}{\sum_{i=1}^n x_i}}
,&\quad \text{if } T(\vec{x}) = t\\
\\
0
,&\quad \text{if } T(\vec{x}) \neq t
\end{cases}
\end{aligned}
++++
This is independent of stem:[p] and hence stem:[T(\vec{X})] is a sufficient statistic for stem:[p].

If we use the equivalent definition for sufficiency,
we obtain the same term as the piecewise function above when stem:[T(\vec{x}) =t].
So, in practice, there is no benefit to using this over the definition
besides the fact that we can avoid the use of a piecewise function.

Finally, we can use the fisher factorization theorem.
[stem]
++++
\begin{aligned}
f_{\vec{X}}(\vec{x} \ | \ p)
= \prod_{i=1}^n p^{x_i}(1-p)^{1-x_i}
= p^{\sum_{i=1}^n x_i}(1-p)^{n-\sum_{i=1}^n x_i}
= p^{T(\vec{x})}(1-p)^{n-T(\vec{x})}
\end{aligned}
++++
So, if we let
[stem]
++++
g(t|p) = p^{t}(1-p)^{n-t}, \quad\text{and}\quad
h(\vec{x}) = 1
++++
The fisher factorization theorem holds and we have that stem:[T(\vec{X})]
is a sufficient statistic for stem:[p].

We can now check whether this statistic is minimal sufficient.
To do this, we use the respective theorem. Note that

[stem]
++++
\frac{f_{\vec{X}}(\vec{x} \ | \ p)}{f_{\vec{X}}(\vec{y} \ | \ p)}
= \frac{p^{T(\vec{x})}(1-p)^{n-T(\vec{x})}}
    {p^{T(\vec{y})}(1-p)^{n-T(\vec{y})}}
= \left(\frac{p}{1-p}\right)^{T(\vec{x}) - T(\vec{y})}
++++
So, the above is constant wrt stem:[p] iff stem:[T(\vec{x}) = T(\vec{y})].
Therefore, we have that stem:[T(\vec{x})] is minimal sufficient for stem:[p].
====



== Ancillary
A statistic stem:[T(\vec{X})] is an *ancillary* statistic for stem:[\theta] if its distribution
does not depend on stem:[\theta].

NOTE: This implies that the likelihood function is constant and hence information about
the statistic does not provide information about the parameter.

.Ancillary from location family
====
Let stem:[X_1,\ldots X_n] be from a location family with pdf stem:[f(x-\mu)]
where stem:[-\infty < \mu < \infty].
Then, the range stem:[R = X_{(n)} - X_{(1)}] is an ancillary statistic.

Assume that the cdf exists and it is stem:[F(x-\mu)].
To see this, let stem:[Z_i = X_i - \mu].
Then the stem:[Z_i] are iid with pdf stem:[f(x)] and cdf stem:[F(x)].
The cdf of stem:[R] is then
[stem]
++++
F_R(r|\mu) = P(R\leq r | \mu)
= F(X_{(n)}-X_{(1)} \leq r | \mu)
= F(Z_{(n)}-Z_{(1)} \leq r | \mu)
= F(Z_{(n)}-Z_{(1)} \leq r)
++++
since the stem:[Z_i] do not depend on stem:[\mu].
So, stem:[R] is an ancillary statistic.
====

== Complete
IMPORTANT: This is a property of a *family* of random variables, not just one random variable.
Also, this can be rephrased for a family of random variables, not just a statistic.

Let stem:[\tau = \{f_T(t|\sigma) \ | \ \tau \in \Sigma\}]
be a family of pdfs (or pms) for a statistic stem:[T(\vec{X})].
The family of probability distributions is called *complete*
if for all stem:[g(t)]
[stem]
++++
\left\{\forall \theta \in \Omega: E[g(T) \ | \ \theta] = 0\right\}
\implies 
\left\{\forall \theta \in \Omega: P(g(T) =0\ | \ \theta) =1\right\}
++++
That is stem:[g(T) = 0] *almost surely*.
Equivalently, we say that stem:[T(\vec{X})] is a *complete statistic*.

.Not complete with finite parameter space
====
TIP: This argument can be extended to any finite parameter space for the Poisson family

Let stem:[\Omega = \{1,2\}]
and stem:[\tau = \left\{f_T: f_T(t|\lambda)=\frac{e^{-\lambda}\lambda^t}{t!}, t=0,\ldots, \lambda\in \Omega\right\}].
We will show that this family is not complete.
To do this, define
[stem]
++++
g(t) = \begin{cases}
g_0 ,\quad&\text{if } t=0\\
g_1 ,\quad&\text{if } t=1\\
g_2 ,\quad&\text{if } t=2\\
0 ,\quad&\text{otherwise}\\
\end{cases}
++++
Then,

* When stem:[\lambda=1], stem:[E[g(T) | \lambda=1\] = g_0 + g_1 + \frac12g_2 = 0]
* When stem:[\lambda=2], stem:[E[g(T) | \lambda=1\] = g_0 + 2g_1 + 2g_2 = 0]

We then find stem:[(g_0, g_1, g_2)] which lies in the nullspace
of the matrix defined by the above two expressions.
This can be done by row reduction.
One possible choice is
[stem]
++++
g(t) = \begin{cases}
2 ,\quad&\text{if } t=0\\
-3 ,\quad&\text{if } t=1\\
2 ,\quad&\text{if } t=2\\
0 ,\quad&\text{otherwise}\\
\end{cases}
++++
From this we obtain that the two expectations are zero but stem:[g(t) \not\equiv 0].
So the family is not complete.
====

.Complete
====
Let stem:[\Omega = (0,\infty)]
and stem:[\tau = \left\{f_T: f_T(t|\lambda)=\frac{e^{-\lambda}\lambda^t}{t!}, t=0,\ldots, \lambda\in \Omega\right\}].
Let stem:[T(\vec{X}) = \sum_{i=1}^n X_i] where the stem:[X_i] are iid stem:[Poisson(\lambda)].
We will show that this stem:[T(\vec{X})] is a complete statistic.

First note that stem:[T(\vec{X}) \sim Poisson(n\lambda)]
Consider an arbitrary stem:[g(t)] such that stem:[E[g(T) | \lambda\]=0] for all stem:[\lambda > 0].
Then this implies that
[stem]
++++
\sum_{t=0}^\infty g(t)\frac{n^t}{t!}\lambda^t = 0\quad \forall \lambda > 0
++++
We view the above as a power series in stem:[\lambda].
This implies that each coefficient is zero and hence stem:[g(t) \equiv 0],
hence proving completeness.

To see why this implies that each coefficient is zero, recall that the expectation
exists iff we have absolute convergence. Therefore, we can take derivatives and
inductively prove that each coefficient must be zero.
====

== Theorems
The numbers quoted are the references in the book by Casella and Berger (2002)

.Functions of statistics
[.proposition]
****
Let stem:[T(\vec{X})] be a statistic and stem:[T^* = r(T)]
where stem:[r] is a one-to-one function
Then

* If stem:[T] is sufficient, then stem:[T^*] is also sufficient
* If stem:[T] is complete, then stem:[T^*] is also complete.
****

[.proposition]
****
Let stem:[T(\vec{X})] be a complete, sufficient statistic. Then,
stem:[T(\vec{X})] is also *minimal sufficient*.
****

.Basu's Theorem (Theorem 6.2.24)
[.theorem]
****
If stem:[T(\vec{X})] is a complete sufficient statistic, then stem:[T(\vec{X})]
is *independent* of every ancillary statistic.
****

.Example of ancillary
====
Let stem:[X_1,\ldots X_n \sim exp(\theta)].
Then stem:[T(\vec{X}) = \sum_{i=1}^n X_i] is
a complete, sufficient statistic.
Define
[stem]
++++
U(\vec{X}) = \frac{X_1}{\sum_{i=1}^n X_i}
++++
It is a well known fact that stem:[U(\vec{X}) \sim Beta(1,n-1)].

Since stem:[U(\vec{X})] is an ancillary statistic, by
Basu's theorem, stem:[T(\vec{X})\perp U(\vec{X})]
====
