= Quality control charts

NOTE: See Section 5.6 of Applied Multivariate Statistics (6th)

Often we may need to monitor systems to ensure proper quality control.
These systems will have some inherent variation, but we need
to be able to identify special cases of variation.
These special cases of variation may indicate that the system
is no longer running within its specified operating parameters and requires
servicing or replacement.

== Univariate case
In order perform monitoring, we plot the sample mean stem:[\overbar{x}]
as well as the observations at the given times, stem:[(t, x(t))].
We then monitor the observations to see if they deviate more
than stem:[3s] away from stem:[\overbar{x}]
where stem:[s] is the standard deviation.
We choose stem:[3s] since virtually all usual observations will occur with
this variation.

== Bivariate case
For the multivariate case, we simply plot the stem:[\vec{x}(t)] as points
in a scatter plot and monitor whether any of then go outside of the
99% confidence ellipse.

== Multivariate case
The multivariate case is a bit more complex.
Instead of monitoring the stem:[(t, x(t))], we monitor stem:[(t, d(\vec{x}(t), \overbar{\vec{x}})]
where stem:[d] is the statistical distance formula
[stem]
++++
d(\vec{x}, \overbar{\vec{x}}) = 
(\vec{x} - \overbar{\vec{x}})^T
\mat{S}^{-1}
(\vec{x} - \overbar{\vec{x}})
\approx \chi^2_p
++++
We then monitor whether or not these distances fall beyond stem:[\chi^2_p(0.05)]
and stem:[\chi^2_p(0.01)]
