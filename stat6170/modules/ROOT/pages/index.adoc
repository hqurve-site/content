= Multivariate Analysis

This course is taught by Dr. Vrijesh Tripathi.
Since most of the students taking this course this semester are data science students,
the course is more applied.

NOTE: Since this course is purely about the multivariate normal
and the multivariate normal is very useful, I have decided that
the majority of the content be placed in xref:stats::multivariate-normal.adoc[].


== Table of contents

include::../nav.adoc[]

== Materials

Essential Text::
** Applied Multivariate Statistical Analysis, by Richard A. Johnson and Dean W. Wichern;
   6th edition; Pearson International Edition.

Other suggested texts::
** Using Multivariate Statistics, by Tabachnick BG and Fidell LS; 6th Edition; Pearson.
** Multivariate Data Analysis A Global Perspective, by Hair JF, Black WC, Babin BJ and Anderson
   RE; 7th Edition; Pearson.


