= Dedekind cuts

Let stem:[C \subseteq \mathbb{Q}]. Then stem:[C] is called a _cut_ if

* stem:[C \neq \varnothing] and stem:[C \neq \mathbb{Q}]
* No first element: stem:[\forall c \in C: \forall a \in \mathbb{Q}: a < c \implies a \in C]
* No last element: stem:[\forall c \in C: \exists b \in C: b > c]


== Nice properties

=== Characterization of inclusion
Let stem:[C] be a cut. Then,
[stem]
++++
x \in C \iff [ \exists c \in C: x < c ]
++++
This is clearly true and may seem insignificant, however, notice that this implies that
[stem]
++++
C' = \{x \in \mathbb{Q}: \forall c \in C: x \geq c\} =\{x \in \mathbb{Q}: \forall c \in C: x > c\}
++++
Notice that the second two sets are equal since if stem:[x = c] for any stem:[c \in C] there exists
stem:[c_1 > c \in C] such that stem:[x \geq c_1 > c]; which is a contradiction.

Additionally, stem:[C] is precisely the set of elements bounded by stem:[C']. That is
[stem]
++++
C = \{x \in \mathbb{Q}: \forall d \in C': c < d\} = C_1
++++
Then, we have that stem:[C \subseteq C_1]. Also,
notice that stem:[x \in C_1] implies that stem:[x \notin C'];
hence stem:[C_1 \subseteq \mathbb{Q}\backslash C' = C]. Therefore, we get equality.

=== Stradling the border
Let stem:[C] be a cut and stem:[r \in \mathbb{Q}^+ ]. Then, there exists stem:[c \in C]
such that stem:[c + r \notin C].

Furthermore, if stem:[r > 1] and stem:[C] is positive, then stem:[\exists c \in C]
such that stem:[cr \notin C]. Equivalently, stem:[C < D].

.Proof
[%collapsible]
====
.Proof of first
[%collapsible]
=====
Suppose BWOC that the opposite is true. That is, 
[stem]
++++
\forall c \in C: c + r \in C
++++
Now, we would show that stem:[C = \mathbb{Q}]. Consider arbitrary
stem:[q \in \mathbb{Q}] and focus on stem:[c \in C]. If stem:[q \leq c], since stem:[C] is a cut
stem:[q \in C]. On the other hand if stem:[q > c], then stem:[\frac{q-c}{r} > 0].
Let stem:[\frac{q-c}{r} = \frac{A}{B}] where stem:[A] and stem:[B]
are both positive. Also let stem:[N = AB].
Then,
[stem]
++++
c + Nr = c + ABr > c + \frac{A}{B}r = c + (q-c) = q
++++
Now, by induction, we know that stem:[c + Nr \in C]. Then,
since stem:[C] is a cut, stem:[q \in C]. Therefore stem:[C = \mathbb{Q}],
contradiction.

REMARK: We could have just used the archemedian property, however, I haven't
proven it here as yet.
=====
.Proof of second
[%collapsible]
=====
Suppose BWOC that the opposite is true. That is,
[stem]
++++
\forall c \in C: cr \in C
++++
Then, consider stem:[c > 0] and notice that
[stem]
++++
cr^n = c(1+s)^n = c \sum_{k=0}^n \binom{n}{k}s^n > c (1 + ns)
++++
where stem:[s = r-1 > 0]. Then since stem:[c \in C],
stem:[cr^n \in C] and is unbounded. Hence, we get a contradiction.
=====
====

=== Shifting the cut
Let stem:[C] be a cut and stem:[r \in \mathbb{Q}]. Then,
[stem]
++++
D = \{c + r: c \in C\}
++++
is a cut and stem:[D' = \{c' + r: c \notin C\}]. Furthermore,
[stem]
++++
D = C + C(r)
++++

.Proof
[%collapsible]
====
Notice that if we would show that stem:[D = C + C(r)], stem:[D] is automatically a cut

.Proof of addition
[%collapsible]
=====
Firstly
[stem]
++++
C + C(r) = \{c_1 + c_2: c_1 \in C, c_2 < r\}
++++
Now, consider stem:[x = c + r \in D] and stem:[c < c_1 \in C]. Then,
stem:[c - c_1 < 0] and stem:[c - c_1 + r < r]. Then
[stem]
++++
x = c + r = c_1 + (c - c_1) + r = c_1 + (c - c_1 + r) \in C + C(r)
++++
Therefore, stem:[D \subseteq C + C(r)].


Conversely, we consider stem:[x = c_1 + c_2 \in C + C(r)].
Then, stem:[c_2 < r] and hence stem:[c_1 + c_2 - r < c_1] and hence
stem:[c_1 + c_2 - r \in C]. Therefore,
[stem]
++++
x = c_1 + c_2 = (c_1 + c_2 - r) + r \in D
++++
Therefore stem:[C + C(r) \subseteq D] and we are done.
=====

.Proof of form of complement
[%collapsible]
=====
We would use characterization of exclusion. Let
[stem]
++++
E = \{c' + r: c' \notin C\}
++++
Now, consider stem:[c' + r \in E]. Then, since stem:[c' > c] for all stem:[c \in C],
stem:[c' + r > d] for all stem:[d \in D] and hence stem:[c' + r \notin D].
Therefore, stem:[E \subseteq D']

Conversely, consider stem:[d' \notin D], then stem:[d' > d] for all
stem:[d \in D]. That is
[stem]
++++
\forall c + r \in D: d' > c+r
++++
Therefore, stem:[d' - r > c] for all stem:[c \in C] and hence,
stem:[d' -r \notin C]. Therefore, stem:[d' = (d' - r) + r \in E]
and stem:[D' \subseteq E] and we are done.
=====
====

=== Expanding the cut
Let stem:[C] be a cut and stem:[r \in \mathbb{Q}^+ ].
Then,
[stem]
++++
D = \{cr: c\in C\}
++++
is a cut and stem:[D' = \{c'r : c \notin C\}]. Furthermore,
[stem]
++++
D = C \cdot C(r)
++++

.Proof
[%collapsible]
====
.Proof of cut
[%collapsible]
=====
Clearly, stem:[D \neq \varnothing]. Also, stem:[D \neq \mathbb{Q}] since
if stem:[e \notin C], stem:[e > c] for all stem:[c \in C]
hence stem:[er > cr] for all stem:[cr \in D] and hence,
stem:[er] is not in stem:[D].

Next, if stem:[x < cr] we get that stem:[\frac{x}{r} < c] and
we get that stem:[x = \frac{x}{r} r \in D] since stem:[C] is a cut.
Also, since stem:[C] is a cut, there exists stem:[c_1 > c] where
stem:[c_1 \in C] and hence stem:[c_1r > cr] and stem:[c_1r \in D].

Therefore, stem:[D] is a cut.
=====

.Proof of mutliplication
[%collapsible]
=====
Lets consider the case where stem:[C > 0] firstly. Then, consider stem:[cr \in D].
If stem:[cr \leq 0], we know it lies in stem:[C \cdot C(r)].
Otherwise, there exists stem:[e > c] where stem:[e \in C] and hence, stem:[\frac{c}{e}r < r]
and we get that
[stem]
++++
cr = e \cdot \frac{cr}{e} \in C \cdot C(r)
++++
Therefore, stem:[D \subseteq C \cdot C(r)].

Otherwise, consider stem:[x \in C \cdot C(r)]. Then, if stem:[x \leq 0],
we again know that it lies in stem:[D] since stem:[0 \in D]. Otherwise,
stem:[x = c e] where stem:[c \in C^+ ] and stem:[0 < e < r].
Then, stem:[\frac{ce}{r} < c] and hence stem:[\frac{ce}{r} \in C]
and
[stem]
++++
x = ce = \frac{ce}{r}r \in D
++++
Therefore stem:[C \cdot C(r) \subseteq D] and we are done.

If stem:[C < 0], let
[stem]
++++
E = \{dr: d \in (-C)\}
++++
Then, stem:[(-C)\cdot C(r) = E]. Also, notice
that
[stem]
++++
E + D = \{(c + d)r: c \in C, d \in (-C)\} = \{f r: f \in C(0)\} = \{fr: f < 0\}
++++
Then clearly, stem:[E + D \subseteq C(0)] also if stem:[x \in C(0)],
stem:[\frac{x}{r}r \in E + D]. Therefore stem:[E + D = C(0)].
Then, we get that
[stem]
++++
C \cdot C(r)
= C \cdot C(r) + (-C)\cdot C(r) + (-E)
= (C + (-C)) \cdot C(r) + (-E)
= C(0) \cdot C(r) + (-E)
= (-E)
= D
++++
=====
.Proof of form of complement
[%collapsible]
=====
We would use characterization of exclusion. Let
[stem]
++++
E = \{c'r: c' \notin C\}
++++
Now, consider stem:[c'r \in E]. Then, since stem:[c' > c] for all stem:[c \in C],
stem:[c'r > cr] for all stem:[cr \in D] and hence stem:[c'r \notin D].
Therefore, stem:[E \subseteq D']

Conversely, consider stem:[d' \notin D], then stem:[d' > d] for all
stem:[d \in D]. That is
[stem]
++++
\forall cr \in D: d' > cr
++++
Therefore, stem:[\frac{d'}{r} > c] for all stem:[c \in C] and hence,
stem:[\frac{d'}{r} \notin C]. Therefore, stem:[d' = \frac{d'}{r}r \in E]
and stem:[D' \subseteq E] and we are done.
=====

====


== Ordering of cuts
Let stem:[R] be our set of cuts. Note that such a set is guaranteed by the axiom of powers and specification.
Then if stem:[C \in R], exactly one of the following hold

Positive:: stem:[\exists c \in C: c > 0]
Zero:: stem:[C = C(0)]
Negative:: stem:[-C] is positive

Then, we let 
[stem]
++++
R^+ = \{C \in R: C \text{ is positive}\}
\quad\text{and}\quad
R^- = \{C \in R: C \text{ is negative}\}
++++

In general if stem:[C_1] and stem:[C_2] are cuts, then 
[stem]
++++
C_1 < C_2 \iff C_2 - C_1 \text{ is positive} \iff C_1 \subset C_2
++++

== Operations
=== Postive segment
Let stem:[C] be a positive cut. Then we define
[stem]
++++
C^+ = \{c \in C: c > 0\}
++++
Then,
[stem]
++++
C = \mathbb{Q}^- \cup \{0\} \cup C^+ = C(0) \cup C^+
++++
=== Addition
Let stem:[C_1, C_2] be cuts. Then
[stem]
++++
C_1 + C_2 = \{c_1 + c_2: c_1 \in C_1, c_2 \in C_2\}
++++
is a cut. Additionally,

* stem:[+ ] is commutative and associative
* stem:[C(0)] is the identity for stem:[+ ]

.Proof
[%collapsible]
====
Commutativity and associativity follow from the commutativity
and associativity of stem:[+ ].

.Proof of cut
[%collapsible]
=====
Clearly, stem:[C_1 + C_2] is nonempty. Next, consider stem:[d_1 \in C_1'] and stem:[d_2 \in C_2'].
Then by characterization of exclusion, stem:[d_1 > c_1] and stem:[d_2 > c_2] for
all stem:[c_1 \in C_1] and stem:[c_2 \in C_2]. Therefore, stem:[d_1 + d_2 > c_1 + c_2]
for all stem:[c_1, c_2] and hence stem:[d_1 + d_2 \notin C_1 + C_2].

Next, consider stem:[c < c_1 + c_2]. Then, stem:[x = c-c_1 < c_2] and stem:[x \in C_2].
Therefore, stem:[c = c_1 + x \in C_1 + C_2].

Finally, for each stem:[c_1 + c_2 \in C_1 + C_2]. There exists,
stem:[c_1 < d_1 \in C_1] and stem:[c_2 < d_2 \in C_2]. Therefore, stem:[c_1 + c_2 < d_1 + d_2 \in C_1 + C_2]
and we are done.
=====

.Proof of identity
[%collapsible]
=====
Consider arbitrary cut stem:[C]. Then,
[stem]
++++
C + C(0) = \{c + r: c \in C, r < 0\}
++++
Then, for each stem:[c+ r \in C + C(0)], stem:[c + r < c \in C] and hence
stem:[c+ r \in C]. Therefore stem:[C + C(0) \subseteq C]

Conversely, if stem:[c \in C], there exists stem:[c < d \in C].
Then stem:[c - d < 0] and stem:[c -d \in C(0)] and we get that stem:[c = d + (c-d) \in C + C(0)].
Therefore, stem:[C \subseteq C + C(0)] and we are done.
=====

====

Additionally, addition preserves ordering. That is
if stem:[C_1 > D_1] and stem:[C_2 > D_2]
[stem]
++++
C_1 + C_2 > D_1 + D_2
++++

=== Negation (additive inverse)
Let stem:[C] be a cut. Then,
[stem]
++++
(-C) = \{x \in \mathbb{Q}: \exists a \in C': x < -a \}
++++
is a cut and 
[stem]
++++
(-C) + C = C + (-C) = C(0)
++++

.Proof
[%collapsible]
====
.Proof of cut
[%collapsible]
=====
Since stem:[C] is a cut stem:[C'] is non empty and hence stem:[(-C)]
is also non empty. Next, if stem:[c \in C], then
for all stem:[a \in C'], stem:[a > c] and hence stem:[-a < -c].
Therefore stem:[-c \notin (-C)].

Next, consider stem:[x \in (-C)] where stem:[x < -a] and stem:[a \in C']. Then if stem:[y < x],
stem:[y < x < -a] and stem:[y \in (-C)]. Also, since the rationals
are dense, stem:[\exists] stem:[x < z < -a] and hence stem:[z \in (-C)].
Therefore stem:[(-C)] is a cut.
=====
.Proof of inverse
[%collapsible]
=====
[stem]
++++
C + (-C)
= \{c_1 + c_2: c_1 \in C, c_2 \in (-C)\}
= \{c_1 + c_2: c_1 \in C, \exists a \in C': c_2 < -a\}
++++
Now, consider stem:[c_1 + c_2] where stem:[c_2 < -a]. Then, stem:[a > c_1]
by characterization of exclusion and hence stem:[c_1 + c_2 < c_1 - a < c_1 - c_1 = 0]
and hence stem:[c_1 + c_2 \in C(0)]. Therefore stem:[C + (-C) \subseteq C(0)]


Conversely, consider stem:[x \in C(0)]. Then stem:[\frac{-x}{2} > 0] and stem:[\exists c_1 \in C]
such that stem:[c_1 + \frac{-x}{2} \notin C]. Then, stem:[-c_1 + x < -c_1 + \frac{x}{2}]
and hence stem:[-c_1 + x \in (-C)]. Therefore stem:[x = c_1 + (-c_1 + x) \in C + (-C)]
and stem:[C(0) \subseteq C + (-C)] and we are done.
=====
====

Therefore, negation is an involution by considering the additive group. Further, negation must
distribute over addition since addition is commutative. We also define subtraction to be
[stem]
++++
A - B = A + (-B)
++++

=== Absolute value
Let stem:[C] be a cut, then we define
[stem]
++++
|C| = \begin{cases}
C &\quad\text{if } C \geq C(0)\\
-C & \text{if } C < C(0)
\end{cases}
++++
Then if stem:[C \neq C(0)], we get that stem:[|C| > C(0)]

=== Multiplication
Let stem:[C_1] and stem:[C_2] be positive cuts, then we define
[stem]
++++
C_1 \cdot C_2 = \mathbb{Q}^- \cup \{0\} \cup (C_1^+ \cdot C_2^+)
++++
where
[stem]
++++
C_1^+ \cdot C_2^+ = \{c_1 \cdot c_2: c_1 \in C_1^+, c_2 \in C_2^+\}
++++
Then, stem:[C_1 \cdot C_2] is a cut.

In general, if stem:[C_1] and stem:[C_2] are arbitrary cuts, we define
[stem]
++++
C_1 \cdot C_2 = \begin{cases}
0 &\quad\text{if } C_1 = C(0) \text{ or } C_2 = C(0)\\
|C_1| \cdot |C_2| &\quad\text{if } [ C_1 > C(0) \text{ and } C_2 > C(0) ] \text{ or } [ C_1 < C(0) \text{ and } C_2 < C(0) ]\\
-(|C_1| \cdot |C_2|) &\quad\text{if } [ C_1 > C(0) \text{ and } C_2 < C(0) ] \text{ or } [ C_1 < C(0) \text{ and } C_2 > C(0) ]\\
\end{cases}
++++
* stem:[\cdot] is commutative and associative
* stem:[C(1)] is the identity for stem:[\cdot]
* stem:[\cdot] distributes over stem:[+ ]


.Proof
[%collapsible]
====
.Proof of cut
[%collapsible]
=====
By definition, it must be non-empty. Also, notice that if stem:[d_1 \notin C_1] and stem:[d_2 \notin C_2],
stem:[d_1] is an upper bound for stem:[C_1] and stem:[d_2] is an upper bound for stem:[C_2]
and hence
[stem]
++++
\forall c_1 \cdot c_2 \in (C_1^+ \cdot C_2^+): c_1 \cdot c_2 \leq d_1 \cdot d_2
++++
Therefore, stem:[C_1 \cdot C_2 \neq \mathbb{Q}].

Next, consider arbitrary stem:[c_1 \cdot c_2 \in (C_1^+\cdot C_2^+)]. Then,
if stem:[0 < x < c_1 \cdot c_2], stem:[0 < q = \frac{x}{c_1 \cdot c_2} < 1].
Let stem:[q < q_2 < 1] and stem:[q_1 = \frac{q}{q_2}], then stem:[0 < q_1 < 1].
Therefore, stem:[0 < c_1q_1 < c_1] and stem:[0 < c_2q_2 < c_2]
with stem:[c_1q_1 \in C_1^+ ] and stem:[c_2q_2 \in C_2^+ ].
Therefore
[stem]
++++
x = c_1c_2 \frac{x}{c_1c_2} = c_1c_2q_1q_2 = (c_1q_1) \cdot (c_2 q_2) \in C_1^+ \cdot C_2^+
++++
Also, if we let stem:[c_1 < e_1 \in C_1^+ ] and stem:[c_2 < e_2 \in C_2^+ ],
we get that
[stem]
++++
c_1c_2 < e_1e_2 \in C_1^+ \cdot C_2^+
++++
and hence we have that stem:[C_1\cdot C_2] is a cut.
=====

.Proof of assiciativity and commutativity
[%collapsible]
=====
By commutativity and associativity of multiplication
in stem:[\mathbb{Q}], multiplication of positive cuts
also exhibit these two properties.

To generalize this for all cuts would just be an exercise in checking
all of the cases. I'm not going to do it, but its pretty easy to see,
especially since the result of multiplication explicitly states which
of the forms we need to take if we multiply again.
=====
.Proof of identity
[%collapsible]
=====
Let stem:[C] be a positive cut and consider
[stem]
++++
C^+ \cdot C(1)^+ = \{c_1 \cdot c_2: c_1 \in C^+, 0 < c_2 < 1\}
++++
Consider arbitrary stem:[c \in C^+ ], then stem:[\exists d \in C^+ ] such that
stem:[d > c]. Then stem:[0 < \frac{c}{d} < 1] and
[stem]
++++
c = d \cdot \frac{c}{d}  \in C^+ \cdot C(1)^+
++++
Therefore stem:[C \subseteq C^+ \cdot C(1)^+ ].

Conversely, if stem:[c_1 \cdot c_2 \in C^+ \cdot C(1)^+ ], we get that
stem:[c_1 \cdot c_2 < c_1] and hence stem:[c_1 \cdot c_2 \in C^+ ]
and stem:[C^+ \cdot C(1)^+ \subseteq C^+ ] and we are done.

Well.. we have shown that stem:[C(1)] is the identity for positive cuts.
It is also clearly the identity for stem:[C(0)] while for negative cuts
[stem]
++++
C \cdot C(1) = -(|C| \cdot C(1)) = -|C| = C
++++
Now, we actually done.
=====

.Proof of distribution over addition
[%collapsible]
=====

NOTE: We follow much of the same process as Ethan Bloch in
"`The Real Numbers and Real Analysis`".
Thank you Mauro ALLEGRANZA for suggesting the book in
https://math.stackexchange.com/questions/1190755/proving-the-distributive-law-of-a-dedekind-cut[this post].

Consider cuts stem:[A,B,C]. Then, we want to prove that
[stem]
++++
(A + B) \cdot C = (A\cdot C) + B \cdot C
++++
Notice that if any of stem:[A, B, C] are zero, the result is trivial.
Then, we would prove the case when all are positive then derive
all other results.

.Proof when all are positive
[%collapsible]
======
Notice that stem:[A+B > C(0)] and hence
[stem]
++++
(A+ B)\cdot C
= \mathbb{Q}^- \cup \{0\} \cup ((A+B)^+ \cdot C^+)
++++
while
[stem]
++++
(A \cdot C) + (B \cdot C) =
(\mathbb{Q}^- \cup \{0\}\cup (A^+ \cdot C^+)
+ (\mathbb{Q}^- \cup \{0\}\cup (B^+ \cdot C^+)
++++
We would show that both sets are equal. Firstly consider stem:[(a+b)c \in (A+B)^+ \cdot C^+ ].
Then clearly, stem:[(a + b)c \in (A \cdot C) + (B \cdot C)] since
stem:[ac \in A \cdot C] and stem:[bc\in B \cdot C] since

* if stem:[a \leq 0], stem:[ac \leq 0] and stem:[ac \in \mathbb{Q}^- \subseteq A \cdot C]
* if stem:[a > 0], stem:[ac \in A^+ \cdot C^+ \subseteq A \cdot C]

and likewise for stem:[bc]. Hence stem:[(A+B)\cdot C \subseteq (A\cdot C) + (B\cdot C)].


Conversely, consider stem:[e_1 + e_2 \in (A\cdot C) + (B \cdot C)].
Note, we only need to show that if both stem:[e_1] and stem:[e_2] are positive,
stem:[e_1 + e_2 \in (A+B)\cdot C] since if either is negative,
there exists a sum consisting of positives which is greater than it.
Now, let stem:[e_1 = ac_1] and stem:[e_2 = bc_2] where stem:[a\in A^+ ],
stem:[b \in B^+ ] and stem:[c_1, c_2 \in C^+ ]. Let stem:[c = \max(c_1, c_2)].
Then,
[stem]
++++
e_1 + e_2 = ac_1 + bc_2 \leq ac + bc = (a+b)c \in (A +C)\cdot C
++++
hence we are done.
======
Now, we have stem:[7] other cases. However, we would omit
when stem:[C] is negative since the result is easily derived.
Now, we have stem:[3] cases to show

If stem:[A < 0] and stem:[B > 0], we would split into two cases.

* If stem:[A +B > 0], then
+
[stem]
++++
\begin{aligned}
AC + BC
&= AC + ((-A) + A + B)C
\\&= AC + (-A)C + (A+B)C
\\&= -((-A)C) + AC + (A+B)C
\\&= (A+B)C
\end{aligned}
++++

* If stem:[A +B < 0], then
+
[stem]
++++
\begin{aligned}
(A+B)C
&= -((-(A+B))C) \quad\text{since } A+B < 0
\\&= -(((-A) + (-B))C) \quad\text{since negation distributes over addition}
\\&= -((-A)C + (-B)C) \quad\text{since }(-A) + (-B) > 0 \text{ (previous case)}
\\&= (-(-A)C) + (-(-B)C)
\\&= AC + BC
\end{aligned}
++++

Now, the case when stem:[A > 0] and stem:[B < 0] is very similar and we can
indeed use the previous case along with the commutativity of addition
to obtain this result.

Finally, if both stem:[A < 0] and stem:[B < 0], then
stem:[A + B < 0] and
[stem]
++++
\begin{aligned}
(A + B)C
&= - ((-(A+B))C)
\\&= - (((-A) + (-B))C)
\\&= - ((-A)C + (-B)C)
\\&= (-(-A)C) + (-(-B)C)
\\&= AC + BC
\end{aligned}
++++
=====
====

=== Reciprical (multiplicative inverse)
If stem:[C] is a positive cut, we define
[stem]
++++
C^{-1} = \mathbb{Q}^- \cup \{0\} \cup (C^+)^{-1}
++++
where
[stem]
++++
(C^+)^{-1}
= \{b \in \mathbb{Q}^+: \exists a \in (\mathbb{Q}^+ \backslash C+): b < a^{-1}\}
= \{b \in \mathbb{Q}^+: \exists a \in C': b < a^{-1}\}
++++
Then stem:[C^{-1}] is a cut.

In general if stem:[C] is a cut, we define
[stem]
++++
C^{-1} = \begin{cases}
|C|^{-1} & \quad\text{if } C > C(0)\\
-(|C|^{-1}) & \quad\text{if } C < C(0)
\end{cases}
++++
Then,
[stem]
++++
C^{-1} \cdot C = C \cdot C^{-1} = C(1)
++++

.Proof
[%collapsible]
====
.Proof of cut
[%collapsible]
=====
Clearly, stem:[C^{-1} \neq \varnothing] also if stem:[c \in C^+ ],
then stem:[c < a] for all stem:[a \in C']
and hence stem:[a^{-1} < c^{-1}]. Therefore,
stem:[c^{-1} \notin (C^+)^{-1}] and hence not in
in stem:[C].

Next, if stem:[b \in C^{-1}] then stem:[\exists a \in C']
such that stem:[b < a^{-1}].
Then if stem:[e < b], stem:[e < a^{-1}]
and hence stem:[e \in C^{-1}].
Also, by the density of rationals,
there exists stem:[b < f < a^{-1}]
and hence stem:[f \in C^{-1}].

Therefore stem:[C^{-1}] is a cut.
=====
.Proof of inverse
[%collapsible]
=====
Firstly suppose that stem:[C] is positive. Then
[stem]
++++
\begin{aligned}
C \cdot C^{-1}
&= \mathbb{Q}^- \cup \{0\} \cup \{c d: c \in C^+, d \in (C^+)^{-1}\}
\\&= \mathbb{Q}^- \cup \{0\} \cup \{c d: c \in C^+, d \in \mathbb{Q}, \exists e \in C': d < e^{-1}\}
\end{aligned}
++++
Then, consider stem:[cd \in (C \cdot C^{-1})^+ ] where stem:[d < e^{-1}] and stem:[e \in C'].
Then stem:[c < e] and hence stem:[cd < ce^{-1} < ee^{-1} = 1].
Therefore, stem:[cd \in C(1)] and hence stem:[C \cdot C^{-1} \subseteq C(1)].

Conversely, consider stem:[x \in C(1)^+ ]; that is stem:[0 < x < 1].
Then, there exists stem:[y \in \mathbb{Q}] such that stem:[x < y < 1]
and hence, stem:[\frac{1}{y} > 1]. Therefore,
there exists stem:[c \in C] such that stem:[c\frac{1}{y} \notin C].
Hence, 
[stem]
++++
\frac{x}{c} < \frac{y}{c} = \left(\frac{c}{y}\right)^{-1} \implies \frac{x}{c} \in C^{-1}
++++
and hence stem:[x = c\frac{x}{c} \in C \cdot C^{-1}]. Therefore,
stem:[C(1) \subseteq C \cdot C^{-1}] and we are done.

For the case when stem:[C] is negative,
[stem]
++++
C \cdot C^{-1} = C \cdot (-(|C|^{-1})) = |C| \cdot |C|^{-1} = C(1)
++++
=====
====

== Rational cuts
Let stem:[r \in \mathbb{Q}], then we define
[stem]
++++
C(r) = \{c \in \mathbb{Q}: c < r\}
++++
Then stem:[C(r)] is a _rational_ cut. Furthermore, stem:[C \in R] is a cut
iff stem:[C'] has a least element.

.Proof
[%collapsible]
====
.Proof of cut
[%collapsible]
=====
Firstly, stem:[C(r) \neq \varnothing] since stem:[r-1 \in C(r)]. Also, stem:[C(r) \neq \mathbb{Q}]
since stem:[r \notin C(r)]. Next, consider arbitrary stem:[c \in C(r)].

If stem:[a < c], then stem:[a < c < r] and stem:[a \in C(r)].
Also, notice that stem:[c < \frac{c + r}{2} < r].

Therefore, stem:[C(r)] is a cut.
=====
.Proof of characterization
[%collapsible]
=====
Consider cut stem:[C = C(r)]. Then,
[stem]
++++
C' = \{c \in \mathbb{Q}: c \geq r\}
++++
Then, stem:[r] is the least element of stem:[C'].

Conversely if we have an arbitrary cut stem:[C] and stem:[r] is the least element of stem:[C'],
then we want to show that stem:[C = C(r)]. Firstly, notice that stem:[r \notin C] and hence, if 
stem:[c > r], stem:[c \notin C] otherwise it would imply that stem:[r \in C] since stem:[C] is a cut.
Therefore, if stem:[c \in C], we get that stem:[c < r] and hence stem:[C \subseteq C(r)].
Conversely, notice that if stem:[c < r], stem:[c \in C] otherwise it would invalidate the fact
that stem:[r] is the lease element in stem:[C']. Therefore stem:[C(r) \subseteq C] and we are done.
=====
====

=== Homomorphism
The function stem:[x \to C(x)] is an injective
ordered field homomorphism from stem:[(\mathbb{Q}, +, \cdot, \leq)]
to stem:[(R, +, \cdot, \leq)]. That is,

* stem:[C(a + b) = C(a) + C(b)]
* stem:[C(-a) = -C(a)]
* stem:[C(ab) = C(a)C(b)]
* stem:[a \leq b \iff C(a) \leq C(b)]

.Proof
[%collapsible]
====
The fact that stem:[x\mapsto C(x)] is injective would follow from order preservation.

.Proof of ordering
[%collapsible]
=====
Suppose that stem:[a < b]. Then, stem:[a \in C(b)] however, stem:[a \notin C(a)]. Therefore, stem:[C(a) \neq C(b)].
Also notice that if stem:[c \in C(a)], then stem:[c < a < b] and stem:[c \in C(b)].
Therefore stem:[C(a) \subset C(b)] and stem:[C(a) < C(b)].

Conversely, suppose that stem:[C(a) < C(b)]. Then, if stem:[a > b], we get a contradiction,
since stem:[b \in C(a) \subset C(b)]. Therefore stem:[a < b] as expected.
=====
.Proof of sum
[%collapsible]
=====
Recall that
[stem]
++++
C(a) + C(b) = \{c_1 + c_2: c_1 \in C(a) , c_2 \in C(b)\} = \{c_1 + c_2: c_1 < a, c_2 < b\}
++++
Now, consider stem:[x \in C(a) + C(b)], then stem:[x = c_1 + c_2 < a +b] and stem:[x \in C(a+b)]
and hence stem:[C(a) + C(b) \subseteq C(a+b)].

On the other hand, if stem:[x \in C(a + b)], then stem:[x < a + b] and hence stem:[0 < a+ b - x] and
[stem]
++++
x = \left(a - \frac{a+b-x}{2}\right) + \left(b - \frac{a+b -x}{2}\right)
++++
and hence stem:[x \in C(a) + C(b)]. Therefore stem:[C(a+b) \subseteq C(a) + C(b)]
and we are done.
=====
.Proof of additive inverse
[%collapsible]
=====
This is a given. Notice that
[stem]
++++
C(0) = C(a-a)= C(a + (-a)) = C(a) + C(-a)
++++
Therefore stem:[C(-a)] is the additive inverse of stem:[C(a)].
=====
.Proof of product
[%collapsible]
=====
Consider product stem:[ab]. If stem:[a = 0] or stem:[b =0], stem:[ab = 0]. Also,
stem:[C(a)C(b) = 0] by definition. Therefore, we only need to consider
when all are non zero.

If stem:[a > 0] and stem:[b > 0], stem:[ab > 0]. Then, we need to show that
[stem]
++++
\{c \in \mathbb{Q}: 0 < ab < c\} = C(ab)^+ = C(a)^+ \cdot C(b)^+ = \{c_1c_2: 0 < c_1 < a, 0 < c_2 < b\}
++++
Firstly, consider stem:[c \in C(ab)^+ ]. Then stem:[0 < \frac{c}{ab} < 1] and there exists
stem:[0 < q_1, q_2 < 1] such that stem:[q_1 q_2 = q = \frac{c}{ab}]. This can be done by considering stem:[q < q_2 < 1]
and notice that stem:[q_1 = \frac{q}{q_2} < 1]. Therefore, we then take stem:[c = aq_1 bq_2]
and notice that stem:[aq_1 < a] and stem:[b q_2 < b]. Therefore stem:[c \in C(a)^ + \cdot C(b)^ + ]
and stem:[C(ab) \subseteq C(a)^ + \cdot C(b)^ + ].

On the other hand, if we consider stem:[c_1 \in C(a)^ + ] and stem:[c_2 \in C(b)^ + ]. Then,
stem:[0 < c_1c_2 < ab] and stem:[c_1c_2 \in C(ab)^ + ]. Therefore, we get stem:[c_1c_2 \in C(ab)^ + ]
and stem:[C(a)^ + \cdot C(b)^ + \subseteq C(ab)^ + ].

If stem:[a < 0] and stem:[b > 0], then
[stem]
++++
C(a)C(b) = - ((-C(a))C(b)) = -(C(-a)C(b)) = - C(-ab) = C(ab)
++++
similarly if stem:[a > 0] and stem:[b < 0].

Finally, if stem:[a < 0] and stem:[b < 0], then
[stem]
++++
C(a)C(b) = (-C(a))(-C(b)) = C(-a)C(-b) = C((-a)(-b)) = C(ab)
++++
and we are done.
=====
====

