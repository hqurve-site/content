= Axiom of Choice

Sometimes during proofs we need to choose arbitrary elements
from sets. More formally, if stem:[\Omega] is a collection
of non-empty sets, we want to choose an element from each
set. Such a selection can instead be thought of as a function.
That is, we can rephrase this my stating that there exists
a function, stem:[f], from stem:[\Omega] to stem:[\cup \Omega] such that stem:[f(A) \in A].
We call such a function a _choice function_.

In the case when stem:[\Omega] is finite, we have no issue
since the set
[stem]
++++
\tilde{f} = \{\{(A_1, a_1), (A_2, a_2), \ldots (A_n, a_n)\}: a_i \in A_i\}
++++
exists and is non-empty. This is the set of all possible choice functions.

.Proof
[%collapsible%open]
====
// We would prove this inductively. 
//
// Firstly, note that stem:[\{A_i\}] is a set and hence the set,
// [stem]
// ++++
// \{A_1\} \times A_1 = \{(A_1, a_1): a_1 \in A_1\}
// ++++
// is a set. Therefore, by using the powerset axiom and the
// axiom of specification, we get that the set of singletons exists. That is
// [stem]
// ++++
// \{\{(A_1, a_1)\}: a_1 \in A_1\}
// ++++
// exists.
//
// Next, suppose that

Notice that since stem:[\Omega] is a collection of sets, stem:[\cup \Omega]
exists and hence the set
[stem]
++++
\Omega \times (\cup \Omega) = \{(A_i, a_j): A_i \in \Omega, a_j \in A_j \in \Omega\}
++++
exists. And hence by the axiom of specification, the set
[stem]
++++
S = \{(A_i, a_i): A_i \in \Omega, a_i \in A_i\}
++++
also exists. Now, consider the set
[stem]
++++
\{f \subseteq S: f \text{ is a function}\}
++++
Then ...

[THOUGHT]
=====
This is a set and I believe its non empty. Help plz

[%collapsible]
======
image::https://i.imgur.com/oscXKEm.png[]
======
=====

{empty}... Anyhow, lets just go along with our ability to somehow do this when
the set is finite.

====

The axiom of choice asserts that there exists such a function for all
collections of sets. Furthermore, it has been shown that the axiom
of choice is independent of the previously stated axioms.

== Equivalent statements
The axiom of choice is equivalent to the following statements

* The Cartesian product of a non-empty family of non-empty sets is non-empty.
* Zorn's Lemma: (HalmostSection 16) If stem:[X] is a poset such that each chain has an upper
bound, then stem:[X] contains a maximal element.
* Well ordering theorem: There exists a well ordering on each set.
