= Relations

A _relation_ stem:[R] is a set of xref:set-theory:pairs.adoc#_ordered_pairs[ordered pairs].
Then, if stem:[(x,y) \in R], we write stem:[xRy] and say that stem:[x] is related
to stem:[y].

Additionally, if stem:[R \subseteq A\times B] for two sets stem:[A] and stem:[B],
we say that stem:[R] is _a relation from stem:[A] to stem:[B]_. Furthermore,
if stem:[A = B], we simply say that stem:[R] is _a relation on stem:[A]_.

Throughout this section, we would be primarily focused on relations between two sets.

== Basic Operations

=== Domain, Codomain and Range
Let stem:[R] be a relation from stem:[A] to stem:[B], then we define the following

[horizontal]
Domain:: stem:[\dom R = \{x \in A: (x,y) \in R\}] 
Codomain:: We say that stem:[B] is the _codomain_ of stem:[R]
Range:: stem:[\ran R = \{y \in B: (x,y) \in R\}] 

=== Composition and inverse
Let stem:[A,B,C] be sets and stem:[R \subseteq A\times B] and stem:[S\subseteq B\times C].
Then, we define the _composition_ of stem:[R] and stem:[S] as follows
[stem]
++++
S \circ R = \{(x,z) \in A \times C: \exists y \in B: (x,y) \in R \text{ and } (y,z) \in S\}
++++

Furthermore, we define the _inverse_ of stem:[R] to be
[stem]
++++
R^{-1} = \{(y,x): (x,y) \in R\}
++++


== Classification of relations
Let stem:[R] be a relation on stem:[A] then we define the following properties

[cols="^,^,^,^",options="header"]
|===
2+| Property | Requirement | Example
.2+.^| Self
| Reflexive | stem:[\forall x \in A: (x,x) \in R] | equality
| Irreflexive | stem:[\forall x \in A: (x,x) \notin R] | strict inequality

.2+.^| Symmetry
| Symmetric | stem:[\forall x,y\in A: (x,y) \in R \implies (y,x) \in R] | equality
| Antisymmetric | stem:[\forall x,y\in A: (x,y) \in R \text{ and } (y, x) \in R \implies x = y] | less than or equal to

.2+.^| Transitivity
| Transitive | stem:[\forall x,y,z \in A: (x,y) \in R \text{ and } (y,z) \in R \implies (x,z) \in R] | equality
| Atransitive | stem:[\forall x,y,z \in A: (x,y) \in R \text{ and } (y,z) \in R \implies (x,z) \notin R] | stem:[R = \{(x,x+1): x \in \mathbb{N}\}]
|===

== Equivalent relations
Let stem:[X] be a set and stem:[\sim] be a relation on stem:[X]. Then, if stem:[\sim]
is reflexive, symmetric and transitive, we call stem:[\sim] an _equivalence relation_ on stem:[X].

Then, for each stem:[x\in X], we define the _equivalence class_ of stem:[x] as
[stem]
++++
x/~ = [x] = \{y \in X: x \sim y\}
++++
Then, we define the _quotient set_ induced by stem:[\sim] as
[stem]
++++
X/\sim = \{[x] \in P(X): x\in X\}
++++

Then, stem:[X/\sim] forms a partition on stem:[X].

REMARK: Yes, xref:math3272::basics.adoc#_quotient_groups[quotient groups]
are a special case of quotient sets.

=== Equivalence relations from partitions
Let stem:[Y\subseteq P(X)] be a partition on stem:[X]. That is,
[stem]
++++
\forall x \in X: [\exists y \in Y: x \in Y] \text{ and } [\forall y_1, y_2 \in Y: x \in y_1 \text{ and } x \in y_2 \implies y_1 = y_2]
++++
Then, stem:[Y] _induces_ an equivalence relation on stem:[X] defined by stem:[a\sim b] iff
[stem]
++++
\exists y \in Y: a \in Y \text{ and } b \in Y
++++

Furthermore, we have the two following results

* For any equivalence relation, the partition it forms induces the original relation
* For any partition, the set of equivalence classes of the relation it induces is the partition itself.
