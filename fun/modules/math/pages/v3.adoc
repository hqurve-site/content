= Nearest point approximation

Previously in xref:v2.adoc[], we attempted to find the distance between two lines which eventually
led us to finding the nearest point to a linear space. Although the discussion was general inner
product spaces, it is very matrix heavy and the essence of the argument is hidden due
to a focus on a particular set of vectors instead of the vector space itself. To clarify,
although the following two discussions are equivalent, the second allows is much
more readily understandable

Previous approach:: Given a set of vectors stem:[\vec{w}_1, \ldots \vec{w}_n],
find coordinate vector stem:[\vec{t}] such that stem:[\left\|\sum_{i=1}^n t_i \vec{w}_i - \vec{a}\right\|^2]
is minimized.

New approach:: Given subspace stem:[W] and vector stem:[\vec{a}], determine
stem:[\vec{w} \in W] which minimizes stem:[\left\|\vec{w} - \vec{a}\right\|^2]

Notice that the first approach is mainly focused on representation. As a result,
we overlooked an important property of stem:[\vec{w}]. Additionally, in
the second form, any result is easier to apply.

== Properties of the nearest point
We should be careful as we are yet to prove that there is only one nearest point.
Let stem:[\vec{a} \in V] and stem:[W \leq V]. Then, we say that stem:[\vec{w} \in W]
is a _nearest point_ of stem:[\vec{a}] if

[stem]
++++
\forall \vec{w}' \in W: \left\|\vec{a} - \vec{w}\right\|^2 \leq \left\|\vec{a} - \vec{w}'\right\|^2
++++
Recall that stem:[\|\vec{x}\|^2 = \abrack{\vec{x}, \vec{x}}].


=== Characterization of nearest point
Our first result is that 

[stem]
++++
\vec{w}\ \text{is a nearest point} \iff (\vec{a} - \vec{w}) \perp W
++++

.Proof
[%collapsible]
====
For the forward direction, let stem:[\vec{w} \in W] be a nearest point
of stem:[\vec{a}], stem:[k\in \mathbb{C}] and stem:[\vec{u} \in W]. Then,
stem:[\vec{w} + k\vec{u} \in W] and hence

[stem]
++++
\begin{aligned}
    0 &\leq \abrack{\vec{a} - \vec{w} - k\vec{u}, \vec{a} - \vec{w} - k\vec{u}} - \abrack{\vec{a} - \vec{w}, \vec{a} - \vec{w}}
    \\&= \abrack{k\vec{u}, k\vec{u}} - \abrack{k\vec{u}, \vec{a} - \vec{w}} - \abrack{\vec{a} - \vec{w}, k\vec{u}}
    \\&= \conj{k}k\abrack{\vec{u}, \vec{u}} - \conj{k}\abrack{\vec{u}, \vec{a} - \vec{w}} - k\abrack{\vec{a} - \vec{w}, \vec{u}}
    \\&= \left(\conj{k} - \frac{\abrack{\vec{a} - \vec{w}, \vec{u}}}{\abrack{\vec{u}, \vec{u}}}\right)\abrack{\vec{u}, \vec{u}}\left(k - \frac{\abrack{\vec{u}, \vec{a} - \vec{w}}}{\abrack{\vec{u}, \vec{u}}}\right)
        - \frac{\abrack{\vec{a} - \vec{w}, \vec{u}}\abrack{\vec{u}, \vec{a} - \vec{w}}}{\abrack{\vec{u}, \vec{u}}}
    \\&= \abrack{\vec{u}, \vec{u}}\left|k - \frac{\abrack{\vec{u}, \vec{a} - \vec{w}}}{\abrack{\vec{u}, \vec{u}}}\right|^2
        - \frac{\left|\abrack{\vec{a} - \vec{w}, \vec{u}}\right|^2}{\abrack{\vec{u}, \vec{u}}}
\end{aligned}
++++
Now, since stem:[k] is a free variable, we can manipulate it such that the first term becomes zero. Hence, we must have that stem:[\abrack{\vec{a} - \vec{w}, \vec{u}} = 0].
Notice that if stem:[\abrack{\vec{u}, \vec{u}} = 0], we automatically have this result. Also, since that choice of stem:[\vec{u}] was arbitrary,
we have that stem:[(\vec{a} - \vec{w}) \perp W].

Conversely, if stem:[(\vec{a} - \vec{w}) \perp W], by use the above equality chain, we have that
stem:[\forall \vec{u} \in W]

[stem]
++++
\begin{aligned}
&\abrack{\vec{a} - \vec{u}, \vec{a} - \vec{u}}
\\&=\abrack{\vec{a} - \vec{w} + \vec{w} - \vec{u}, \vec{a} - \vec{w} + \vec{w} - \vec{u}}
\\&=\abrack{\vec{a} - \vec{w}, \vec{a} - \vec{w}} + \abrack{\vec{w} - \vec{u}, \vec{a} - \vec{w}}
    + \abrack{\vec{a} - \vec{w}, \vec{w} - \vec{u}} + \abrack{\vec{w} - \vec{u}, \vec{w} - \vec{u}}
\\&=\abrack{\vec{a} - \vec{w}, \vec{a} - \vec{w}} + \abrack{\vec{w} - \vec{u}, \vec{w} - \vec{u}} \quad\text{since } \vec{w} - \vec{u} \in W
\\&\geq \abrack{\vec{a} - \vec{w}, \vec{a} - \vec{w}}
\end{aligned}
++++
Hence, stem:[\vec{w}] is a nearest point as desired.
====

Intuitively, this makes sense, at least in standard euclidean space. However, it is nice to see that
intuition extends to the general case. Furthermore, it provides a relatively simple test
for the nearest point since we can use any spanning set of vectors.

It is also important to note that this relies on the fact that stem:[W] is a linear space. Take for example, a region
with a hole in it. Then, there the difference to the nearest point can often not be orthogonal to the region.

=== Uniqueness of nearest point
Let stem:[\vec{w}_1] and stem:[\vec{w}_2] be nearest points of stem:[\vec{a}];
then, stem:[\vec{w}_1 = \vec{w}_2].
This allows us speak of the _the_ nearest point and perhaps
inspires us to think of a function which maps vectors to their nearest
points. However, caution is needed as we have not proven that a nearest
point exists.

.Proof
[%collapsible]
====
[stem]
++++
\begin{aligned}
0
&= \abrack{\vec{a} - \vec{w}_1, \vec{a} - \vec{w}_1} - \abrack{\vec{a} - \vec{w}_2, \vec{a} - \vec{w}_2}
\\&= \abrack{\vec{a} - \vec{w}_2 + (\vec{w}_2 - \vec{w}_1), \vec{a} - \vec{w}_2 + (\vec{w}_2 -\vec{w}_1)} - \abrack{\vec{a} - \vec{w}_2, \vec{a} - \vec{w}_2}
\\&= \abrack{\vec{w}_1 - \vec{w}_2, \vec{w}_1 - \vec{w}_2} + \abrack{\vec{a} - \vec{w}_2, \vec{w}_1 - \vec{w}_2} + \abrack{\vec{w}_1 - \vec{w}_2, \vec{a} - \vec{w}_2}
\\&= \abrack{\vec{w}_1 - \vec{w}_2, \vec{w}_1 - \vec{w}_2}
\end{aligned}
++++
and hence stem:[\vec{w}_1 - \vec{w}_2 = \vec{0}].
====

== Projection onto nearest points
We claim that the partial function which maps vectors in stem:[V] onto their nearest point
in stem:[W] is a projection. Furthermore, we claim that the domain of this function is a subspace stem:[V'] such that stem:[W \leq V' \leq V].
We would denote this partial function stem:[P_W \in \mathcal{L}(V', W)].

.Proof
[%collapsible]
====
We first see that stem:[V'] contains stem:[W] since for all stem:[\vec{w} \in W]

* stem:[\vec{w} \in W]
* stem:[\vec{w} - \vec{w} = \vec{0} \perp W]

Hence stem:[P_W(\vec{w}) = \vec{w}]. This at the same time proves that stem:[P_W] is idempotent.

Next stem:[V'] is a linear space and stem:[P_W] is linear since if stem:[P_W(\vec{a}_1) = \vec{w}_1]
and stem:[P_W(\vec{a}_2) = \vec{w}_2],

* stem:[\vec{w}_1 + \lambda \vec{w}_2 \in W]
* stem:[(\vec{a}_1 - \vec{w}_1), (\vec{a}_2 - \vec{w}_2) \perp W] implies that
+
[stem]
++++
(\vec{a}_1 + \lambda \vec{a}_2) - (\vec{w}_1 + \lambda \vec{w}_2)) \perp W
++++

Hence stem:[P_W(\vec{a}_1 + \lambda\vec{a}_2) = \vec{w}_1 + \lambda\vec{w}_2 = P_W(\vec{a}_1) + \lambda P_W(\vec{a}_2)].

NOTE: Throughout this proof we only needed to prove that there is a nearest point in stem:[W] since
we know that it is unique. However, we need to ensure that this nearest point is indeed in stem:[W]
otherwise the mapping would not be valid.
====

Conversely, by the characterization of nearest points, we see that any orthogonal projection maps points to their nearest points.
Additionally, since we know that stem:[P_W] is an orthogonal projection, we can utilize existing strategies to compute
it. Primarily, if stem:[I = \{\vec{w}_i\}] forms an orthogonal basis for stem:[W], stem:[P_W] is simply given by

[stem]
++++
P_W(\vec{a}) = \sum_{\vec{w}_i \in I} \abrack{\vec{w}_i, \vec{a}} \vec{w}_i
++++
We may now have some questions about convergence of the above sum. However, these are implementation details.

// At this point, we should also ask the obvious question: "`When does the nearest point exist?`" In general,
// we cannot tell; however, in each of the following cases we 
