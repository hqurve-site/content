= Combinatorial Arguments for rational statements

This is promped by "A weird proof that stem:[e \leq 3]" by 
https://www.youtube.com/channel/UC21xJ4CJRyoGvI4Icr-J3aQ[Liam Appelbe]

// https://www.youtube.com/watch?v=myKkhVy74V4
video::myKkhVy74V4[youtube]

The end card of the video proposes 4 problems. For each problem, there would be two equivalent proofs:
a simple worded explaination and a set theoretic proof. Note that on this page, stem:[0] is considered
as a natural number and we define stem:[J_k] to be the set of natural numbers less than or equal to stem:[k].

== stem:[\sum_{k=0}^n k(n-k) = \binom{n+1}{3}] difficulty: stem:[3/5]
Consider the right hand expression. Then, this is simply number of ways of choosing stem:[3] of stem:[n+1] items.

For the left hand expression, consider a line of stem:[n+1] items which are zero-indexed. Then, stem:[k]
represents one of these items. We then choose one of the stem:[k] items preceeding the stem:[k]'th item
and one of the stem:[n-k] items following the stem:[k]'th item. Then, this is an equivalent way of choosing stem:[3]
from stem:[n+1] where the stem:[k]'th item is the middle choice.
Note that if stem:[k \in \{0,n\}], obviously, there are no choices to make since
the middle element is at the end.

=== Set theory
We can also do this in a more rigourous fashion using set theory. Consider the two sets

* stem:[\bigcup_{k=0}^n \{a,b \in J_{n+1} \ \mid \ a < k < b\}]
* stem:[\{a,k,b \in J_{n+1} \ \mid \ a < k < b\}]

Then, the cardinality of these two sets are the left and right side (respectively) of the desired equality. Then,
the result follows since both sets are clearly equal.

== stem:[\sum_{k=0}^n k^2\binom{n}{k} = 2^{n-1}\binom{n+1}{2} = 2^{n-1}n + 2^{n-2}n(n-1)] difficulty: stem:[4/5]
Consider the middle expression and notice that it counts the number of ways of colouring stem:[n+1] objects in red, blue
and white where exactly two objects are coloured white.

For the first expression, consider the same stem:[n+1] objects. We first choose stem:[k] of the first stem:[n] objects;
we call these objects squishy. Then, of the squishy objects,
we make two independent selections. We then colour based on the following three cases

* If the two selections are the same, we colour the object white. We also colour the stem:[n+1]'th object white
* If the two selections are different, we colour them both white and colour the stem:[n+1]'th object
** red if the first selection is before the first
** blue if the first selection is after the first

We then colour all remaining squishy objects red and the unselected objects blue. Note that from a colouring
we can uniquely determine a selection which produces such a colouring.

For the final expression, consider the same stem:[n+1] objects. If we colour the last object
white, we then choose one of the other stem:[n] objects to be white and then
colour the remaining objects red and blue in one of stem:[2^{n-1}] ways.

Otherwise, we make two different selections of the first stem:[n] objects to be white. If the first
selection is before the first, we colour the stem:[n+1]'th object red, otherwise we colour it blue.
We then colour the remaining objects in one of stem:[2^{n-2}] ways.

//
// We choose either method A or method B to colour. We claim that method A yeilds stem:[2^{n-1}n]
// colourings while method stem:[B] yeilds stem:[2^{n-2}n(n-1)] colourings, and each colouring can be obtained by
// exactly one method.
// 
// Method A:: We choose an adjacent pair of objects to be coloured white. There are stem:[n] ways of doing this.
// We can then colour the remaining objects in stem:[2^{n-1}] ways.
// 
// Method B:: Consider the spaces between objects. We make two different selections. We then colour the object
// to the left of the leftmost selection white and the object to the right of the rightmost selection white.
// Note that the two white objects cannot be adjacent. If the first selection was to the left of the second,
// we colour the object to the right of the leftmost selection blue; otherwise, we colour it red. We can then
// colour the remaining stem:[n-2] objects in stem:[2^{n-2}] ways.



Hence, we have proven that all three expressions are equal.

=== Set theoretic
Consider the following set

[stem]
++++
\{(W,R,B) \ \mid \ W \sqcup R \sqcup B = J_{n+1}, |W| = 2\}
++++
We would show that the following sets are equivalent to set above (and hence to each other)

* stem:[\bigcup_{k=0}^n\left\{(a,b,C) \ \mid \ \ C \subseteq J_{n},\ |C| = k, \ a,b \in C \right\}]
* stem:[\{(W,R) \ \mid \ W \subseteq J_{n+1},\ |W| = 2, \ R \subseteq J_{n+1} - W\}]
* stem:[
\{(x,n, S) \ | \ x \in J_{n}, \ S \subseteq J_n - \{x\}\}
\ \cup\ \{(x,y, S) \ \mid | \ x,y \in J_n,\ x\neq y,\ S \subseteq J_n - \{x,y\}\}
]

where each of the above sets has the cardinality of the respective expressions.

==== Second equivalent to reference
The reference set is equivalent to the middle set by the following function
[stem]
++++
(W,R) \mapsto (W, R, J_{n+1} - W - R)
++++

==== First and second equivalent
The first and second sets are equivalent by the following function
[stem]
++++
(a,b,C) \mapsto \begin{cases}
(\{a,n\},\ C - \{a\}) &\quad\text{if } a =b\\
\left(\{a,b\},\ (C - \{a,b\}) \cup \{n\}\right) &\quad\text{if } a < b\\
\left(\{a,b\},\ C - \{a,b\}\right) &\quad\text{if } a > b
\end{cases}
++++
This function is clearly injective; however, it is a little less obvious as to why it is surjective.
To see that it is surjective, consider its inverse

[stem]
++++
(\{s,t\}, R) \mapsto \begin{cases}
(s,\ s,\ R \cup\{s\}) &\quad\text{if } t = n\\
(s,\ t,\ (R - \{n\})\cup\{s, t\}) &\quad\text{if } n \in R\\
(s,\ t,\ R \cup\{s, t\}) &\quad\text{if } n \not\in R \text{ and } t \neq n
\end{cases}
++++
where stem:[s < t].

==== Second and third equivalent
The second and third sets are equivalent by the following function

[stem]
++++
(x,y,S) \mapsto \begin{cases}
(\{x,y\}, S) \quad&\text{if } y = n\\
(\{x,y\}, S) \quad&\text{if } x < y \text{ and } y \neq n\\
(\{x,y\}, S \cup \{n\}) \quad&\text{if } x > y
\end{cases}
++++
We only needed two cases, however, the first two cases were split to emphasize which
of the two sets the element came from. Also, this function is clearly bijective.


== stem:[\binom{n}{k-1}\binom{n}{k+1} \leq \binom{n}{k}^2] difficulty: stem:[6/5]

== stem:[\sqrt[n\]{\prod x_i} \leq \frac{1}{n}\sum x_i] difficulty: emoji:skull[]stem:[/5]
This is the AM-GM inequality where stem:[x_i \in \mathbb{N}]. The video also gave a transformed
form which only includes natural numbers.

[stem]
++++
n^n\prod_{i=1}^n x_i \leq \left(\sum_{i=1}^n x_i\right)^n
++++

// THE BELOW PROOF IS WRONG
////
First consider stem:[n] children each of which has stem:[x_i] marbles,
where no two marbles look the same. We then have an artist who
would like to paint some of these marbles. In particular,
he would like to make stem:[n] paintings.

On the first day, each time he makes a painting,
he chooses one of the marbles from any of the children
as a subject (possibly choosing the same marble multiple times).
There are stem:[\left(\sum_{i=1}^n x_i\right)^n] possible ways in
which he can do this.

On the second day, he chooses his favourite marble from each of the children's
collections. That is, he chooses one marble from each of the children to obtain
stem:[n] marbles. There are stem:[\prod_{i=1}^n x_i] ways of selecting these
marbles. Then, he makes stem:[n] paintings from this selection
possibly painting the same marble more than once. There are stem:[n^n]
ways of doing this.

It is seen that his method of choosing subjects for painting on the second
day yeilds a valid selection from the first day.

By doing this, he has to choose one of the
Then the expression on the right is the number of ways of selecting n
////

