\section{Binomial Theorem}
The numbers $\binom{n}{r}$ are known as binomial coefficients. This is due to fact that 
the coefficient of $x^r$ in $(1+x)^n$ is precisely equal to $\binom{n}{r}$. In fact
$$(1+x)^n = \sum_{r=0}^n \binom{n}{r} x^r$$
But why is this? Well perhaps the simplest way to see it is by counting how many times 
$x^r$ appears in the expansion of $(1+x)^n$. This can be done by choosing $r$
of the brackets containing $1+x$ to contribute an $x$. Alternatively, we could choose
$n-r$ of the brackets to choose a $1$ from. Here we see our first identity
$$\binom{n}{r} = \binom{n}{n-r}$$
In this section, we will explore various properties of binomial coefficients

\subsection{Proper binomial theorem}
In fact, the binomial theorem states that 
$$(x+y)^n = \sum_{r=0}^n \binom{n}{r} x^r y^{n-r}$$
This is a more general case than the previous statement, however, its proof is basically identical.

\subsection{Pascal's Triangle}
Pascal's triangle is a structure made entirely of binomial coefficients. Additionally, it
is a useful tool when examining binomial coefficients. Heres the first $5$ rows of it
\begin{center}
\begin{tabular}[c]{r@{\hspace*{0.6cm}} ccccccccccc}
$n=0$:&        &    &    &    &    & 1  &    &    &    &    &   \\
$n=1$:&        &    &    &    & 1  &    & 1  &    &    &    &   \\
$n=2$:&        &    &    & 1  &    & 2  &    & 1  &    &    &   \\
$n=3$:&        &    & 1  &    & 3  &    & 3  &    & 1  &    &   \\
$n=4$:&        & 1  &    & 4  &    & 6  &    & 4  &    & 1  &   \\
$n=5$:&     1  &    & 5  &    & 10 &    & 10 &    & 5  &    & 1 \\
\end{tabular}
\end{center}
As seen, the $n$'th row is made up of the values $\binom{n}{0}, \binom{n}{1}, \ldots, \binom{n}{1}$.
Additionally, each entry in the computed by summing the two values above it (taking missing values as zero).
This clearly matches the following property
$$\binom{n}{r} = \binom{n-1}{r-1} + \binom{n-1}{r}$$
\paragraph*{Note: } this identity can be derived arithmetically or via the following combinatorial argument.
Consider how to construct the choices of $r$ objects from $n$, focusing on the first object. If the first object
is in the selection we have $\binom{n-1}{r-1}$ ways to select the remaining objects otherwise there are $\binom{n-1}{r}$
ways to select the remaining objects.


\subsection{Analysis of Binomial Theorem}
The question arises of whether the binomial theorem also applies to negative as well as other real values of n and if it does,
to what extent. In other words, is it possible to expand $(1+x)^n$ for all real values of $n$?

\noindent
Firstly, it should be noted that the factorial function is only defined on non-negative integers, so its 
no good trying to extend that. However, we can consider the following for non-negative integers
$$(1+x)^n = \sum_{r=0}^{n} \binom{n}{r}x^r = 1 + nx + \frac{n(n-1)}{1(2)} + \frac{n(n-1)(n-2)}{1(2)(3)} + \cdots$$
Firstly, notice that the sum on the left can truly be considered to be infinite since, for non-negative integers, there will exist
a point where all following terms contain a zero in the numerators product. On the other hand, this never happens for $n \notin \mathbb{Z}^+ \cup \{0\}$.

\noindent
So does this formula work although it is just an extended formula? Does it reflect the actual behaviour of the function? 
Yes, of course it does, since it is simply the taylor series expansion. Perhaps now it would be fair to state that the following 
is an alternative definition for binomial coefficients for all $n$
$$\binom{n}{r} = \frac{n(n-1)(n-2) \cdots (n+r-1)}{1(2)(3)\cdots (r)} = \frac{\prod_{i=0}^{r-1} (n-i)}{r!}$$
Note that although we discussed this formula as an alternative definition, it fits perfectly with the definition that the binomial coefficient $\binom{n}{r}$
is the coefficient of $x^r$ in the expansion of $(1+x)^n$.

\subsubsection*{Special case when $n$ is a negative integer}
Although the factorial is not defined for negative integers, we can try to deduce a simplified formula as followed
$$\binom{-n}{r}= \frac{\prod_{i=0}^{r-1} (-n-i)}{r!} = (-1)^r \frac{\prod_{i=0}^{r-1} (n+i)}{r!} = (-1)^r \frac{(n+r-1)!}{(n-1)!r!} = (-1)^r \binom{n+r-1}{r}$$
From this the following expansion is immediate
$$(1+x)^{-n} = \sum_{r=0}^{\infty} (-1)^r \binom{n+r-1}{r} x^r$$
and can be simplified if we replace $x$ with $-x$
$$(1-x)^{-n} = \sum_{r=0}^{\infty} \binom{n+r-1}{r} x^r$$

\subsection{Generalization: Multinomial theorem}
The binomial theorem is in fact a special case of the more general multinomial theorem.
The multinomial theorem states
$$
    (x_1 + x_2 + \ldots + x_m)^n 
    = \sum_{k_1 + k_2 +\ldots + k_m = n} 
        \binom{n}{k_1, k_2, \ldots, k_m} \prod_{t=1}^m x_t^{k_t}
$$
where
$$
    \binom{n}{k_1, k_2, \ldots, k_m}  = \frac{n!}{k_1! k_2! \ldots k_m!}
$$
and is called the multinomial coefficient.
\paragraph*{Note: } the multinomial coefficient is equivalent to the number of ways to order
non-distinct objects.