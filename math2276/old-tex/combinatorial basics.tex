\section{Combinatorial Basics}
Perhaps the most important thing in mathematics is counting.
Of course, the simplest way to count is by enumerating over all objects.
However, this can prove tedious when there is a large number of objects.
In order to combat this, many different strategies of varying difficulty
are employed. Moreover, these strategies can be used together to follow 
complex situations where no one strategy suffices. These strategies 
will be explored in this section.

\subsection{Basic principles}
\subsubsection{Multiplication}
Suppose there is an algorithm which enumerates over all possible objects (once).
If in each of the $k$ stages of the algorithm, the number of following
outcomes, $n_i$, is constant, the total number of objects, $N$, is given by
$$N = n_1 n_2 \ldots n_k = \prod_{i=1}^k n_i$$

% \subsubsection*{Examples}
% \begin{itemize}
%     \item In an ice-cream parlour, there are $5$ different flavours
%             and $3$ different toppings. Therefore, there are a total $15$ different ice-creams 
%             that can they can serve.
%     \item 
% \end{itemize}

\subsubsection{Addition}
If there are $k$ disjoint sets each containing $n_i$ items. The total number of ways, $N$, of 
choosing one item is 
$$N = n_1 + n_2 +\ldots + n_k = \sum_{i=1}^k n_i$$

\subsubsection{Division}
Sometimes, it is easier to count the number of cases including repetition and then 
attempt to remove it afterwards. If the count including repetition was $n$ but each of the 
objects counted was repeated $k$ times, the count excluding repetition, $N$, is simply
$$N = \frac{n}{k}$$

\subsubsection{One to one correspondence}
Suppose we have two sets $A$ and $B$ and there exists a bijection $f$ between the two.
This means that each element in $B$ is uniquely identified by $A$ through $f$. Hence the
cardinality of sets $A$ and $B$ must be the same. \\
In practice, if we want to count the number of elements in $A$, we can either directly
find it or we could alternatively find a set $B$ with bijection $f$ and instead count the
number of elements in $B$. 

\paragraph*{Generalization: } Note that the relation $f$ need not be a bijection but in fact
it can be any relation where each element in $A$ is related to exactly $m$ elements in $B$. 
In this case we instead find that $|A| = \frac{|B|}{m}$. Also, note that the division principle
is based on this generalization.

\subsubsection{Recursion}
If there is a complex problem where the number of outcomes $f(n)$
is based on one or more smaller cases, the problem is said to be recursive.
This is the same principle that mathematical induction is based upon.
Additionally, just like mathematical induction a boundary condition is needed.\\
Although there is no one solution for recursive problems a few strategies will
be discussed later on.


\subsection{Permutations}
Strictly speaking, a permutation $\pi$ is a bijection on the set $S$. Although this fact won't
be used (yet?), its useful and interesting to think of a permutation as a function. Anyways \ldots
in these notes permutations will be thought of as orderings. \\
So now, given $n$ distinct objects, how many ways are there to order them? \\
Here are two ways to approach this problem:
\begin{enumerate}%[label=\arabic*:]
    \item Recursive approach\\
            Let this number be $p(n)$ and note that $p(1) = 1$. Note that there are $n$ ways
            to choose the first item and for each choice of the first item, there are
            $p(n-1)$ ways to order the remaining $n-1$ items.
            Simply put
            $$p(n) = \begin{cases}
                1 \quad &\text{if } n = 1\\
                n\times p(n-1) \quad &\text{otherwise}
            \end{cases}$$
    \item Direct approach\\
            Using the information gained from the first approach we can clearly see that 
            $$p(n) - n(n-1)(n-2)\ldots (2)(1)$$
            since there are $n$ ways to place the first item, $n-1$ for the second, $n-2$ for the third and so on.\\
            Additionally, since the function $p$ is very common, we denote it with a special notation
            $$n! = p(n)$$
            pronounced ``$n$ factorial''
\end{enumerate}
\paragraph*{Note: }
we define $0! = 1$. In many of the following formulae $0!$ appears and in fact it makes sense since there is only 
$1$ way to order nothing. In general, you need not worry whether the argument of the factorial is positive number
as many arguments also work when the $n=0$.

\subsubsection*{Permutation of non-distinct objects}
Suppose there are $k$ classes of objects each with $n_i$ identical objects. Such that 
$\sum_{i=1}^k n_i = n$ where $n$ is the total number of objects. Then how many different permutations are there?\\
The simplest solution is to first act as if all the objects are distinct and then removing repetition. If the $n$ objects 
were distinct, we get $n!$. However, for each of different classes, there are $n_i!$ ways to reorder the objects such that
we get the same permutation (since they are identical). Therefore, the total number of ways to permute these objects, $N$, is
given by 
$$N = \frac{n!}{n_1! n_2! \ldots n_k!} = \frac{n!}{\prod_{i=1}^k n_i!}$$


\subsection{Ordered selection}
Suppose a selection of $r$ objects must be made from a set $n$ distinct objects. In how many ways can this be done?
We denote this number $\Perm{n}{r}$. Here are two approaches
\subsubsection*{Approach 1}
Perhaps the simplest approach to this problem is by using an algorithm similar to that used in the ``Permutations''.
However, we stop after the $r$'th step.  This means that the last number to multiply is $n - r + 1$. Therefore
$$\Perm{n}{r} = n(n-1)(n-2)\ldots(n-r+2)(n-r+1)$$
And after some algebraic manipulation we get
$$\Perm{n}{r} = \frac{n(n-1)(n-2)\ldots(n-r+2)(n-r+1)(n-r)(n-r-1)\ldots(2)(1)}{(n-r)(n-r-1)\ldots(2)(1)} = \frac{n!}{(n-r)!}$$

\subsubsection*{Approach 2}
Although this approach is not initially apparent, it is in fact more elegant. One way to enumerate over all possible selections
is to first write each of the permutations of the $n$ objects and only take the first $r$ objects.\\
Of course we can see that we get all possible selections of $r$ objects but not we obviously have repeats. But how many?
Well, the number of repetitions of a single selection of $r$ objects is precisely the number of ways the last $n-r$ objects 
appear. This is simply $(n-r)!$ and therefore our final answer is
$$\Perm{n}{r} = \frac{n!}{(n-r)!}$$
\paragraph*{Note:} this argument also works on the boundary of $r=n$ due to the definition of $0!$. Moreover, note that in this case,
the $\Perm{n}{r}$ reduces to $n!$ as expected.

\subsection{Unordered selection}
Now why did we go through ordered selections first? Well of course, its because the concept of unordered selection readily applies.
Simply put the number of ways to select $r$ objects from $n$ objects where the order doesn't matter is given by
$$\Comb{n}{r} = \binom{n}{r} = \frac{\Perm{n}{r}}{r!} = \frac{n!}{r!(n-r)!}$$
How does this formula work? Well very similar to the second approach of computing $\Perm{n}{r}$. We first choose our 
$r$ objects from $n$ with order. However, since the objects are distinct, there are exactly $r!$ ways that this set
of $r$ objects was selected, so we divide.

\subsection{Ordered Selection with repetition}
Quite simply, the number of ways to choose $r$ objects from $n$ distinct objects with repetition is 
$$n^r$$
This is because we have an algorithm of $r$ steps where we have $n$ choices at each step.

\subsection{Unordered Selection with repetition}
What is the number of ways to place $n$ objects into $r$ classes of objects with repetition?
This at first seems like a pretty hard problem since we cant just divide because each selection may does not 
have the same number of occurrences. So how exactly do we solve this problem?

\subsubsection*{Solution} 
Suppose we have our selection of $n$ objects. Then we can order the classes and place each of the $n$
objects into their respective class (in a line). Finally, we place bars between each class. Now we have a line
of bars and objects objects of the same class are next to each other. Now, notice a few things:
\begin{itemize}
    \item the number of bars is $r-1$ and hence the length of the line is $r+n-1$
    \item the classification of the objects is uniquely determined by the positioning of the bars as well as the order of the unique classes.
\end{itemize}
Therefore, there exists a one to one correspondence between choosing placing $r-1$ bars into $r+n-1$ spaces and 
placing $n$ objects into $r$ classes. Therefore, the number of ways to place $n$ objects into $r$ classes is
$$\binom{r+n-1}{r-1} = \binom{n+r-1}{n}$$






\subsection{Principle of Inclusion-Exclusion}
Let $S$ be a set of objects and $P_1, \ldots, P_r$ be properties which the 
elements of $S$ may or may not possess (ie predicates on $S$). Let $N(i, j, \ldots, k)$
denote the number of elements of $S$ which possess properties $P_i, P_j, \ldots, P_k$
(and possibly others). Then the number of elements of $S$ possessing  at least one of the 
properties is 
$$\sum_i N(i) - \sum_{i < j} N(i, j) + \sum_{i < j < k} N(i, j, k) - \ldots + (-1)^{r-1} N(1, 2, \ldots, r)$$
\paragraph*{Note:} the proof of this property is simply by considering how much each element in $S$ contributes
to the sum. If the element satisfies no properties, its contribution is $0$ however, if it contributes to $t\geq 1$
properties, its contribution is $1$.

\subsubsection*{Inverted version}
There is another version of this which is perhaps a little more useful. What is the number of objects not fitting a
certain criteria is required. Well simply this is the total number of objects minus the number of objects fitting the criteria.
Now if there are multiple criteria, we can simply apply the principle of inclusion-exclusion then invert.
Therefore, the number of ways in which a set of criteria is not met is given by
$$N - \sum_i N(i) + \sum_{i < j} N(i, j) - \sum_{i < j < k} N(i, j, k) + \ldots + (-1)^{r} N(1, 2, \ldots, r)$$
where $N$ is the total number of objects.



\subsection{Pigeonhole Principle}
The pigeonhole principle, also referred to as Dirichlet's drawer principle, asserts the following
\begin{center}
    \begin{minipage}{0.7\linewidth}
        Given $n > m$ objects, partitioned into $m$ subsets, there exists at least one subset with more than
        one object in it.
    \end{minipage}
\end{center}
This is easy to prove (by contradiction) and even though it is easily proven on spot it is much easier to quote the 
result.

Furthermore, the principle has a more generalized form as follows
\begin{center}
    \begin{minipage}{0.7\linewidth}
        Given $n > km$ objects, partitioned into $m$ subsets, there exists at least one subset with 
        more than $k$ objects in it.
    \end{minipage}
\end{center}