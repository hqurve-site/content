Heres a little outline.

You can first consider any block. Without loss of generality, this block contains (1,2,4). Also, 2 must also appear in a second block, it 1 and 4 cannot appear in this block since each pair appears once, so say that 3 and 5 do, so let this second block contain (2,3,5). Finally, consider the element 5, it must be paired with 1 in some block. Also, the last item in the block cannot be 2, 3 or 4, so it must be 6 or 7. We arbitrarily choose 6. So we get our final set (1,5,6).

^Thats how they generated their first sets arbitrarily (up to relabelling).

Below is what the question is asking you to do. Im just going to go through the first two steps.

So far we have (1,2,4), (2,3,5) and (1,5,6)
Next consider the elements 1 and 3, they must appear in a block together. This block cannot
contain 2,4,5 or 6 so it must contain 7. So we get another set

So far we have (1,2,4), (2,3,5), (1,5,6) and (1,3,7)
Next consider the pair 2,6 they must also appear in a block together. This block cannot contain 1,4,3 or 5 so it must contain 7. So we get yet another set

So far we have (1,2,4),  (2,3,5), (1,5,6), (1,3,7) and (2,6,7).

You continue this process until you get 7 blocks and then show that it is a valid (7,7,3,3,1)block design. You can then state that since the first three blocks were arbitrary up to relabelling and all following blocks were fixed, there is essentially only one (7,7,3,3,1) block design

for number 56 in discrete when they asked you to verify that its a (10,6,5,3,2) design, did you just draw the incidence matrix and then state (for instance) "it is seen that each pair of columns has ones in exactly 2 rows" and so 


In the incidence matrix each column represents the a variety (a,b .. f) and each row represents a block. Moreover, the a_{ij} entry is 1 of the jth variety in the the ith block and 0 if it isnt. Using this, we can make the following observations
- there are 10 rows meaning that there are 10 blocks (b = 10)
- there are 6 columns meaning that there are 6 varieties (\nu = 6)
- each column has exactly 5 1s meaning that each variety appears in exactly 5 blocks (r = 5)
- each row has exactly 3 1s meaning that each block has exactly 3 varieties (k=3)
- for any pair of columns, common 1s exist in eactly 2 rows. This means that each pair of varieties occur together in exactly 2 blocks (\lambda = 2).
Therefore, the collection is a (10,6,5,3,2) block design.