\section{Graphs Further}

\subsection{Trees}
A \emph{tree} is a connected simple graph with no cycles.

Note:
\begin{itemize}
    \item If a tree has $p \geq 2$  vertices, then $T$ contains at least two pendant vertices. \\
            This is easily proven by considering the longes path in $T$.
\end{itemize}

\subsubsection*{Equivalent definitions}
Let $T$ be a simple graph with $p$ vertices. Then following statements are equivalent:
\begin{enumerate}[label=\roman*)]
    \item $T$ is a tree
    \item $T$ has $p-1$ edges and no cycles.
    \item $T$ has $p-1$ edges and is connected.
\end{enumerate}
\begin{proof} We will prove these three in a cyclic manner.

    \noindent
    (i) $\Rightarrow$ (ii) All we have to show is that the graph has $p-1$ edges.
    We will prove this by induction. This is certainly true when $p=1$. Suppose it
    is true for all trees with $k \geq 1$ vertices. Then consider a tree $T$
    with $k+1$ vertices. Since all trees have at least two pendant vertices, consider
    $T'$ with one of them removed. Now $T'$ has $k$ vertices and $k-1$ edges by the inductive
    hypothesis. So $T$ has $k-1 + 1 = k$ edges as required.

    \noindent
    (ii) $\Rightarrow$ (iii) All we need to show is that $T$ is connected. Suppose $T$ is consists
    of $t \geq 1$ components. Since all of these components are connected and have no cycles, they are all trees.
    Let $\{p_i\}$ be the number of vertices in each component. Then $\sum_i p_i = p$. Also since, each component
    is a tree, the total number of edges is $\sum_i p_i - 1 = p - t = p - 1$. Therefore, $t=1$ and $T$ is connected.

    \noindent
    (iii) $\Rightarrow$ (i) All we need to show is that $T$ has no cycles. Suppose $T$
    did have cycles. Removing an edge from a cycle does not destroy connectedness, so we can remove
    edges from cycles until no cycles are left meanwhile preserving the connectedness. The
    resulting graph must be a tree with $p$ vertices but since edges where removed, it has $q < p-1$
    edges. However, this contradicts (ii) and hence $T$ must be a tree.
\end{proof}


\subsection{Spanning Trees}
A \emph{spanning tree} of a connected graph $G$ is a tree which contains all the vertices of 
$G$ and which is a subgraph of $G$. Essentially, a spanning tree is a tree created from $G$ using a
process similar to (iii) $\Rightarrow$ (i) in the above proof.

\subsection{Perfect Matching theorem}
Roughly speaking, the failure to find a perfect matching is due to the lack of edges. In fact,
if each vertex of a graph of $2n$ vertices has a degree of at least $n$, then the graph has a 
perfect matching.

\begin{proof}
    This proof is constructive and will, in addition to proving the theorem, show how to generate 
    such a matching. This proof will show how to increase the number of edges in the matching 
    as long as the total number of vertices in the matching is less than $2n$.

    Suppose that $r < n$ matches have been made so far. If there exists two adjacent vertices not
    already paired, add them to the matching. Hence the total number matches has increased to $r + 1$.
    Otherwise, consider that no such trivial matching can be made. This means that each vertex not in the matching
    is only adjacent to vertices which are already paired.

    Take any two vertices, $a$ and $b$, not currently paired. Notice that the sum of their degrees is 
    at least $k \geq 2n$. Also, as previously noted, their adjacent vertices are already paired off. 
    Therefore, if we take all vertices $x$ and $y$ which are already paired and count the 
    total number of edges from $a$ and $b$ to $x$ and $y$, we also arrive at $k$. Notice that there are $r < n$
    such pairs $x, y$ and $k \geq 2n$ which implies that $k > 2r$. Therefore, by the pigeonhole principle, there exists a pair of already
    matched vertices $x, y$ such that there are three edges from $a$ and $b$ to $x$ and $y$.

    Now, note that at least one of $a$ and $b$ is adjacent to both $x$ and $y$. Without loss of generality, let this vertex
    be $a$. Additionally, there is at least one more edge joining $b$ to $x$ or $y$. Without loss of generality, let this vertex be
    $x$. Now we have at least the following set of edges: $xy, ax, ay, bx$.
    
    \begin{figure}[h!]
        \begin{center}
            \includegraphics{images/perfect-matching-1.png}
        \end{center}
    \end{figure}
    We can now remove edge $xy$ and 
    replace it by $ay$ and $bx$. Therefore, the net effect is increasing the total number of matches to $r+1$.

    We continue this process until $r = n$.
\end{proof}