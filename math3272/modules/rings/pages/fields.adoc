= Fields

Let stem:[R] be a ring. Then, we say that stem:[u \in R]
is a _unit_ iff stem:[u] is invertible. Then, we define
[stem]
++++
R^\times = \{u \in R\ | \ u \text{ is a unit}\}
++++
Then this set (together with stem:[0]) forms a subring of stem:[R] and in fact, it forms a field. But first,
we need to define what a field is. 


We say that integral domain stem:[F] is a field iff all its non-zero elements are units.

== Ideals in fields
Let stem:[S \neq 0] be a ring. Then, stem:[S] is a field
iff its only ideals are stem:[0] and stem:[S]

.Proof
[%collapsible]
====
Firstly, suppose that stem:[S] is a field
and let stem:[I] be an ideal. Then, if stem:[I \neq 0], we have non-zero stem:[a \in I].
Then stem:[1 = aa^{-1} \in I] and hence stem:[I = S].

Conversely, if stem:[0] and stem:[S] are the only ideals of stem:[S].
Consider non-zero stem:[a \in S] and the principal
ideal stem:[(a)]. Then, stem:[(a) = S] which contains stem:[1]
and hence an inverse for stem:[a]. Therefore stem:[a]
is a unit and we are done.
====


== Field of fractions
Let stem:[R] be an integral domain. Then, there exists a field in which stem:[R]
can be embedded. Consider
[stem]
++++
Q(R) = \{[a,b]: a, b \in R, b \neq 0\}
++++
where stem:[[a,b\] = [c,d\]] iff stem:[ad = bc]. Then stem:[Q(R)]
forms a field and we have the embedding stem:[x \mapsto [x,1\]].

More concretely, we follow the following steps

. Let stem:[X = R\times(R - \{0\})]
. We define stem:[\sim] on stem:[X] be stem:[(a,b) \sim (c, d)] iff stem:[ad = bc].
Then stem:[\sim] is an equivalence relation.
. Define stem:[Q(R) = X / \sim] and prove that this set forms a ring
. Prove that non-zero elements of stem:[Q(R)] from a field
