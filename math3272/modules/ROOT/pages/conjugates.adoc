= Conjugates

Let stem:[G] be a group and stem:[g\in G]. Then we define
stem:[\phi_g: G \to G] by
[stem]
++++
\phi_g(h) = ghg^{-1}
++++
where stem:[\phi_g(h)] is the _conjugate of stem:[h] by stem:[g]_ and we
say that stem:[h] and stem:[\phi_g(h)] are conjugates.
Hence, we have found another way to describe normality of a subgroup.
A subgroup is normal iff it is closed under conjugation.

Additionally, note that stem:[\phi_g] is an isomorphism on stem:[G] (ie an automorphism).
Even more, if stem:[a,b \in G]
[stem]
++++
\phi_a \circ \phi_b = \phi_{ab}
\quad\text{and}\quad
(\phi_a)^{-1} = \phi_{a^{-1}}
++++
Therefore, there is a homomorphism between stem:[G] and stem:[S_G] defined by
stem:[g \mapsto \phi_g].

== Conjugacy classes
If stem:[a \in G], then we define the _conjugacy class_ of stem:[a] as
[stem]
++++
\{gxg^{-1} : g \in G\}
++++
Then it is clear to see that if we relate two elements iff they are conjugates, we
obtain an equivalence relation.

== Set of conjugators
Let stem:[a, b \in G] such that stem:[a] and stem:[b] are conjugates.
Then, if stem:[g a g^{-1} = b]
[stem]
++++
\{h \in G: h a h^{-1} = b\} = g C(a) = C(b)g
++++
where stem:[C(a)] is the xref:basics.adoc#_centralizer[centralizer] of stem:[a].

.Proof
[%collapsible]
====
Firstly, if stem:[h a h^{-1} = b], we get that
[stem]
++++
g^{-1}ha = g^{-1}bh = ag^{-1}h \implies g^{-1}h \in C(a)
++++
and hence stem:[h \in gC(a)]. Conversely, if stem:[h \in gC(a)],
[stem]
++++
h a h^{-1} = gx a (gx)^{-1} = gx a x^{-1}g^{-1} = gag^{-1} = b
++++
where stem:[x \in C(a)].
Therefore, we get that the first two sets are equal.
For, the second half if stem:[h a h^{-1} = b], we get
[stem]
++++
hg^{-1}b = h ag^{-1} = bh g^{-1} \implies hg^{-1} \in C(b)
++++
and hence stem:[h \in C(b)g]. Conversely, if stem:[h \in C(b)g]
[stem]
++++
hah^{-1} = yga(yg)^{-1} = ygag^{-1}y^{-1} = yby^{-1} = b
++++
where stem:[y \in C(b)]. Therefore, we get that the second two sets are equal.
====

== Permuations
Consider stem:[S_n] and stem:[\sigma = (a_1\ a_2\ \ldots a_r) \in S_n] and stem:[\tau \in S_n].
Then
[stem]
++++
\tau \sigma \tau^{-1} = (\tau(a_1) \ \tau(a_2) \ldots \tau(a_r))
++++
Therefore, the conjugate of an stem:[r]-cycle is a stem:[r]-cycle.
Additionally, it follows that for any stem:[\sigma_1, \sigma_2 \in S_n] 
there exists stem:[\tau \in S_n] such that stem:[\tau] maps the labels
in stem:[\sigma_1] to those in stem:[\sigma_2] hence making
stem:[\sigma_1] and stem:[\sigma_2] conjugates. Therefore,
the set of stem:[r]-cycles form a _conjugation class_.

Furthermore, we have the following statement.
Two permutations stem:[\sigma_1] and stem:[\sigma_2]
are conjugates iff they have the same xref:math3610:cycle-index:index.adoc[cycle structure].

.Proof
[%collapsible]
====
Firstly, suppose that stem:[\sigma_1] and stem:[\sigma_2] be two cycles
with the same cycle structure. Then, we can pair each of the cycles
between each decomposition. Suppose we have
[stem]
++++
(a_1,\ a_2, \ \ldots a_k) \sim (b_1,\ b_2, \ \ldots b_k)
++++
where the left belongs to stem:[\sigma_1] while the right belongs to stem:[\sigma_2].
Then we let stem:[\tau] be such that it maps stem:[a_i] to stem:[b_i].
We do this for each pair of cycles.
Notice that stem:[\tau] is a function and must be injective and hence stem:[\tau \in S_n].

Then, consider the product
stem:[\tau^{-1}\sigma_2\tau] and notice that
[stem]
++++
\tau^{-1}\sigma_2\tau (a_i) = \tau^{-1}\sigma_2(b_i) = \tau^{-1}(b_{i+1}) = a_{i+1} = \sigma_1(a_i)
++++
where stem:[a_{k+1} = a_1] and the same for stem:[b]. Then,
stem:[\tau] is as desired.

Conversely, if we consider the conjugate stem:[\tau\sigma_2\tau^{-1}] then,
from above (before this proof), it maps each of the cycles within stem:[\sigma_2] to
a cycle of equal length. Furthermore, all the stem:[n] symbols
must be used because the cycles consist of the image of stem:[\{1,\ldots n\}] in stem:[\tau].
Therefore, the resultant permutation must naturally be a product of disjoint cycles
and hence have the same cycle structure.
====

