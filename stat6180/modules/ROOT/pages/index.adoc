= Advanced Topics in Statistics


This course is taught by Dr. Isaac Dialsingh.
It is heavy on R.

== Table of contents

include::../nav.adoc[]

== Materials

Required reading::
* Computational Statistics, by G. H. Givens and J. A. Hoeting, (Wiley 2005)
* Statistical Computing with R by M. Rizzo, Chapman and Hall

Recommended reading::
* Hastie, T., Tibshirani, R. and Friedman J. 2009. Elements of Statistical Learning Springer.

== Topics

* Optimization using Newton-Rapson
* EM algorithm
