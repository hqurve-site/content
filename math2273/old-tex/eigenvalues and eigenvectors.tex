\section{Eigenvalues and Eigenvectors}

\subsection{Definition}
Let $\mathbb{F}$ be a field and $A \in \mathbb{F}^{n\times n}$.

An element $\lambda \in \mathbb{F}$  is called an \emph{eigenvalue}  or
\emph{characteristic root} of $A$ iff
\[
    \exists \vec{v} \in \mathbb{F}^n \backslash \{\vec{0}\}:
    A \vec{v} = \lambda \vec{v}
\]
where any corresponding vector $v$ is called an \emph{eigenvector}
or \emph{characteristic vector}. Furthermore,
\[
    A \vec{v} = \lambda \vec{v} \implies (A - \lambda I_n) \vec{v} = \vec{0}
\]
where $I_n \in \mathbb{F}^{n\times n}$ (or simply $I$) is the identity element for matrix multiplication.
Therefore, $\vec{v} \in Null(A -\lambda I_n)$, referred to as the \emph{eigenspace} corresponding to 
$\lambda$, and since $\vec{v} \neq \vec{0}$ it follows
that $\det(A - \lambda I_n) = 0$. In fact, its not hard to see that the converse is also true,
that is
\[
    \lambda \text{ is an eigenvalue} \Longleftrightarrow \det(A -\lambda I) = 0
\]

\subsubsection*{Characteristic polynomial and equation}
The polynomial $p_A(\lambda) = det(A - \lambda I)$ is the \emph{characteristic polynomial}
of $A$ and has order $n$. Additionally, the equation $p_A(\lambda) = 0$ is 
\emph{characteristic equation}

\subsubsection*{Computing eigenvalues and eigenvectors}
In order to determine the eigenvalues of $A$, the characteristic equation needs to be solved.
Then, for each eigenvalue $\lambda$, the eigenspace space is determined by finding the
solution set of $(A - \lambda I) \vec{v} = \vec{0}$. All vectors (excluding $\vec{0}$)
are eigenvectors corresponding to $\lambda$.

\subsection{Spectrum}
The \emph{spectrum} of a matrix $A\in \mathbb{F}^{n \times n}$, denoted $\sigma(A)$
is the collection eigenvalues of $A$ (not necessarily) distinct. Additionally, the 
\emph{algebraic multiplicity}, of $\lambda \in \sigma(A)$ is the number of 
times it appears in $A$ or equivalently, the multiplicity of the root $\lambda$
in $p_A(\lambda) = 0$. Furthermore, the \emph{geometric multiplicity}
of $\lambda \in \sigma(A)$ is the dimension of the 
eigenspace of $\lambda$ or equivalently the dimension of $Null(A-\lambda I)$.

\subsubsection*{Relationship of geometric and algebraic multiplicities}
The geometric multiplicity is always at most the algebraic multiplicity.
% Proof obtained from wikipedia
\begin{proofi}
    Consider eigenvalue $\lambda$ with geometric multiplicity $k$. Then their exists 
    $k$ orthonormal vectors, $\vec{b}_1, \ldots, \vec{b}_{k}$, forming 
    a basis for $Null(A - \lambda I)$. We can now extend this orthonormal basis to span the entirety
    of $\mathbb{F}^n$. Let these vectors be $\vec{b}_{k + 1}, \ldots, \vec{b}_n$. Then,
    \[
        \begin{bmatrix}
            \vec{b}_1 & \vec{b}_2 & \cdots & \vec{b}_n
        \end{bmatrix}^T
        \begin{bmatrix}
            \vec{b}_1 & \vec{b}_2 & \cdots & \vec{b}_n
        \end{bmatrix}
        =
        \begin{bmatrix}
            \abrack{\vec{b}_1, \vec{b}_1} & \abrack{\vec{b}_1, \vec{b}_2} & \cdots & \abrack{\vec{b}_1, \vec{b}_n}\\
            \abrack{\vec{b}_2, \vec{b}_1} & \abrack{\vec{b}_2, \vec{b}_2} & \cdots & \abrack{\vec{b}_2, \vec{b}_n}\\
            \vdots & \vdots & \ddots & \vdots\\
            \abrack{\vec{b}_n, \vec{b}_1} & \abrack{\vec{b}_n, \vec{b}_2} & \cdots & \abrack{\vec{b}_n, \vec{b}_n}
        \end{bmatrix}
        = I
    \]
    That is $P = \begin{bmatrix} \vec{b}_1 & \vec{b}_2 & \cdots & \vec{b}_n \end{bmatrix}$ is 
    an orthogonal matrix. Then, let $D = P^{-1} A P$ and since for $1 \leq i \leq k$
    \[
        \vec{b}_i^T A \vec{b}_i = \vec{b}_i^T (\lambda \vec{b}_i) = \lambda \abrack{\vec{b}_i, \vec{b}_i} = \lambda
    \]
    the upper left hand of $D$ is the diagonal matrix $\lambda I_{k}$. 
    Furthermore,
    \[
        P^{-1} (A - x I) P = P^{-1} A P - x P^{-1} I P = D - xI \quad\text{where } x\in \mathbb{F}
    \]
    where the upper left hand of $D-xI$ is the diagonal matrix $(\lambda - x)I_{k}$. 
    Therefore, since $p_A(x) = \det(A-xI) =\det(D-xI)$ contains a factor $(\lambda - x)^{k}$,
    the algebraic multiplicity is at least $k$.
\end{proofi}
\subsubsection*{Similar matrices}
If two matrices $A$ and $B$ are similar, they share the same characteristic
polynomial. This result is clear since $A$ and $B$ represent the same 
transformation however it can also be seen by the following equality chain
\begin{align*}
    p_A(\lambda)
    &= \det(A - \lambda I)\\
    &= \det(P^{-1} B P - \lambda P^{-1} I P)\\
    &= \det(P^{-1} (B - \lambda I ) P)\\
    &= \det(P^{-1})\det(B - \lambda I) \det(P)\\
    &= \det(B - \lambda I)\\
    &= p_B(\lambda)
\end{align*}

\subsubsection*{Diagonalizable}
Recall, a matrix $A$ is diagonalizable if it is similar to a diagonal
matrix. Then the following statements are equivalent
\begin{enumerate}[label=\roman*)]
    \item $\exists P \in \mathbb{F}^{n\times n} $ such that $P^{-1} A P = diag(d_1, d_2, \ldots, d_n)$
    \item $A$ has $n$ linearly independent eigenvectors
            and the $i$'th column of $P$ is the eigenvector corresponding to $d_i$ and 
            geometric and algebraic multiplicities are equal for all eigenvalues $\lambda$.
\end{enumerate}
\begin{proofi}
    Firstly,
    \[
        P^{-1} A P = diag(d_1, d_2, \ldots, d_n) 
        \implies AP = P \, diag(d_1, d_2, \ldots, d_n) 
    \]
    Now, by focusing on the $i$'th column of the matrices of above, $A \vec{v}_i = d_i\vec{v}_i$, that is
    $\vec{v}_i$ is an eigenvalue corresponding to $d_i$. Furthermore, since $P$ is invertible,
    each of the $\vec{v}_i$ are linearly independent and hence the geometric multiplicity of 
    each of the $d_i$ is at its maximum. That is the geometric multiplicity is equal to the 
    algebraic multiplicity for all eigenvalues.
    
    Conversely, by constructing the matrix $P$ using the $n$ linearly independent eigenvectors corresponding to
    $d_1, d_2, \ldots, d_n \in \sigma(A)$, we obtain an invertible matrix $P$. This is possible
    since the geometric multiplicity is equal to the algebraic multiplicity and that no eigenvector
    can be part of two eigenspaces. Therefore, we get that $A P = P\, diag(d_1, d_2, \ldots, d_n)$
    and since $P$ is invertible, we get the desired result.
\end{proofi}

\subsubsection*{Cayley-Hamilton Theorem}
Each matrix $A \in \mathbb{F}^{n\times n}$ satisfies its own characteristic equation.