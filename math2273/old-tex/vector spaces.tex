%2020-10-01
%2020-10-04

\section{Vector Spaces}



\subsection{Fields}
Let $\mathbb{F}$ be a nonempty set and let $+$ and $\cdot$ be two binary operators on $\mathbb{F}$.
Then $\mathbb{F}$ is a \emph{field} if it satisfies the following
\begin{enumerate}[label=\roman*)]
    \newcommand{\field}{\mathbb{F}}
    \item $\forall x, y \in \field: x + y \in \field$ and $x \cdot y \in \field$ (closure of $\field$ under $+$ and $\cdot$).
    \item $+$ and $\cdot$ are both commutative. That is $\forall x, y \in \field: x + y = y + x$ and $x \cdot y = y \cdot x$.
    \item $+$ and $\cdot$ are both associative. That is $\forall x, y, z \in \field: (x+y)+z = x+(y+z)$ and $(x\cdot y)\cdot z = x \cdot (y \cdot z)$.
    \item $\cdot$ distributes over $+$. That is $\forall x, y, z \in \field: x \cdot(x + z) = (x \cdot y) + (x \cdot z)$.
    \item $\exists\, 0, 1 \in \field$ which are the identities under $+$ and $\cdot$ (respectively) in $\field$.
    \item $\forall x \in \field: \exists (-x) \in \field: x + (-x) = 0$.
    \item $\forall x \in \field - \{0\}: \exists x^{-1} \in \field: x \cdot x^{-1} = 1$.
\end{enumerate}
Note 
\begin{itemize}
    \item a field is specified by a set $\mathbb{F}$ together with two the operators $+$ and $\cdot$.
    \item $+$ is called \emph{field addition}
    \item $\cdot$ is called \emph{field multiplication}
    \item $0$ is the \emph{additive field identity}
    \item $1$ is the \emph{multiplicative field identity}
    \item $-x$ is the \emph{additive field inverse} of $x$
    \item $x^{-1}$ is the \emph{multiplicative field inverse} of $x$
\end{itemize}



\subsection{Vector space}
A \emph{vector space} (or \emph{linear space}) over a field $\mathbb{F}$ is a nonempty set $V$ with
two operators $\oplus: (V, V) \rightarrow V$ and $\odot: (\mathbb{F}, V) \rightarrow V$ that satisfy the following
axioms
\begin{enumerate}[label=\roman*)]
    \newcommand{\field}{\mathbb{F}}
    \item $\forall \vec{u}, \vec{v} \in V: \vec{u} \oplus \vec{v} \in V$.
    \item $\forall \vec{u}, \vec{v} \in V: \vec{u} \oplus \vec{v} = \vec{v} \oplus \vec{u}$.
    \item $\forall \vec{u}, \vec{v}, \vec{w} \in V: \vec{u} \oplus (\vec{v} \oplus\vec{w}) = (\vec{u} \oplus \vec{v}) \oplus\vec{w}$.
    \item $\exists \, \vec{0}\in V: \forall \vec{u} \in V: \vec{v} \oplus \vec{0} = \vec{u}$.
    \item $\forall \vec{u} \in V: \exists (-\vec{u}) \in V: \vec{u} + (-\vec{u}) = \vec{0}$.
    \item $\forall a \in \field \text{ and } \vec{u} \in V: a \odot \vec{u} \in V$.
    \item $\forall a \in \field \text{ and } \vec{u}, \vec{v} \in V: a \odot (\vec{u} \oplus \vec{v}) = (a \odot \vec{u})\oplus(a \odot \vec{v})$.
    \item $\forall a, b \in \field \text{ and } \vec{u} \in V: (a + b) \odot \vec{u} = (a\odot\vec{u}) \oplus (b \odot\vec{u})$.
    \item $\forall a, b \in \field \text{ and } \vec{u} \in V: (a \cdot b) \odot \vec{u} = a\odot(b\odot\vec{u})$.
    \item $\forall \vec{u} \in V: 1 \odot \vec{u} = \vec{u}$.
\end{enumerate}
Note:
\begin{itemize}
    \item If $V$ is a vector space over a field $\mathbb{F}$, then the elements in $V$ are referred to as \emph{vectors}
            and the elements in $\mathbb{F}$ are called \emph{scalars}. Also, we refer to $\oplus$ and $\odot$ as \emph{vector addition}
            and \emph{scalar multiplication} (respectively).
    \item If $\mathbb{F} = \mathbb{R}$, then $V$ is called a \emph{real vector space}.
    \item If $\mathbb{F} = \mathbb{C}$, then $V$ is called a \emph{complex vector space}.
    \item Usually, for the sake of brevity we write 
        \begin{itemize}
            \item $\vec{u} \oplus \vec{v}$ as simply $\vec{u} + \vec{v}$
            \item $a \odot \vec{u}$ as $a\vec{u}$
            \item $a \cdot b$ as $ab$
            \item and other usual implicit conventions (such as subtraction)
        \end{itemize}
\end{itemize}

\subsubsection*{Immediate Theorems}
Let $V$ be a vector space over $\mathbb{F}$ with $a \in \mathbb{F}$ and $\vec{u}, \vec{v} \in V$.
Then
\begin{enumerate}[label=\roman*)]
    \item $\vec{u} + \vec{v} = \vec{u} \implies \vec{v} = \vec{0}$.
        \begin{proof}
           $\vec{v} = \vec{0} + \vec{v} = (-\vec{u} + \vec{u}) + \vec{v} = -\vec{u} + (\vec{u} + \vec{v}) = -\vec{u} + \vec{u} = \vec{0}$
        \end{proof}
    \item $a\vec{u} = \vec{0}$ if and only if either $a = 0$ or $\vec{u} = \vec{0}$.
        \begin{proof}We will do the reverse direction first\\
            Reverse Direction:\\
            Case 1 ($a = 0$): Since the additive identity exists
            \begin{align*}
                &0 + 0 = 0\\
                \implies& (0 + 0)\vec{u} = 0\vec{u}\\
                \implies& 0\vec{u} + 0\vec{u} = 0\vec{u}\\
                \implies& 0\vec{u} = \vec{0} \quad \text{from above}
            \end{align*}
            Case 2 ($\vec{u} = \vec{0}$): Since the additive identity exists
            \begin{align*}
                &\vec{0} + \vec{0} = \vec{0}\\
                \implies& a(\vec{0} + \vec{0}) = a\vec{0}\\
                \implies& a\vec{0} + a\vec{0} = a\vec{0}\\
                \implies& a\vec{0} = \vec{0} \quad \text{from above}
            \end{align*}

            \noindent
            Forward direction: \\
            Case 1 ($a = 0$): The result follows from before\\
            Case 2 ($a \neq 0$): Since $a \in \mathbb{F} - \{0\}$, $\exists a^{-1} \in \mathbb{F}$ such that $a^{-1}a = 1$. Then
                \begin{align*}
                    \vec{u} = 1\vec{u} = (a^{-1}a)\vec{u} = a^{-1}(a \vec{u}) = a^{-1}\vec{0} = \vec{0}
                \end{align*}
        \end{proof}
    \item $(-1)\vec{u} = -\vec{u}$.
        \begin{proof}Although this result is trivial, it is very useful
            $$(-1)\vec{u} + \vec{u} = (-1)\vec{u} + 1\vec{u} = (-1 + 1)\vec{u} = 0\vec{u} = \vec{0}$$
        \end{proof}
\end{enumerate}

\subsubsection*{Example vector spaces}
\begin{enumerate}[label=\roman*)]
    \item Let $V = \mathbb{F}^n$ where $\mathbb{F}$ is a field. For $a\ in \mathbb{F}$ and 
            $\vec{u} = (u_1, u_2, \ldots, u_n),\, \vec{v}=(v_1, v_2, \ldots, v_n) \in \mathbb{F}^n$ we can defined 
            $\oplus$ and $\odot$ by 
                $$\vec{u} \oplus \vec{v} = (u_1 + v_1, u_2 + v_2, \ldots, u_n + v_n)$$
            and 
                $$a \odot \vec{u} = (a u_1, a u_2, \ldots, a u_n)$$
            Then $\mathbb{F}^n$ is a vector space over $\mathbb{F}$. These definitions of $\oplus$ and $\odot$ are sometimes
            referred to as \emph{standard vector addition} and \emph{standard scalar multiplication} (respectively).
    \item Let $V = \left\{f| f: [0, 1]\rightarrow \mathbb{R}\right\}$. For $a\in \mathbb{R}$ and $f, g\in V$, we can
            define $\oplus$ and $\odot$ as
            $$
                (f \oplus g)(x) = f(x) + g(x)
                \quad \text{ and }\quad
                (a\odot f)(x) = a\dot f(x)
            $$
            Then $V$ is a vector space over $\mathbb{R}$ and the identity element (the zero function) is defined
            by $\theta(x) = 0$.
    \item A real polynomial of degree $n \in \mathbb{N}$ is a function $p: \mathbb{R} \rightarrow \mathbb{R}$ defined by
                $$p(x) = a_0 + a_1 x + a_2 x^2 + \cdots + a_n x^n$$
            where $a_0, a_1, \ldots, a_{n} \in \mathbb{R}$ and $a_n \neq 0$. Then if we define the set 
            $$\mathbb{P}_n = \left\{p | p \text{ is a real polynomial of degree at most } n\right\}$$
            then $\mathbb{P}_n$ is a vector space over $\mathbb{R}$ (with addition and scalar multiplication as defined above for functions).
            Note that there is an isomorphism between $\mathbb{P}_n$ and $\mathbb{R}^{n+1}$ (more on this later).
            
\end{enumerate}

\subsection{Subspace}
Let $V$ be a vector space over field $\mathbb{F}$. A nonempty subset $W$ of $V$ is a \emph{subspace}
of $V$ if it also a vector space with vector addition and scalar multiplication defined in $V$.
In fact, for $W$ to be a subspace of $V$ only the following need to be proven (since all others are automatically true):
\begin{enumerate}[label=\roman*)]
    \item $\vec{0} \in W$.
    \item $\forall \vec{u}, \vec{v} \in W: \vec{u} + \vec{v} \in W$.
    \item $\forall\, a\in \mathbb{F}, \vec{u}\in W: a\vec{u} \in W$.
\end{enumerate}
Basically all we need to prove is closure.\\
Note: 
\begin{itemize}
    \item if $V$ is any vector space, then $V$ and $\{\vec{0}\}$ are subspaces (called \emph{trivial subspaces}).
    \item if $W$ and $U$ are subspaces of $V$, then $W \cap U$ is also a subspace of $V$.
\end{itemize}


\subsubsection*{Alternative requirements}
In fact, if $V$ is a vector subspace over a field $\mathbb{F}$ and $W \subseteq V$, $W \neq \varnothing$. Then $W$
is a subspace of $V$ if and only if $\forall\, a, b \in \mathbb{F}$ and $\vec{u}, \vec{v} \in W$: $a\vec{u} + b\vec{v} \in W$.
\begin{proof}
    The forward direction is immediately true by conditions (ii) and (iii). However, for the reverse direction
    \begin{enumerate}
        \item taking $a = b = 0$ we get $\vec{0}\in W$
        \item taking $a = b = 1$ we get $\vec{u} + \vec{v} \in W$
        \item taking $b=0$ we get $a\vec{u} \in W$
    \end{enumerate}
\end{proof}

\subsection{Linear combination}
Let $V$ be a vector space over a field $\mathbb{F}$. An element $\vec{u}\in V$ is called a \emph{linear combination} of 
$\vec{u}_1, \vec{u}_2, \ldots, \vec{u}_n \in V$ if 
$\exists \, a_1, a_2, \ldots, a_n \in \mathbb{F}$ such that 
$$\vec{u} = a_1\vec{u}_1 + a_2\vec{u}_2 + \cdots + a_n\vec{u}_n$$

\subsection{Span}
Let $V$ be a vector space over a field $\mathbb{F}$ and $S$ be a nonempty subset of $V$. The (\emph{linear}) \emph{span} of $S$, denoted
by $span(S)$, is the set of all linear combinations of the elements in $S$.
$$span(S) = 
    \left\{
        a_1\vec{u}_1 + a_2\vec{u}_2 + \cdots + a_n\vec{u}_n 
        | n\in \mathbb{N} \text{ and }  \vec{u}_i \in S, a_i \in \mathbb{F} \text{ for }
            i = 1, 2, \ldots, n
    \right\}
$$
If $span(S) = V$, then we say that the set $S$ \emph{spans} V or that $S$ is a \emph{spanning set} for V.

\noindent
Note that the span of $S$ is a subspace of $V$. In fact it is the smallest subspace of $V$ that contains 
$S$. This is an immediate result due to the construction of $span(S)$. Also by convention, $span(\varnothing) = \{\vec{0}\}$ (actually this can be proven but its easy).

\subsection{Linear Independence}
Let $V$ be a vector space over a field $\mathbb{F}$. The elements $\vec{u}_1, \vec{u}_2, \ldots, \vec{u}_n \in V$ are \emph{linearly dependent} over 
$\mathbb[F]$ if there exists, $a_1, a_2, \ldots, a_n \in \mathbb{F}$ (not all equal to 0) such that
$$a_1\vec{u}_1 + a_2\vec{u}_2 + \cdots + a_n\vec{u}_n = \vec{0}$$
Otherwise, $\vec{u}_1, \vec{u}_2, \ldots, \vec{u}_n$ are \emph{linearly independent}.
Alternatively, $\vec{u}_1, \vec{u}_2, \ldots, \vec{u}_n$ are linearly independent if 
$$a_1\vec{u}_1 + a_2\vec{u}_2 + \cdots + a_n\vec{u}_n = \vec{0} \implies a_i = 0\; \forall i = 1, 2, \ldots n$$
Furthermore, we say the set $S = \{\vec{u}_1, \vec{u}_2, \ldots, \vec{u}_n\}$ is linearly independent if
$\vec{u}_1, \vec{u}_2, \ldots, \vec{u}_n$ are linearly independent; otherwise we call $S$ linearly dependent.

\subsubsection*{Some results}
\begin{enumerate}[label=\roman*)]
    \item Any superset of a linearly dependent set in $V$ is also linearly dependent.
            \begin{proof}
                Consider linearly dependent set $S$ and a superset $S' \supseteq S$. Then
                let $S = \{\vec{u}_1, \vec{u}_2, \ldots, \vec{u}_n\}$ and $S' = \{\vec{u}_1, \vec{u}_2, \ldots, \vec{u}_m\}$
                where $n \leq m$. 
                Therefore, $\exists a_1, a_2, \ldots a_n \in \mathbb{F}$ with at least one $a_i \neq 0$ such that
                    $$a_1\vec{u}_1 + a_2\vec{u}_2 + \cdots + a_n\vec{u}_n = \vec{0}$$
                Then by taking $b_1, b_2, \ldots b_m \in \mathbb{F}$ where $a_i = b_i$ $\forall i = 1, 2, \ldots, n$
                and $b_i = 0$ $\forall i > n$.
                    $$
                        b_1\vec{u}_1 + b_2\vec{u}_2 + \cdots + b_n\vec{u}_n + \cdots + b_m\vec{u}_m = 
                        a_1\vec{u}_1 + a_2\vec{u}_2 + \cdots + a_n\vec{u}_n + 0\vec{u}_{n+1} + \cdots + 0\vec{u}_m 
                        = \vec{0} + \vec{0} = \vec{0}
                    $$
                Therefore $S'$ is also linearly dependent.
            \end{proof}
    \item Every nonempty subset of a linearly independent set in $V$ is also linearly independent.
            \begin{proof}
                Follows by contrapositive of (i)
            \end{proof}
    \item If $S \subseteq V$ and $\vec{0} \in S$, then $S$ is a linearly dependent set.
            \begin{proof}
                Note that $1\cdot\vec{0} = \vec{0}$ which implies that $\{\vec{0}\}$ is linearly dependent and 
                hence $S \supseteq \{0\}$ is also linearly dependent.
            \end{proof}
    \item If $S$ is a linearly independent set of $V$ and $\vec{v} \in V$ then $S \cup \{\vec{v}\}$ is linearly
            dependent if and only if $\vec{v} \in span(S)$.
            \begin{proof}
                Suppose $S \cup \{\vec{v}\}$ is linearly dependent then there exists $\vec{v}_1, \vec{v}_2, \ldots, \vec{v}_n \in S$ and 
                $a_0, a_1, a_2, \ldots a_n \in \mathbb{F}$ (not all zero) such that 
                $$a_0\vec{v} + a_1\vec{v}_1 + a_2\vec{v}_2 + \cdots + a_n\vec{v}_n = \vec{0}$$
                Note that $a_0 \neq 0$ otherwise $S$ would be linearly dependent. Then there exists $a_0^{-1} \in \mathbb{F}$ 
                (the multiplicative inverse of $a_0$). Therefore
                $$\vec{v} + a_0^{-1}a_1\vec{v}_1 + a_0^{-1}a_2\vec{v}_2 + \cdots + a_0^{-1}a_n\vec{v}_n = a_0^{-1}\vec{0} = \vec{0}$$
                and hence
                $$\vec{v} = -a_0^{-1}a_1\vec{v}_1 - a_0^{-1}a_2\vec{v}_2 - \cdots - a_0^{-1}a_n\vec{v}_n = a_0^{-1}\vec{0}$$
                Since each of $-a_0^{-1}a_i \in \mathbb{F}$ $\forall \, i = 1, 2, \ldots, n$, $v \in span(S)$.

                \noindent
                Conversely, if $v \in span(S)$, $\exists a_1\vec{v}_1 + a_2\vec{v}_2 + \cdots + a_n\vec{v}_n \in S$ and 
                $b_1, b_2, \ldots, b_n \in \mathbb{F}$ such that
                $$v = b_1\vec{v}_1 + b_2\vec{v}_2 + \cdots + b_n\vec{v}_n$$
                and hence
                $$(-1)v + b_1\vec{v}_1 + b_2\vec{v}_2 + \cdots + b_n\vec{v}_n = \vec{0}$$
                Therefore, $S \cup \{v\}$ is linearly dependent.
            \end{proof}
\end{enumerate}

\subsection{Basis}
Let $V$ be a vector space over a field $\mathbb{F}$. Then, a set $S \subseteq V$ is called a \emph{basis} 
of $V$ if $S$ if $S$ is a linearly independent set that spans $V$.

\subsubsection*{Coordinates}
Let $S = \{\vec{u}_1, \vec{u}_2, \ldots, \vec{u}_n\}$ be a basis of $V$. The scalars $a_1, a_2, \ldots, a_n \in \mathbb{F}$
are said to be the \emph{coordinates} of an element $\vec{u} \in V$  with respect to the basis $S$ if 
$$\vec{u} = a_1\vec{u}_1 + a_2\vec{u}_2 + \cdots + a_n\vec{u}_n$$
Furthermore, we say the n-tuple $[\vec{u}]_S = (a_1, a_2, \ldots, a_n)^T$ is the \emph{coordinate vector} of $\vec{u}$
with respect to $S$.

\subsubsection*{Standard Basis of $\mathbb{R}^n$}
Let $S = \{\vec{e}_1, \vec{e}_2, \ldots, \vec{e}_n\}$ where 
$$
    \vec{e}_1 = (1, 0, \ldots, 0)
    \; , \;
    \vec{e}_2 = (0, 1, \ldots, 0)
    \; \ldots \;
    \vec{e}_n = (0, 0, \ldots, 1)
$$
Then $S$ is called \emph{standard basis} of $\mathbb{R}^n$. Also, given $\vec{u} = (u_1, u_2, \ldots, u_n) \in \mathbb{R}^n$,
the coordinate vector with respect to the standard basis $[\vec{u}]_S = \vec{u}^T$.

\subsubsection*{Equivalent definitions for basis}
Let $V \neq \{\vec{0}\}$ then the following statements are equivalent
\begin{enumerate}[label=\roman*)]
    \item $S$ is a basis of $V$.
    \item $S$ is a minimal spanning set of $V$.
    \item $S$ is a maximal linearly independent subset of $V$.
\end{enumerate}
\begin{proof}Consider each of the following\\
    (i) $\Rightarrow$ (ii)\\
        Suppose $S$ is not a minimal spanning set of $V$, then let $S = S' \cup \{\vec{v}\}$
        where $S'$ is also a spanning set of $V$ (not necessarily minimal). Then $\vec{v} \in V \implies \vec{v} \in span(S')$ which 
        contradicts the linear independence of $S$.\\
    (ii) $\Rightarrow$ (i)\\
        Suppose $S$ is linearly dependent, then $\exists \vec{v} \in S$ such that 
        $S = S' \cup \{\vec{v}\}$ and $\vec{v}\in span(S')$. Then, 
        \begin{align*}
            V 
            &= span(S)\\
            &= span(S' \cup \{\vec{v}\})\\
            &= \{a\vec{v} + b\vec{w}: a, b\in \mathbb{F}, \vec{w}\in span(S')\}\\
            &= span(S')
        \end{align*}
        Therefore, $V = span(S')$ contradicting the minimality of $S$.\\
    (ii) $\Rightarrow$ (iii)\\
        If $S$ is linearly dependent, $\exists: \vec{v} \in S$ such that $\vec{v} \in span(S \backslash\{\vec{v}\})$ which implies
        that $span(S) = span(S \backslash \{\vec{v}\})$ hence contradicting the minimiality of $S$. Also, note that the $S$ spanning $V$ implies
        that $S$ is a maximal linearly independent set of $V$.\\
    (iii) $\Rightarrow$ (ii)\\
        If $S$ is not a spanning set of $V$, $\exists: \vec{v} \in V$ such that $\vec{v} \notin span(S)$ hence contradicting the maximality of $S$.
        Also, note that if $S$ is not a minimal spanning set, this implies that $\exists \vec{v} \in S$ such that $\vec{v} \in span(S \backslash \{\vec{v}\})$
        hence contradicting the linear independence of $S$.\\
    (i) $\Rightarrow$ (iii)\\
        Since $S$ spans $V$, $\forall \vec{v} \in V: v \in span(S)$ then $\nexists \vec{v} \in V$ such 
        that $S \cup \{\vec{v}\}$ is linearly independent. Hence the result follows.\\
    (iii) $\Rightarrow$ (i)\\
        Suppose $S$ does not span $V$, then $\exists \vec{v} \in V$ such that $\vec{v} \notin span(S)$ which implies
        that $S\cup\{v\}$ is also linearly independent hence contradicting the maximality of $S$.
\end{proof}

\subsubsection*{Uniqueness of the coordinate vector}
Consider $\vec{u} \in V$ and basis $S$ of $V$. Then there exists a unique $\vec{u}_1, \vec{u}_2, \ldots, \vec{u}_n \in S$
and unique $a_1, a_2, \ldots, a_n \in \mathbb{F}$ such that 
$$\vec{u} = a_1\vec{u}_1 + a_2\vec{u}_2 + \cdots + a_n\vec{u}_n $$
This follows from the independence of $S$.

\subsubsection*{Exchange property of bases}
Let $A$ and $B$ be distinct bases of $V$. Note that distinctness implies that they are both non-empty. Then 
$$\forall \vec{a}\in A\backslash B: \exists \vec{b}\in B\backslash A \text{ st } (A\backslash\{\vec{a}\})\cup\{\vec{b}\} \text{ is a basis of } V$$
\begin{proof}
    We will show that $\exists \vec{b} \in B\backslash A$ such that $S = (A\backslash\{\vec{a}\})\cup\{\vec{b}\}$ is linearly independent. 
    Then we will show that this same $S$ also spans $V$.

    Suppose that $\forall \vec{b} \in B\backslash A$, $S$ is linearly dependent which means that 
    $\forall \vec{b} \in B \backslash A$, $\vec{b} \in span(A\backslash\{\vec{a}\})$. Moreover, note that if
    we include common elements of both $B$ and $A\backslash\{\vec{a}\}$ we get the same result; ie
    $\forall \vec{b} \in B = B \backslash\{\vec{a}\}: \vec{b} \in span(A\backslash\{\vec{a}\})$. This means that $V = span(B) \subseteq span(A\backslash\{\vec{a}\}) \subset V$
    since $A$ is a minimal spanning set. This obviously is a contradiction and therefore
    $$\exists \vec{b} \in B\backslash A\text{ such that }S = (A\backslash\{\vec{a}\})\cup\{\vec{b}\} \text{ is linearly independent }$$
    
    Consider one such $\vec{b}$ and respective $S$. We will show that it also spans $V$.\\
    Firstly notice that since $A$ spans $V$,
    $$\exists \alpha \in \mathbb{F}, \vec{a}' \in span(A \backslash \{\vec{a}\}): \vec{b} = \alpha \vec{a} + \vec{a}'$$
    Moreover, since $b \notin span(A \backslash \{\vec{a}\})$, $\alpha \neq 0$. Therefore there exists multiplicative 
    inverse of, $\alpha^{-1}$, and hence
    $$\vec{a} = \alpha^{-1}\vec{b} - \alpha^{-1} \vec{a}'$$
    Now, consider an arbitrary $u \in V$. Then since $A$ spans $V$
    $$\exists \gamma \in \mathbb{F}, \vec{u}' \in span(A \backslash \{\vec{a}\}): \vec{u} = \gamma \vec{a} + \vec{u}'$$
    and by using our previous formulation of $\vec{alpha}$ we get
    $$\vec{u} = \gamma(\alpha^{-1}\vec{b} - \alpha^{-1} \vec{a}') + \vec{u}' = \gamma\alpha^{-1}\vec{b} + (-\gamma\alpha^{-1} \vec{a}' + \vec{u}')$$
    Now notice that $(-\gamma\alpha^{-1} \vec{a}' + \vec{u}') \in span(A \backslash \{\vec{a}\})$ and hence $u$ is a linear combination of $\vec{b}$
    and $A \backslash\{\vec{a}\}$.
    Since the choice of $u \in V$ was arbitrary, $S = (A\backslash\{\vec{a}\})\cup\{\vec{b}\}$ also spans $V$.

    Therefore, $S$ is a basis and we are done.
\end{proof}
\paragraph*{Honorary mention} The Steinitz exchange lemma is basically the above statement however with less restrictions. Not really, important since
its mainly the first half of the above proof and $A$ only needs to be linearly independent while $B$ only needs to be spanning (as those are the
only conditions required for the first part of the proof).

\subsection{Dimensionality}
Let $V$ be a vector space over $\mathbb{F}$ with $V \neq \{\vec{0}\}$, and suppose that $S$ is a 
basis of $V$. Then the \emph{dimension} of $V$, denoted $\dim(V)$, is the cardinality of $S$ ($|S|$ or the 
number of element in $S$). By convention $\dim(\vec{0}) = 0$. If $S$ is finite, then $V$ is called
a \emph{finite-dimensional} vector space. Otherwise, $V$ is called \emph{infinite-dimensional}.

\noindent
Note: 
\begin{itemize}
    \item In this course, we will only be considering finite-dimensional vector spaces.
    \item By the previously established exchange property, all bases of $V$ have the same cardinality.
\end{itemize}


\subsection{Sums and direct sums}
Let $U$ and $W$ be subsets of a vector space $V$ over field $\mathbb{F}$. Then the \emph{sum} of $U$ and $W$ is
defined as the set
$$U + W = \left\{\vec{u} + \vec{w}: \vec{u} \in U \text{ and } \vec{w} \in W \right\}$$
and we say that $A$ is a \emph{direct sum} of $U$ and $W$, denoted by $U \oplus W$, if for every element 
$\vec{a} \in A$, there exists unique elements $\vec{u}\in U$ and $\vec{w} \in W$ such that 
$\vec{a} = \vec{u} + \vec{w}$.

\subsubsection*{Closure of subspaces}
If $U$ and $W$ are subspaces of $V$, then $U + W$ is also a subspace of $V$.
\begin{proof}
    Consider subspaces $U$ and $W$ of $V$ and let $A = U + W$. 
    Now consider arbitrary $\alpha, \beta \in \mathbb{F}$ and $\vec{a}_1, \vec{a}_2 \in A$. Then there exists
    $\vec{u}_1, \vec{u}_2 \in U$ and $\vec{w}_1, \vec{w}_2 \in W$ such that
    $\vec{a}_1 = \vec{u}_1 + \vec{w}_1$ and $\vec{a}_2 = \vec{u}_2 + \vec{w}_2$.
    Now consider
    \begin{align*}
        \alpha \vec{a}_1 + \beta \vec{a}_2 
        &= \alpha (\vec{u}_1 + \vec{w}_1) + \beta (\vec{u}_2 + \vec{w}_2)\\
        &= (\alpha \vec{u}_1 + \beta \vec{u}_2) + (\alpha \vec{w}_1 + \beta \vec{w}_2)
    \end{align*}
    also notice that
    $$
        \vec{u}_3 = (\alpha \vec{u}_1 + \beta \vec{u}_2)\in U
    $$
    $$
        \vec{w}_3 = (\alpha \vec{w}_1 + \beta \vec{w}_2)\in W
    $$
    since $U$ and $W$ are subspaces. Therefore $\vec{a} = \vec{u}_3 + \vec{w}_3 \in A$ and hence the result follows.
\end{proof}

\subsubsection*{Vector space as direct sum of disjoint subspaces}
If $U$ and $W$ are subspaces of $V$ such that $U + W = V$ and $U \cap W = \{\vec{0}\}$
then $V = U \oplus W$.
\begin{proof}
    Consider an arbitrary $\vec{v} \in V$.
    Then $\exists \vec{u}_1, \vec{u}_2 \in U$ and $\vec{w}_1, \vec{w}_2 \in W$ such that 
    $$\vec{u}_1 + \vec{w}_1 = \vec{u}_2 + \vec{w}_2 = v$$.
    This implies that 
    $$\vec{u}_1 - \vec{u}_2 = \vec{w}_1 - \vec{w}_2$$
    Let the value of this term be $\vec{v}'$. Notice that since $U$ and $W$ are subspaces,
    $\vec{v}' \in U$ and $\vec{v}' \in W$. However, since $U \cap W = \{\vec{0}\}$,
    $\vec{v}' = \vec{0}$ and hence the result follows.
\end{proof}




\subsubsection*{Dimensionality of a direct sum}
If $V$ is finite-dimensional with subspaces $U$ and $W$ such that $V = U \oplus W$, then 
$$\dim(V) = \dim(U) + \dim(W)$$
\begin{proof}
    Since $U$ and $W$ are vector spaces then there exists bases $S_U$ and $S_W$ of $U$ and $W$
    respectively. We now need to show that $S_U \cap S_W = \varnothing$ and $S_U \cup S_W$ is 
    basis of $V$.

    Firstly, if $S_U \cap S_W \neq \varnothing$, clearly $U \cap W \neq \varnothing$ leading to
    a contradiction.

    Next, consider an arbitrary $\vec{v} \in V$. Then $\exists \vec{u}\in U$ and $\vec{w}\in W$ such that 
    $\vec{v} = \vec{u} + \vec{w}$. Since $\vec{u}$ and $\vec{v}$ can be written as a 
    linear combination of elements $S_U$ and $S_W$ (respectively), $\vec{v}$ can be written
    as a linear combination of $S_U \cup S_W$. Therefore $S_U \cup S_W$ spans $V$.

    Finally consider a linear combination of $S_U \cup S_W$ which is the zero vector. That is 
    $$\vec{0} = \vec{u} + \vec{w}$$
    where $vec{u} \in U$ and $\vec{w} \in W$ are the amalgamates of the linear combinations from $S_U$ and $S_W$. 
    This implies that $\vec{u} = -\vec{w} = \vec{0}$ since $\vec{0}$ is the only common element. Hence, all coefficients
    are equal to $0$ and hence $S_U \cup S_W$  is linearly independent.

    Therefore $S_U \cup S_W$ is a basis and the result follows.
\end{proof}

\subsubsection*{Dimensionality of a sum of subspaces}
% insparation obtained from http://mathonline.wikidot.com/the-dimension-of-a-sum-of-subspaces
% accidentally read about it and read the proof at the same time
% however, my proof is a continuation from the dimensionality of direct sum while theirs is from first principles.
If $V$ is finite-dimensional with subspaces $U$ and $W$, then the dimension of the sum $U + V$ is given by
$$\dim(U + W) = \dim(U) + \dim(W) - \dim(U \cap W)$$
\begin{proof}
    Since $U$ and $W$ are vector spaces then there exists bases for each of them. Furthermore, $U \cap W$ is also a vector space, so let $S'$ be its basis.
    Since $S'$ is an independent set in $U \cap W$ it is also an independent set in both $U$ and $W$. Therefore, the bases
    of $U$ and $W$ can be constructed using $S'$ as a starting point such that a basis of $U$ is $S' \cup S_U$ and a basis of $W$ is
    $S' \cup S_W$. Additionally, it is easily seen that $U = span(S') \oplus span(S_U)$ and $W = span(S') \oplus span(S_W)$.

    Now, notice that $S' \cup S_U \cup S_W$ spans $U + W$ since each $\vec{u} \in U$ and $\vec{w} \in W$ both exists in the 
    span of $S' \cup S_U \cup S_W$. Another way to say this is that $U + W = span(S') + span(S_U) + span(S_W)$.
    
    Also, consider a linear combination of $S' \cup S_U \cup S_W$ which is equal to $\vec{0}$. That is 
    $$\vec{0} = \vec{v} + \vec{u} + \vec{w}$$
    where $\vec{v} \in span(S')$, $\vec{u} \in span(S_U)$ and $\vec{w} \in span(S_W)$. This implies that 
    $$\vec{u} = -\vec{v} -\vec{w} \in span(S') + span(S_W) = W$$
    Notice that $\vec{u} \in span(S_U) \subseteq U$ which implies that $\vec{u} \in U \cap W$. However, $span(S_U) \cap (U \cap W) = span(S_U) \cap span(S')
    = \{\vec{0}\}$. Therefore $\vec{u} = \vec{0}$ and by a similar arguement $\vec{w} = \vec{0}$. Together, this implies that
    $\vec{v} = \vec{0}$ which then implies that $S' \cup S_U \cup S_W$ is linearly independent. Therefore, $S' \cup S_U \cup S_W$ is a basis of $U + W$.

    Finally, consider the dimension of $span(S_U)$. Since $U = span(S') \oplus span(S_U)$, $\dim(U) = \dim(span(S')) + \dim(span(S_U))$ and 
    $\dim(span(S_U)) = \dim(U) - \dim(span(S'))$. Similarly, $\dim(span(S_W)) = \dim(W) - \dim(span(S'))$. Also, $\dim(span(S')) = \dim(U \cap W)$.
    In conclusion,
    \begin{align*}
        \dim(U + W) 
        &= \dim(span(S')) + \dim(span(S_U)) + \dim(span(S_W))\\
        &= \dim(U \cap W) + \dim(U) - \dim(U\cap W) + \dim(W) - \dim(U \cap W) \\
        &= \dim(U) + \dim(W) - \dim(U \cap W)
    \end{align*}
\end{proof}