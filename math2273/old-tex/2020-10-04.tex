\subsection{Sums and direct sums}
Let $U$ and $W$ be subsets of a vector space $V$ over field $\mathbb{F}$. Then the \emph{sum} of $U$ and $W$ is
defined as the set
$$U + W = \left\{\vec{u} + \vec{w}: \vec{u} \in U \text{ and } \vec{w} \in W \right\}$$
and we say that $A$ is a \emph{direct sum} of $U$ and $W$, denoted by $U \oplus W$, if for every element 
$\vec{a} \in A$, there exists unique elements $\vec{u}\in U$ and $\vec{w} \in W$ such that 
$\vec{a} = \vec{u} + \vec{w}$.

\subsubsection*{Closure of subspaces}
If $U$ and $W$ are subspaces of $V$, then $U + W$ is also a subspace of $V$.
\begin{proof}
    Consider subspaces $U$ and $W$ of $V$ and let $A = U + W$. 
    Now consider arbitrary $\alpha, \beta \in \mathbb{F}$ and $\vec{a}_1, \vec{a}_2 \in A$. Then there exists
    $\vec{u}_1, \vec{u}_2 \in U$ and $\vec{w}_1, \vec{w}_2 \in W$ such that
    $\vec{a}_1 = \vec{u}_1 + \vec{w}_1$ and $\vec{a}_2 = \vec{u}_2 + \vec{w}_2$.
    Now consider
    \begin{align*}
        \alpha \vec{a}_1 + \beta \vec{a}_2 
        &= \alpha (\vec{u}_1 + \vec{w}_1) + \beta (\vec{u}_2 + \vec{w}_2)\\
        &= (\alpha \vec{u}_1 + \beta \vec{u}_2) + (\alpha \vec{w}_1 + \beta \vec{w}_2)
    \end{align*}
    also notice that
    $$
        \vec{u}_3 = (\alpha \vec{u}_1 + \beta \vec{u}_2)\in U
    $$
    $$
        \vec{w}_3 = (\alpha \vec{w}_1 + \beta \vec{w}_2)\in W
    $$
    since $U$ and $W$ are subspaces. Therefore $\vec{a} = \vec{u}_3 + \vec{w}_3 \in A$ and hence the result follows.
\end{proof}

\subsubsection*{Vector space as direct sum of disjoint subspaces}
If $U$ and $W$ are subspaces of $V$ such that $U + W = V$ and $U \cap W = \{\vec{0}\}$
then $V = U \oplus W$.
\begin{proof}
    Consider an arbitrary $\vec{v} \in V$.
    Then $\exists \vec{u}_1, \vec{u}_2 \in U$ and $\vec{w}_1, \vec{w}_2 \in W$ such that 
    $$\vec{u}_1 + \vec{w}_1 = \vec{u}_2 + \vec{w}_2 = v$$.
    This implies that 
    $$\vec{u}_1 - \vec{u}_2 = \vec{w}_1 - \vec{w}_2$$
    Let the value of this term be $\vec{v}'$. Notice that since $U$ and $W$ are subspaces,
    $\vec{v}' \in U$ and $\vec{v}' \in W$. However, since $U \cap W = \{\vec{0}\}$,
    $\vec{v}' = \vec{0}$ and hence the result follows.
\end{proof}




\subsubsection*{Dimensionality of a direct sum}
If $V$ is finite-dimensional with subspaces $U$ and $W$ such that $V = U \oplus W$, then 
$$\dim(V) = \dim(U) + \dim(W)$$
\begin{proof}
    Since $U$ and $W$ are vector spaces then there exists bases $S_U$ and $S_W$ of $U$ and $W$
    respectively. We now need to show that $S_U \cap S_W = \varnothing$ and $S_U \cup S_W$ is 
    basis of $V$.

    Firstly, if $S_U \cap S_W \neq \varnothing$, clearly $U \cap W \neq \varnothing$ leading to
    a contradiction.

    Next, consider an arbitrary $\vec{v} \in V$. Then $\exists \vec{u}\in U$ and $\vec{w}\in W$ such that 
    $\vec{v} = \vec{u} + \vec{w}$. Since $\vec{u}$ and $\vec{v}$ can be written as a 
    linear combination of elements $S_U$ and $S_W$ (respectively), $\vec{v}$ can be written
    as a linear combination of $S_U \cup S_W$. Therefore $S_U \cup S_W$ spans $V$.

    Finally consider a linear combination of $S_U \cup S_W$ which is the zero vector. That is 
    $$\vec{0} = \vec{u} + \vec{w}$$
    where $vec{u} \in U$ and $\vec{w} \in W$ are the amalgamates of the linear combinations from $S_U$ and $S_W$. 
    This implies that $\vec{u} = -\vec{w} = \vec{0}$ since $\vec{0}$ is the only common element. Hence, all coefficients
    are equal to $0$ and hence $S_U \cup S_W$  is linearly independent.

    Therefore $S_U \cup S_W$ is a basis and the result follows.
\end{proof}

\subsubsection*{Dimensionality of a sum of subspaces}
% insparation obtained from http://mathonline.wikidot.com/the-dimension-of-a-sum-of-subspaces
% accidentally read about it and read the proof at the same time
% however, my proof is a continuation from the dimensionality of direct sum while theres is from first principles.
If $V$ is finite-dimensional with subspaces $U$ and $W$, then the dimension of the sum $U + V$ is given by
$$\dim(U + W) = \dim(U) + \dim(W) - \dim(U \cap W)$$
\begin{proof}
    Since $U$ and $W$ are vector spaces then there exists bases for each of them. Furthermore, $U \cap W$ is also a vector space, so let $S'$ be its basis.
    Since $S'$ is an independent set in $U \cap W$ it is also an independent set in both $U$ and $W$. Therefore, the bases
    of $U$ and $W$ can be constructed using $S'$ as a starting point such that a basis of $U$ is $S' \cup S_U$ and a basis of $W$ is
    $S' \cup S_W$. Additionally, it is easily seen that $U = span(S') \oplus span(S_U)$ and $W = span(S') \oplus span(S_W)$.

    Now, notice that $S' \cup S_U \cup S_W$ spans $U + W$ since each $\vec{u} \in U$ and $\vec{w} \in W$ both exists in the 
    span of $S' \cup S_U \cup S_W$. Another way to say this is that $U + W = span(S') + span(S_U) + span(S_W)$.
    
    Also, consider a linear combination of $S' \cup S_U \cup S_W$ which is equal to $\vec{0}$. That is 
    $$\vec{0} = \vec{v} + \vec{u} + \vec{w}$$
    where $\vec{v} \in span(S')$, $\vec{u} \in span(S_U)$ and $\vec{w} \in span(S_W)$. This implies that 
    $$\vec{u} = -\vec{v} -\vec{w} \in span(S') + span(S_W) = W$$
    Notice that $\vec{u} \in span(S_U) \subseteq U$ which implies that $\vec{u} \in U \cap W$. However, $span(S_U) \cap (U \cap W) = span(S_U) \cap span(S')
    = \{\vec{0}\}$. Therefore $\vec{u} = \vec{0}$ and by a similar arguement $\vec{w} = \vec{0}$. Together, this implies that
    $\vec{v} = \vec{0}$ which then implies that $S' \cup S_U \cup S_W$ is linearly independent. Therefore, $S' \cup S_U \cup S_W$ is a basis of $U + W$.

    Finally, consider the dimension of $span(S_U)$. Since $U = span(S') \oplus span(S_U)$, $\dim(U) = \dim(span(S')) + \dim(span(S_U))$ and 
    $\dim(span(S_U)) = \dim(U) - \dim(span(S'))$. Similarly, $\dim(span(S_W)) = \dim(W) - \dim(span(S'))$. Also, $\dim(span(S')) = \dim(U \cap W)$.
    In conclusion,
    \begin{align*}
        \dim(U + W) 
        &= \dim(span(S')) + \dim(span(S_U)) + \dim(span(S_W))\\
        &= \dim(U \cap W) + \dim(U) - \dim(U\cap W) + \dim(W) - \dim(U \cap W) \\
        &= \dim(U) + \dim(W) - \dim(U \cap W)
    \end{align*}
\end{proof}