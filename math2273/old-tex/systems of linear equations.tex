\section{Systems of Linear Equations}

\subsection{Definition}
Let $\mathbb{F}$ be a field. A system of $m$ linear equations in $n$
unknowns, $x_1, x_2, \ldots, x_n\in \mathbb{F}$ is a set of equations
of the form
\[
    a_{i1}x_1 + a_{i2}x_2 + \cdots+ a_{in}x_n + =b_i
\]
for $1 \leq i \leq m$. By creating matrix $A = (a_{ij})$ and column vectors
$\vec{x} = (x_1, \ldots, x_n)^T \in \mathbb{F}^n$ and 
$\vec{b} = (b_1, \ldots, b_m)^T \in \mathbb{F}^m$, the above system can be
compactly written as 
\[
    A \vec{x} = \vec{b}
\]
Furthermore, 
\begin{itemize}
    \item the system is called \emph{homogeneous} if $\vec{b} = \vec{0}$ and 
                \emph{non-homogeneous} otherwise.
    \item the set of all solutions is called the \emph{solution set} of the system.
    \item the system is called \emph{consistent} if the solution set is non-empty
            and \emph{inconsistent} otherwise.
\end{itemize}

\subsection{Elementary row operations}
Let $A$ be an $m\times n$ matrix with field (or commutative ring) $\mathbb{F}$.
Then the \emph{elementary row operations} on $A$ are
\begin{enumerate}[label=\roman*)]
    \item Interchange rows $i$ and $j$ (written as $R_i \leftrightarrow R_j$).
    \item Multiply row $i$ by $c \neq 0$ (written as $R_i \rightarrow cR_i$).
    \item Add a non-zero multiple of row $j$ to row $i$ (written as $R_i \rightarrow R_i + cR_j$). 
\end{enumerate}


\subsubsection*{Matrix representation}
Notice that all three operations are linear in their row arguments and are hence linear
transformations. Then these operations can be written as matrices called \emph{elementary
matrices}. They are simply constructed by applying the operation 
to the $m\times m$ identity matrix $I_m$. Furthermore, since each of the operations
are invertible, their matrix representations are also invertible (with respect to matrix
multiplication).

\subsubsection*{Matrix equivalence}
Two $m\times n$ matrices $A$ and $B$ are equivalent, denoted $A\cong B$ if $B$ 
can be obtained from $A$ by a finite number of elementary row operations. That is
\[
    A \cong B \Longleftrightarrow E_1E_2\cdots E_k A = B
\]
This equivalence is in fact an equivalence relation on $\mathbb{R^{m\times n}}$ since elementary row operations
are invertible and their inverse is an elementary row operation.

\subsection{Augmented matrix}
Consider the linear system of equations $A\vec{x} = \vec{b}$. Then the block matrix
$[A, \vec{b}]$ is called the \emph{augmented matrix} of the system. This augmented
matrix allows for easy computation of the solution set of the system. In fact,
if $[A, \vec{b}] \cong [C, \vec{d}]$ have the same solution set.
\begin{proofi}
    Since $[A, \vec{b}] \cong [C, \vec{d}]$, there exists elementary matrices $E_1, \ldots, E_n$
    such that 
    \[
        E_1E_2\cdots E_n [A, \vec{b}] = [C, \vec{d}] 
        \implies
        C = E_1E_2\cdots E_n A 
        \quad\text{and}\quad
        \vec{d} = E_1E_2\cdots E_n \vec{b}
    \]
    Now consider any $\vec{u}\in \mathbb{F}^n$ such that $A\vec{u} = \vec{b}$. Then
    \[
        C\vec{u} 
        = E_1E_2\cdots E_n A
        = E_1E_2\cdots E_n \vec{b}
        = \vec{d}
    \]
    and the converse also follows by the symmetry of matrix equivalence.
\end{proofi}

\subsection{Row echelon form}
Consider a $m\times n$ matrix, $A$. Then the pivot of any row is the first non-zero entry in the row.
Then $A$ is \emph{row echelon form} (REF) if
\begin{itemize}
    \item All zero rows (rows filled with zeros) are at the bottom
    \item The pivot in row $j$ is to the right of all pivots in rows $i < j$.
\end{itemize}
Furthermore, $A$ is in \emph{reduced row echelon form} (RREF) if it is in REF and
\begin{itemize}
    \item the pivots are the only non-zero entries in their columns
    \item all the pivots are $1$ (multiplicative identity)
\end{itemize}
The process of computing the REF of a matrix is called \emph{Gaussian Elimination} while
that of computing the RREF is called \emph{Gauss-Jordan Elimination}, both of which consists
of elementary row operations. Therefore, two matrices are similar iff they share 
a row echelon form. Furthermore, the reduced row echelon form is unique and hence
equivalence classes of RREFs partition the set of matrices.

\subsection{Row and column space}
Let $A$ be an $m\times n$ matrix. Then the subspace spanned by the rows of $A$
is called the \emph{row space} of $A$ and is denoted by $RS(A) \subseteq \mathbb{F}^n$.
Similarly, the subspace spanned by the columns of $A$ is called the \emph{column space}
of $A$ and is denoted $CS(A) \subseteq \mathbb{F}^m$. Then 
\begin{itemize}
    \item The $CS(A)$ is the range of $\vec{x}\mapsto A\vec{x}$ where $\vec{x} \in \mathbb{F}^n$.
    \item The $RS(A)$ is the range of $\vec{y}\mapsto \vec{y}A = (A^T\vec{y}^T)^T$ where $\vec{y} \in \mathbb{F}^m$.
\end{itemize}

\subsubsection*{Rank}
Notice that the rows in a REF matrix are independent and hence form a basis for the row space. Then,
the rank of $\vec{x}\mapsto A\vec{x}$ is the number of non-zero rows of a row echelon form of matrix $A$. 
This rank is called the 
\emph{column rank} and is written as 
\[
    \dim(CS(A))
\]
While the \emph{row rank} is the number of non-zero rows of a row echelon form of $A^T$.
Furthermore, there is no reason to distinguish between the two since they are both equal. 
% This fact is easy
% to see by transposing a REF to produce an almost RREF.