%2020-10-12

\section{Linear Transformations}

\subsection{Definition}
Let $V$ and $W$ be vector spaces over a field $\mathbb{F}$. A function $T: V \rightarrow W$ is called
called a \emph{linear transformation} if $\forall \alpha \in \mathbb{F}$ and $\vec{u}, \vec{v} \in V$
$$T(\alpha \vec{u}) = \alpha T(\vec{u}) \quad \text{and}\quad T(\vec{u} + \vec{v}) = T(\vec{u}) + T(\vec{v})$$
Furthermore, the set of all linear Transformations from $V$ to $W$ is denoted by $\mathcal{L}(V, W)$.
If $V = W$, then the linear transformation $T$ is called a \emph{linear operator}, and the set of linear
operators is denoted $\mathcal{L}(V)$.

\paragraph*{Note}
\begin{itemize}
    \item The identity function $\theta: V \rightarrow V$ defined by $\theta(\vec{u}) = \vec{u}$ $\forall \vec{u} \in V$
                is a linear transformation (called the \emph{identity operator})
    \item The zero function $\theta: V \rightarrow W$ defined by $\theta(\vec{u}) = \vec{0}_W$ $\forall \vec{u} \in V$
                is a linear transformation (called the \emph{zero transformation})
    \item The following statement provides an alternative to definition. $T \in \mathcal{L}(V, W)$ iff
                $\forall\, \alpha, \beta \in \mathbb{F}$ and $\vec{u}, \vec{v} \in V$: 
                $T(\alpha\vec{u} + \beta\vec{y}) = \alpha T(\vec{u}) + \beta T(\vec{v})$.
    \item $T(\vec{0}_V) = \vec{0}_W$. The proof of this is trivial. Since $T(\vec{0}_V) \in W$ and $T(\vec{0}_V) = T(\vec{0}_V + \vec{0}_V) = T(\vec{0}_V) + T(\vec{0}_V)$ 
                it follows that $T(\vec{0}_V) = \vec{0}_W$.
    \item $T \in \mathcal{L}(V, W)$ is completely defined by specifying $T(\vec{b})$ for all $\vec{b}$ in any basis of $V$.
\end{itemize}

\subsection{Range and null space}
Let $V$ and $W$ be vector spaces over the field $\mathbb{F}$ and $T \in \mathcal{L}(V, W)$. Then 
\begin{itemize}
    \item The set $T(V) = Ran(T) = \{T(\vec{u}) \mid \vec{u} \in V\}$ is called the \emph{range} or \emph{range space} of $T$.
    \item The set $\ker(T) = Null(T) = \{\vec{u} \in V \mid T(\vec{u}) = \vec{0}\}$ is called the \emph{kernel} or \emph{null space} of $T$.
\end{itemize}
Furthermore
\begin{enumerate}[label=\arabic*)]
    \item $T(V)$ is a subspace of $W$.
    \item $\ker(T)$ is a subspace of $V$.
    % \item $T$ is injective iff $\ker(T) = \{\vec{0}\}$
\end{enumerate}
\subsubsection*{Proof of (i)}
\begin{proof}
    Notice that $\vec{0} \in T(V)$, since $T(\vec{0}) = \vec{0}$, and hence $T(V) \neq \varnothing$.

    Now consider $\vec{x}, \vec{y} \in T(V)$ and $\alpha, \beta \in \mathbb{F}$. Then $\exists$ 
    $\vec{u}, \vec{v} \in V$ such that $\vec{x} = T(\vec{u})$ and $\vec{y} = T(\vec{v})$. Then
    $$\alpha \vec{x} + \beta\vec{y} = \alpha T(\vec{u}) + \beta T(\vec{y}) = T(\alpha \vec{u} + \beta\vec{v}) \in T(V)$$
    Therefore, $T(V)$ is a subspace of $W$.
\end{proof}
\subsubsection*{Proof of (ii)}
\begin{proof}
    Notice that $\vec{0} \in \ker(T)$ since $T(\vec{0}) = \vec{0}$ and hence $\ker(T) \neq varnothing$.

    Now consider $\vec{u}, \vec{v} \in \ker(T)$ and $\alpha, \beta \in \mathbb{F}$. Then
    $$T(\alpha \vec{u} + \beta\vec{v}) = \alpha T(\vec{u}) + \beta T(\vec{v}) = \vec{0}$$
    hence $\alpha \vec{u} + \beta \vec{v} \in \ker(T)$. Therefore, $\ker(T)$ is a subspace of $V$.
\end{proof}
% \subsubsection*{Proof of (iii)}
% \begin{proof}
%     Suppose that $T$ is injective and consider $\vec{u} \in \ker(T)$. Then $T(\vec{u}) = \vec{0} = T(\vec{0})$
%     and hence $\vec{u} = \vec{0}$. Therefore, $\ker(T) = \{\vec{0}\}$.

%     Conversely, suppose $\ker(T) = \{\vec{0}\}$ and consider $\vec{u}, \vec{v} \in V$ such that 
%     $T(\vec{u}) = T(\vec{v})$. Then $\vec{0} = T(\vec{u}) - T(\vec{v}) = T(\vec{u} - \vec{v})$ which implies that
%     $\vec{u} - \vec{v} \in \ker(T)$ and hence $\vec{u} - \vec{v} = \vec{0}$. Therefore, $T$ is injective.
% \end{proof}


\subsection{Rank-Nullity Theorem}
Let $V$ and $W$ be vector spaces over the field $\mathbb{F}$ and $T \in \mathcal{L}(V, W)$ such that
$\dim(V)$ is finite. Then we define
\begin{itemize}
    \item $Rank(T) = \dim(Ran(T))$
    \item $Nullity(T) = \dim(Null(T))$
\end{itemize}
which must both be finite. Then the rank-nullity theorem asserts 
$$Rank(T) + Nullity(T) = \dim(V)$$

\begin{proof}
    Let the $\dim(V) = n$ and $Nullity(T) = m$. Since $Null(T)$ is a subspace, and hence a vector space, 
    there exists vectors
        $$\vec{b}_1, \vec{b}_2 , \ldots, \vec{b}_m \in Null(T) \subseteq V$$ 
    which form a basis of $Null(T)$. Furthermore, a basis of $V$ can be constructed by extending the 
    previously stated basis by the $n-m$ vectors
    $$\vec{b}_{m+1}, \vec{b}_{m+2} , \ldots, \vec{b}_n \in V$$ 
    Now we claim that the independent vectors $\vec{b}_{m+1}, \ldots, \vec{b}_n$ are also independent under
    transformation $T$. That is the following vectors are independent
    $$ T(\vec{b}_{m+1}), T(\vec{b}_{m+2}) , \ldots, T(\vec{b}_n) $$
    Suppose that these vectors weren't independent. Then there exists a linear combination of them 
    (with not all coefficients begin zero) which is equal to zero. 
    Let $a_{m+1}, a_{m+2}, \ldots, a_n \in \mathbb{F}$ be the coefficients of this linear combination. Then
    $$
        \vec{0} 
        = a_{m+1}T(\vec{b}_{m+1})+ a_{m+2}T(\vec{b}_{m+2}) + \cdots + a_nT(\vec{b}_n) 
        = T(a_{m+1}\vec{b}_{m+1} +a_{m+2}\vec{b}_{m+2} + \cdots + a_n\vec{b}_n)
    $$
    which implies that a linear combination of $\vec{b}_{m+1}, \ldots, \vec{b}_n$ which is in the null
    space of $T$. This violates the independence of the formed basis thereby causing a contradiction. Therefore,
    the vectors $T(\vec{b}_{m+1}) , \ldots, T(\vec{b}_n)$ are linearly independent. Furthermore,
    $$T(V) = span(\{T(\vec{b}_{1}), \ldots, T(\vec{b}_{m+1}), \ldots, T(\vec{b}_n)\}) = span(\{T(\vec{b}_{m+1}), \ldots, T(\vec{b}_n)\})$$
    since $\vec{b}_1, \ldots, \vec{b}_m \in Null(T)$. Therefore $T(\vec{b}_{m+1}) , \ldots, T(\vec{b}_n)$
    form a basis of $T(V)$ and the result follows.
\end{proof}

% \subsection{Functional properties of linear transformations}
% Let $V$ and $W$ be vector spaces over the field $\mathbb{F}$ and $T \in \mathcal{L}(V, W)$. Since linear transformations
% are functions, they also can be injective, surjective and bijective. Moreover, these terms correspond to various linear algebra
% specific properties.

\subsection{Singular}
Let $V$ and $W$ be vector spaces over the field $\mathbb{F}$ and $T \in \mathcal{L}(V, W)$.
Then, $T$ is said to be \emph{singular} if $\exists \vec{v} \in V$ such that $\vec{v} \neq \vec{0}$ but 
$T(\vec{v}) = \vec{0}$. Otherwise, $T$ is said to be \emph{non-singular}. Furthermore, the following
statements are equivalent
\begin{enumerate}[label=\roman*)]
    \item $T$ is injective
    \item $T$ is non-singular
    \item $|\ker(T)| = 1$
    \item If $S \subseteq V$ is linearly independent, then $T(S)$ is also linearly independent
\end{enumerate}

\begin{proof}These equivalences will be proven in a cyclic manner.

    \paragraph*{(i) $\Rightarrow$ (ii)} Given that $T$ is injective, suppose $\exists vec{v} \in V$ such
    that $T(\vec{v}) = \vec{0}$. However, $T(\vec{0}) = \vec{0} = T(\vec{v})$ and by the injectivity
    of $T$, $\vec{v} = \vec{0}$. Therefore, $T$ is non-singular.

    \paragraph*{(ii) $\Rightarrow$ (iii)} Given that $T$ is non-singular, clearly $\ker(T) = \{\vec{0}\}$
    and hence the result follows.

    \paragraph*{(iii) $\Rightarrow$ (iv)} Given that $|\ker(T)| = 1$ and $S$ is linearly independent, suppose
    that $T(S)$ is linearly dependent. Then there exists a linear combination of $T(S)$ (with not all
    coefficients being $0$) which is equal to $\vec{0}$. By using the linearity of $T$, this linear combination
    of $T(S)$ being $\vec{0}$ corresponds to a linear combination of $S$ which cannot be equal to $\vec{0}$ since 
    $S$ is linearly independent. However, this contradicts the fact that $|\ker(T)| = 1$ since $T(\vec{0}) = \vec{0}$.
    Therefore, $T(S)$ must ber linearly independent.

    \paragraph*{(iv) $\Rightarrow$ (i)} Given that $S$ is linearly independent implies that $T(S)$
    is also linearly independent, suppose that $T$ was singular. Then $\exists v \in V$ such that
    $\vec{v} \neq 0$ and $T(\vec{v}) = \vec{0}$. However, the singleton $\{\vec{v}\}$ is linearly independent
    while $T(\{\vec{v}\}) = \{\vec{0}\}$ is linearly dependent. This is a contradiction and hence 
    $T$ is non-singular.
    % \paragraph*{(iii) $\Rightarrow$ (i)} Given that $|\ker(T)| = 1$ and $\vec{0} \in \ker(T)$, 
    % $\ker(T) = \{\vec{0}\}$. Now suppose $\exists \vec{u}, \vec{v} \in V$ such that $T(\vec{u}) = T(\vec{v})$,
    % then by the linearity of $T$, $T(\vec{u} - \vec{v}) = \vec{0}$. Since $\ker(T) = \{\vec{0}\}$, 
    % $\vec{u} - \vec{v} = \vec{0}$ and the result follows.
    
\end{proof}

\subsection{Isomorphisms}
Let $V$ and $W$ be vector spaces over the field $\mathbb{F}$ and $T \in \mathcal{L}(V, W)$.
Then, $T$ is called an \emph{isomorphism} if $T$ is bijective. Furthermore, vectors spaces $V$ and $W$ are referred
to as \emph{isomorphic}, denoted by $V \cong W$, if there is an isomorphism from $V$ to $W$.

\subsubsection*{Pre-image of an isomorphism}
$T$ is an isomorphism if and only if 
$$\forall \vec{w} \in W: \left|T^{-1}(\{\vec{w}\})\right| = 1$$
\begin{proof}
    Consider any $\vec{w} \in W$. Then $\exists \vec{v} \in V$ such that $T(\vec{v}) = \vec{w}$ since $T$
    is onto. This implies that $T^{-1}(\{\vec{w}\}) \neq \varnothing$. Furthermore, consider
    $\vec{v}_1, \vec{v}_2 \in T^{-1}(\{\vec{w}\})$. Then $T(\vec{v}_1) = \vec{w} = T(\vec{v}_2) \implies \vec{v}_1 = \vec{v}_2$.
    Therefore, $\left|T^{-1}(\{\vec{w}\})\right| = 1$.

    Conversely, suppose that $\forall \vec{w} \in W: \left|T^{-1}(\{\vec{w}\})\right| = 1$, then $T$ is onto.
    Furthermore, $T$ is injective since the pre-image of all $\vec{w}$ contains at most one element.
\end{proof}

\subsubsection*{Inverse of an isomorphism}
If $T$ is an isomorphism, $T^{-1}$ is also an isomorphism.
\begin{proof}
    Since $T$ is an isomorphism and hence a bijection, there exists $T^{-1}$ which is also a bijection.
    Now all that is needed to show is that $T^{-1} \in \mathcal{L}(W, V)$.

    Consider $\vec{w}_1, \vec{w}_2 \in W$. Then $\exists$ unique $\vec{v}_1, \vec{v}_2 \in V$ such that
    $T(\vec{v_1}) = \vec{w}_1$ and $T(\vec{v_2}) = \vec{w}_2$. Let $\alpha, \beta \in \mathbb{F}$.
    Then since $T$ is a linear transformation
    $$
        T(\alpha \vec{v}_1 + \beta \vec{v}_2) 
        = \alpha T(\vec{v}_1) + \beta T(\vec{v}_2) 
        = \alpha\vec{w}_1 + \alpha\vec{w}_2
    $$
    and hence
    $$
        T^{-1}(\alpha\vec{w}_1 + \alpha\vec{w}_2) 
        = \alpha \vec{v}_1 + \beta \vec{v}_2
        = \alpha T^{-1}(\vec{w}_1) + \beta T^{-1}(\vec{w}_2)
    $$
    Therefore, $T^{-1} \in \mathcal{L}(W, V)$ and $T^{-1}$ is also an isomorphism. 
\end{proof}

\subsubsection*{Isomorphism as an equivalence relation}
The relation $\cong$ is in fact an equivalence relation.
\begin{proof}Let $U, V, W$ be vector spaces over the field $\mathbb{F}$.
    \paragraph*{Reflexive} $U \cong U$ since the identity function $I$ is an isomorphism from $U$ to $U$.
    \paragraph*{Symmetric} Suppose $U \cong V$, then $\exists T \in \mathcal{L}(U, V)$ such that 
                $T$ is is an isomorphism. Therefore, $\exists T^{-1} \in \mathcal{L}(V, U)$  which is also
                an isomorphism and hence $V \cong U$.
    \paragraph*{Transitive} Suppose $U \cong V$ and $V \cong W$. Then $\exists$ isomorphisms
                $T_1$ and $T_2$ such that $T_1 \in \mathcal{L}(U, V)$ and $T_2 \in \mathcal{L}(V, W)$.
                Immediately $T_1 \circ T_2$ is bijective. Also, consider $\vec{u}_1, \vec{u}_2 \in U$
                and $\alpha, \beta \in \mathbb{F}$. Then
                \begin{align*}
                    (T_2 \circ T_1)(\alpha \vec{u}_1 + \beta \vec{u}_2)
                    &= T_2(T_1 (\alpha \vec{u}_1 + \beta \vec{u}_2))\\
                    &= T_2(\alpha T_1(\vec{u}_1) + \beta T_1(\vec{u}_2))\\
                    &= \alpha T_2(T_1(\vec{u}_1)) + \beta T_2(T_1(\vec{u}_2))\\
                    &= \alpha (T_2 \circ T_1)(\vec{u}_1) + \beta (T_2 \circ T_1)(\vec{u}_2)
                \end{align*}
                and hence $T_2 \circ T_1 \in \mathcal{L}(U, W)$. Therefore, $T_2 \circ T_1$
                is an isomorphism from $U$ to $W$ and hence $U \cong W$.
    
    \vspace{0.6em}
    Since $\cong$ is reflexive, symmetric and transitive, $\cong$ is an equivalence relation.
\end{proof}

\subsubsection*{Isomorphism with $\mathbb{F}^n$}
If $V$ is finite dimensional with $\dim(V) = n$, then $V \cong \mathbb{F}^n$. Note that this isomorphism is what allows
for the coordinate vector representation of vectors given any basis.
\begin{proof}This proof will construct a bijective, linear transformation from $\mathbb{F}^n$ to $V$.

    Let $\vec{v}_1, \vec{v}_2, \ldots, \vec{v}_n$ be form basis of $V$ and $\vec{e}_1, \vec{e}_2, \ldots,
    \vec{e}_n$ be the standard basis of $\mathbb{F}^n$. Now, define $T:\mathbb{F}^n\rightarrow V$ as follows
    $$
        T(\vec{a})
        =
        T(a_1\vec{e}_1 + a_2\vec{e}_2 +\cdots a_n\vec{e}_n)
        =
        T \left(\begin{bmatrix}
            a_1\\ a_2\\ \vdots \\ a_n
        \end{bmatrix}\right)
        =
        a_1\vec{v}_1 + a_2\vec{v}_2 +\cdots a_n\vec{v}_n
    $$
    Now clearly, $T$ is surjective since all vectors in $V$ can be represented as a linear combination
    of the aforementioned basis which. Furthermore $T$ is injective since 
    $$\vec{a} \in \ker(T) \implies a_1 = a_2 = \cdots = a_n = 0 \implies \vec{a} = \vec{0}$$
    and hence $\ker(T) = \{\vec{0}\}$. 
    
    Now all that remains is to show that $T \in \mathcal{L}(\mathbb{F}^n, V)$. This fact is easy to show.
    $$
        T(\alpha \vec{a})
        = T(\alpha a_1\vec{e}_1 + \alpha a_2\vec{e}_2  + \cdots + \alpha a_n\vec{e}_n )
        = \alpha a_1\vec{v}_1 + \alpha a_2\vec{v}_2  + \cdots + \alpha a_n\vec{v}_n 
        = \alpha T(\vec{a})
    $$
    \begin{align*}
        T(\vec{a} + \vec{b}) 
        &= T((a_1+b_1)\vec{e}_1 + (a_2 + b_2)\vec{e}_2 +\cdots (a_n+b_n)\vec{e}_n)\\
        &= (a_1+b_1)\vec{v}_1 + (a_2 + b_2)\vec{v}_2 +\cdots (a_n+b_n)\vec{v}_n\\
        &= T(\vec{a}) + T(\vec{b})
    \end{align*}

    Therefore $\mathbb{F}^n \cong V$.
\end{proof}


\subsection{Algebra of Linear Transformations}
Let $V$ and $W$ be vector spaces over the field $\mathbb{F}$ and $S, T \in \mathcal{L}(V, W)$. Then
\begin{itemize}
    \item The \emph{sum} of $S$ and $T$, denoted $S + T$, is defined by
            $$(S+T)(\vec{u}) = S(\vec{u}) + T(\vec{v}),\; \forall \vec{u} \in V$$
    \item \emph{Scalar multiplication}, denoted $\alpha T$ for $\alpha \in \mathbb{F}$, is defined by
            $$(\alpha T)(\vec{u}) = \alpha T(\vec{u}),\; \forall \vec{u} \in V$$
\end{itemize}
Then, $\mathcal{L}(V, W)$ is a vector space over $\mathbb{F}$ where the additive identity
is the zero transformation, $\theta$ (proof is trivial). Moreover,
$$\dim(\mathcal{L}(V, W)) = \dim(V)\dim(W)$$
This result directly follows by representing a linear transformation by a matrix.


\subsection{Matrix of a Linear Transformation}
Let $V$ and $W$ be vector spaces over the field $\mathbb{F}$ and let $T\in \mathcal{L}(V, W)$.
Let $\mathcal{B}=\{\vec{v}_1, \vec{v}_2, \ldots, \vec{v}_n\}$ and 
$\mathcal{C}=\{\vec{w}_1, \vec{w}_2, \ldots, \vec{w}_m\}$ be bases of $V$ and $W$ respectively. 
Then the $m\times n$ matrix, $A=[T]_\mathcal{B}^\mathcal{C}$ whose entries, $a_{ij}$ are defined by
\[
    T(\vec{v}_j) = a_{1j}\vec{w}_1 + a_{2j}\vec{w}_2 + \cdots + a_{mj}\vec{w}_m\quad \text{for }j=1,2,\ldots n
\]
is called the \emph{matrix of linear transformation} $T$ with respect to bases $\mathcal{B}$ and $\mathcal{C}$.
Also, if $V=W$ and $\mathcal{B} = \mathcal{C}$, we can also denote, $A = [T]_\mathcal{B}$.
Note that $(a_{1j}, a_{2j},\ldots,a_{mj})$ is just the coordinate vector $[T(\vec{v}_j)]_\mathcal{C}$ and hence $A$
is well-defined.

\subsubsection*{Linear Transformations as matrix multiplication}
The matrix of linear transformation can be used to compute linear transformations of any vector. In fact
\[[T(\vec{v})]_\mathcal{C} = [T]_\mathcal{B}^\mathcal{C} [\vec{v}]_\mathcal{B} \quad \forall \vec{v} \in V\]
\begin{proof}Since $\mathcal{B}$ is a basis of $V$, $\exists b_1, b_2,\ldots b_n \in \mathbb{F}$ such that
    \[\vec{v} = b_1\vec{v}_1 + b_2\vec{v}_2 +\cdots +b_n\vec{v}_n\]
and hence
    \begin{align*}
        [T(\vec{v})]_\mathcal{C}
            &= [T(b_1\vec{v}_1 + b_2\vec{v}_2 +\cdots +b_n\vec{v}_n)]_\mathcal{C}\\
            &= [b_1T(\vec{v}_1) + b_2 T(\vec{v}_2) +\cdots +b_nT(\vec{v}_n)]_\mathcal{C}\\
            &= b_1[T(\vec{v}_1)]_\mathcal{C} + b_2 [T(\vec{v}_2)]_\mathcal{C} +\cdots +b_n[T(\vec{v}_n)]_\mathcal{C}\\
            &= \begin{bmatrix}
                [T(\vec{v}_1)]_\mathcal{C},
                &[T(\vec{v}_2)]_\mathcal{C},
                &\ldots,
                &[T(\vec{v}_n)]_\mathcal{C}
            \end{bmatrix}
            (b_1, b_2, \ldots, b_n)^T\\
            &= [T]_\mathcal{B}^\mathcal{C}[\vec{v}]_\mathcal{B}
    \end{align*}
\end{proof}

\paragraph*{Corollary:} Composition of linear transformations is equivalent to matrix multiplication.
That is, given vector spaces $U$, $V$ and $W$ with bases $A$, $B$ and $C$ (respectively)
and linear transformations $T \in \mathcal{L}(U, V)$ and $S \in \mathcal{L}(V, W)$. 
\[
    [S\circ T]_A^C = [S]_B^C \times [T]_A^B
\]


\subsubsection*{Change of basis}
A linear transformation which is a change of basis is just the identity function $I$. Therefore,
if $A$ and $B$ are bases of a vector space $V$, the matrix representing the change of basis
from $A$ to $B$ is $[I]_A^B$.

\subsubsection*{Similar Matrices}
Two square matrices, $A$ and $B$, are similar if there exists an invertible matrix $P$ such
that $P^{-1}AP = B$. This definition is equivalent to saying that $A$ and $B$ both represent the same linear map 
but in two different bases where $P$ is the change of basis matrix. Furthermore, similarity forms an
equivalence relation where those similar to diagonal matrices are called \emph{diagonalizable}.

