\section{Inner products}

\subsection{Definition}
Let $V$ be a real vector space. An \emph{inner product} on $V$ is a function
$\abrack{\cdot, \cdot}: V \times V \rightarrow \mathbb{R}$ which satisfies the following
\begin{enumerate}[label=\roman*)]
    \item $\abrack{\vec{u}, \vec{u}} \geq 0$, $\forall \vec{u} \in V$ (non-negativity)
    \item $\forall \vec{u} \in V: \abrack{\vec{u}, \vec{u}} = 0 \Longleftrightarrow \vec{u} = \vec{0}$ (coincidence)
    \item $\abrack{\vec{u}, \vec{v}} = \abrack{\vec{v}, \vec{u}}$, $\forall \vec{u}, \vec{v} \in V$ (symmetry)
    \item $\abrack{a\vec{u} + b\vec{v}, \vec{w}} = a\abrack{\vec{u}, \vec{w}} + b\abrack{\vec{v}, \vec{w}}$, $\forall \vec{u}, \vec{v}, \vec{w} \in V$ (linearity)
\end{enumerate}
Furthermore, the pair $(V, \abrack{\cdot, \cdot})$ is called an \emph{inner product space}.
\paragraph*{Note}
\begin{itemize}
    \item Condition (ii) can be weakened using (iv) since $0\vec{u} = \vec{0}$.
    \item Condition (iv) can be extended to bilinearity using symmetry.
    \item Inner products can be defined for different fields not only $\mathbb{R}$. However, their definitions vary slightly. (I don't know if there is a unified version).
\end{itemize}

\subsubsection*{Examples}
\begin{enumerate}[label=\roman*)]
    \item Consider the real vector space $\mathbb{R}^n$. Then the \emph{dot product} or \emph{Euclidean inner product}
            $\mathbb{R}^n$ denoted $\vec{x} \cdot \vec{y}$ is in inner product where 
            \[
                \vec{x}\cdot \vec{y} = x_1y_1 + x_2y_2 +\cdots + x_ny_n = \sum_{i=1}^nx_iy_i
            \]
            and $\vec{x} = (x_1, x_2, \ldots, x_n), \vec{y} = (y_1, y_2, \ldots, y_n) \in \mathbb{R}^n$.
            \begin{proof}
                Non-negativity: since $x_i \in \mathbb{R}$
                \[\vec{x} \cdot \vec{x} = x_1^2 + x_2^2 + \cdots +x_n^2 \geq 0\]
                Coincidence: since $x_i \in \mathbb{R}$
                \begin{align*}
                    &\vec{x} \cdot \vec{x} = x_1^2 + x_2^2 + \cdots +x_n^2 = 0\\
                    &\implies x_1^2 = x_2^2 = \cdots = x_n^2\\
                    &\implies x_1 = x_2 = \cdots = x_n\\
                    &\implies \vec{x} = \vec{0}
                \end{align*}
                Symmetry: 
                    \[
                        \vec{x}\cdot \vec{y} 
                        = x_1y_1 + x_2y_2 +\cdots + x_ny_n 
                        = y_1x_1 + y_2x_2 +\cdots + y_nx_n 
                        = \vec{y}\cdot\vec{x}
                    \]
                Linearity:
                    \begin{align*}
                        (a\vec{x} + b\vec{y}) \cdot \vec{z}
                        &= (ax_1 + by_1)z_1 + (ax_2 + by_2)z_2 + \cdots + (ax_n + by_n)z_n\\
                        &= a(x_1z_1 + x_2z_2 + \cdots + x_nz_n) + b(y_1z_1 + y_2z_2 + \cdots + y_nz_n)\\
                        &= a(\vec{x} \cdot \vec{z}) + b(\vec{y} \cdot \vec{z})
                    \end{align*}
            \end{proof}
    \item Consider the real vector space $\mathbb{P}_2$ and distinct $\alpha, \beta, \gamma \in \mathbb{R}$. Then the 
            following function is an inner product over $\mathbb{P}_2$
            \[
                \abrack{p, q} = p(\alpha)q(\alpha) + p(\beta)q(\beta) + p(\gamma)q(\gamma)
            \]
            where $p, q\in \mathbb{P}_2$. Notice that this can be extended to $\mathbb{P}_n$ by taking $(n+1)$ distinct real values instead of $3$.
            \begin{proof}
                Non-negativity: 
                \[
                    \abrack{p, p} 
                    = p(\alpha)p(\alpha) + p(\beta)p(\beta) + p(\gamma)p(\gamma)
                    = p(\alpha)^2 + p(\beta)^2 + p(\gamma)^2
                    \geq 0
                \]
                Coincidence:
                \begin{align*}
                    & \abrack{p, p} = p(\alpha)^2 + p(\beta)^2 + p(\gamma)^2 = 0\\
                    &\implies  p(\alpha)=0, \quad p(\beta)=0, \quad p(\gamma) = 0\\
                    &\implies  p = \theta \in \mathbb{P}_2
                \end{align*}
                since $deg(p) =2$ while it has $3$ distinct roots.\\
                Symmetry:
                \[
                    \abrack{p, q} 
                    = p(\alpha)q(\alpha) + p(\beta)q(\beta) + p(\gamma)q(\gamma)
                    = q(\alpha)p(\alpha) + q(\beta)p(\beta) + q(\gamma)p(\gamma)
                    = \abrack{q, p}
                \]
                Linearity:
                \begin{align*}
                    \abrack{ap + bq, r} 
                    &= (ap + bq)(\alpha)r(\alpha) + (ap + bq)(\beta)r(\beta) + (ap + bq)(\gamma)r(\gamma)\\
                    &= a(p(\alpha)r(\alpha) + p(\beta)r(\beta) + p(\gamma)r(\gamma)) + b(q(\alpha)r(\alpha) + q(\beta)r(\beta) + q(\gamma)r(\gamma))\\
                    &= a\abrack{p, r} + b\abrack{q,r}
                \end{align*}
            \end{proof}

    \item Consider the real vector space $\mathbb{P}_n$ and $R \subseteq \mathbb{R}$ then the following function is an inner product over $\mathbb{P}_n$
                \[
                    \abrack{p, q} = \int_R p(t)q(t) dt
                \]
                where $p, q\in \mathbb{P}_n$. The proof of this is fairly trivial and mostly follows from the linearity of the integral 
                and that order is preserved under integration.\\
                Side node: I would like to know what this function is called in general. It looks like a convolution but its not. Perhaps
                its a special type of convolution or rather another special case of what a convolution really is.\\
                Update: upon reading about hilbert spaces, it have come along the same inner product but no name was mentioned.
\end{enumerate}

\subsection{Norm and unit vector}
Let $V$ be in inner product space with inner product $\abrack{\cdot, \cdot}$. Then the 
\emph{norm} of $\vec{u}\in V$ is denoted and defined by
\[
    \norm{\vec{u}} = \sqrt{\abrack{\vec{u}, \vec{u}}}
\] 
Additionally, a \emph{unit vector}  is any vector $\vec{u} \in V$ such that 
$\norm{u} = 1$. Furthermore for any $\vec{u} \in V\backslash\{\vec{0}\}$, the
\emph{unit vector in the direction of $\vec{u}$} is denoted and defined by
\[
    \hat{\vec{u}} = \frac{1}{\norm{\vec{u}}}\vec{u}
\]
\paragraph{Examples}
\begin{enumerate}[label=\roman*)]
    \item The norm associated with the dot product in $\mathbb{R}^n$ is called the \emph{Euclidean norm} on $\mathbb{R}^n$
\end{enumerate}

\subsubsection*{Cauchy-Schwarz Inequality}
\[
    \abs{\abrack{\vec{u}, \vec{v}}} \leq \norm{\vec{u}}\norm{\vec{v}}.
\]
Moreover, equality holds if and only if $\vec{u}$ and $\vec{v}$ are linearly dependent.
\begin{proofi}
    Clearly, if $\vec{u} = \vec{0}$ or $\vec{v} = \vec{0}$ the result follows. So we
    will take $\vec{u}, \vec{v} \in V \backslash \{\vec{0}\}$. 
    Let $\lambda = -\frac{\abrack{\vec{u}, \vec{v}}}{\norm{\vec{v}}^2} \in \mathbb{R}$ then
    \begin{align*}
        \abrack{\vec{u} + \lambda\vec{v},\vec{u} + \lambda\vec{v}}
        &= \abrack{\vec{u}, \vec{u}} 
            + \abrack{\vec{u}, \lambda \vec{v}}
            + \abrack{\lambda \vec{v}, \vec{u}}
            + \abrack{\lambda \vec{v}, \lambda\vec{v}}
            \\
        &= \norm{\vec{u}}^2
            + 2 \lambda \abrack{\vec{u}, \vec{v}}
            + \lambda^2 \norm{\vec{v}}^2
            \\
        &= \norm{\vec{u}}^2 
                - 2\frac{\abrack{\vec{u}, \vec{v}}^2}{\norm{v}^2} 
                + \frac{\abrack{\vec{u}, \vec{v}}^2\norm{\vec{v}}^2}{\norm{v}^4}
            \\
        &= \norm{\vec{u}}^2 
                - 2\frac{\abrack{\vec{u}, \vec{v}}^2}{\norm{v}^2} 
                + \frac{\abrack{\vec{u}, \vec{v}}^2}{\norm{v}^2}
            \\
        &= \norm{\vec{u}}^2 
                - \frac{\abrack{\vec{u}, \vec{v}}^2}{\norm{v}^2} 
    \end{align*}
    Therefore, by the non-negativity of the inner product (with identical vectors), we get
    \[
        0 \leq \norm{\vec{u}}^2 - \frac{\abrack{\vec{u}, \vec{v}}^2}{\norm{v}^4} 
        \quad\Longleftrightarrow\quad
        \abrack{\vec{u}, \vec{v}}^2 \leq \norm{\vec{u}}^2\norm{\vec{v}}^2 
        \quad\Longleftrightarrow\quad
        \abs{\abrack{\vec{u}, \vec{v}}} \leq \norm{\vec{u}}\norm{\vec{v}}
    \]
    Furthermore, by the coincidence property,
    \[
        \abs{\abrack{\vec{u}, \vec{v}}} = \norm{\vec{u}}\norm{\vec{v}}
        \quad\Longleftrightarrow\quad
        \vec{u} + \lambda\vec{v} = \vec{0}
        \quad\Longleftrightarrow\quad
        \text{$\vec{u}$ and $\vec{v}$ are linearly dependent}
    \]
\end{proofi}

\subsection{Orthogonality}
Let $V$ be an inner product space with inner product $\abrack{\cdot, \cdot}$. 
Then the angle, $\theta \in [0, \pi]$, between two vectors $\vec{u}$
and $\vec{v}$ in $V$ is given by 
\[
    \theta = \arccos\paren{
        \frac{
            \abrack{\vec{u}, \vec{v}}
        }{
            \norm{\vec{u}}\norm{\vec{v}}
        }
    }
\]
Furthermore, $\vec{u}$ and $\vec{v}$ are \emph{orthogonal} or \emph{perpendicular},
denoted $\vec{u}\perp\vec{v}$ if $\abrack{\vec{u}, \vec{v}} = 0$ or $\theta=\frac{\pi}{2}$.

\subsubsection*{Orthogonal complement}
The \emph{orthogonal component} of a set $S \subset V$ is the set
\[
    S^\perp = \cbrack{\vec{u}\in V| \forall \vec{v} \in S: \abrack{\vec{u}, \vec{v}}= 0}
\]
Furthermore, note that $S^\perp$ is a subspace and that $\{\vec{0}\}^\perp = V$.

\subsubsection*{Orthogonal and orthonormal sets}
A set $S = \{\vec{u}_1, \vec{u}_2, \ldots, \vec{u}_n\}$ is an \emph{orthogonal set} if
\[
    \vec{0} \notin S \text{ and }i\neq j \implies \abrack{\vec{u}_i, \vec{u}_j} = 0 
\]
That is, the elements in $S$ are mutually orthogonal. Furthermore, $S$ is a linearly
independent set.
\begin{proofi}
    Let $a_1, a_2, \ldots, a_n \in \mathbb{R}$ such that 
    \[
        a_1\vec{u}_1 + a_2\vec{u}_2 + \cdots + a_n\vec{u}_n = \vec{0}
    \]
    Then, consider any $\vec{u}_i \in S$
    \begin{align*}
        \abrack{\vec{u}_i, a_1\vec{u}_1 + a_2\vec{u}_2 + \cdots + a_n\vec{u}_n}
        &= a_1\abrack{\vec{u}_i, \vec{u}_1} 
            + a_2\abrack{\vec{u}_i, \vec{u}_2}
            +\cdots
            + a_n\abrack{\vec{u}_i, \vec{u}_n}
            \\
        &= a_i\abrack{\vec{u}_i, \vec{u}_i}\\
        &= a_i\norm{\vec{u}_i}^2
    \end{align*}
    Then since $\abrack{\vec{u}_i, \vec{0}} = 0$ and $\abrack{\vec{u}_i, \vec{u}_i} = \norm{\vec{u}_i}^2 \neq 0$, $a_i = 0$.
    \\
\end{proofi}
\noindent
Additionally, if $\vec{u} \in span(S)$ then
\[
    \vec{u} = a_1\vec{u}_1 + a_2\vec{u}_2 + \cdots + a_n\vec{u}_n
\]
and by using the formulation in the proof above we get that 
\[
    \abrack{\vec{u}, \vec{u}_i} 
    = \abrack{a_1\vec{u}_1 + a_2\vec{u}_2 + \cdots + a_n\vec{u}_n, \vec{u}_i}
    = a_i\norm{\vec{u}_i}^2
\]
This result provides an easy way to evaluate the coefficients of a linear combination of
a orthogonal set. Furthermore, $S$ is \emph{orthonormal}, that is $\forall \vec{u} \in S: \norm{\vec{u}} = 1$, then
\[
    \abrack{\vec{u}, \vec{u}_i} = a_i\norm{\vec{u}_i}^2 = a_i
\]
hence providing solid motivation for using orthonormal sets as bases.

\subsubsection*{Orthogonal bases and the Gram-Schmidt Orthogonalization}
If $R =\{\vec{v}_1, \vec{v}_2, \ldots, \vec{v}_n\} \subset V$ is linearly independent
then there exists an orthogonal set $S = \{\vec{u}_1, \vec{u}_2, \ldots, \vec{u}_n\} \subset V$ 
such that $span(R) = span(S)$.
This set can be constructed using the \emph{Gram-Schmidt Orthogonalization Process} defined as followed.
\subparagraph*{Step 1:} take $\vec{u}_1 = \vec{v}_1$
\subparagraph*{Step k: } for $k = 2,3,\ldots, n$ take
\begin{align*}
    \vec{u}_k 
    &= \vec{v}_k - \sum_{i=1}^{k-1} Proj_{\vec{u}_i}(\vec{v}_k) \\
    &= \vec{v}_k - \sum_{i=1}^{k-1} \abrack{\vec{v}_k, \hat{\vec{u}_i}}\hat{\vec{u}_i}\\
    &= \vec{v}_k - \sum_{i=1}^{k-1} \frac{\abrack{\vec{v}_k, \vec{u}_i}}{\norm{\vec{u}_i}^2} \vec{u}_i
\end{align*}
Furthermore, an orthonormal basis can be constructed by taking the set 
$\{\hat{\vec{u}_1}, \hat{\vec{u}_2}, \ldots, \hat{\vec{u}_n}\}$ instead.
\begin{proofi}
    The proof will be conducted in two steps. Firstly, we need to show that the set generated is indeed orthogonal.
    This will be done inductively using the following sets and proposition. For $1 \leq k \leq n$
    \[
        S_k = \{\vec{u}_1, \vec{u}_2, \ldots, \vec{u}_k\}
    \]
    where $S_k$ is an orthogonal. Clearly, this is true for $k=1$ (vacuous truth) now assume it is true for some $k\geq 1$.
    Now consider $S_{k+1}$ and $\vec{u}_j$, where $1 \leq j \leq k$, then 
    \begin{align*}
        \abrack{\vec{u}_j, \vec{u}_{k+1}} 
        &= \abrack{\vec{u}_j,\;\vec{v}_{k+1} - \sum_{i=1}^{k} \frac{\abrack{\vec{v}_{k+1}, \vec{u}_i}}{\norm{\vec{u}_i}^2} \vec{u}_i}\\
        &= \abrack{\vec{u}_j, \vec{v}_{k+1}} - \abrack{\vec{u}_j, \frac{\abrack{\vec{v}_{k+1}, \vec{u}_j}}{\norm{\vec{u}_j}^2} \vec{u}_j}\\
        &= \abrack{\vec{u}_j, \vec{v}_{k+1}} - \frac{\abrack{\vec{v}_{k+1}, \vec{u}_j}}{\norm{\vec{u}_j}^2} \abrack{\vec{u}_j, \vec{u}_j}\\
        &= \abrack{\vec{u}_j, \vec{v}_{k+1}} - \frac{\abrack{\vec{v}_{k+1}, \vec{u}_j}}{\norm{\vec{u}_j}^2} \norm{\vec{u}_j}^2\\
        &= \abrack{\vec{u}_j, \vec{v}_{k+1}} - \abrack{\vec{v}_{k+1}, \vec{u}_j}\\
        &= 0
    \end{align*}
    and is hence $\vec{u}_{k+1}$ is orthogonal to $S_k$. However, we need to show that $\vec{u}_{k+1} \neq \vec{0}$. 
    Notice that each of $\vec{u}_k$ is linear combination of $\{\vec{v}_1, \vec{v}_2, \ldots, \vec{v}_k\}$. Therefore, if $\vec{u}_{k+1}$
    were $\vec{0}$ it would invalidate the linear independence of $R$ since the coefficients of $\vec{v}_{k+1}$ in $\vec{u}_{k+1}$ is 
    $1$. Therefore, the final set, $S = S_n$, is orthogonal.

    Furthermore, by quick rearrangement of the generating equation, we see that $R \subseteq span(S)$. However, since $S$ 
    consists of elements in the span of $R$, $span(R) = span(S)$ and we are done.
\end{proofi}