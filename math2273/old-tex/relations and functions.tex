%2020-09-18
\section{Relations and functions}
\subsection{Relation definition}
A set $R$ is a \emph{relation} between two sets $A$ and $B$ iff $R \subseteq A \times B$.
In this case the set $A$ is referred to as the \emph{domain} of the relation while
the set $B$ is called the \emph{codomain} of the relation. Additionally, if $A = B$, then the
relation $R \subseteq A^2$ is said to be a relation on $A$.\\
Note, an element $x \in A$ is related by $R$ to $y \in B$ (denoted $xRy$) iff $(x, y) \in R$.

\subsection{Equivalence relation}
A relation $R$ on a set $A$ is an \emph{equivalence relation} iff it has the following properties
$\forall x, y, z \in A$.
\begin{enumerate}[label=\roman*)]
    \item reflexive:  $xRx$ 
    \item symmetric: $xRy \implies yRx$
    \item transitive: $xRy \wedge yRz \implies xRz$
\end{enumerate}

\subsection{Equivalence classes}
If $R$ is an equivalence relation, the \emph{equivalence class} (with respect to $R$) of $x \in A$ is defined by
$$
    [x] = \{y \in A: yRx\}
$$
\subsubsection*{Notes}
\begin{itemize}
    \item $x$ belongs to its own equivalence class (due to the reflexive property)
    \item Two different equivalence classes are disjoint (due to the symmetric and transitive property)
\end{itemize}

\subsection{Function definition}
A \emph{function} from $A$ to $B$ is a non-empty relation (st $f \subseteq A \times B$) that satisfies the following
\begin{enumerate}[label=\roman*)]
    \item Existence: $\forall x \in A, \exists y \in B$ such that $(x, y) \in f$
    \item Uniqueness: $(x, y) \in f$ and $(x, z) \in f$  $\implies y = z$
\end{enumerate}
\subsubsection*{Notes}
\begin{itemize}
    \item we write $f: A \rightarrow B$ to indicate that $f$ has domain $A$ and codomain $B$.
    \item If $(x, y) \in f$ we say $f$ maps $x$ onto $y$, or that the image of $x$ under $f$ is $y$. 
          Additionally, instead of writing $(x,y) \in f$, we write $f(x) = y$
\end{itemize}

\subsection{Image and pre-image}
Consider the sets $A$ and $B$ and function $f: A \rightarrow B$. Then
\begin{itemize}
    \item The \emph{image} of $C \subseteq A$ in $B$ is given by $f(C) = \{y \in B: y = f(x) \text{ for some } x\in C\}$
    \item The \emph{pre-image} of $D \subseteq B$ in $A$ is given by $f^{-1}(D) = \{x \in A: f(x) \in D\}$
\end{itemize}
Note: If $C = A$ then $f(C) = f(A)$ is called the \emph{range} of the function $f$ (also denoted $Ran f$).

\subsection{Injections, surjections and bijections}
A function $f: A \rightarrow B$ is said to be
\begin{itemize}
    \item injective (one-to-one) if $\forall x_1, x_2 \in A: x_1 \neq x_2 \implies f(x_1) \neq f(x_2)$\\
            Note that it is sometimes easier to prove the contrapositive $f(x_1) = f(x_2) \implies x_1 = x_2$
    \item surjective (onto) if $f(A) = B$\\
            Note that is is usually simpler to prove $\forall y \in B: \exists x\in A $ such that $f(x) = y$
    \item bijective if $f$ is both an injection and surjection
\end{itemize}

\subsection{Some theorems}
Suppose $f: A \rightarrow B$ and $C, C_1, C_2 \subseteq A$. Then
\begin{enumerate}[label=\roman*)]
    \item $f(\varnothing) = \varnothing$
    \item $C_1 \subseteq C_2 \implies f(C_1) \subseteq f(C_2)$
        \begin{proof}
            \begin{align*}
                y \in f(C_1) 
                &\implies \exists x \in C_1 \text{ such that } f(x) = y\\
                &\implies \exists x \in C_2 \text{ such that } f(x) = y\\
                &\implies y \in f(C_2)
            \end{align*}
        \end{proof}
    \item $C \subseteq f^{-1}[f(C)]$
        \begin{proof}
            $x \in C \implies f(x) \in f(C) \iff x \in f^{-1}[f(C)]$
        \end{proof}
        Note, the first step is reversible if $f$ is injective.
    \item $f[f^{-1}(D)] \subseteq D$
        \begin{proof}
            \begin{align*}
                y \in f[f^{-1}(D)] 
                &\implies \exists x \in f^{-1}(D): f(x) = y\\
                &\implies \exists x \in f^{-1}(D): f(x) = y \wedge f(x) \in D
                        \quad (\text{since } x \in f^{-1}(D) \equiv f(x) \in D)\\
                &\implies y \in D
            \end{align*}
        \end{proof}
    \item $f(C_1 \cap C_2) \subseteq f(C_1) \cap f(C_2)$
        \begin{proof}
            \begin{align*}
                y \in f(C_1 \cap C_2)
                &\iff \exists x \in (C_1 \cap C_2): f(x) = y\\
                &\iff 
                    \left(\exists x \in (C_1 \cap C_2): f(x) = y\right) \wedge
                    \left(\exists x \in (C_1 \cap C_2): f(x) = y\right)\\
                &\implies 
                    \left(\exists x \in C_1: f(x) = y\right) \wedge
                    \left(\exists x \in C_2: f(x) = y\right)\\
                &\iff y\in f(C_1) \wedge y\in f(C_2)\\
                &\iff y\in (f(C_1) \cap f(C_2))
            \end{align*}
        \end{proof}
    \item $f(C_1 \cup C_2) = f(C_1) \cup f(C_2)$
        \begin{proof}
            \begin{align*}
                y \in f(C_1 \cup C_2) 
                &\iff \exists x\in (C_1 \cup C_2): f(x) = y\\
                &\iff (\exists x \in C_1: f(x) = y) \vee (\exists x \in C_2: f(x) = y)\\
                &\iff y\in f(C_1) \vee y\in f(C_2)\\
                &\iff y\in (f(C_1) \cup f(C_2))
            \end{align*}
        \end{proof}
    \item If $f$ is injective, then $f(C_1 \cap C_2) = f(C_1) \cap f(C_2)$
        \begin{proof}
            Same as in part (v) above, however the one non-reversible implication is now reversible. This 
            is because $x$ is unique (due to injectivity) and hence must be present in both $C_1$ and $C_2$.
        \end{proof}
\end{enumerate}


\subsection{Function equality}
Two functions $f: A \rightarrow B$ adn $g: A \rightarrow B$ are said to be \emph{equal}
(written as $f = g$) if
$$\forall x \in A: f(x) = g(x)$$
Note: function equality is an equivalence relation. ie, reflexive, symmetric, transitive.


\subsection{Function composition}
If $f: A \rightarrow B$ and $g: B \rightarrow C$, then the composition of $f$ and 
$g$ (denoted $g \circ f$ or just $gf$) is defined by
$$(g \circ f)(x) = g(f(x)) \forall x \in A$$
Alternatively
$$g \circ f = \{(x, z): x \in A, z \in C, \exists y \in B: (x, y) \in f \wedge (y, z) \in g\}$$
Note: that the composition of functions is not commutative

\subsubsection*{Equivalence and inspection of definitions}
We shall walk through the process of the evaluation of $(g \circ f)(x)$\\
Consider $x \in A$. Then there exists a unique $y \in B$ such that $(x, y) \in f$ or $f(x) = y$.
Next there exists a unique $z \in C$ such that $(y, z) \in g$ or $g(y) = g(f(x)) = z$. 
Therefore, the two above definitions are equivalent and well defined.

\noindent
It should be additionally noted that both in the above proof and the set builder notation above, the existential
quantifier always evaluates once for a unique $y$ and $z$

\subsubsection*{Function composition and function equality}
Consider $x\in A \implies g_1(x) = g_2(x) \implies f_1(g_1(x)) = f_2(g_2(x))$




\subsubsection*{The identity function}
The \emph{identity} function (map) on a set $A$ is the function $I_A: A \rightarrow A$ defined by
$$I_A(x) = x \quad \forall x \in A$$
As its name suggests, the identity function is the identity for function composition. That is
$$f \circ I_A = I_B \circ f = f$$
Note however, the left and right identities are not necessarily the same



\subsubsection*{Some theorems}
Consider the functions $f: A \rightarrow B$, $g: B \rightarrow C$ and $h: C \rightarrow D$. 
Then the following are true
\begin{itemize}
    \item $(f \circ g) \circ h = f \circ (g \circ h)$
    \item If $f$ and $g$ are injective, then $g \circ f$ is injective\\
        Proof: $
            (f \circ g)(x_1) = (f \circ g)(x_2) 
            \implies f(g(x_1)) = f(g(x_2))
            \implies g(x_1) = g(x_2)
            \implies x_1 = x_2
        $
    \item If $f$ and $g$ are surjective, then $g \circ f$ is surjective\\
        Proof: $
            [\forall y \in C:  \exists x' \in B \text{ st } g(x') = y]
            \implies 
            [\forall y \in C: 
                \exists x' \in B \text{ st } g(x') = y \wedge \exists x \in A \text{ st } f(x) = x'
            ]
            \implies 
            [\forall y \in C: \exists x \in A \text{ st } gf(x) = y]
        $
    \item If $f$ and $g$ are bijective, then $g \circ f$ is bijective
\end{itemize}


\subsection{Inverse functions}
Consider a function $f: A \rightarrow B$. Then the following statements are equivalent
\begin{enumerate}[label=(\roman*)]
    \item $f$ is bijective
    \item There exists a function $f^{-1}: B \rightarrow A$, called the \emph{inverse} of $f$ such that
        $$f^{-1}\circ f = I_A \text{ and } f \circ f^{-1} = I_B$$
\end{enumerate}
\subsubsection*{(i) $\rightarrow$ (ii)}
\begin{proof}
    We will prove this by showing the following relation is a function then show that it satisfies the necessary conditions
    $$f^{-1} = \{(y, x) : (x, y) \in f \}$$
    Note, this is equivalent to $(x, y) \in f  \iff  (y, x) \in f^{-1}$
    \begin{itemize}
        \item Existence: $y \in B \implies \exists x \in A: (x, y) \in f$ (since $f$ is surjective) and hence $(y, x) \in f^{-1}$
        \item Uniqueness: $(y_1, x) \in f^{-1}$ and $(y_2, x) \in f^{-1}$ $\implies$ $(x, y_1) \in f$ and $(x, y_2) \in f$ $\implies$ $y_1 = y_2$ (since $f$ is injective)
    \end{itemize}
    Now we know $f^{-1}$ is a function and need to show the necessary properties.
    \begin{itemize}
        \item Consider, $x \in A$. Therefore, $\exists y \in B$ such that $(x,y) \in f$. 
                Also, note that $(y, x) \in f^{-1}$. Therefore, $f(x) = y$ and $f^{-1}(y) = x$ and hence
                $(f^{-1} \circ f)(x) = f^{-1}(f(x)) = f^{-1}(y) = x$\\
                Since we considered an arbitrary $x$, $\forall x \in A: (f^{-1} \circ f)(x) = x = I_A(x)$.\\
                Therefore, $f^{-1} \circ f = I_A$
        \item Consider, $y \in B$. Therefore, $\exists x \in A$ such that $(y, x) \in f^{-1}$.
                Also, note that $(x, y) \in f$. Therefore, $f^{-1}(y) = x$ and $f(x) = y$ and hence
                $(f \circ f^{-1})(y) = f(f^{-1}(y)) = f(x) = y$\\
                Since we considered an arbitrary $y$, $\forall y \in B: (f^{-1} \circ f)(y) = y = I_B(y)$.\\
                Therefore, $f \circ f^{-1} = I_B$
    \end{itemize}
    % Now we must show that this $f^{-1}$ is unique. \\
    % Suppose there is another function $g$ that satisfies the necessary properties. That is
    % $$g\circ f = I_A \text{ and } f \circ g = I_B$$
    % Consider $y \in B$\\
    % Note that, $y = I_B(y) = f(f^{-1}(y)) = f(g(y))$ which (by the injectivity of $f$) 
    % implies that $f^{1}(y) = g(y)$.\\
    % Since the choice of $y$ was arbitrary, $\forall y \in B: f^{-1}(y) = g(y)$ and hence $f^{-1} = g$ and $f^{-1}$ is unique.
\end{proof}

\subsubsection*{(ii) $\rightarrow$ (i)}
\begin{proof}.
    \begin{itemize}
        \item injectivity: $
                f(x_1) = f(x_2) 
                \implies I_A(x_1) = f^{-1}(f(x_1)) = f^{-1}(f(x_2)) = I_A(x_2)
                \implies x_1 = x_2
            $
        \item surjectivity: $\forall y \in B: \exists x \in A$ (namely $x = f^{-1}(y)$) such that $f(x) = f(f^{-1}(y)) = I_B(y) = y$
    \end{itemize}
\end{proof}

\subsubsection*{Uniqueness of the inverse function}
\begin{proof}
Consider two functions $g_1: A\rightarrow B$ and $g_2: A\rightarrow B$ that satisfy (ii).
    \begin{align*}
        \text{Therefore: } f \circ g_1 = I_B = f \circ g_2 
        &\implies g_1 \circ (f \circ g_1) = g_1 \circ (f \circ g_2)\\
        &\implies (g_1 \circ f) \circ g_1 = (g_1 \circ f) \circ g_2\\
        &\implies I_B \circ g_1 = I_B \circ g_2\\
        &\implies g_1 = g_2
    \end{align*}
\end{proof}

\subsubsection*{Inverse of composed function}
Consider two bijective functions $f: A\rightarrow B$ and $g: B \rightarrow C$. 
Then $g \circ f$ is bijective and 
$$ (g \circ f)^{-1} = f^{-1} \circ g^{-1}$$
\begin{proof}
    Right inverse
    \begin{align*}
        (g \circ f) \circ f^{-1} \circ g^{-1}
        &= g \circ (f \circ f^{-1}) \circ g^{-1}\\
        &= g \circ I_B \circ g^{-1}\\
        &= g \circ g^{-1}\\
        &= I_C\\
    \end{align*}
    Also, left inverse
    \begin{align*}
        (f^{-1} \circ g^{-1}) \circ (g \circ f)
        &= f^{-1} \circ (g^{-1} \circ g) \circ f\\
        &= f^{-1} \circ I_B \circ f\\
        &= f^{-1} \circ f\\
        &= I_A
    \end{align*}
\end{proof}


\subsection{Binary operations}
A \emph{binary operation} on a set $A$ is a function from $A \times A$ to $A$.

\noindent
Note, binary operations are usually denoted by special symbols rather than letters.
Additionally, binary operators are usually written using infix notation instead of prefix.
That is, with the binary operation $\oplus: A \times A \rightarrow A$, we write $a \oplus b$
instead of $\oplus(a, b)$.


\subsection{Exercise}
Waste of my time (answers are trivial)