= Completely Random Design

A completely random design consists of stem:[k] _treatments_ each
of which are allocated stem:[n] random _observations_ of one
factor. That is, given stem:[N = nk] possible situations, each
treatment is randomly allocated stem:[n] of them. This design
allows us to utilize statistical theory established prior since the
randomization ensures that there is no systematic or patterned variation
in the observations.

This design seeks to test whether all of the stem:[k] treatment
means stem:[\mu_i] are the same.

== Note

the approach in this section uses the _fixed effects model_ instead of
the _random effects model_. That is, the stem:[k] treatments are
selected beforehand instead of choosing from a larger pool. This choice
would limit us to only make conclusions about the selected
stem:[k] treatments and not the larger pool of treatments as a
whole.

== Approach and neccessity of Analysis of Variance (ANOVA)

As would be emphasized later, this section seeks to produce a test for
the following hypothesis

H~0~: stem:[\mu_1 = \mu_2 \cdots = \mu_k].
H~1~: not all of the stem:[\mu_i] are equal

The simple approach would be to simply test if each pair
stem:[(\mu_i, \mu_j)] are equal. Suppose that each of these are
conducted at a stem:[\alpha] level of significance. Then,
assuming each pair is independent

[stem]
++++
P(\text{type I error})
    = 1- (1-\alpha)^{\binom{k}{2}}
++++
Notice that stem:[(1-\alpha)^{\binom{k}{2}}] tends to
stem:[0] rather quickly by the nature of
stem:[\binom{k}{2}] and hence the probability of a type I error
increase. There are two ways to counteract this,

. Accept the high probability of committing a type I error
. Reduce the probability of committing type I error for each pairwise
test which in return causes type II errors to be more likely.

Both cases are undesirable.

Instead, we use a wholistic approach to testing if all of the
stem:[\mu_i] are equal. Notice that if each of the
stem:[\mu_i] are equal, the variation of the entire
stem:[N=nk] observations should mirror the variation within each
of the stem:[k] treatments. The analysis of variance (ANOVA),
does this but instead uses a more rigorous approach which will be
discussed later.

== The models

We will denote the stem:[j]’th observation for the
stem:[i]’th treatment as stem:[y_{ij}]. Then, we can write
each stem:[y_{ij}] from the stem:[i]th treatment as

[stem]
++++
y_{ij} = \mu_i + \varepsilon_{ij}
++++
where stem:[\varepsilon_{ij}] is due to random error. This model
is called the _means model_. We assume that the
stem:[\varepsilon_{ij}] are independent and identically
distributed with stem:[\varepsilon_{ij} \sim N(0, \sigma^2)].

Additionally, we denote stem:[\mu] to be the mean of the
treatment means. That is
stem:[\mu = \frac{1}{k}\sum_{i=1}^k \mu_i]. Then, we can denote
stem:[\alpha_i = \mu - \mu_i] and rewrite stem:[y_{ij}] as

[stem]
++++
y_{ij} = \mu +\alpha_i + \varepsilon_{ij}
++++
This model is called the _means model_ and would be the basis of our
investigation. Furthermore, we can obtain that each of the
stem:[y_{ij}] are independent and

[stem]
++++
y_{ij} \sim N(\mu + \alpha_i, \sigma^2)
++++
with each of the observations within one treatment being iid.

At this point, our hypotheses can be simplified as

H~0~: stem:[\alpha_i = 0\ \forall i = 1\ldots k].
H~1~: at least one stem:[\alpha_i \neq 0]

=== Note

Both of these models are examples of linear models.

=== Some common notations

Due to the large use of indices, the following summation conventions are
used

[cols="^,^"]
|===

| stem:[\displaystyle y_{i.} = \sum_{j=1}^n y_{ij}].
| stem:[\displaystyle \overbar{y}_{i.} = \frac{1}{n}\sum_{j=1}^n y_{ij}].

| stem:[\displaystyle y_{..} = \sum_{i=1}^k\sum_{j=1}^n y_{ij}].
| stem:[\displaystyle \overbar{y}_{..} = \frac{1}{N}\sum_{i=1}^k\sum_{j=1}^n y_{ij}].

|===

== Variations

We define the following

* stem:[\displaystyle \text{Total variation} = \SST= \sum_{i=1}^k\sum_{j=1}^n (y_{ij} - \overbar{y}_{..})^2]
* stem:[\displaystyle \text{Random variation} = \SSE= \sum_{i=1}^k\sum_{j=1}^n (y_{ij} - \overbar{y}_{i.})^2]
* stem:[\displaystyle \text{Variation due to treaments} = \SSTr= n\sum_{i=1}^k (\overbar{y}_{i.} - \overbar{y}_{..})^2]

=== Important result

We have the following result

[stem]
++++
\SST= \SSTr+ \SSE
++++

.Proof
[%collapsible]
====


We will prove the result for each treatment then sum all of the
treatments. That is, we first need to prove

[stem]
++++
\sum_{j=1}^n (y_{ij} - \overbar{y}_{..})^2 
        = n(\overbar{y}_{i.} - \overbar{y}_{..})^2 + \sum_{j=1}^n (y_{ij} - \overbar{y}_{i.})^2
++++
Note that this result is quite similar to that which relates MSE as the
sum of variance and bias squared. Regardless, we will prove this result
directly.

[stem]
++++
\begin{aligned}
        &\sum_{j=1}^n (y_{ij} - \overbar{y}_{..})^2 \\
        &=\sum_{j=1}^n (y_{ij} - \overbar{y}_{i.} + \overbar{y}_{i.} - \overbar{y}_{..})^2 \\
        &=\sum_{j=1}^n \left[(y_{ij} - \overbar{y}_{i.})^2 
            + (\overbar{y}_{i.} - \overbar{y}_{..})^2 
            + 2(y_{ij} - \overbar{y}_{i.})(\overbar{y}_{i.} - \overbar{y}_{..})\right]\\
        &=\sum_{j=1}^n (y_{ij} - \overbar{y}_{i.})^2 
            + \sum_{j=1}^n(\overbar{y}_{i.} - \overbar{y}_{..})^2 
            + 2(\overbar{y}_{i.} - \overbar{y}_{..})\sum_{j=1}^n(y_{ij} - \overbar{y}_{i.})\\
        &=\sum_{j=1}^n (y_{ij} - \overbar{y}_{i.})^2 
            + n(\overbar{y}_{i.} - \overbar{y}_{..})^2 
            + 2(\overbar{y}_{i.} - \overbar{y}_{..})\left[\sum_{j=1}^ny_{ij} - n\overbar{y}_{i.}\right]\\
        &=\sum_{j=1}^n (y_{ij} - \overbar{y}_{i.})^2 
            + n(\overbar{y}_{i.} - \overbar{y}_{..})^2 
            + 2(\overbar{y}_{i.} - \overbar{y}_{..})\left[y_{i.} - n\overbar{y}_{i.}\right]\\
        &=\sum_{j=1}^n (y_{ij} - \overbar{y}_{i.})^2 
            + n(\overbar{y}_{i.} - \overbar{y}_{..})^2 
    \end{aligned}
++++
Now, if we sum both sides of the equation for stem:[i=1\ldots k],
we get the desired result of

[stem]
++++
\sum_{i=1}^k\sum_{j=1}^n (y_{ij} - \overbar{y}_{..})^2
        =
            n\sum_{i=1}^k (\overbar{y}_{i.} - \overbar{y}_{..})^2 
            + \sum_{i=1}^k \sum_{j=1}^n (y_{ij} - \overbar{y}_{i.})^2
++++

 ◻
====

=== Distributions

Given our means model and the null hypothesis, we have the following
results

[stem]
++++
\frac{\SSTr}{\sigma^2} \sim \bigchi_{(k-1)}^2
    \quad\text{and}\quad
    \frac{\SSE}{\sigma^2} \sim \bigchi_{N-k}^2
++++
its important to note that stem:[N-k = k(n-1)] which gives a hint
as to how prove the result.

.Proof
[%collapsible]
====


Each result will be proven separately.

==== Result (i)

Now, by our model, each of the stem:[y_{ij}] are independent and
hence, their treatment means are independent. Therefore, each of their
treatment means are independent with distribution
stem:[N\left(\mu + \alpha_i, \tfrac{\sigma^2}{n}\right)]. Now,
under stem:[H_0], we have stem:[\alpha_i = 0] and hence
each of the
stem:[\overbar{y}_{i.}]
are iid with distribution
stem:[N\left(\mu, \tfrac{\sigma^2}{n}\right)]. Therefore, the
sample variance of the
stem:[\overbar{y}_{i.}]
are distributed such that

[stem]
++++
\frac{(k-1) \frac{1}{k-1}\sum_{i=1}^k(\overbar{y}_{i.} - \overbar{y}_{..})^2}{\sigma^2/n}
        = \frac{n\sum_{i=1}^k(\overbar{y}_{i.} - \overbar{y}_{..})^2}{\sigma^2}
        = \frac{\SSTr}{\sigma^2} \sim \bigchi_{(k-1)}
++++

==== Result (ii)

Let stem:[s_i] be the sample variance for the stem:[i]’th
treatment. Then
stem:[\tfrac{(n-1)s_i^2}{\sigma^2} \sim \bigchi_{(n-1)}]
for each treatment. Now, since the stem:[y_{ij}] are independent,
their sample variances are also independent. Therefore,

[stem]
++++
\sum_{i=1}^k \frac{(n-1)s_i^2}{\sigma^2} 
        = \sum_{i=1}^k \frac{\sum_{j=1}^n(y_{ij} - \overbar{y}_{i.})^2}{\sigma^2} 
        = \frac{\SSE}{\sigma^2} 
        \sim \bigchi_{k(n-1)} = \bigchi_{(N-k)}
++++

 ◻
====

== The test

In order to determine whether each of the stem:[\alpha_i] are
zero, we consider the distribution of

[stem]
++++
F = \frac{\left.\frac{\SSTr}{\sigma^2}\right/(k-1)}{\left.\frac{\SSE}{\sigma^2}\right/(N-k)} \sim F_{(k-1), (N-k)}
++++
and by letting
stem:[\tfrac{\SSTr}{k-1} = \MSTr]
and
stem:[\tfrac{\SSE}{N-k} = \MSE]
we get that

[stem]
++++
F = \frac{\MSTr}{\MSE} \sim F_{(k-1), (N-k)}
++++
We then produce the following table

[cols="5*^",options=header]
|===
| Source of variation | Sum of Squares (SS) | Degrees of freedom (df) | Mean square (MS) | F-statistic

| Treatments 
| stem:[\SSTr] 
| stem:[k-1] 
| stem:[\MSTr= \frac{\SSTr}{k-1}]
| stem:[F = \frac{\MSTr}{\MSE}]

| Random variation 
| stem:[\SSE] 
| stem:[N-k] 
| stem:[\MSE= \frac{\SSE}{N-k}]
|

| Total 
| stem:[\SST] 
| stem:[N-1] 
| 
| 

|===

At which point we perform a right tailed test on the F-statistic. We use
the following R code to produce the test.

[source,r]
----
data <- read.table("data.csv", header=TRUE, sep=",")
# 2d array with column headings representing the independent and dependent variables
# each row contains a pair (independent, dependent)

is.factor(data$independent) #ensure that independent data is a a factor
data$independent <- as.factor(data$independent) #convert if necessary

result <- lm(dependent ~ independent, data=data)
anova(result)
----

=== Tukey’s HSD Test

The ANOVA test is a omnibus test, in that it only tells us whether our
results are significant overall and not exactly where the differences
lie. Tukey’s HSD post-hoc analysis produces adjusted probabilities for
each pair of treatments which allow us to determine which treatments may
have caused the ANOVA test to produce significant findings.

[source,r]
----
result <- lm(dependent ~ independent, data=data)

result2 <- aov(result)

summary(result2) #produces similar information to anova(result)

TukeyHSD(result2)
----

==== Note

this test can only be conducted if the ANOVA test produces significant
findings.

== Model Adequacy Checking

While performing the ANOVA test, there were several assumptions made.
This section shows methods which can be used to test whether the data
collected satisfies those assumptions and hence also satisfies the
model.

=== Normality

The simplest method is to use a histogram as follows

[source,r]
----
hist(result$res, main="Histogram of residuals", xlab="residuals")
----

However, this is can only be used if the sample size is large. As an
alternative, we can use a Q-Q plot to see if the data matches what would
theoretically be obtained if the data was exactly normal.

[source,r]
----
# normality probability plot
qqnorm(result$res)
# 45-degree line (y=x)
qqline(result$res)
----

=== Independence

This can only be validated during the collection phase.

=== Constant variance

We can conduct Bartlett’s test with the null hypothesis begin that the
treatment variances are equal and alternative hypothesis being that at
least one is different using

[source,r]
----
bartlett.test(dependent ~ independent, data=data)
----

Alternatively, we can use box plots, however this only works if the
number of observations stem:[n] is not small

[source,r]
----
boxplot(dependent ~ independent, xlab="independent", ylab="dependent", data=data)
----

Finally, we can use a scatter plot to plot the residuals against the
fitted values. If the data is randomly distributed, the plot should be a
random cloud and should not show any widening or shrinking.

[source,r]
----
plot(result$fit, result$res)
----

