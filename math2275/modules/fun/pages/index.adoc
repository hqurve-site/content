= Fun

NOTE: These two pages were originally separate from the notes, but currently I'm trying to get everything in here.
They were manually translated (with help of regex substitutions) from their original latex form and may still have 
several graphical errors.

* xref:best-linear-mean-estimator.adoc[]
* xref:best-affine-mean-estimator.adoc[]
