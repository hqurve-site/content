\section{Simple Linear Regression}
Regression analysis is used to predict the value of a dependent variable based on the 
values of one or more independent variables. Additionally, it is used to explain
the how changes in the independent variable(s) affect the value of the dependent variable.

In this section, we will only focus on the case when there is only one independent 
variable and where the relationship can be described using a linear function. We call
this approach \emph{simple linear regression}.


\subsection{The model}
For simple linear regression, we have the following requirements
\begin{itemize}
    \item Linearity: The relationship between the independent and dependent variable is linear.
    \item Independence: The random errors are independent
    \item Normality of error: The random error is normally distributed with mean $0$.
    \item Equal variance: The probability distribution of the random error has constant variance. 
        This is also called homoscedasticity.
\end{itemize}
We will denote the independent variables $X_1, \ldots X_n$ and dependent variables
$Y_1, \ldots Y_n$, then we write each of the $Y_i$ as follows
\[
    Y_i = \alpha + \beta X_i + \varepsilon_i
\]
where 
\begin{itemize}
    \item $\varepsilon_i$ is the random error associated with $Y_i$.
    \item $\alpha$ is the \emph{population $Y$ intercept}.
    \item $\beta$ is the \emph{population slope coefficient}.
\end{itemize}

\subsubsection{Least squares model}
In order to estimate the values of $\alpha$ and $\beta$, we use the least squares model 
in which we seek to minimize the sum of squares difference between
the observed and predicted values of the dependent variable. That is, we seek to minimize
\[
    \sum_{i=1}^n (Y_i - \hat{Y}_i)^2 = \sum_{n=1}^n (Y_i - (\hat\alpha + \hat\beta X_i))^2
\]
where $\hat{Y}_i = \hat\alpha + \hat\beta X_i$ is the estimated value for $Y_i$.
Then, we obtain that 
\[
    \hat\alpha = \overbar{y} - \hat\beta\overbar{x} \sim N\left( \alpha, \sigma^2\left[ \frac{1}{n} + \frac{\overbar{x}^2}{S_{XX}} \right] \right)
\]
and 
\[
    \hat\beta = \frac{S_{XY}}{S_{XY}} \sim N\left(\beta, \frac{\sigma^2}{S_{XX}}\right)
\]
where 
\begin{itemize}
    \item $S_{XX} = \sum_{i=1}^n (x_i - \overbar{x})^2 = \sum_{i=1}^n x_i^2 - n \overbar{x}^2$.
    \item $S_{YY} = \sum_{i=1}^n (y_i - \overbar{y})^2 = \sum_{i=1}^n y_i^2 - n \overbar{y}^2$.
    \item $S_{XY} = \sum_{i=1}^n (x_i - \overbar{x})(y_i - \overbar{y}) = \sum_{i=1}^n x_iy_i - n\overbar{x}\overbar{y}$
\end{itemize}
\proofinline{10} Then, we see that $\hat\alpha$ and $\hat\beta$ are unbiased estimators and we can interpret
their values as follows
\begin{itemize}
    \item $\hat{\alpha}$ is the estimated average value for $Y$ when $X$ is zero
    \item $\hat{\beta}$ is the estimated change in the average value of $Y$ as a result of a one unit 
            change in $X$
\end{itemize}


\subsection{ANOVA Table for regression}
We can split the total variation from the regression line as follows
\[
    \mathrm{SST} = \mathrm{SRR} + \mathrm{SSE} \quad\text{\proofinline{11}}
\]
where 
\begin{itemize}
    \item $\mathrm{SST} = \sum_{i=1}^n (Y_i - \overbar{Y})^2$ is the total variation
    \item $\mathrm{SSR} = \sum_{i=1}^n (\hat{Y}_i - \overbar{Y})^2$ is the variation due to the regression (explained variation)
    \item $\mathrm{SSE} = \sum_{i=1}^n (Y_i- \hat{Y}_i)^2$ is the unexplained variation
\end{itemize}
we then define \emph{coefficient of determination}, $r^2$ as 
\[
    r^2 = \frac{\mathrm{SSR}}{\mathrm{SST}}
\]
which can be interpreted as the proportion of variation which is explained by the model. 
Below are some interpretations of theoretical values
\begin{itemize}
    \item $r^2 = 0$, there is no linear relationship between $X$ and $Y$.
    \item $0 < r^2 < 1$, there is some linear relationship between $X$ and $Y$.
    \item $r^2 = 1$, there is a perfect linear relationship between $X$ and $Y$.
\end{itemize}

\subsubsection{Estimation }
\subsubsection{Standard Error of estimate}
We define the \emph{standard error of estimate} as
\[
    S = \sqrt{\frac{\mathrm{SSE}}{n-1}}
\]
which is the standard deviation of the variation of the observations around the regression line.

\subsection{Residual Analysis}
Firstly, we use the following function to construct the linear model for the data
and store it in the \lstinline|result| variable,
\begin{lstlisting}[language=R]
    result <- lm(formula = dependent ~ independent, data=data)
\end{lstlisting}
we then plot a scatter plot of the residuals using 
\begin{lstlisting}[language=R]
    plot(data$independent, result$residuals)
    # or
    plot(result$fitted, result$residuals)
\end{lstlisting}
Then
\begin{itemize}
    \item The data is linear and independent if the points are evenly distributed around the x-axis.
    \item The data has constant variance if the points are evenly distributed around the x-axis (or abound the regression line)
\end{itemize}
For normality, we use the qq plot as follows
\begin{lstlisting}[language=R]
    qqnorm(result$residuals)
    qqline(result$residuals)
\end{lstlisting}

\subsection{Inference from Slope}
We can perform a T-test on the slope coefficient $\beta$ to determine whether or not there is 
a linear relationship between the independent and dependent variables. We test the two hypotheses,
\begin{adjustwidth}{2em}{}
    $H_0: \beta = 0$ (no linear relationship between the variables)\\
    $H_1: \beta \neq 0$ (there is a linear relationship between the variables)
\end{adjustwidth}
We use the test statistic
\[
    t_{stat} = \frac{\hat{\beta} - \beta}{S_\beta} \sim T_{n-2}
\]
where
\begin{itemize}
    \item $\hat\beta$ is the estimated regression slope coefficient
    \item $\beta$ the hypothesized slope under $H_0$
    \item $S_\beta = \frac{S}{\sqrt{S_{XX}}}$ is the standard error of the slope
    \item $S = \sqrt{\frac{\mathrm{SSE}}{n-2}}$ is the standard error of the estimate (see above)
\end{itemize}
We can use the output of the \lstinline|result| variable produced in residual analysis
to obtain values for this test
\begin{lstlisting}[language=R]
    result <- lm(formula = dependent ~ independent, data=data)
    result
    # look in the coefficients table in the row corresponding to the independent variable
\end{lstlisting}

\subsubsection{F-test of significance: OMNIBUS F-test}
Likewise to previous ANOVA, we have a $F$ statistic which we can use to perform an overall
test on the linear relationship. This test, unlike the previous $T$ test, can
be used with more than one independent variable. We use the following test-statistic
\[
    F_{stat} = \frac{\mathrm{MSR}}{\mathrm{MSE}} \sim F_{k, n-k-1}
\]
where
\begin{itemize}
    \item $\mathrm{MSR} = \frac{\mathrm{SSR}}{k}$
    \item $\mathrm{MSE} = \frac{\mathrm{SSE}}{n - k -1}$
    \item $k$ is the number of independent variables in the regression model.
\end{itemize}
We can again use the output of the \lstinline|result| to obtain values for this test
\begin{lstlisting}[language=R]
    result <- lm(formula = dependent ~ independent, data=data)
    result
    # look for the F-statistic on the last line
\end{lstlisting}
Additionally, we can use the \lstinline|anova| function to perform the calculations
\begin{lstlisting}[language=R]
    anova(result)
\end{lstlisting}
\paragraph*{Note} when $k=1$ (there is only one independent variable), this 
test is equivalent to the t-test. That is, the result always coincides
with the result of the t-test.

\subsubsection{Confidence interval}
Finally, we can construct a confidence interval to test the hypotheses.
We can use the output of the \lstinline|confint| command as follows
\begin{lstlisting}[language=R]
    confint(result, level=0.95)
\end{lstlisting}
and if $0$ lies in the interval, we do not reject $H_0$.


\subsection{Correlation}
Suppose instead, that we are only interested in the presence of 
a linear relationship between the two variables $X$ and $Y$ (note there
is neither any independent nor dependent variable). Then, we 
want to measure the correlation between the two variables.

We use the \emph{correlation coefficient}, $\rho$, defined as follows,
\[
    \rho = \frac{Cov(X, Y)}{\sigma_X \sigma_Y}
\]
For samples, we use Person's Correlation Coefficient $r$
\[
    r = \frac{S_{XY}}{\sqrt{S_{XX}S_{YY}}}
\]
as a estimate for $\rho$. We have the following interpretations for (theoretical) values
\begin{itemize}
    \item $r=1$, then $X$ and $Y$ have a perfect positive linear correlation.
    \item $r=-1$, then $X$ and $Y$ have a perfect negative linear correlation.
    \item $r = 0$, then $X$ and $Y$ are not linearly correlated.
    \item $r > 0$, $X$ and $Y$ have a positive linear relationship.
    \item $r < 0$, $X$ and $Y$ have a negative linear relationship.
\end{itemize}
While, we use the magnitude of $r$ to determine the strength of the relationship
\begin{itemize}
    \item $0 \leq |r| \leq 0.5$ then we say that $X$ and $Y$ have a weak linear relationship
    \item $0.5 \leq |r| \leq 0.8$ then we say that $X$ and $Y$ have a moderate linear relationship
    \item $0.8 \leq |r|$ then we say that $X$ and $Y$ have a strong linear relationship
\end{itemize}
Note, that if there are any unaccounted outliers in the data set,
we perform the test both with and without the point and determine whether it
causes a significant effect.

\subsubsection{Properties of coefficient}
Notice firstly that
\[
    -1 \leq r \leq 1
\]
since $S_{XY}$ is an inner product. Next, notice that $r$ is 
\begin{itemize}
    \item symmetric in $X$ and $Y$
    \item scale independent
    \item location independent
\end{itemize}
Finally note that as hinted by notation $r^2$ is the same coefficient
of determination which was previously discussed.

\subsubsection{Hypothesis test}
We can perform a $T$-test on the correlation coefficient, to determine
whether the two variables are linearly correlated. We test the hypotheses,
\begin{adjustwidth}{2em}{}
    $H_0: \rho = 0$ (no linear relationship between the variables)\\
    $H_1: \rho \neq 0$ (there is a linear relationship between the variables)
\end{adjustwidth}
and use the test statistic
\[
    t_{stat} = \frac{r}{\sqrt{\frac{1-r^2}{n-2}}} \sim T_{n-2}
\]
Note that we must first inspect the data to determine if there is any
indication of a linear relationship. We use the following command
\begin{lstlisting}[language=R]
    plot(X, Y)
\end{lstlisting}
and compute the correlation coefficient using \lstinline|cor(X, Y)|.
Next, we perform the test as follows
\begin{lstlisting}[language=R]
    cor.test(X, Y, alternative="two-sided", method="pearson", conf.level=0.95)
\end{lstlisting}
