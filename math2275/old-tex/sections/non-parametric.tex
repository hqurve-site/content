\section{Non-parametric statistics}
Non-parametric tests have few (if any) restrictions on the population which the sample
data comes from and as such, they are sometimes called \emph{distribution-free tests}.
This is unlike previous methods which required that the sample come from a population
with a specific distribution for example a normal distribution.
As a result, non-parametric methods have the following advantages
\begin{itemize}
    \item They can be applied to many different data as they do not require 
        that the population have any specific distribution or that it be known at all.
    \item They can be applied to qualitative (categorical) data unlike parametric tests which use 
        quantitative data.
    \item Typically, the computations are much simpler and easier to understand and apply.
    \item More resilient to outliers.
\end{itemize}
However, there are also several disadvantages:
\begin{itemize}
    \item Non-parametric tests waste information about the sample data. Intuitively,
        this is caused by placing the data into categories hence loosing 
        the exact values in the process.
    \item Non-parametric tests are less efficient and require more evidence. That is,
        they require larger sample sizes and greater differences to produce a 
        significant findings.
\end{itemize}

\subsection{Sign Test}
The \emph{sign test} is a test about the median of a population. It has no requirements
beside that the sample data must be randomly selected. Additionally, this is the 
non-parametric equivalent one-sample t-test. We use the following command to conduct 
the test from the \texttt{BSDA} package
\begin{lstlisting}[language=R]
    SIGN.test(data, md=m, alternative="less", conf.level=0.95)
\end{lstlisting}
where \texttt{m} is the median in the null hypothesis. In this case, our hypotheses were
\begin{adjustwidth}{2em}{}
    H\textsubscript{0}: median $=m$\\
    H\textsubscript{1}: median $<m$
\end{adjustwidth}
Additionally, the sign test can be used to test matched pairs of data by taking the 
differences. This is the non-parametric equivalent of the paired t-test. 

Finally, the sign-test can be used for nominal (categorical) data by coding each
of the states by a numerical value. This is sometimes called the \emph{xsort} method.

\subsection{Wilcoxon Signed Rank test}
Like the sign-test, the Wilcoxon signed rank test is a test about the median of a population
and is another equivalent of the one-sample t-test.
However, there is the additional requirement that the distribution of the population
is symmetric. It is performed with the following command
\begin{lstlisting}[language=R]
    wilcox.test(data, mu=m, alternative="less", conf.level=0.95)
\end{lstlisting}
which has identical hypotheses as the sign-test. Additionally, the above command
can be used with paired data as follows
\begin{lstlisting}[language=R]
    wilcox.test(data1, data2, mu=0, alternative="less", conf.level=0.95, paired=TRUE)
\end{lstlisting}
Note that you can still just supply the difference of the two vectors, as was done in the 
sign-test above.

\subsubsection{Wilcoxon Rank-Sum test for two independent samples}
The wilcoxon test can also be used to test the medians of two independent samples
however, it has the additional requirement that each sample contains at 
least $10$ observations. This test is sometimes also called the \emph{Mann-Whitney Test}
and is performed as follows
\begin{lstlisting}[language=R]
    wilcox.test(data1, data2, mu=0, alternative="less", conf.level=0.95, paired=FALSE, correct=FALSE)
\end{lstlisting}
Note that the \texttt{correct} flag is used to signify whether continuity correction is to be used.
The hypotheses are 
\begin{adjustwidth}{2em}{}
    H\textsubscript{0}: median of data1 - median of data2 $= 0$\\
    H\textsubscript{1}: median of data1 - median of data2 $< 0$
\end{adjustwidth}

\subsection{Kruskal-Wallis Test}
The \emph{Kruskal-Wallis Test} (also called the H-test) is the non-parametric equivalent of the completely randomized 
design (one-way ANOVA). This test does not require normal distributions however, it requires
that we have at least three independent random samples, each with at least $5$ observations.
Our hypotheses are 
\begin{adjustwidth}{2em}{}
    H\textsubscript{0}: all populations have the same median.\\
    H\textsubscript{1}: at least one population has a different median.
\end{adjustwidth}
We use the following variables for the test
\begin{itemize}
    \item $N$ - total number of observatiosn in all samples combined
    \item $k$ - number of samples
    \item $n_i$ - number of observations in sample $i$.
    \item $R_i$ - sum of ranks in sample $i$
\end{itemize}
In order to produce the test statistic, we do the following
\begin{enumerate}
    \item Combine all of the samples into one list (remembering the sample from which each observation came from)
    \item Sort the list and assign each observation its ``rank'' (ie position). Note that if two observations
        have a tie, there are different approaches to assigning the rank. For example if 
        sample $1$ and $2$ both contain a obervation of value $5$ to take ranks $10$ and $11$ respectively,
        we can 
        \begin{itemize}
            \item assign both rank $10$
            \item assign both rank average $10.5$
            \item assign them $10$ and $11$ randomly
        \end{itemize}
        The method which is used should be consistent.
        Note according to the wikipedia article, the average of the ranks should be used.
    \item calculate the sum of the ranks $R_i$ for each of the samples.
    \item calculate the test statistic $H$ as follows
        \[
            H = \frac{12}{N(N+1)}\paren{\frac{R_1^2}{n_1} + \frac{R_2^2}{n_2} + \cdots + \frac{R_k^2}{n_k}}
            - 3(N+1)
        \]
\end{enumerate}
The test statistic $H$ follows an approximate chi-squared distribution with $k-1$
degrees of freedom and a right tailed test is performed. All the above 
calculations can be done in R with the following command
\begin{lstlisting}[language=R]
    kruskal.test(dependent ~ independent, data=data)
\end{lstlisting}
Also, like the ANOVA, ensure that the independent variables are factors by using the \texttt{as.factor} command.


\subsection{Chi-squared goodness of fit}
The chi-squared goodness of fit test is used to determine how well a sample matches a 
given distribution. Firstly, the sample space is partitioned into $k$ categories each
with $p_i$ probability of occurring. Also, let $n$ be the sample size
and $Y_i$ be the number of observations for the $i$'th category. Then, our test statistic
is given by 
\[
    \bigchi^2_{obs} = \sum_{i} \frac{(Y_i - np_i)^2}{np_i}
\]
which follows a $\bigchi^2_{k-1-r}$ degrees of freedom where 
$r$ is the number of estimated parameters. Additionally, if any of
the $np_i$ are less than $5$, we merge the category with another one.

We then perform a left-tailed test with the following hypotheses
\begin{adjustwidth}{2em}{}
    H\textsubscript{0}: $p_1 = ..,\ p_2 = ..,\  \cdots \ p_k = ..$ that is each of the events have a specified
                probability of occurring.
    H\textsubscript{1}: at least one of the $p_i$ are not as specified.
\end{adjustwidth}

\subsection{Chi-squared test for homogeneity}
This test is used to determine whether the distributions of multiple populations 
are equal. That is, given that there are $h$ populations and $k$ categories, we 
are testing whether the probability of each of the populations occurring in 
each of the categories are the same. As can be seen already, in this test, the 
size of the $h$ populations are fixed before collecting data while the amount
that falls into each category is measured. We then use the following symbols
\begin{itemize}
    \item $N$ is the total number of observations.
    \item $n_i$ is the amount of observations in the sample of the $i$'th population
    \item $y_{ij}$ is the number of observations in the $i$'th sample that fall into the $j$'th category.
        Note that $\sum_{j=1}^k y_{ij} = n_i$.
    \item $\hat{p}_{ij} = \frac{y_{ij}}{n_i}$ is the estimated proportion of the $i$'th sample that falls in the $j$'th category.
    \item $y_{j} = \sum{i=1}^h y_{ij}$ is the total number of observations in the $j$'th category.
    \item $\hat{p}_j = \frac{y_{j}}{N}$ is the estimated proportion of the entire population that falls in the $j$'th category.
\end{itemize}
For visualization see the table below.
\begin{equation}
    \renewcommand\arraystretch{1.3}
    \begin{array}{c|c|c|c|c|c|c}
        \multicolumn{2}{c|}{\multirow{2}{*}{}} & \multicolumn{4}{c|}{\text{Category, } j}&\\\cline{3-7}
        \multicolumn{2}{c|}{} & 1 & 2 & \cdots & k & \text{Total}\\\hline
        \multirow{4}{*}{\rotatebox[origin=c]{90}{\text{Sample, }i}} 
        & 1 & y_{11} & y_{12} & \cdots & y_{1k} & n_1\\\cline{2-7}
        & 2 & y_{21} & y_{22} & \cdots & y_{2k} & n_2\\\cline{2-7}
        & \vdots & \cdots & \vdots & \ddots& \vdots & \vdots \\\cline{2-7}
        & h & y_{h1} & y_{h2} & \cdots &  y_{hk} & n_h\\\cline{1-7}
        &\text{Total} & y_{1} & y_2 & \cdots & y_k & N
    \end{array}
\end{equation}
Then, our hypotheses are 
\begin{adjustwidth}{2em}{}
    H\textsubscript{0}: $p_{1j} = p_{2j} = \cdots p_{hj} = p_{j}$ for all $j=1\ldots k$. That is, the probability of occurring in\\
    \indent the $j$'th category is not dependent on the population which the observation came from.
    H\textsubscript{1}: at least one of the $p_{ij}$ are not as specified.
\end{adjustwidth}
We then perform a right-tailed test on the test statistic
\[
    \bigchi^2_{obs} = \sum_{i=1}^h \sum_{j=1}^k \frac{(y_{ij} - n_i\hat{p}_j)^2}{n_i \hat{p}_j}
\]
which has a distribution of $\bigchi^2_{(h-1)(k-1)}$.


\subsection{Chi squared test for independence}
Although the test for independence looks similar to the test for homogeneity, there is one key difference,
there is only one population. For the test of independence, we first take a fixed sample of size $n$
and place observations into categories of two variables $A$ and $B$. We then use the following notation
\begin{itemize}
    \item $a$ is the number of categories in variable $A$.
    \item $b$ is the number of categories in variable $B$.
    \item $y_{ij}$ is the number of observations in the $i$'th category of variable $A$ and the $j$'th category of variable $B$.
    \item $\hat{p}_{ij} = \frac{y_{ij}}{n}$ is the estimated probability of falling into the $i$'th category of variable $A$
        and the $j$'th category of variable $B$.
    \item $\hat{p}_{i.} = \sum_{j=1}^b \hat{p}_{ij}$ is the estimated probability of falling into the $i$'th category of variable $A$.
    \item $\hat{p}_{.j} = \sum_{i=1}^a \hat{p}_{ij}$ is the estimated probability of falling into the $j$'th category of variable $B$.
\end{itemize}
For visualization see the table below
\begin{equation}
    \renewcommand\arraystretch{1.3}
    \begin{array}{c|c|c|c|c|c|c}
        \multicolumn{2}{c|}{\multirow{2}{*}{}} & \multicolumn{4}{c|}{\text{Variable $B$, } j}&\\\cline{3-7}
        \multicolumn{2}{c|}{} & 1 & 2 & \cdots & b & \text{Total}\\\hline
        \multirow{4}{*}{\rotatebox[origin=c]{90}{\text{Variable $A$, }i}} 
        & 1 & y_{11} & y_{12} & \cdots & y_{1b} & y_{1.}\\\cline{2-7}
        & 2 & y_{21} & y_{22} & \cdots & y_{2b} & y_{2.}\\\cline{2-7}
        & \vdots & \cdots & \vdots & \ddots& \vdots & \vdots \\\cline{2-7}
        & a & y_{a1} & y_{a2} & \cdots &  y_{ab} & y_{a.}\\\cline{1-7}
        &\text{Total} & y_{.1} & y_{.2} & \cdots & y_{.b} & n
    \end{array}
\end{equation}
Then, our hypotheses are 
\begin{adjustwidth}{2em}{}
    H\textsubscript{0}: Variables $A$ and $B$ are independent. That is $P(A_i \cap B_j) = P(A_i)\times P(B_j)$ for all $i=1\ldots a$ and $j=1\ldots b$.\\
    H\textsubscript{1}: Variables $A$ and $B$ are not independent.
\end{adjustwidth}
We then perform a right-tailed test with the test statistic
\[
    \bigchi^2_{obs} = \sum_{i=1}^a \sum_{j=1}^b \frac{\paren{y_{ij} - \frac{y_{i.}y_{.j}}{n}}^2 }{\frac{y_{i.}y_{.j}}{n}}
\]
which has a distribution of $\bigchi^2_{(a-1)(b-1}$. In order to understand the test statistic
notice that under the null hypothesis,
\[
    np_{ij} = nP(A_i \cap B_j) = nP(A_i)P(B_j) 
    \approx n \hat{p}_{i.}\hat{p}_{j.}
    = n\paren{\frac{y_{i.}}{n}}\paren{\frac{y_{.j}}{n}}
    = \frac{y_{i.}y_{.j}}{n}
\]
