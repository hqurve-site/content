\section{Randomized Complete Block Design}
A randomized complete block design consists of $a$ treatments which are each randomly allocated to once to $b$ blocks.
That is, each block contains each of the $a$ treatments, each occurring once, in a random ``order.'' 
Furthermore, there are (possible) differences between pairs of different blocks, however,
there is to be no difference between different positions in a particular block. 

In a similar manner to the completely random design, this design seeks to test whether the $a$ 
treatment means are the same.

\paragraph*{Note} I believe that these tests can be done with blocking in more than one way.

\subsubsection{Approach and neccessity of two-way ANOVA}
As with everything else, there are sources of error for everything we do. 
We refer to these sources of error as \emph{nuisance factors} and if it is
\begin{itemize}
    \item unknown and uncontrolled: randomization is the technique of choice in order to 
            combat it.
    \item known but uncontrollable: we observe the behaviour of that particular 
            nuisance factor throughout the experiment. In this case analysis of covariance
            is used.
    \item known and controllable: we design the experiment around fixed instances of that
            particular factor. We use \emph{blocking} to segment the tests which
            can then be examined while taking into account the value of the nuisance
            factor.
\end{itemize}
In our particular situation, we know that each of the blocks may
yield different results and hence instead of testing all of the 
treatments on just one block, we test all of the treatments on
all of the blocks. Note that randomization of treatment order within each of the 
blocks is necessary to mitigate against any unknown nuisance
factors within each of the blocks.

After all of the tests are conducted, the information of each of the treatments
within each of the blocks are used to generate information for each of
the treatments independent of the blocks (assuming that the blocks 
used are representative of the population of blocks).

\subsection{The models}
We will denote the observation for the $i$'th treatment within the $j$'th 
block as $y_{ij}$. Then, as before, we can write each of the $y_{ij}$
as
\[
    y_{ij} = \mu_{ij} + \varepsilon_{ij}
\]
where $\mu_{ij}$ is the mean of the $i$'th treatment within the $j$'th block
and $\varepsilon_{ij}$ is due to random error. This is called the 
\emph{means model} and we assume that each of the $\varepsilon_{ij}$
are iid with $\varepsilon_{ij} \sim N(0, \sigma^2)$.

We can simplify this in a manner similar to that used in the completely random
design (however, this time requiring an assumption). 
By assuming the effect of the treatment and block on the mean are
additive, we can write $\mu_{ij} = \mu + \alpha_i + \beta_j$.
Where $\alpha_i$ is the effect of the $i$'th treatment and $\beta_j$
is the effect of the $j$'th block. Then we get that $\sum \alpha_i = \sum \beta_j = 0$
and that 
\[
    y_{ij} = \mu + \alpha_i + \beta_j + \varepsilon_{ij}
\]
This model is called the \emph{means model} and would be the basis of our investigation,
furthermore, we obtain that each of the $y_{ij}$ are independent and
\[
    y_{ij} \sim N(\mu + \alpha_i + \beta_j, \sigma^2)
\]
At this point, we can again simplify our hypotheses as 
\begin{center}
    \begin{minipage}{0.8\linewidth}
        H\textsubscript{0}: $\alpha_i = 0\ \forall i = 1\ldots k$.\\
        H\textsubscript{1}: at least one $\alpha_i \neq 0$
    \end{minipage}
\end{center}

\subsubsection{Some common notations}
Due to the large use of indices, the following summation conventions are used
\begin{itemize}
    \begin{multicols}{2}
    \item $\displaystyle y_{i.} = \sum_{j=1}^b y_{ij}$.
    \item $\displaystyle \overbar{y}_{i.} = \frac{1}{b}\sum_{j=1}^b y_{ij}$.
    \end{multicols}
    \begin{multicols}{2}
    \item $\displaystyle y_{.j} = \sum_{i=1}^a y_{ij}$.
    \item $\displaystyle \overbar{y}_{.j} = \frac{1}{a}\sum_{i=1}^a y_{ij}$.
    \end{multicols}
    \begin{multicols}{2}
    \item $\displaystyle y_{..} = \sum_{i=1}^a\sum_{j=1}^b y_{ij}$.
    \item $\displaystyle \overbar{y}_{..} = \frac{1}{ab}\sum_{i=1}^a\sum_{j=1}^b y_{ij}$.
    \end{multicols}
\end{itemize}

\subsection{Variations}
We define the following 
\begin{itemize}
    \item $\displaystyle \text{Total variation} = \SST = \sum_{i=1}^a\sum_{j=1}^b (y_{ij} - \overbar{y}_{..})^2$
    \item $\displaystyle \text{Random variation} = \SSE = \sum_{i=1}^a\sum_{j=1}^b (y_{ij} - \overbar{y}_{i.} - \overbar{y}_{.j} + \overbar{y}_{..})^2$
    \item $\displaystyle \text{Variation due to treaments} = \SSTr = b\sum_{i=1}^a (\overbar{y}_{i.} - \overbar{y}_{..})^2$
    \item $\displaystyle \text{Variation due to blocks} = \SSB = a\sum_{j=1}^b (\overbar{y}_{.j} - \overbar{y}_{..})^2$
\end{itemize}
To see the rational behind $\SSE$, notice that 
\[
    y_{ij} - \overbar{y}_{i.} - \overbar{y}_{.j} + \overbar{y}_{..}
    = (y_{ij} - \overbar{y}_{..}) - (\overbar{y}_{i.} - \overbar{y}_{..}) - (\overbar{y}_{.j}- \overbar{y}_{..})
\]

\subsubsection{Important result}
We have the following result
\[
    \SST = \SSTr + \SSB + \SSE
\]
\begin{proofi}We will prove that $\SSE = \SST - \SSTr - \SSB$.
    Now, from above
    \begin{align*}
        &\SSE = \sum_{i=1}^a \sum_{j=1}^b ((y_{ij} - \overbar{y}_{..}) - (\overbar{y}_{i.} - \overbar{y}_{..}) - (\overbar{y}_{.j}- \overbar{y}_{..}))^2\\
        &=\sum_{i=1}^a\sum_{j=1}^b (y_{ij} - \overbar{y}_{..})^2 + \sum_{i=1}^a\sum_{j=1}^b (\overbar{y}_{i.} - \overbar{y}_{..})^2 + \sum_{i=1}^a\sum_{j=1}^b (\overbar{y}_{.j} - \overbar{y}_{..})^2\\
        &\quad\quad - 2\sum_{i=1}^a\sum_{j=1}^b (y_{ij} - \overbar{y}_{..})(\overbar{y}_{i.} - \overbar{y}_{..}) - 2\sum_{i=1}^a\sum_{j=1}^b (y_{ij} - \overbar{y}_{..})(\overbar{y}_{.j} - \overbar{y}_{..})\\
        &\quad\quad + 2\sum_{i=1}^a\sum_{j=1}^b (\overbar{y}_{i.} - \overbar{y}_{..})(\overbar{y}_{.j} - \overbar{y}_{..})\\
        &=\SST + b\sum_{i=1}^a (\overbar{y}_{i.} - \overbar{y}_{..})^2 + a\sum_{j=1}^b (\overbar{y}_{.j} - \overbar{y}_{..})^2\\
        &\quad\quad - 2\sum_{i=1}^a(\overbar{y}_{i.} - \overbar{y}_{..})\sum_{j=1}^b (y_{ij} - \overbar{y}_{..}) - 2\sum_{j=1}^b(\overbar{y}_{.j} - \overbar{y}_{..})\sum_{i=1}^a (y_{ij} - \overbar{y}_{..})\\
        &\quad\quad + 2\sum_{i=1}^a(\overbar{y}_{i.} - \overbar{y}_{..})\sum_{j=1}^b (\overbar{y}_{.j} - \overbar{y}_{..})\\
        &=\SST + \SSTr + \SSB\\
        &\quad\quad - 2\sum_{i=1}^a(\overbar{y}_{i.} - \overbar{y}_{..})b(\overbar{y}_{i.} - \overbar{y}_{..}) - 2\sum_{j=1}^b(\overbar{y}_{.j} - \overbar{y}_{..})a(\overbar{y}_{.j} - \overbar{y}_{..}) + 0\\
        &=\SST + \SSTr + \SSB- 2\SSTr -2\SSB\\
        &=\SST - \SSTr - \SSB
    \end{align*}
\end{proofi}

\subsubsection{Distributions}
Given our effects model and our null hypothesis, we have the following results
\[
    \frac{\SSTr}{\sigma^2} \sim \bigchi_{(a-1)}^2
    \quad\text{and}\quad
    \frac{\SSE}{\sigma^2} \sim \bigchi_{(a-1)(b-1)}^2
\]
\begin{proofi}
    These proofs are almost identical to those done for the CRD.
\end{proofi}

\subsection{The test}
In order to determine whether each of the $\alpha_i$ are zero, we consider the distribution
of 
\[
    F 
    = \frac{\left.\frac{\SSTr}{\sigma^2}\right/(a-1)}{\left.\frac{\SSE}{\sigma^2}\right/((a-1)(b-1))}
    \sim F_{(a-1), (a-1)(b-1)}
\]
and by letting $\tfrac{\SSTr}{a-1} = \MSTr$ and $\tfrac{\SSE}{(a-1)(b-1)} = \MSE$, we get that 
\[
    F = \frac{\MSTr}{\MSE} \sim F_{(a-1), (a-1)(b-1)}
\]
We then produce the following table
\begin{center}
    \renewcommand{\arraystretch}{1.4}
    \begin{tabular}{|C{6.5em}|C{6.5em}|C{6.6em}|C{7.5em}|c|}\hline
        \bfseries Source of variation & \bfseries Sum of squares (SS) & \bfseries Degrees of freedom (df) & \bfseries Mean square (MS) & \bfseries F-statistic\\\hline
        Treatments & $\SSTr$ & $a-1$ & $\MSTr = \frac{\SSTr}{a-1}$ & $F = \frac{\MSTr}{\MSE}$\\\hline
        Blocks & $\SSB$ & $b-1$ & $\MSB = \frac{\SSB}{b-1}$ & ${\frac{\MSB}{\MSE}}^*$\\\hline
        Random variation & $\SSE$ & $(a-1)(b-1)$ & $\MSE = \frac{\SSE}{(a-1)(b-1)}$ & \\\hline
        Total & $\SST$ & $ab-1$ & &\\\hline
    \end{tabular}
\end{center}
At which point, we perform a right tailed test on the $F-statistic$. We use the following R-code to
produce the test.


\begin{lstlisting}[language=R]
    data <- read.table("data.csv", header=TRUE, sep=",")
    # 2d array with column headings representing the treatment, blocks and dependent variables
    # each row contains a pair (treatment, block, dependent)

    is.factor(data$treatment) #ensure that treatment data is a a factor
    data$treatment <- as.factor(data$treatment) #convert if necessary

    is.factor(data$block) #ensure that block data is a a factor
    data$block <- as.factor(data$block) #convert if necessary

    result <- lm(dependent ~ treatment + block, data=data)
    anova(result)
\end{lstlisting}
Note that since blocking puts a restriction an randomization, the ratio $\frac{\MSB}{\MSE}$
does not have an exact $F$ distribution, however its magnitude can be used to determine
whether or not blocking was effective.

\subsection{Model Adequacy checking}
As with the one-way ANOVA test, there were several assumptions made. The methods used to
validate these assumptions are the same as what was done with the completely random
block design.