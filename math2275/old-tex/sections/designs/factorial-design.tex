\section{Factorial Design}
A factorial design consists of $2$ or more factors of interest such that 
each combination of levels of factors is tested $n \geq 2$ times.
This model is preferred to multiple completely random designs (by holding all
but one factor of interest constant) as the interactions between factors can be more easily examined.
Therefore, the model used has many degrees of freedom and there must be
enough information in the sample (data points) satisfy the mode,
hence each combination of factors must have $n \geq 2$ replicates.

\subsection{The model}
For the case of two factors, the model used is 
\[
    y_{ijk} = \mu + \alpha_i + \beta_j + (\alpha\beta)_{ij}
    \ 
    \begin{cases}
      i=1,2,\ldots, a  \\
      j=1,2,\ldots, b  \\
      k=1,2,\ldots, n  
    \end{cases}
\]
where
\begin{itemize}
    \item $y_{ijk}$ is the observation for the $i$'th level of the first factor, the $j$'th level of the second factor and $k$'th replicate.
    \item $\mu$ is the overall mean.
    \item $\alpha_i$ is the effect of the $i$'th level of the first factor.
    \item $\beta_j$ is the effect of the $j$'th level of the second factor.
    \item $(\alpha\beta)_{ij}$ is the effect of the interaction between the $i$'th level of the first factor and the $j$'th level of the second factor.
    \item $\varepsilon_{ijk}$ is the random error associated with the $y_{ijk}$ observation.
\end{itemize}
Additionally, we require that 
\[
    \sum_{i=1}^a \alpha_i = 0 \quad\text{and}\quad\sum_{j=1}^b \beta_j = 0
\]
Similarly, the interactions must be such that 
\[
    \sum_{i=1}^a (\alpha\beta)_{ij}= 0 \quad\text{and}\quad \sum_{j=1}^b (\alpha\beta)_{ij} = 0
\]
Finally, as before, we have the following identity
\[
    \SST = \mathrm{SSA} + \mathrm{SSB} + \mathrm{SSAB} + \SSE
\]

\subsubsection{The test}
Consider the following table
\begin{center}
    \renewcommand{\arraystretch}{1.4}
    \begin{tabular}{|C{6.5em}|C{6.5em}|C{6.6em}|C{7.9em}|c|}\hline
        \bfseries Source of variation & \bfseries Sum of squares (SS) & \bfseries Degrees of freedom (df) & \bfseries Mean square (MS) & \bfseries F-statistic\\\hline
        A Treatments & $\mathrm{SSA}$       & $a-1$        & $\mathrm{MSA} = \frac{\mathrm{SSA}}{a-1}$          & $\frac{\mathrm{MSA}}{\MSE}$\\\hline
        B Treatments & $\mathrm{SSB}$       & $b-1$        & $\mathrm{MSB} = \frac{\mathrm{SSB}}{b-1}$          & $\frac{\mathrm{MSB}}{\MSE}$\\\hline
        Interaction  & $\mathrm{SSAB}$      & $(a-1)(b-1)$ & $\mathrm{MSAB} = \frac{\mathrm{SSAB}}{(a-1)(b-1)}$ & $\frac{\mathrm{MSAB}}{\MSE}$\\\hline
  Random Variation   & $\SSE$               & $ab(n-1)$    & $\MSE = \frac{\SSE}{ab(n-1)}$                   & \\\hline
        Total & $\SST$ & $abn-1$ & &\\\hline
    \end{tabular}
\end{center}
and since, there is no restriction on randomization, all the F-statistics can be tested in the usual manner.
The tests which can be performed are as follows
\begin{itemize}
    \item for the F-value for A treatments
            \begin{adjustwidth}{2em}{}
                $H_0:$ $\alpha_i = 0$ for all $i=1,2,\ldots a$.\\
                $H_1:$ at least one $\alpha_i \neq 0$.
            \end{adjustwidth}
    \item for the F-value for B treatments
            \begin{adjustwidth}{2em}{}
                $H_0:$ $\beta_j = 0$ for all $j=1,2,\ldots b$.\\
                $H_1:$ at least one $\beta_j \neq 0$.
            \end{adjustwidth}
    \item for the F-value associated with interaction of A and B treatments
            \begin{adjustwidth}{2em}{}
                $H_0:$ $(\alpha\beta)_{ij} = 0$ for all $i=1,2,\ldots a$ and $j=1,2,\ldots b$.\\
                $H_1:$ at least one $(\alpha\beta)_{ij} \neq 0$.
            \end{adjustwidth}
\end{itemize}
Note however, that the interaction should always be tested first as a significant interaction can lead to distorted
results for the tests with just one treatment.

\subsubsection{Interaction plot}
In order to assist with interpretation of the results, an interaction plot is useful. 
An interaction plot has on its $x$-axis one factor and on its $y$-axis the mean (wrt to the other factor) 
of the dependent variable.
Additionally, each curve in the plot is associated with the levels of other factor. 
We use the following command to produce the plot
\begin{lstlisting}[language=R]
    with(data, interaction.plot(x.factor=factorA, trace.factor=factorB, response=dependent, fun=mean))
\end{lstlisting}
Note that the \texttt{with} function allows us to use the attributes of data within its scope. (similar to 
with in kotlin).
