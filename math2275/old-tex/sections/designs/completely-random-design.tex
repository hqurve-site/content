\section{Completely Random Design}
A completely random design consists of $k$ \emph{treatments} each of which are
allocated $n$ random \emph{observations} of one factor. That is, given $N = nk$
possible situations, each treatment is randomly allocated $n$ of them.
This design allows us to utilize statistical theory established prior 
since the randomization ensures that there is no systematic
or patterned variation in the observations.

This design seeks to test whether all of the $k$ treatment means $\mu_i$
are the same.

\paragraph*{Note} the approach in this section uses the \emph{fixed effects model}
instead of the \emph{random effects model}. That is, the $k$ treatments are 
selected beforehand instead of choosing from a larger pool. This choice would 
limit us to only make conclusions about the selected $k$ treatments 
and not the larger pool of treatments as a whole.

\subsubsection{Approach and neccessity of Analysis of Variance (ANOVA)}
As would be emphasized later, this section seeks to produce a test for the following hypothesis
\begin{center}
    \begin{minipage}{0.8\linewidth}
        H\textsubscript{0}: $\mu_1 = \mu_2 \cdots = \mu_k$. \\
        H\textsubscript{1}: not all of the $\mu_i$ are equal
    \end{minipage}
\end{center}
The simple approach would be to simply test if each pair $(\mu_i, \mu_j)$
are equal. Suppose that each of these are conducted at a $\alpha$ level of 
significance. Then, assuming each pair is independent
\[
    P(\text{type I error})
    = 1- (1-\alpha)^{\binom{k}{2}}
\]
Notice that $(1-\alpha)^{\binom{k}{2}}$ tends to $0$ rather quickly by the nature
of $\binom{k}{2}$ and hence the probability of a type I error increase. There
are two ways to counteract this,
\begin{enumerate}[label=\roman*)]
    \item Accept the high probability of committing a type I error
    \item Reduce the probability of committing type I error for each pairwise test which in return causes 
            type II errors to be more likely.
\end{enumerate}
Both cases are undesirable. 

Instead, we use a wholistic approach to testing 
if all of the $\mu_i$ are equal. Notice that if each of the $\mu_i$ are equal,
the variation of the entire $N=nk$ observations should mirror the variation
within each of the $k$ treatments. The analysis of variance (ANOVA), does this but 
instead uses a more rigorous approach which will be discussed later.

\subsection{The models}
We will denote the $j$'th observation for the $i$'th treatment as $y_{ij}$.
Then, we can write each $y_{ij}$ from the $i$th treatment as
\[
    y_{ij} = \mu_i + \varepsilon_{ij}
\]
where $\varepsilon_{ij}$ is due to random error. This model is called
the \emph{means model}. We assume that the $\varepsilon_{ij}$
are independent and identically distributed with $\varepsilon_{ij} \sim N(0, \sigma^2)$.

Additionally, we denote $\mu$ to be the mean of the treatment means. That is
$\mu = \frac{1}{k}\sum_{i=1}^k \mu_i$. Then, we can denote $\alpha_i = \mu - \mu_i$
and rewrite $y_{ij}$ as 
\[
    y_{ij} = \mu +\alpha_i + \varepsilon_{ij}
\]
This model is called the \emph{means model} and would be the basis of our investigation.
Furthermore, we can obtain that each of the $y_{ij}$ are independent and 
\[
    y_{ij} \sim N(\mu + \alpha_i, \sigma^2)
\]
with each of the observations within one treatment being iid.

At this point, our hypotheses can be simplified as 
\begin{center}
    \begin{minipage}{0.8\linewidth}
        H\textsubscript{0}: $\alpha_i = 0\ \forall i = 1\ldots k$.\\
        H\textsubscript{1}: at least one $\alpha_i \neq 0$
    \end{minipage}
\end{center}

\paragraph*{Note} Both of these models are examples of linear models.

\subsubsection{Some common notations}
Due to the large use of indices, the following summation conventions are used
\begin{multicols}{2}
    \begin{itemize}
        \item $\displaystyle y_{i.} = \sum_{j=1}^n y_{ij}$.
        \item $\displaystyle \overbar{y}_{i.} = \frac{1}{n}\sum_{j=1}^n y_{ij}$.
        \item $\displaystyle y_{..} = \sum_{i=1}^k\sum_{j=1}^n y_{ij}$.
        \item $\displaystyle \overbar{y}_{..} = \frac{1}{N}\sum_{i=1}^k\sum_{j=1}^n y_{ij}$.
    \end{itemize}
\end{multicols}

\subsection{Variations}
We define the following 
\begin{itemize}
    \item $\displaystyle \text{Total variation} = \SST = \sum_{i=1}^k\sum_{j=1}^n (y_{ij} - \overbar{y}_{..})^2$
    \item $\displaystyle \text{Random variation} = \SSE = \sum_{i=1}^k\sum_{j=1}^n (y_{ij} - \overbar{y}_{i.})^2$
    \item $\displaystyle \text{Variation due to treaments} = \SSTr = n\sum_{i=1}^k (\overbar{y}_{i.} - \overbar{y}_{..})^2$
\end{itemize}

\subsubsection{Important result}
We have the following result
\[
    \SST = \SSTr + \SSE
\]
\begin{proofi}
    We will prove the result for each treatment then sum all of the treatments. That is,
    we first need to prove 
    \[
        \sum_{j=1}^n (y_{ij} - \overbar{y}_{..})^2 
        = n(\overbar{y}_{i.} - \overbar{y}_{..})^2 + \sum_{j=1}^n (y_{ij} - \overbar{y}_{i.})^2
    \]
    Note that this result is quite similar to that which relates MSE as the sum of variance and bias squared.
    Regardless, we will prove this result directly.
    \begin{align*}
        &\sum_{j=1}^n (y_{ij} - \overbar{y}_{..})^2 \\
        &=\sum_{j=1}^n (y_{ij} - \overbar{y}_{i.} + \overbar{y}_{i.} - \overbar{y}_{..})^2 \\
        &=\sum_{j=1}^n \brack{(y_{ij} - \overbar{y}_{i.})^2 
            + (\overbar{y}_{i.} - \overbar{y}_{..})^2 
            + 2(y_{ij} - \overbar{y}_{i.})(\overbar{y}_{i.} - \overbar{y}_{..})}\\
        &=\sum_{j=1}^n (y_{ij} - \overbar{y}_{i.})^2 
            + \sum_{j=1}^n(\overbar{y}_{i.} - \overbar{y}_{..})^2 
            + 2(\overbar{y}_{i.} - \overbar{y}_{..})\sum_{j=1}^n(y_{ij} - \overbar{y}_{i.})\\
        &=\sum_{j=1}^n (y_{ij} - \overbar{y}_{i.})^2 
            + n(\overbar{y}_{i.} - \overbar{y}_{..})^2 
            + 2(\overbar{y}_{i.} - \overbar{y}_{..})\brack{\sum_{j=1}^ny_{ij} - n\overbar{y}_{i.}}\\
        &=\sum_{j=1}^n (y_{ij} - \overbar{y}_{i.})^2 
            + n(\overbar{y}_{i.} - \overbar{y}_{..})^2 
            + 2(\overbar{y}_{i.} - \overbar{y}_{..})\brack{y_{i.} - n\overbar{y}_{i.}}\\
        &=\sum_{j=1}^n (y_{ij} - \overbar{y}_{i.})^2 
            + n(\overbar{y}_{i.} - \overbar{y}_{..})^2 
    \end{align*}
    Now, if we sum both sides of the equation for $i=1\ldots k$, we get the desired result of
    \[
        \sum_{i=1}^k\sum_{j=1}^n (y_{ij} - \overbar{y}_{..})^2
        =
            n\sum_{i=1}^k (\overbar{y}_{i.} - \overbar{y}_{..})^2 
            + \sum_{i=1}^k \sum_{j=1}^n (y_{ij} - \overbar{y}_{i.})^2 
    \]
\end{proofi}

\subsubsection{Distributions}
Given our means model and the null hypothesis, we have the following results
\[
    \frac{\SSTr}{\sigma^2} \sim \bigchi_{(k-1)}^2
    \quad\text{and}\quad
    \frac{\SSE}{\sigma^2} \sim \bigchi_{N-k}^2
\]
its important to note that $N-k = k(n-1)$ which gives a hint as to how prove the result.
\begin{proofi}
    Each result will be proven separately.

    \paragraph*{Result (i)} Now, by our model, each of the $y_{ij}$ are independent and hence,
    their treatment means are independent. Therefore, each of their treatment means are independent
    with distribution $N\paren{\mu + \alpha_i, \tfrac{\sigma^2}{n}}$. Now, under $H_0$, 
    we have $\alpha_i = 0$ and hence each of the $\overbar{y}_{i.}$ are iid with distribution
    $N\paren{\mu, \tfrac{\sigma^2}{n}}$. Therefore, the sample variance of the $\overbar{y}_{i.}$
    are distributed such that 
    \[
        \frac{(k-1) \frac{1}{k-1}\sum_{i=1}^k(\overbar{y}_{i.} - \overbar{y}_{..})^2}{\sigma^2/n}
        = \frac{n\sum_{i=1}^k(\overbar{y}_{i.} - \overbar{y}_{..})^2}{\sigma^2}
        = \frac{\SSTr}{\sigma^2} \sim \bigchi_{(k-1)}
    \]

    \paragraph*{Result (ii)} Let $s_i$ be the sample variance for the $i$'th treatment. Then
    $\tfrac{(n-1)s_i^2}{\sigma^2} \sim \bigchi_{(n-1)}$ for each treatment. Now, since
    the $y_{ij}$ are independent, their sample variances are also independent.
    Therefore,
    \[
        \sum_{i=1}^k \frac{(n-1)s_i^2}{\sigma^2} 
        = \sum_{i=1}^k \frac{\sum_{j=1}^n(y_{ij} - \overbar{y}_{i.})^2}{\sigma^2} 
        = \frac{\SSE}{\sigma^2} 
        \sim \bigchi_{k(n-1)} = \bigchi_{(N-k)}
    \]
\end{proofi}

\subsection{The test}
In order to determine whether each of the $\alpha_i$ are zero,
we consider the distribution of 
\[
    F = \frac{\left.\frac{\SSTr}{\sigma^2}\right/(k-1)}{\left.\frac{\SSE}{\sigma^2}\right/(N-k)} \sim F_{(k-1), (N-k)}
\]
and by letting $\tfrac{\SSTr}{k-1} = \MSTr$ and $\tfrac{\SSE}{N-k} = \MSE$
we get that 
\[
    F = \frac{\MSTr}{\MSE} \sim F_{(k-1), (N-k)}
\]
We then produce the following table

\begin{center}
    \renewcommand{\arraystretch}{1.4}
    \begin{tabular}{|C{6.5em}|C{6.5em}|C{6.6em}|C{6.5em}|c|}\hline
        \bfseries Source of variation & \bfseries Sum of squares (SS) & \bfseries Degrees of freedom (df) & \bfseries Mean square (MS) & \bfseries F-statistic\\\hline
        Treatments & $\SSTr$ & $k-1$ & $\MSTr = \frac{\SSTr}{k-1}$ & $F = \frac{\MSTr}{\MSE}$\\\hline
        Random variation & $\SSE$ & $N-k$ & $\MSE = \frac{\SSE}{N-k}$ & \\\hline
        Total & $\SST$ & $N-1$ & &\\\hline
    \end{tabular}
\end{center}
At which point we perform a right tailed test on the F-statistic. We use the following R code
to produce the test.

\begin{lstlisting}[language=R]
    data <- read.table("data.csv", header=TRUE, sep=",")
    # 2d array with column headings representing the independent and dependent variables
    # each row contains a pair (independent, dependent)

    is.factor(data$independent) #ensure that independent data is a a factor
    data$independent <- as.factor(data$independent) #convert if necessary

    result <- lm(dependent ~ independent, data=data)
    anova(result)
\end{lstlisting}


\subsubsection{Tukey's HSD Test}
The ANOVA test is a omnibus test, in that it only tells us whether our results are significant
overall and not exactly where the differences lie. Tukey's HSD post-hoc analysis produces
adjusted probabilities for each pair of treatments which allow us to determine which treatments
may have caused the ANOVA test to produce significant findings.

\begin{lstlisting}[language=R]
    result <- lm(dependent ~ independent, data=data)

    result2 <- aov(result)
    
    summary(result2) #produces similar information to anova(result)

    TukeyHSD(result2)
\end{lstlisting}
\paragraph*{Note} this test can only be conducted if the ANOVA test produces significant findings.

\subsection{Model Adequacy Checking}
While performing the ANOVA test, there were several assumptions made. This section
shows methods which can be used to test whether the data collected satisfies those
assumptions and hence also satisfies the model. 

\subsubsection{Normality}
The simplest method is to use a histogram as follows
\begin{lstlisting}[language=R]
    hist(result$res, main="Histogram of residuals", xlab="residuals")
\end{lstlisting}
However, this is can only be used if the sample size is large. As
an alternative, we can use a Q-Q plot to see if the data matches what would
theoretically be obtained if the data was exactly normal.
\begin{lstlisting}[language=R]
    # normality probability plot
    qqnorm(result$res)
    # 45-degree line (y=x)
    qqline(result$res)
\end{lstlisting}

\subsubsection{Independence}
This can only be validated during the collection phase.

\subsubsection{Constant variance}
We can conduct Bartlett's test with the null hypothesis begin that the treatment
variances are equal and alternative hypothesis being that at least one is different
using
\begin{lstlisting}[language=R]
    bartlett.test(dependent ~ independent, data=data)
\end{lstlisting}
Alternatively, we can use box plots, however this only works if the number of observations 
$n$ is not small
\begin{lstlisting}[language=R]
    boxplot(dependent ~ independent, xlab="independent", ylab="dependent", data=data)
\end{lstlisting}
Finally, we can use a scatter plot to plot the residuals against the fitted values.
If the data is randomly distributed, the plot should be a random cloud and should
not show any widening or shrinking.
\begin{lstlisting}[language=R]
    plot(result$fit, result$res)
\end{lstlisting}
