
diff = 0.0;
m = 10;
n = 10;

p1 = linspace(0.01, 0.99, 100);
p2 = linspace(0.01, 0.99, 100);

[P1, P2] = meshgrid(p1, p2);

Z = (diff - (P1 - P2))./sqrt(P1.*(1-P1)/n + P2.*(1-P2)/m);

mesh(P1, P2, Z)


