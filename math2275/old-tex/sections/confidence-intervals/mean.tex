\subsection{Population mean}
This subsection outlines different methods of constructing a confidence interval
for the population mean.

\subsubsection{Z-interval}
The z-interval has the following requirements
\begin{itemize}
    \item $\overbar{X}$ is normally distributed with mean $\mu$ and variance $\frac{\sigma^2}{n}$.
    \item The value of $\sigma^2$ is known.
\end{itemize}
Recall that $Z = \frac{\overbar{X} - \mu}{\sigma / \sqrt{n}} \sim N(0, 1)$.
Then, let $z_{\sfrac{\alpha}{2}}$ be the value such that $P(Z > z_{\sfrac{\alpha}{2}}) = \alpha_2$
and then since the distribution of $Z$ is symmetric about $0$
\[
    P\paren{-z_{\sfrac{\alpha}{2}} < Z < z_{\sfrac{\alpha}{2}}} = 1-\alpha
\]
Now since
\[
    -z_{\sfrac{\alpha}{2}} < Z < z_{\sfrac{\alpha}{2}} 
    \iff \overbar{x} - z_{\sfrac{\alpha}{2}}{\frac{\sigma}{\sqrt{n}}} < \mu < \overbar{x} - z_{\sfrac{\alpha}{2}}{\frac{\sigma}{\sqrt{n}}}
\]
A $100(1-\alpha)\%$ confidence interval for $\mu$ is 
\[
    \overbar{x} \pm z_{\sfrac{\alpha}{2}}{\frac{\sigma}{\sqrt{n}}}
\]

\subsubsection{T-interval}
The t-interval has the following requirements
\begin{itemize}
    \item $\overbar{X}$ is normally distributed with mean $\mu$ and variance $\frac{\sigma^2}{n}$.
    \item $\frac{(n-1)S^2}{\sigma^2}$ has a chi-squared distribution with $n-1$ degrees of freedom.
    \item $\overbar{X} \perp S^2$.
\end{itemize}
Then, since $Z= \frac{\overbar{X} - \mu}{\sigma / \sqrt{n}} \sim N(0, 1)$ and $U =\frac{(n-1)S^2}{\sigma^2} \sim \bigchi^2_{n-1}$
\[
    T 
    = \frac{Z}{\sqrt{\sfrac{U}{n-1}}} 
    = \frac{\frac{\overbar{X} - \mu}{\sigma / \sqrt{n}}}{\sqrt{\frac{\frac{(n-1)S^2}{\sigma^2}}{n-1}}}
    = \frac{\overbar{X} - \mu}{S/ \sqrt{n}} \sim T_{n-1}
\]
Now, by letting $t_{\sfrac{\alpha}{2}, n-1}$ be the value such that $P(T > t_{\sfrac{\alpha}{2}, n-1}) = \frac{\alpha}{2}$
and since the distribution of $T$ is symmetric about $0$
\[
    P\paren{-t_{\sfrac{\alpha}{2}, n-1} < T < t_{\sfrac{\alpha}{2}, n-1}} = 1-\alpha
\]
Now since
\[
    -t_{\sfrac{\alpha}{2}, n-1} < T < t_{\sfrac{\alpha}{2}, n-1} 
    \iff \overbar{x} - t_{\sfrac{\alpha}{2}, n-1}{\frac{S}{\sqrt{n}}} < \mu < \overbar{x} - t_{\sfrac{\alpha}{2}, n-1}{\frac{S}{\sqrt{n}}}
\]
A $100(1-\alpha)\%$ confidence interval for $\mu$ is 
\[
    \overbar{x} \pm t_{\sfrac{\alpha}{2}, n-1}{\frac{S}{\sqrt{n}}}
\]

\paragraph*{Guidelines}
A t-interval should be used whenever
\begin{itemize}
    \item The data is normally distributed
    \item If $n \geq 30$
\end{itemize}

\paragraph*{Terminology}
We say that 
\begin{itemize}
    \item $\overbar{x}$ is a point estimate of $\mu$.
    \item $\overbar{x} \pm t_{\sfrac{\alpha}{2}, n-1}{\frac{S}{\sqrt{n}}}$ is an interval estimate of $\mu$.
    \item $\frac{s}{\sqrt{n}}$ is the standard error of the mean.
    \item $t_{\sfrac{\alpha}{2}, n-1}{\frac{S}{\sqrt{n}}}$ is the margin of error.
\end{itemize}