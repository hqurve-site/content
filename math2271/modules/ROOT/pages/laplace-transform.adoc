= Laplace Transform

Let stem:[f:[a, b\] \to \mathbb{R}] be a function, then a
_integral transform_ is of stem:[f] is defined by

[stem]
++++
F(s) = \int_a^b k(s, t) f(t) \ dt
++++
where stem:[k] is the _kernel_ of the ransform. In this section,
we focus on the _Laplace transform_ with
stem:[k(s, t) = e^{-st}], stem:[a = 0] and
stem:[b = \infty] and we denote it by

[stem]
++++
F(s) = \mathcal{L}[f(t)] = \int_a^\infty e^{-st}f(t) \ dt
++++
which exists only if the integral converges to a finite value.


[NOTE]
====
The laplace transform is an improper integral and should really be
written as

[stem]
++++
\mathcal{L}[f(t)] = \lim_{R \to \infty} \int_0^R e^{-st} f(t) \ dt
++++
however, for brevity, we often write it as an improper integral.
====

== Linearity

The laplace transform is a linear operator. That is, if
stem:[f, g: [0, \infty) \to \mathbb{R}] have finite laplace
transforms at stem:[s] and stem:[\alpha] and
stem:[\beta] are constants,

[stem]
++++
\mathcal{L}[\alpha f + \beta g] = \alpha \mathcal{L}[f] + \beta \mathcal{L}[g]
++++
this follows immediately from the linearity of limits and integrals.

== Exponential order, piecewise continuity and convergence

A function stem:[f] is said to be of _exponential order_
stem:[a] if stem:[\exists M, t_0 \in \mathbb{R}] such that

[stem]
++++
\forall t \in \mathbb{R}: t > t_0 \implies |f(t)| < Me^{at}
++++

Note, that the value of stem:[a] is not unique. Additionally, we
simply say that stem:[f] has _exponential order_ if
stem:[\exists] such a value stem:[a].

Next, we say that stem:[f] is _piecewise continuous_ if
stem:[f] is continuous on its domain, possibly excluding a finite
number of points at which stem:[f] is finite in value.
Equivalently, we can say that stem:[f] is piecewise continuous on
its domain, if its domain can be divided into finitely many subintervals
on which it is continuous with finite left and right limits.

=== Sufficient definition for existance of laplace transforms

Let stem:[f] be a piecewise continuous function on on
stem:[[0, \infty)] and with exponential order stem:[a].
Then, the laplace transform of stem:[f],
stem:[\mathcal{L}[f\]], exists stem:[\forall s > a].

.Proof
[%collapsible]
====
IMPORTANT: For the concept of absolutely integrable, consult the
https://en.wikipedia.org/wiki/Absolute_convergence#Absolute_convergence_of_integrals[wiki page].
I am unsure how to properly do it, however, one concept would be to 
integrate through the intervals stem:[[n,n+1\]] and form a series which converges
absolutely.

Notice that since stem:[f] is piecewise continuous, it is integrable and hence,
its product with stem:[e^-{st}] is also integrable. Next, notice that
[stem]
++++
\int_0^\infty |f(t)e^{-st}| \ dt 
\leq \int_0^\infty Me^{at}e^{-st} \ dt
\leq M \int_0^\infty e^{(a-s)t} \ dt
++++
which converges if stem:[a-s < 0 \iff s > a]. Therefore, stem:[f(t)e^{-st}]
is absolutely integrable on stem:[[0, \infty)] and hence stem:[\mathcal{L}[f\]]
exists.

====

NOTE: This is condition is only sufficient and not necessary. For example, the
function stem:[t \mapsto \frac{1}{\sqrt{t}}] is neither piecewise
continuous, not has exponential order but
stem:[\mathcal{L}[\tfrac{1}{\sqrt{t}}\] = \frac{\sqrt{\pi}}{\sqrt{s}}].
 [xref:laplace-transform-proofs.adoc#_reciprocal_square_root[see here]]

== Popular functions

Below is a table containing popular laplace transforms.
See xref:laplace-transform-proofs.adoc[here] for proofs.

[cols="3*^.^",options=header]
|===
| stem:[f(t)] | stem:[\mathcal{L}[f(t)\]] | Validity

| stem:[1] | stem:[\frac{1}{s}] | stem:[s > 0]
| stem:[t^n] | stem:[\frac{n!}{s^{n+1}} = \frac{\Gamma(n+1)}{s^{n+1}}] | stem:[s > 0, n > 0]
| stem:[e^{at}] | stem:[\frac{1}{s-a}] | stem:[s > \mathrm{Re}(a)]
| stem:[\sin(kt)] | stem:[\frac{k}{s^2 + k^2}] | stem:[s > 0] 
| stem:[\cos(kt)] | stem:[\frac{s}{s^2 + k^2}] | stem:[s > 0 ]
| stem:[\sinh(kt)] | stem:[\frac{k}{s^2 - k^2}] | stem:[s > \|k\|]
| stem:[\cosh(kt)] | stem:[\frac{s}{s^2 - k^2}] | stem:[s > \|k\|]
| stem:[y(t)] | stem:[\mathcal{L}[y(t)\] = Y(t)] | stem:[s \in I]
| stem:[e^{at}y(t)] | stem:[Y(s-a)] | stem:[s -a \in I \iff s \in I + a]
| stem:[H(t - t_0)y(t-t_0)] | stem:[e^{-st_0}Y(s)] | stem:[t_0 \geq 0, s \in I]
| stem:[\delta(t-t_0)] | stem:[e^{-st_{0}}] |
| stem:[y'(t)] | stem:[sY(s) - y(0)] | stem:[s \in I]
| stem:[y''(t)] | stem:[s^2Y(s) - sy(0) - y'(0)] | stem:[s \in I]
|===
where

* stem:[H(t - t_0)] is the _Heaviside step function_ defined by
+
[stem]
++++
H(t-t_0) = \begin{cases}
                0, \quad & t < t_0\\
                1, \quad & t \geq t_0
            \end{cases}
++++
* stem:[\delta(t - t_0)] is the _Dirac delta function_ which is
defined by
+
[stem]
++++
\int_{-\infty}^\infty f(t)\delta(t - t_0) \ dt = f(t_0)
++++
or in a less formal way as
+
[stem]
++++
\delta(t-t_0) = \begin{cases}
                \infty, \quad & t = t_0\\
                0, \quad & \text{otherwise}
            \end{cases}
++++
