= Second-Order Linear Differential Equations

stem:[\def\bm#1{{\bf #1}}]

In general a second-order linear ODE can be written as

[stem]
++++
\numberandlabel{2ol:gen}
    \frac{d^2y}{dx^2} + p(x)\frac{dy}{dx} + q(x)y = g(x)
++++
Also, recall that a second-order linear ODE is said to be homogenous if
stem:[g = \theta] (the zero function), that is

[stem]
++++
\numberandlabel{2ol:gen_hom}
    \frac{d^2y}{dx^2} + p(x)\frac{dy}{dx} + q(x)y = 0
++++

NOTE: Most of the methods here work for higher order linear ODEs.

== Fundamental Solutions

Fundamental solutions utilize the principle of superposition in order to
generate solutions for a homogeneous ode. Then, since we are dealing
with a second-order ode, we only need two two linearly independent
solutions of stem:[\eqref{2ol:gen_hom}]. Hence, if

* stem:[y_1] and stem:[y_2] are solutions to stem:[\eqref{2ol:gen_hom}].
* stem:[W(y_1, y_2) \neq 0]

then our general solution for the ODE is stem:[y = ay_1 + by_2].
Also, note that for our region of validity we will exclude value of
stem:[x] which cause stem:[W(y_1, y_2) = 0].

== Reduction of order

If we know a solution stem:[y_1] to stem:[\eqref{2ol:gen_hom}]
then we can generate another solution stem:[vy_1] by solving for
the function stem:[v] in stem:[\eqref{2ol:gen_hom}]. We will
analyze the substitution term by term and will utilize the Leibniz rule
for differentiation. Then

[stem]
++++
\frac{d^n(vy_1)}{dx^n} = v \frac{d^ny_1}{dx^n} + \text{terms in }v', v'', \ldots v^{(n)}
++++
Note that the above statement even holds when stem:[n=0]. Then,
since stem:[y_1] is a solution of stem:[\eqref{2ol:gen_hom}] and
by adding all the differential terms, we get an equation only including
terms in stem:[v', v'' \ldots v^{(n)}]. We can then use the
substitution stem:[w = v'] and solve a differential equation of
lower order (first-order in this case).

=== Read more

https://math.stackexchange.com/questions/235855/why-does-reduction-of-order-work-for-linear-odes

== Constant Coefficients

If our homogenous second-order linear ODE has has constant coefficients,
that is

[stem]
++++
a\frac{d^2y}{dx^2} + b\frac{dy}{dx} + cy =0
++++
then one of our solutions will be in the form stem:[y=e^{mx}]
where stem:[m] is a constant. By substituting, we get that

[stem]
++++
am^2e^{mx} + bme^{mx} + e^{mx} = e^{mx}(am^2 + bm + c) = 0
++++
Since stem:[e^mx \neq 0], finding a solution to the ODE reduces
to solving a quadratic equation. Note though that we have three cases to
consider

* Two distinct real roots. In this case, our solutions are already
independent, so stem:[y = Ae^{m_1x} + Be^{m_2x}].
* Two complex roots. Since stem:[a, b, c\in \mathbb{R}], the
roots are a conjugate pair. So let stem:[m = \lambda \pm \mu i].
Then, again the solutions are linearly independent but we have complex
numbers (eww). So by applying Euler’s formula for
stem:[e^{\theta i}] and simplifying, we get that our solution is
stem:[y=e^{\lambda x}(A\sin\mu x + B\cos \mu x)].
* One real root. In this case stem:[m = \frac{-b}{2a}], we will
have to use reduction of order. So by letting stem:[y = ve^{mx}].
and substituting, we get that
+
[stem]
++++
av'' + 2amv' + bv' = av'' + (2am+b)v' = 0
++++
Because of the value of stem:[m], the coefficient of
stem:[v'] is stem:[0] and we get that
stem:[v = Ax + B] (affine). Therefore, our general solution is
stem:[y=e^{mx}(Ax + B)].

=== Method of undetermined coefficients

If stem:[g] can be represented as the sum and/or product of

* polynomials
* exponential functions including sine and cosine by Euler’s formula

then we can construct a particular solution using the method of
undetermined coefficients. Our particular solution would have a similar
'form' to that of stem:[g]. The following table outlines some of
the forms

[cols="^,^", options="header"]
|===
|stem:[\bm{g(x)}] |stem:[\bm{y_p(x)}]
|stem:[x^n]
|stem:[a_nx^n + a_{n-1}x^{n-1} + \cdots + a_1x + a_0]

|stem:[e^{\alpha x}] |stem:[ke^{\alpha x}]

|stem:[\sin (\alpha x)] or stem:[\cos (\alpha x)]
|stem:[a_1 \sin (\alpha x) + a_2 \cos (\alpha x)]

|stem:[x^n e^{\alpha x}]
|stem:[(a_n x^n + \cdots + a_0)e^{\alpha x}]

|stem:[e^{\beta x}\sin (\alpha x)] or
stem:[e^{\beta x}\cos (\alpha x)]
|stem:[a_1 e^{\beta x} \sin (\alpha x) + a_2 e^{\beta x} \cos (\alpha x)]

|stem:[\cdots] |stem:[\cdots]
|===

Additionally, if the designated stem:[y_p] already occurs as part
of the homogeneous solution, then successively multiply
stem:[y_p] by stem:[x] until it no longer does.

==== Note:

I think this method arises due to the Laplace transform however at the
time of writing, I know very little about the aforementioned
transformation.

== Variation of Parameters

If stem:[y_1] and stem:[y_2] are solutions to
stem:[\eqref{2ol:gen_hom}] then we can generate the general solution to
stem:[\eqref{2ol:gen}] by letting

[stem]
++++
y(x) = u_1(x) y_1(x) + u_2(x) y_2(x)
++++
Then by imposing the restriction of stem:[u_1'y_1 + u_2'y_2 = 0],
we get that

[stem]
++++
\begin{aligned}
        u_1'y_1 + u_2'y_2 &= 0\\
        u_1'y_1' + u_2'y_2' &= g(x)
    \end{aligned}
    \iff
    \begin{bmatrix}
        y_1 & y_2\\
        y_1'& y_2'
    \end{bmatrix}
    \begin{bmatrix}
        u_1'\\
        u_2'
    \end{bmatrix}
    =
    \begin{bmatrix}
        0\\
        g(x)
    \end{bmatrix}
++++
we can then apply Cramer’s rule and integrating to get that

[stem]
++++
u_1(x) = \int \frac{\begin{vmatrix} 0 & y_2 \\ g & y_2' \end{vmatrix}}{W(y_1, y_2)} dx = \int \frac{-y_2(x)g(x)}{W(y_1, y_2)} dx
++++

[stem]
++++
u_2(x) = \int \frac{\begin{vmatrix} y_1 & 0 \\ y_1' & g \end{vmatrix}}{W(y_1, y_2)} dx = \int \frac{y_1(x)g(x)}{W(y_1, y_2)} dx
++++
Note that this method can be extended to any stem:[n]’th order
linear ODE by using the restrictions of
stem:[u_1^{(r)}y_1 + u_2^{(r)}y_2 = 0] for
stem:[r \in {1, 2, \cdots (n-2)}].

=== Read more

https://en.wikipedia.org/wiki/Variation_of_parameters#Description_of_method.

