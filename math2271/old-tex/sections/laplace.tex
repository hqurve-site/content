\section{Laplace Transform}
Let $f:[a, b] \to \mathbb{R}$ be a function, then a \emph{integral transform}
is of $f$ is defined by
\[
    F(s) = \int_a^b k(s, t) f(t) \ dt
\]
where $k$ is the \emph{kernel} of the ransform. In this section,
we focus on the \emph{Laplace transform} with $k(s, t) = e^{-st}$, $a = 0$
and $b = \infty$ and we denote it by 
\[
    F(s) = \mathcal{L}[f(t)] = \int_a^\infty e^{-st}f(t) \ dt
\]
which exists only if the integral converges to a finite value. 
% The set of values $I$ for which $s$ can take is called the domain of validity.

\paragraph*{Note: }The laplace transform is an improper integral and should really
be written as 
\[
    \mathcal{L}[f(t)] = \lim_{R \to \infty} \int_0^R e^{-st} f(t) \ dt
\]
however, for brevity, we often write it as an improper integral.

\subsubsection{Linearity}
The laplace transform is a linear operator. That is, if
$f, g: [0, \infty) \to \mathbb{R}$ have finite laplace transforms at $s$
and $\alpha$ and $\beta$ are constants,
\[
    \mathcal{L}[\alpha f + \beta g] = \alpha \mathcal{L}[f] + \beta \mathcal{L}[g]
\]
this follows immediately from the linearity of limits and integrals.

\subsection{Exponential order, piecewise continuity and convergence}
A function $f$ is said to be of \emph{exponential order} $a$ if
$\exists M, t_0 \in \mathbb{R}$ such that 
\[
    \forall t \in \mathbb{R}: t > t_0 \implies |f(t)| < Me^{at} 
\]
Note, that the value of $a$ is not unique. Additionally, 
we simply say that $f$ has \emph{exponential order} if $\exists$ such a value $a$.

Next, we say that $f$ is \emph{piecewise continuous} if $f$
is continuous on its domain, possibly excluding a finite number of points
at which $f$ is finite in value. Equivalently, we can say
that $f$ is piecewise continuous on its domain, if its domain 
can be divided into finitely many subintervals on which it is continuous
with finite left and right limits.

\subsubsection{Sufficient definition for existance of laplace transforms}
Let $f$ be a piecewise continuous function on on $[0, \infty)$ and with exponential
order $a$. Then, the laplace transform of $f$, $\mathcal{L}[f]$, exists $\forall s > a$.\proofref{13}

\paragraph*{Note:} This is condition is only sufficient and not necessary. For example,
the function $t \mapsto \frac{1}{\sqrt{t}}$ is neither piecewise continuous,
not has exponential order but $\mathcal{L}[\tfrac{1}{\sqrt{t}}] = \frac{\sqrt{\pi}}{\sqrt{s}}$.\proofref{14}


\subsection{Popular functions}
Below is a table containing popular laplace transforms
\begin{equation*}
    \renewcommand\arraystretch{1.5}
    \begin{array}{|c|c|c|c|}\hline
        \bm{f(t)} & \bm{\mathcal{L}[f(t)]} & \text{\bfseries Validity} & \text{\bfseries Proof location}\\\hline\hline
        1 & \frac{1}{s} & s > 0 & \multirow{2}{*}{\proofinline{16}} \\\cline{1-3}
        t^n & \frac{n!}{s^{n+1}} = \frac{\Gamma(n+1)}{s^{n+1}} & s >0, n > 0 &\\\hline
        e^{at} & \frac{1}{s-a} & s > \Re(a) &  \text{\proofinline{17}} \\\hline
        \sin(kt) & \frac{k}{s^2 + k^2} & s > 0 & \multirow{2}{*}{\proofinline{18}} \\\cline{1-3}
        \cos(kt) & \frac{s}{s^2 + k^2} & s > 0 & \\\hline
        \sinh(kt) & \frac{k}{s^2 - k^2} & s > |k| & \multirow{2}{*}{\proofinline{19}}\\\cline{1-3}
        \cosh(kt) & \frac{s}{s^2 - k^2} & s > |k| & \\\hline
        y(t) & \mathcal{L}[y(t)] = Y(t) & s \in I & \text{for notation}\\\hline
        e^{at}y(t) & Y(s-a) & s -a \in I \iff s \in I + a & \text{\proofinline{20}}\\\hline
            H(t - t_0)y(t-t_0) & e^{-st_0}Y(s) & t_0 \geq 0, s \in I & \text{\proofinline{21}}\\\hline
            \delta(t-t_0) & e^{-st_{0}} & & \text{\proofinline{22}}\\\hline
            y'(t) & sY(s) - y(0) & s \in I & \multirow{2}{*}{\proofinline{23}}\\\cline{1-3}
                y''(t) & s^2Y(s) - sy(0) - y'(0) & s \in I & \\\hline
    \end{array}
\end{equation*}
where 
\begin{itemize}
    \item $H(t - t_0)$ is the \emph{Heaviside step function} defined by 
        \[
            H(t-t_0) = \begin{cases}
                0, \quad & t < t_0\\
                1, \quad & t \geq t_0
            \end{cases}
        \]
    \item $\delta(t - t_0)$ is the \emph{Dirac delta function} which is defined by 
        \[
            \int_{-\infty}^\infty f(t)\delta(t - t_0) \ dt = f(t_0)
        \]
        or in a less formal way as
        \[
            \delta(t-t_0) = \begin{cases}
                \infty, \quad & t = t_0\\
                0, \quad & \text{otherwise}
            \end{cases}
        \]
\end{itemize}
