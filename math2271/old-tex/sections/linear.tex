\section{Linear Differential Equations}
In general, a linear ODE can be written as 
\begin{equation}
    \label{lin:gen}
    a_n(x) y^{(n)} + a_{n-1}(x) y^{(n-1)} + \cdots + a_1(x)y' + a_0(x)y = g(x)
\end{equation}
where $a_n \neq \theta$ (the zero function) and as a result, the
above general linear ODEs may also be written as 
\[
    y^{(n)} + a_{n-1}(x) y^{(n-1)} + \cdots + a_1(x)y' + a_0(x)y = g(x)
\]
Furthermore, a linear ODE is said to be homogenous if $g = \theta$ (the zero function), that is
\begin{equation}
    \label{lin:gen_hom}
    a_n(x) y^{(n)} + a_{n-1}(x) y^{(n-1)} + \cdots + a_1(x)y' + a_0(x)y = 0
\end{equation}

\subsection{Existence and Uniqueness of solutions}
Consider \eqref{lin:gen} where 
$a_0, \ldots, a_n$ as well as $g$ are continuous in the interval $I=\{a < x < b\}$.
Then, there exists a unique function $y(x)$ satisfying \eqref{lin:gen} in $I$
as well as the following IVP
\[
    y(x_0) = y_0, \; y'(x_0) = y_1, \;\cdots\; , y^{(n-1)}(x_0)=y_{n-1}
\]

\subsection{General Approach}
In order to find the general solution for the ODE \eqref{lin:gen}, the following steps are taken
\begin{enumerate}
    \item Find the \emph{complementary solution} $y_c$ which is the 
            general solution for \eqref{lin:gen_hom}.
    \item Find a \emph{particular solution} $y_p$ which is
            one solution for \eqref{lin:gen}.
\end{enumerate}
Then, the general solution for \eqref{lin:gen} would be given by
\[
    y = y_h + y_p
\]
In order to see why this method is used, consider two solutions $y_1$ and $y_2$ of \eqref{lin:gen}
then
\[
    y_1^{(n)} + a_{n-1}(x) y_1^{(n-1)} + \cdots + a_1(x)y_1' + a_0(x)y_1 = g(x)
\]
and
\[
    y_2^{(n)} + a_{n-1}(x) y_2^{(n-1)} + \cdots + a_1(x)y_2' + a_0(x)y_2 = g(x)
\]
Then, by taking the difference, we get that 
\[
    (y_2-y_1)^{(n)} + a_{n-1}(x) (y_2-y_1)^{(n-1)} + \cdots + a_1(x)(y_2-y_1)' + a_0(x)(y_2-y_1) = 0
\]
since the differential is a linear operator. Therefore, the difference between any 2 solutions
of \eqref{lin:gen} is a solution to \eqref{lin:gen_hom} and hence all solutions can be generated
using the set of solutions to \eqref{lin:gen_hom} and any one solution of \eqref{lin:gen}.

\subsubsection{Principle of Superposition}
Notice that since the differential is a linear operator, if $y_1$
and $y_2$ are solutions to \eqref{lin:gen_hom}, then their linear combination is 
also a solution. Therefore, in general if \eqref{lin:gen_hom} has solutions $y_1, \ldots y_k$
\[
    y = C_1 y_1 + \cdots C_k y_k
\]
is also a solution.

\subsubsection{Finding the complementary solution}
By using the Principle of Superposition, it is clear that
finding the complementary solution gets reduced to finding a set of linearly independent
solutions for \eqref{lin:gen_hom}. Now, since the number of arbitrary constants of
and ODE is equal to its degree, the dimension of set of solutions is $n$. Therefore,
there are at most $n$ linearly independent solutions for \eqref{lin:gen_hom}; let
these solutions be $f_1, f_2, \ldots f_n$. Then, the complementary solution is 
given by 
\[
    y_c
    = C_1 f_1 + C_2 f_2 + \cdots + C_n f_n
\]
where $C_1, \ldots C_n$ are arbitrary constants.

\subsubsection{Wronskian and linear independence}
The Wronskian of the functions $f_1, \ldots f_n$ is defined as
\[
    W(f_1, \ldots f_n)
    = \begin{vmatrix}
        f_1 & f_2 &\cdots &f_n\\
        f_1' & f_2' &\cdots &f_n'\\
        \vdots & \vdots & \ddots & \vdots\\
        f_1^{(n-1)} & f_2^{(n-1)} &\cdots &f_n^{(n-1)}\\
    \end{vmatrix}
\]
Then, if the functions are linearly dependent, so are the columns 
(since the differential is linear) and hence the Wronskian
vanishes. Therefore, by taking the contrapositive
\[
    W(f_1, \ldots f_n) \neq 0
    \implies 
    \{f_1, \ldots f_n\} \text{ are linearly independent}
\]
Notice though that this is not a two way implication
as there exists sets of functions which are linearly independent,
yet the Wronskian is equal to $0$. A notable example is 
$\{x^2, x|x|\}$ which was pointed out by Peano. Therefore, using the Wronskian
to test linear independence may produce false negatives (but never produces false
positives).