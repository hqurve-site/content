\section{Second-Order Linear Differential Equations}
In general a second-order linear ODE can be written as
\begin{equation}
    \label{2ol:gen}
    \frac{d^2y}{dx^2} + p(x)\frac{dy}{dx} + q(x)y = g(x)
\end{equation}
Also, recall that a second-order linear ODE is said to be homogenous if $g = \theta$ (the zero function), that is
\begin{equation}
    \label{2ol:gen_hom}
    \frac{d^2y}{dx^2} + p(x)\frac{dy}{dx} + q(x)y = 0
\end{equation}
\paragraph*{Note}
Most of the methods here work for higher order linear ODEs.


\subsection{Fundamental Solutions}
Fundamental solutions utilize the principle of superposition
in order to generate solutions for a homogeneous ode. Then, since
we are dealing with a second-order ode, we only need two two linearly
independent solutions of \eqref{2ol:gen_hom}. Hence, if
\begin{itemize}
    \item $y_1$ and $y_2$ are solutions to \eqref{2ol:gen_hom}.
    \item $W(y_1, y_2) \neq 0$
\end{itemize}
then our general solution for the ODE is $y = ay_1 + by_2$.
Also, note that for our region of validity we will exclude value of $x$
which cause $W(y_1, y_2) = 0$.

\subsection{Reduction of order}
If we know a solution $y_1$ to \eqref{2ol:gen_hom} then we can generate
another solution $vy_1$ by solving for the function $v$ in \eqref{2ol:gen_hom}.
We will analyze the substitution term by term and will utilize the Leibniz rule for differentiation.
Then
\[
    \frac{d^n(vy_1)}{dx^n} = v \frac{d^ny_1}{dx^n} + \text{terms in }v', v'', \ldots v^{(n)}
\]
Note that the above statement even holds when $n=0$. Then, since $y_1$ is a solution of \eqref{2ol:gen_hom}
and by adding all the differential terms, we get an equation only including terms in $v', v'' \ldots v^{(n)}$.
We can then use the substitution $w = v'$ and solve a differential equation of lower order (first-order in this case).

\paragraph*{Read more} \url{https://math.stackexchange.com/questions/235855/why-does-reduction-of-order-work-for-linear-odes}

\subsection{Constant Coefficients}
If our homogenous second-order linear ODE has has constant coefficients, that is
\[
    a\frac{d^2y}{dx^2} + b\frac{dy}{dx} + cy =0
\]
then one of our solutions will be in the form $y=e^{mx}$
where $m$ is a constant. By substituting, we get that 
\[
    am^2e^{mx} + bme^{mx} + e^{mx} = e^{mx}(am^2 + bm + c) = 0
\]
Since $e^mx \neq 0$, finding a solution to the ODE reduces to solving a 
quadratic equation. Note though that we have three cases to consider
\begin{itemize}
    \item Two distinct real roots. In this case, our solutions are already independent, so $y = Ae^{m_1x} + Be^{m_2x}$.
    \item Two complex roots. Since $a, b, c\in \mathbb{R}$, the roots are a conjugate pair. So let $m = \lambda \pm \mu i$.
            Then, again the solutions are linearly independent but we have complex numbers (eww). So by applying Euler's
            formula for $e^{\theta i}$ and simplifying, we get that our solution is $y=e^{\lambda x}(A\sin\mu x + B\cos \mu x)$.
    \item One real root. In this case $m = \frac{-b}{2a}$, we will have to use reduction of order. So by letting $y = ve^{mx}$.
        and substituting, we get that 
        \[
            av'' + 2amv' + bv' = av'' + (2am+b)v' = 0
        \]
        Because of the value of $m$, the coefficient of $v'$ is $0$ and we get that $v = Ax + B$ (affine). Therefore, our general
        solution is $y=e^{mx}(Ax + B)$.
\end{itemize}


\subsubsection{Method of undetermined coefficients}
If $g$ can be represented as the sum and/or product of 
\begin{itemize}
    \item polynomials
    \item exponential functions including sine and cosine by Euler's formula
\end{itemize}
then we can construct a particular solution using the method of undetermined coefficients.
Our particular solution would have a similar `form' to that of $g$. The following table outlines
some of the forms
\begin{center}
    \renewcommand{\arraystretch}{1.3}
    \begin{tabular}{|c|c|}
        \hline
        $\bm{g(x)}$ & $\bm{y_p(x)}$\\\hline
        $x^n$ & $a_nx^n + a_{n-1}x^{n-1} + \cdots + a_1x + a_0$\\\hline
        $e^{\alpha x}$ & $ke^{\alpha x}$\\\hline
        $\sin (\alpha x)$ or $\cos (\alpha x)$ & $a_1 \sin (\alpha x) + a_2 \cos (\alpha x)$\\\hline
        $x^n e^{\alpha x}$ & $(a_n x^n + \cdots + a_0)e^{\alpha x}$\\\hline
        $e^{\beta x}\sin (\alpha x)$ or $e^{\beta x}\cos (\alpha x)$ 
                & $a_1 e^{\beta x} \sin (\alpha x) + a_2 e^{\beta x} \cos (\alpha x)$\\\hline
        $\cdots$ & $\cdots$\\\hline
    \end{tabular}
\end{center}
Additionally, if the designated $y_p$ already occurs as part of the homogeneous solution, then
successively multiply $y_p$ by $x$ until it no longer does.
\paragraph*{Note:} I think this method arises due to the Laplace transform however
at the time of writing, I know very little about the aforementioned transformation.


\subsection{Variation of Parameters}
If $y_1$ and $y_2$ are solutions to \eqref{2ol:gen_hom} then we can generate 
the general solution to \eqref{2ol:gen} by letting
\[
    y(x) = u_1(x) y_1(x) + u_2(x) y_2(x)
\]
Then by imposing the restriction of $u_1'y_1 + u_2'y_2 = 0$,
we get that 
\[
    \begin{aligned}
        u_1'y_1 + u_2'y_2 &= 0\\
        u_1'y_1' + u_2'y_2' &= g(x)
    \end{aligned}
    \iff
    \begin{bmatrix}
        y_1 & y_2\\
        y_1'& y_2'
    \end{bmatrix}
    \begin{bmatrix}
        u_1'\\
        u_2'
    \end{bmatrix}
    =
    \begin{bmatrix}
        0\\
        g(x)
    \end{bmatrix}
\]
we can then apply Cramer's rule and integrating to get that 
\[
    u_1(x) = \int \frac{\begin{vmatrix} 0 & y_2 \\ g & y_2' \end{vmatrix}}{W(y_1, y_2)} dx = \int \frac{-y_2(x)g(x)}{W(y_1, y_2)} dx
\]
\[
    u_2(x) = \int \frac{\begin{vmatrix} y_1 & 0 \\ y_1' & g \end{vmatrix}}{W(y_1, y_2)} dx = \int \frac{y_1(x)g(x)}{W(y_1, y_2)} dx
\]
Note that this method can be extended to any $n$'th order linear ODE by using the restrictions of $u_1^{(r)}y_1 + u_2^{(r)}y_2 = 0$
for $r \in {1, 2, \cdots (n-2)}$.

\paragraph*{Read more} \url{https://en.wikipedia.org/wiki/Variation_of_parameters#Description_of_method}.