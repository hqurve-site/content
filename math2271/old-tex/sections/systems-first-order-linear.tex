\section{Systems of First-Order Linear ODEs}
Let $t$ be an independent variable and $x_1, \ldots x_n$
be functions of $t$.
Then, an $n$'th order system of first-order linear ODEs can be written as
\begin{align*}
    x_1'(t) &= c_{11} x_1(t) + c_{12} x_2(t) + \cdots + c_{1n} x_n(t) + g_1(t)\\
    x_2'(t) &= c_{21} x_1(t) + c_{22} x_2(t) + \cdots + c_{2n} x_n(t) + g_2(t)\\
    &\vdots \\
    x_n'(t) &= c_{n1} x_1(t) + c_{n2} x_2(t) + \cdots + c_{nn} x_n(t) + g_n(t)
\end{align*}
where the $g_i$ and $c_{ij}$ are functions of $t$. This system of equations
can also be written in a more compact manner as
\begin{equation}\label{sfol:gen}
    \vec{x}'(t) = \mat{A}(t)\vec{x}(t) + \vec{g}(t)
    \quad \text{where }
    \vec{x} = \begin{bmatrix}
        x_1 \\ x_2 \\ \vdots \\ x_n
    \end{bmatrix}
    ,\ 
    \vec{g} = \begin{bmatrix}
        g_1 \\ g_2 \\ \vdots \\ g_n
    \end{bmatrix}
    \text{ and }
    \mat{A} = \begin{bmatrix}
        c_{11} & c_{12} & \cdots & c_{1n}\\
        c_{21} & c_{22} & \cdots & c_{2n}\\
        \vdots & \vdots & \ddots & \vdots\\
        c_{n1} & c_{n2} & \cdots & c_{nn}
    \end{bmatrix}
\end{equation}
And as usual, \eqref{sfol:gen} is called homogeneous if $\vec{g} = \vec{\theta}$ (the vector of zero functions)
and can hence be written as 
\begin{equation}\label{sfol:gen_hom}
    \vec{x}'(t) = \mat{A}(t)\vec{x}(t)
\end{equation}

\subsubsection{Note on notation}
If we want to discriminate between two solutions $\vec{x}$, we place an identifier in parentheses
as a superscript. For example
\begin{multicols}{4}
    \begin{itemize}
        \item $\vec{x}^{(1)}$
        \item $\vec{x}^{(2)}$
        \item $\vec{x}^{(n)}$
        \item $\vec{x}^{(k)}$
    \end{itemize}
\end{multicols}
\paragraph*{Important} THIS IS NOT REPEATED DIFFERENTIATION! That is $\vec{x}^{(1)} \neq \vec{x}'$
\paragraph*{Speculation} I believe that this is to be able to discriminate between 
\begin{itemize}
    \item Two different solutions
    \item Entries in vectors and matrices
\end{itemize}
since entries in vectors and matrices will use subscripts. Furthermore, it will allow
us to easily write for instance $\vec{x}^{(1)}_1$ and $\vec{x}^{(2)}_3$.

\subsubsection{Appology}
I am sorry that this section divulged into the realm of linear algebra. I really got distracted by the
idea of generalized eigenvectors. The equations I came up with, however, are quite interesting
and I don't think it would have been just to not clutter this section with it.

\subsection{General Approach}
The general approach for solving \eqref{sfol:gen} is quite similar to that employed
for solving linear-differential equations. That is, the following steps are taken
\begin{enumerate}
    \item Find the \emph{complementary solution} $\vec{x}^{(c)}$ which is the general solution to \eqref{sfol:gen_hom}
    \item Find a \emph{particular solution} $\vec{x}^{(p)}$ which is a solution to \eqref{sfol:gen}
\end{enumerate}
Then, the general solution to \eqref{sfol:gen} is given by 
\[
    \vec{x}(t) = \vec{x}^{(c)}(t) + \vec{x}^{(p)}(t)
\]
The rational behind this method is the same as that which was employed for solving linear-differential equations
\paragraph*{Note: } I do not know if ``complementary solution'' and ``particular solution'' are the correct terminology.

\subsubsection{Principle of superposition}
Notice that since
\begin{itemize}
    \item the differential is a linear operator
    \item matrix multiplication distributes over addition
\end{itemize}
if $\vec{x}^{(1)}$ and $\vec{x}^{(2)}$ are solutions to \eqref{sfol:gen_hom}, then
any of their linear combinations are also a solution. Therefore, in general if \eqref{sfol:gen_hom}
has solutions $\vec{x}^{(1)}, \ldots, \vec{x}^{(k)}$ then
\[
    \vec{x} = C_1\vec{x}^{(1)} + \cdots + C_k\vec{x}^{(k)}
\]
is also a solution where $C_1, \ldots C_n$ are arbitrary constants.

\subsection{Vector space of homogenous solutions}
Firstly, note that the set of all vector valued functions,
form a vector space over field $\mathbb{R}$ with infinite dimension.
Then, by the principle of superposition,
each linear combination of solutions to \eqref{sfol:gen_hom}
is also a solution to \eqref{sfol:gen_hom} and hence this
set of solutions forms a subspace of the vector space of all vector valued
functions.

Then, we claim that vector space of homogeneous solutions has finite 
dimension $n$. Then, we can find $n$ vector valued functions $\vec{x}^{(1)}\ldots \vec{x}^{(n)}$
\begin{itemize}
    \item all satisfy \eqref{sfol:gen_hom}
    \item are linearly independent
    \item span the set of solutions
\end{itemize}
In other words, these $n$ vectors form a basis for the set of solutions.
We call such a set of vectors a \emph{set of fundamental solutions}.
Note that since the vector space has dimension $n$, we only need
these $n$ to be linearly independent or span the set of solutions for them
to form a basis.

Furthermore, we can also form the \emph{fundamental matrix} $\mat{\Psi}$ whose columns 
are consists of a set of fundamental solutions. That is
\[
    \mat{\Psi}
    =\begin{bmatrix}
        \vec{x}^{(1)} & \vec{x}^{(2)} & \cdots & \vec{x}^{(n)}
    \end{bmatrix}
    =\begin{bmatrix}
        x_1^{(1)} & x_1^{(2)} & \cdots & x_1^{(n)}\\
        x_2^{(1)} & x_2^{(2)} & \cdots & x_2^{(n)}\\
        \vdots    & \vdots    & \ddots & \vdots \\
        x_n^{(1)} & x_n^{(2)} & \ldots & x_n^{(n)}
    \end{bmatrix}
\]
Additionally, since the columns of $\mat{\Psi}$ form a basis, any solution to \eqref{sfol:gen_hom}
can be written as
\[
    \vec{x}(t) = \mat{\Psi}(t) \vec{c}
\]
where $\vec{c} \in \mathbb{R}^{n}$ is a constant vector. Also,
I would like to emphasize that both $\vec{x}$ and $\mat{\Psi}$ functions of $t$ (the independent variable).

\subsubsection{Linear independence}
Let $\vec{x}^{(1)}, \ldots, \vec{x}^{(n)}$ be solutions to \eqref{sfol:gen_hom},
then
\[
    \{
        \vec{x}^{(1)}, \ldots, \vec{x}^{(n)}
    \}
    \text{ is linearly independent}
    \iff 
    W\brack{\vec{x}^{(1)}, \ldots, \vec{x}^{(n)}}
    = \det{\mat{\Psi}} \neq \theta
\]
over some interval $I \subseteq \mathbb{R}$.
This is mainly a result from linear algebra.

\subsection{Solving IVP coefficients}
Let $\mat{\Psi}$ be the fundamental matrix for \eqref{sfol:gen_hom}
then for the IVP $\vec{x}(t_0) = \vec{x}^0$, we need to find $\vec{c} \in \mathbb{R}^n$
such that 
\[
    \vec{x}^0 = \vec{x}(t_0) = \mat{\Psi}(t_0) \vec{c}
\]
Then, since $\mat{\Psi}$ is invertible (due to linear independence), we can ``easily'' solve for $\vec{c}$
\[
    \vec{c} = \mat{\Psi}^{-1}(t_0)\vec{x}^0
\]
and obtain the solution to the IVP as
\[
    \vec{x}(t) = \mat{\Psi}(t)\mat{\Psi}^{-1}(t_0)\vec{c}
\]
where $t$ lies in interval $I \subseteq \mathbb{R}$

\subsubsection{Nice theorem}
Let $\mat{\Phi}$ be a matrix who's columns consist of solutions of \eqref{sfol:gen_hom}, then 
if 
\[
    \mat{\Phi}(t_0) = \mat{I} \text{ the identity}
\]
where $t_0$ lies in an interval $I \subseteq \mathbb{R}$, then $\mat{\Phi}$ is a fundamental matrix for \eqref{sfol:gen_hom}.

Although this theorem may seem obvious, it corollary is very useful. That is, if the solution
vectors $\vec{x}^{(i)}$ at $t_0$ are the standard basis vectors, then they form a set of fundamental solutions.

Additionally, consider another fundamental matrix $\mat{\Psi}$. Then the solution is 
\[
    \vec{x}(t) = \mat{\Psi}(t)\mat{\Psi}^{-1}(t_0) \vec{x}^0
\]
more importantly we can generate $\mat{\Phi}$ as
\[
    \mat{\Phi}(t) = \mat{\Psi}(t) \mat{\Phi}^{-1}(t_0)
\]

\subsection{Resolving complex solutions}
We will first prove a more general result about complex solutions.
Suppose that $\vec{x}$ is a complex valued solution to $\eqref{sfol:gen_hom}$,
then we want to firstly show that $\conj{\vec{x}}$ is also a solution then show
that the real and imaginary parts are both linearly independent solutions.

\begin{proofi}
    Let $\vec{x}$ be a complex solution to \eqref{sfol:gen_hom}, with non-zero real and imaginary components,  then
    \[
        \vec{x}' = \mat{A}\vec{x}
        \implies \conj{\vec{x}'} = \conj{\mat{A}\vec{x}}
        \implies \paren{\conj{\vec{x}}}' = \conj{\mat{A}}\conj{\vec{x}} = \mat{A}\conj{\vec{x}}
    \]
    since $A$ consists of real functions.
    Therefore, $\conj{\vec{x}}$ is also a solution. Next, let $a , b \in \mathbb{C}$ (this allows the argument to 
    be more general) then if both the
    \begin{align*}
        &a \vec{x} + b \conj{\vec{x}} = 0 \\
        &\iff a\Re(\vec{x}) = b\Re(\conj{\vec{x}}) \text{ and } a \Im(\vec{x}) = b \Im(\conj{\vec{x}})\\
        &\iff a\Re(\vec{x}) = b\Re(\vec{x}) \text{ and } a \Im(\vec{x}) = - b \Im(\vec{x})\\
        &\iff a = b \text{ and } a = -b\\
        &\iff a = b = 0
    \end{align*}
    Therefore, $\vec{x}$ and $\conj{\vec{x}}$ are linearly independent. Then
    \begin{align*}
        &\vec{x} \ \perp\  \conj{\vec{x}}\\
        &\iff \tfrac{1}{2}(\vec{x} + \conj{\vec{x}}) \ \perp \ \conj{\vec{x}}\\
        &\iff \Re(\vec{x}) = \tfrac{1}{2}(\vec{x} + \conj{\vec{x}}) \ \perp \ \tfrac{1}{2}(\vec{x} + \conj{\vec{x}}) - \conj{\vec{x}} = \tfrac{1}{2}(\vec{x} - \conj{\vec{x}}) = i\Im(\vec{x})\\
        &\iff \Re(\vec{x}) \ \perp \ \Im(\vec{x})
    \end{align*}
    and we are done.
\end{proofi}
That is, given a complex valued solution, its complex conjugate would also be a linearly independent solution.
Furthermore, the linearly independent real solutions can be generated using the real and imaginary of the complex valued
solution.


\subsection{Homogenous with constant coefficients}
If our homogeneous system of first-order differential equations \eqref{sfol:gen_hom} has a constant coefficient
matrix $\mat{A}$, then solutions can be easily generated. Suppose that one
solution was of the form $\vec{x} = \vec{v}e^{\lambda t}$, then we get that 
\[
    \vec{x}' = \mat{A} \vec{x} \implies \lambda\vec{v} = \mat{A} \vec{v} \iff (\mat{A} - \lambda \mat{I}) \vec{v} = \vec{0}
\]
That is, $\vec{v}$ is the eigenvector of $\mat{A}$ corresponding to eigenvalue $\lambda$. 
Therefore, if the geometric and algebraic multiplicities of each eigenvalue $\lambda$
are equal, then we have $n$ linearly independent solutions
\[
    \vec{x}^{(i)} = \vec{v}_ie^{\lambda_i t}
\]
and we are done.

\subsubsection{Dealing with smaller geometric multiplicities}

Suppose, we have an eigenvalue $\lambda$ whose geometric multiplicity is strictly less than
its algebra multiplicity. Then, we are unable to from the necessary linearly independent eigenvectors
corresponding to $\lambda$. In such a case, we must resort to the use of \emph{generalized eigenvectors}.
A generalized eigenvector of rank $j$ is a vector $\vec{u}_j$ such that 
\[
    (\mat{A} - \lambda \mat{I})^j \vec{u}_j = \vec{0} \quad \text{ but }\quad
    (\mat{A} - \lambda \mat{I})^{j-1} \vec{u}_j \neq \vec{0}
\]
Or in other words, $(\mat{A} - \lambda\mat{I})^{j-1}\vec{u}_j$ is an eigenvector corresponding
to eigenvalue $\lambda$.
Then immediately, we see that normal eigenvectors are generalized eigenvectors of rank $1$.
The generalized eigenvectors then create a \emph{generalized eigenspace} with dimension equal to the
algebraic multiplicity of $\lambda$. Quick side note: \url{https://en.wikipedia.org/wiki/Jordan_normal_form#Generalized_eigenvectors}. 
Note, I do not quite understand the information in the stated url, however, I was able to 
use the information there in order to obtain a beautiful result. Hopefully, the quoted results are correct.


Additionally, note that 
$(\mat{A} -\lambda \mat{I})^{i} \vec{u}_j$ is a generalized eigenvector of rank $(j-i)$. Therefore,
we see that the generalized eigenvectors form a chain of sorts, this chain is called
a \emph{Jordan Chain}. Another important point to note is that 
the chain generalized eigenvectors are linearly independent and the nodes on these chains constitute 
a basis of the generalized eigenspace. 

We now have the tools necessary to generate more solutions to \eqref{sfol:gen_hom} when our eigenvalue
has a smaller algebraic multiplicity than geometric multiplicity. Suppose that we have the chain
of generalized eigenvectors 
\[
    \vec{u}_1, \vec{u}_2, \ldots, \vec{u}_m
\]
where $(\mat{A} - \lambda \mat{I})\vec{u}_j = \vec{u}_{j-1}$. Then we claim that 
we have the set of independent solutions to \eqref{sfol:gen_hom}
\begin{equation}
    \label{sfol:const:hom:sol1}
    \vec{x}^{(k)} 
    = e^{\lambda t} \sum_{r=0}^{k-1} \frac{t^r}{r!} \vec{u}_{k - r}
    = e^{\lambda t} \paren{ \vec{u}_k + t \vec{u}_{k-1} + \frac{1}{2}t^2 \vec{u}_{k-2} +\cdots +\frac{1}{(k-1)!}t^{k-1} \vec{u}_1}
\end{equation}
where $k = 1\ldots m$
\begin{proofi}
    Clearly, each of the $\vec{x}^{(k)}$ are linearly independent. Now, first, lets look at the derivative, we get
    \[
        (\vec{x}^{(k)})' 
        = e^{\lambda t}\brack{\lambda\sum_{r=0}^{k-1} \frac{t^r}{r!} \vec{u}_{k - r} + \sum_{r=1}^{k-1} \frac{t^{r-1}}{(r-1)!} \vec{u}_{k - r}}
        = e^{\lambda t}\brack{\lambda\sum_{r=0}^{k-1} \frac{t^r}{r!} \vec{u}_{k - r} + \sum_{r=0}^{k-2} \frac{t^{r}}{r!} \vec{u}_{k - r - 1}}
    \]
    note that $k -(r +1) = k - r - 1$.
    Also, since for each $i$,
    \[
        (\mat{A} - \lambda\mat{I})\vec{v}_i = \vec{v}_{i-1} \implies \mat{A} \vec{v}_i = \lambda\vec{v}_i + \vec{v}_{i-1}
    \]
    we get that
    \begin{align*}
        \mat{A}\vec{x}^{(k)} 
        &= e^{\lambda t} \mat{A}\sum_{r=0}^{k-1} \frac{t^r}{r!} \vec{u}_{k - r}\\
        &= e^{\lambda t} \brack{\frac{t^{k-1}}{(k-1)!}\mat{A} \vec{u}_1 + \mat{A}\sum_{r=0}^{k-2} \frac{t^r}{r!} \vec{u}_{k - r}}\\
        &= e^{\lambda t} \brack{\frac{t^{k-1}}{(k-1)!}\lambda \vec{u}_1 + \sum_{r=0}^{k-2} \frac{t^r}{r!} (\lambda \vec{u}_{k - r} + \vec{u}_{k-r-1})}\\
        &= e^{\lambda t} \brack{\frac{t^{k-1}}{(k-1)!}\lambda \vec{u}_1 + \sum_{r=0}^{k-2} \frac{t^r}{r!} \lambda\vec{u}_{k - r} + \sum_{r=0}^{k-2} \frac{t^r}{r!}\vec{u}_{k-r-1})}\\
        &= e^{\lambda t} \brack{\sum_{r=0}^{k-1} \frac{t^r}{r!} \lambda\vec{u}_{k - r} + \sum_{r=0}^{k-2} \frac{t^r}{r!}\vec{u}_{k-r-1})}\\
        &= (\vec{x}^{(k)})' 
    \end{align*}
\end{proofi}

% What lies below is a failed attempt at proving a false conjecture. Hopefully what is above is true and I dont need to go though this again.
% where $(\mat{A} - \lambda \mat{I})^{j-1}\vec{u}_j = \vec{v}$. Then we claim that 
% we have the set of independent solutions to \eqref{sfol:gen_hom}
% \[
%     \vec{x}^{(k)} = (\vec{u}_1 t + \vec{u}_2 t + \cdots + \vec{u}_{k-1} t + \vec{u}_k)e^{\lambda t}
%     \quad \text{ where } k = 1\ldots m
% \]
% \begin{proofi}
%     Firstly, since the generalized eigenvectors are linearly independent, each of the $\vec{x}^{(k)}$ are also linearly
%     independent. Now, first, lets look at the the derivative, we get 
%     \begin{equation}
%         \label{sfol:const:eigen:gen:1}
%         (\vec{x}^{(k)})' 
%         = \lambda\paren{\sum_{i=1}^{k-1} \vec{u}_i t + \vec{u}_k}e^{\lambda t}
%              + \sum_{i=1}^{k-1} \vec{u}_ie^{\lambda t}
%     \end{equation}
%     Also, since for each $i$,
%     \[
%         (A - \lambda)\vec{v}_i = \vec{v}_{i-1}
%     \]
%     we get that
%     \begin{align*}
%         A\vec{x}^{(k)} 
%         &= A\paren{\sum_{i=1}^{k-1} \vec{u}_i t + \vec{u}_k}e^{\lambda t}\\
%         &= \sum_{i=1}^{k-1} A\vec{u}_i te^{\lambda t} + A \vec{u}_ke^{\lambda t}\\
%         &= A\vec{u}_1 te^{\lambda t} + \sum_{i=2}^{k-1} A\vec{u}_i te^{\lambda t} + A \vec{u}_ke^{\lambda t}\\
%         &= \lambda\vec{u}_1 te^{\lambda t} + \sum_{i=2}^{k-1} (\lambda\vec{u}_i + \vec{u}_{i-1}) te^{\lambda t} + (\lambda \vec{u}_k + \vec{u}_{k-1})e^{\lambda t}\\
%         &= \lambda\paren{\vec{u}_1 t + \sum_{i=2}^{k-1} \vec{u}_i t +\vec{u}_k}e^{\lambda t} + \sum_{i=2}^{k-1} \vec{u}_{i-1} te^{\lambda t} + \vec{u}_{k-1}e^{\lambda t}\\
%         &= \lambda\paren{\sum_{i=1}^{k-1} \vec{u}_i t +\vec{u}_k}e^{\lambda t} + \paren{\sum_{i=1}^{k-2} \vec{u}_{i} t + \vec{u}_{k-1}}e^{\lambda t}\\
%     \end{align*}
% \end{proofi}








\subsection{Alternative derivation of homogenous solutions}
Let $\mat{A}$ be a constant matrix and $\vec{x}$ be a solution to \eqref{sfol:gen_hom}. Then,
upon closer inspection of \eqref{sfol:const:hom:sol1}, the following important fact can be noted
\begin{align*}
    (\mat{A} - \lambda \mat{I}) \frac{\vec{x}^{(k)}}{e^{\lambda t}}
    &= (\mat{A} - \lambda \mat{I}) \brack{\vec{u}_k + t \vec{u}_{k-1} + \frac{1}{2}t^2 \vec{u}_{k-2} +\cdots +\frac{1}{(k-1)!}t^{k-1} \vec{u}_1 }\\
    &= \vec{u}_{k-1} + t \vec{u}_{k-2} + \frac{1}{2}t^2 \vec{u}_{k-3} +\cdots +\frac{1}{(k-2)!}t^{k-2} \vec{u}_1 +\frac{1}{(k-1)!}t^{k-1} \vec{0}\\
    &= \vec{u}_{k-1} + t \vec{u}_{k-2} + \frac{1}{2}t^2 \vec{u}_{k-3} +\cdots +\frac{1}{(k-2)!}t^{k-2} \vec{u}_1\\
    &= \frac{d}{dt}\paren{\frac{\vec{x}^{(k)}}{e^{\lambda t}}}
\end{align*}
Pretty beautiful.
Or in general, if we have a solution $\vec{x}$ of \eqref{sfol:gen_hom}, we see that 
\[
    \frac{d}{dt}\paren{\frac{\vec{x}}{e^{\kappa t}}}
    = \frac{e^{\kappa t} (\vec{x})' - \kappa e^{\kappa t} \vec{x}}{(e^{\kappa t})^2 }
    = \frac{(\vec{x})' - \kappa \vec{x}}{e^{\kappa t}}
    = \frac{\mat{A} \vec{x} - \kappa \vec{x}}{e^{\kappa t}}
    = (\mat{A} - \kappa I)\frac{\vec{x}}{e^{\kappa t}}
\]
where $\kappa$ is a constant (not necessarily an eigenvalue)
or in a more beautiful manner
\begin{align*}
    &\vec{x}' = \mat{A}\vec{x}\\
    &\iff \vec{x}' - \kappa \vec{x} = \mat{A} \vec{x} - \kappa \vec{x} = (\mat{A} - \kappa \mat{I}) \vec{x}\\
    &\iff e^{-\kappa t}(\vec{x}' - \kappa \vec{x}) =  e^{-\kappa t}(\mat{A} - \kappa \mat{I}) \vec{x}\\
    &\iff (e ^{-\kappa t} \vec{x})' =  (\mat{A} - \kappa \mat{I}) e^{-\kappa t} \vec{x}\\
\end{align*}
Now, if we let $\vec{y} = e^{-\kappa t} \vec{x}$, we get that 
\[
    \vec{y}' = (\mat{A} - \kappa \mat{I})\vec{y}
\]
Note that this last expression may not seem to useful as it really isn't. However,
consider the taylor series expansion of $\vec{y}$
\[
    \vec{y} = \vec{a}_0 + \vec{a}_1t + \vec{a}_2 \frac{t^2}{2} + \cdots
\]
Then we see that 
\[
    \vec{a}_{r+1} = (\mat{A} - \kappa \mat{I})\vec{a}_{r}
\]
Furthermore, we see that 
\[
    \vec{a}_r = (\mat{A} - \kappa \mat{I})^r\vec{a}_0
\]
and hence we have a feasible way of solving homogeneous equations, provided 
that $(\mat{A} - \kappa \mat{I})^r\vec{a}_0$ converges.

\subsubsection{Nice solutions}
Suppose that the taylor series of $\vec{y}$ was finite with $m$ terms, or alternatively,
$\vec{y}$ is a polynomial of degree $m-1$. Then,
\[
    \vec{0} = \vec{a}_{m} = (\mat{A} - \kappa \mat{I}) \vec{a}_{m-1}
\]
and hence $\vec{a}_{m-1}$ is an eigenvector of $\mat{A}$ corresponding to the eigenvalue $\kappa$.
Then, we see that 
\[
    (\mat{A} - \kappa \mat{I})^{m-1-r}\vec{a}_r = \vec{a}_{m-1}
\]
Therefore, none of the $\vec{a}_r$ can be equal to zero when $r \leq m$.
Furthermore, $\vec{a}_0$ must be a generalized eigenvector of rank $m$.
Clearly, the converse of the above statements is also true, so we obtain the following result
\begin{center}
    \begin{minipage}{0.8\linewidth}
        $\vec{y} = e^{-\kappa t} \vec{x}$ has a finite taylor series expansion with $m$ terms
        if and only if, the constant term of $\vec{y}$ is a generalized eigenvector of rank $m$
        corresponding to eigenvalue $\kappa$ of $\mat{A}$.
    \end{minipage}
\end{center}
In such a case, our solution is given by 
\[
    \vec{x} = e^{\kappa t}\vec{y} = e^{\kappa t} \sum_{r=0}^{m-1} \frac{t^r}{r!} \ (\mat{A} - \kappa \mat{I})^r \vec{\xi}
\]
where $\vec{\xi}$ is a generalized eigenvector of rank $m$ corresponding to eigenvalue $\kappa$.
Note that this is equivalent to our previous result \eqref{sfol:const:hom:sol1}


\subsection{Method of diagonalization}
Let $\mat{A}$ be a constant diagonalizable matrix. That is $\exists \mat{T}, \mat{D} \in \mathbb{R}^{n \times n}$
where $\mat{T}$ is invertible and $\mat{D}$ is diagonal,
such that
\[
    \mat{D} = \mat{T}^{-1}\mat{A}\mat{T} \iff \mat{T}\mat{D} = \mat{A} \mat{T}
\]
Then, let $\vec{x}$ be a solution of \eqref{sfol:gen} and $\vec{y} = \mat{T}^{-1} \vec{x}$
then
\begin{align*}
    &\vec{x}' = \mat{A} \vec{x} + \vec{g}\\
    &\iff \mat{T}\vec{y}' = \mat{A} \mat{T} \vec{y} + \vec{g}\\
    &\iff \vec{y}' = \mat{T}^{-1}\mat{A} \mat{T} \vec{y} + \mat{T}^{-1}\vec{g}\\
    &\iff \vec{y}' = \mat{D} \vec{y} + \mat{T}^{-1}\vec{g}
\end{align*}
Then, this system consists of $n$ uncoupled first-order differential equations.
At this point, these equations can each be solved in the usual manner then collected 
as a single solution $\vec{x} = \mat{T} \vec{y}$.

\subsection{Method of undetermined coefficients}
If $\vec{g}$ in \eqref{sfol:gen} can be represented as the sum/or product of 
\begin{itemize}
    \item polynomials
    \item exponential functions including sine and cosine by Euler's formula
\end{itemize}
then we can construct a particular solution using the method of undetermined coefficients.
Suppose that one of the terms $\vec{g}$ has the following form
\[
    (\vec{c}_n t^{n} + \cdots \vec{c}_0)e^{\alpha t}
\]
Then our particular solution would have the same form
\[
    (\vec{a}_n t^{n} + \cdots \vec{a}_0)e^{\alpha t}
\]
where $\vec{a}_0 \ldots \vec{a}_n$ are vectors that need to be determined by substitution into \eqref{sfol:gen}.
Note that a polynomial occurs when $\alpha = 0$ whilst sine and cosine
occur when $\alpha$ is imaginary. The table below outlines some common forms
\begin{center}
    \renewcommand{\arraystretch}{1.3}
    \begin{tabular}{|c|c|}
        \hline
        $\vec{g}(t)$ & $\vec{x}^{(p)} (t)$\\\hline
        $t^n$ & $\vec{a}_nt^n + \vec{a}_{n-1}t^{n-1} + \cdots + \vec{a}_1t + \vec{a}_0$\\\hline
        $e^{\alpha t}$ & $\vec{a}e^{\alpha t}$\\\hline
        $\sin (\alpha t)$ or $\cos (\alpha t)$ & $\vec{a} \sin (\alpha t) + \vec{b} \cos (\alpha t)$\\\hline
        $t^n e^{\alpha t}$ & $(\vec{a}_n t^n + \cdots + \vec{a}_0)e^{\alpha t}$\\\hline
        $e^{\beta t}\sin (\alpha t)$ or $e^{\beta t}\cos (\alpha t)$ 
                & $\vec{a} e^{\beta t} \sin (\alpha t) + \vec{b}e^{\beta t} \cos (\alpha t)$\\\hline
        $\cdots$ & $\cdots$\\\hline
    \end{tabular}
\end{center}
Although this method is quite similar to that employed for linear differential equations, one
major difference occurs when the form of $\vec{g}$ is a solution of the homogeneous equation.
In such a case, instead of just multiplying by the independent variable $t$, we must
increase the degree of the polynomial prefixing $e^{\alpha t}$.


\subsection{Method of variation of parameters}
Consider the general non-homogeneous system \eqref{sfol:gen} where
$\mat{A}(t)$ and $\vec{g}(t)$ are both continuous on an interval $I \subseteq \mathbb{R}$. 
Furthermore, if we have fundamental matrix $\mat{\Psi}(t)$ (that is also valid in $I$) 
for its homogeneous counterpart, we can use the substitution 
$\vec{x}(t) = \mat{\Psi}(t) \vec{u}(t)$ to solve the general system.
Then, upon substitution
\[
    \mat{\Psi}'\vec{u} + \mat{\Psi}\vec{u}' 
        =(\mat{\Psi} \vec{u})' = \mat{A}\mat{\Psi}\vec{u} + \vec{g}
    \iff \mat{\Psi}\vec{u}' = \vec{g}
\]
since $\mat{\Psi}' = \mat{A}\mat{\Psi}$. We therefore get that 
\[
    \vec{u}(t) = \int \mat{\Psi}^{-1}(t)\vec{g}(t) \ dt
\]
and hence, the solution to \eqref{sfol:gen} is 
\[
    \vec{x}(t) = \mat{\Psi}(t) \int \mat{\Psi}^{-1}(t)\vec{g}(t) \ dt
\]

\subsection{Relation to higher-order linear differential equations}
Consider the $n$'th order linear differential equation \eqref{lin:gen}
\[
    a_n(x) y^{(n)} + a_{n-1}(x) y^{(n-1)} + \cdots + a_1(x)y' + a_0(x)y = g(x)
\]
Then, we can convert this into a system of $n$ linear differential equations
by letting $x_i = y^{(i-1)}$ where $i = 1\ldots n$. Hence, 
we get that $x_i' = x_{i+1}$ and
\[
    \begin{bmatrix}
        a_1 & a_{2} & \cdots & a_{n-1} & a_n\\
        1   & 0 & \cdots  & 0 & 0\\
        0   & 1 & \cdots  & 0 & 0\\
        \vdots & \vdots & \ddots & \vdots &\vdots\\
        0   & 0 &\cdots & 1  & 0
    \end{bmatrix}
    \begin{bmatrix}
        x_1' \\ x_2' \\ \vdots \\ x_{n-1}' \\x_n'
    \end{bmatrix}
    % =
    % \begin{bmatrix}
    %     -a_0 x_1 \\ x_2 \\ \vdots \\ x_n
    % \end{bmatrix}
    % +
    % \begin{bmatrix}
    %     g \\ 0 \\ \vdots \\ 0
    % \end{bmatrix}
    =
    \begin{bmatrix}
        -a_0 & 0 & 0 & \cdots & 0\\
        0 & 1 & 0 & \cdots & 0\\
        0 & 0 & 1& \cdots & 0\\
        \vdots & \vdots & \vdots & \ddots & \vdots \\
        0 & 0 & 0 & \cdots & 1
    \end{bmatrix}
    \begin{bmatrix}
        x_1 \\ x_2 \\ \vdots \\ x_{n-1}\\ x_n
    \end{bmatrix}
    +
    \begin{bmatrix}
        g \\ 0 \\ \vdots \\ 0 \\ 0
    \end{bmatrix}
\]
Note that determinant of the matrix on the right is equal to $(-1)^{n-1}a_n$ and is hence
invertible. Its inverse is equal to 
\[
    \begin{bmatrix}
        a_1 & a_{2} & \cdots & a_{n-1} & a_n\\
        1   & 0 & \cdots  & 0 & 0\\
        0   & 1 & \cdots  & 0 & 0\\
        \vdots & \vdots & \ddots & \vdots &\vdots\\
        0   & 0 &\cdots & 1  & 0
    \end{bmatrix}
    ^{-1}
    =
    \begin{bmatrix}
        0 & 1 & 0 & \cdots & 0\\
        0 & 0 & 1 & \cdots & 0\\
        \vdots & \vdots & \vdots & \ddots & \vdots\\
        0 & 0 & 0 & \cdots & 1\\
        \tfrac{1}{a_n} & \tfrac{-a_1}{a_n} & \tfrac{-a_2}{a_n} & \cdots & \tfrac{-a_{n-1}}{a_n}
    \end{bmatrix}
\]
Therefore, we get 
\begin{align*}
    \begin{bmatrix}
        x_1' \\ x_2' \\ \vdots \\ x_{n-1}' \\x_n'
    \end{bmatrix}
    &=
    \begin{bmatrix}
        0 & 1 & 0 & \cdots & 0\\
        0 & 0 & 1 & \cdots & 0\\
        \vdots & \vdots & \vdots & \ddots & \vdots\\
        0 & 0 & 0 & \cdots & 1\\
        \tfrac{1}{a_n} & \tfrac{-a_1}{a_n} & \tfrac{-a_2}{a_n} & \cdots & \tfrac{-a_{n-1}}{a_n}
    \end{bmatrix}
    \begin{bmatrix}
        -a_0 & 0 & 0 & \cdots & 0\\
        0 & 1 & 0 & \cdots & 0\\
        0 & 0 & 1& \cdots & 0\\
        \vdots & \vdots & \vdots & \ddots & \vdots \\
        0 & 0 & 0 & \cdots & 1
    \end{bmatrix}
    \begin{bmatrix}
        x_1 \\ x_2 \\ \vdots \\ x_{n-1}\\ x_n
    \end{bmatrix}
    + 
    \begin{bmatrix}
        0 & 1 & 0 & \cdots & 0\\
        0 & 0 & 1 & \cdots & 0\\
        \vdots & \vdots & \vdots & \ddots & \vdots\\
        0 & 0 & 0 & \cdots & 1\\
        \tfrac{1}{a_n} & \tfrac{-a_1}{a_n} & \tfrac{-a_2}{a_n} & \cdots & \tfrac{-a_{n-1}}{a_n}
    \end{bmatrix}
    \begin{bmatrix}
        g \\ 0 \\ \vdots \\ 0 \\ 0
    \end{bmatrix}
    \\
    &=
    \begin{bmatrix}
        0 & 1 & 0 & \cdots & 0\\
        0 & 0 & 1 & \cdots & 0\\
        \vdots & \vdots & \vdots & \ddots & \vdots\\
        0 & 0 & 0 & \cdots & 1\\
        \tfrac{-a_0}{a_n} & \tfrac{-a_1}{a_n} & \tfrac{-a_2}{a_n} & \cdots & \tfrac{-a_{n-1}}{a_n}
    \end{bmatrix}
    \begin{bmatrix}
        x_1 \\ x_2 \\ \vdots \\ x_{n-1}\\ x_n
    \end{bmatrix}
    + 
    \begin{bmatrix}
        0 \\ 0 \\ \vdots \\ 0 \\ \tfrac{g}{a_n}
    \end{bmatrix}
\end{align*}
Now, if all of the $a_i$ are constant, we see that the characteristic function of the coefficient matrix is
\begin{align*}
    &\begin{vmatrix}
        -\lambda & 1 & 0 & \cdots & 0 & 0\\
        0 & -\lambda & 1 & \cdots & 0 & 0\\
        0 & 0 & -\lambda & \cdots & 0 & 0\\
        \vdots & \vdots & \vdots & \ddots & \vdots\\
        0 & 0 & 0 & \cdots & 1 & 0\\
        0 & 0 & 0 & \cdots & -\lambda &1\\
        \tfrac{-a_0}{a_n} & \tfrac{-a_1}{a_n} & \tfrac{-a_2}{a_n} & \cdots & \tfrac{-a_{n-2}}{a_{n-1}}& \tfrac{-a_{n-1}}{a_n} -\lambda
    \end{vmatrix}\\
    &= 
    \sum_{i=0}^{n-2} (-1)^{n+i+1} (-\lambda)^{i} \paren{-\tfrac{a_i}{a_n}}
        + \paren{\tfrac{-a_{n-1}}{a_n} -\lambda}(-\lambda)^{n-1}
        \\
    &= 
    \sum_{i=0}^{n-2} (-1)^{n+1} \lambda^i \paren{-\tfrac{a_i}{a_n}}
        + \paren{\tfrac{-a_{n-1}}{a_n}}(-\lambda)^{n-1} + (-\lambda)^{n}
        \\
    &= 
    \sum_{i=0}^{n-2} (-1)^{n} \lambda^i \paren{\tfrac{a_i}{a_n}}
        + \paren{\tfrac{a_{n-1}}{a_n}}(-1)^{n}\lambda^{n-1} + (-1)^n\lambda^{n}
        \\
    &= \frac{(-1)^n}{a_n}\paren{a_0 + \lambda a_1 + \cdots a_{n-1}\lambda^{n-1} + a_n \lambda^n}
\end{align*} 
which has the same roots as the characteristic polynomial generated using the method in the previous chapter.
Also, notice if we have eigenvalue $\lambda$ with eigenvector $\vec{v} = (v_1,\ v_2,\ \ldots, v_n)^T$, we get that 
$\lambda v_i = v_{i+1}$ and hence all the coefficients must be non-zero and hence $y = ke^{\lambda t}$ where $k \neq 0$.
Also, notice no eigenvalue can have geometric multiplicity higher than $1$ since the entry $v_1$ determines all
other entries. Furthermore, this generalizes for all higher rank generalized eigenvectors.

Therefore, we obtain the same solutions
generated by using the method in the previous chapter since \eqref{sfol:const:hom:sol1} divulges
to repeated multiplication by $t$.

\subsection{Equilibrium and stability}
Consider the equation \eqref{sfol:gen_hom} with constant coefficient matrix.
Then, we say a solution is 
\begin{itemize}
    \item \emph{asymptotically stable} if the real parts of all of the eigenvalues is less than $0$. 
    \item \emph{neutrally stable} if
        \begin{itemize}
            \item the real parts of all of the eigenvalues is less than or equal to $0$
            \item at least one of the eigenvalues has a real part equal to $0$ and equal algebraic and geometric multiplicities.
        \end{itemize}
    \item \emph{unstable} if either of the following occur
        \begin{itemize}
            \item the real part of at least one of the eigenvalues is greater than $0$.
            \item the real part of at least one of the eigenvalues is equal to $0$ and has a smaller geometric multiplicity than algebraic multiplicity.
        \end{itemize}
\end{itemize}
To see why, consider the solution \eqref{sfol:const:hom:sol1} and notice
that the behaviour of $|e^{\lambda t}|$ determines the behaviour of the solution. 
As $t \to \infty$,
\begin{itemize}
    \item $|e^{\lambda t}| \to \infty$ if $\Re(\lambda) > 0$
    \item $|e^{\lambda t}| \to 0$ if $\Re(\lambda) < 0$
    \item $|e^{\lambda t}| = 1$ if $\Re(\lambda) = 0$
\end{itemize}
Additionally, if $\Re(\lambda) = 0$ and it has a smaller algebraic multiplicity than geometric multiplicity,
we obtain a term with $t^n$ (where $n \geq 1$) in \eqref{sfol:const:hom:sol1} and hence the solution 
diverges.

\subsubsection{Classifictation and stability for n=2}
The following table gives the possible classifications and types of stability 
of the equilibrium solution when $n = 2$.
\begin{center}
    \renewcommand{\arraystretch}{1.3}
    \begin{tabular}{|c | c |c | c | c|}\hline
        \multicolumn{3}{|c|}{\textbf{Eigenvalues}}
        & \textbf{Type of Equilibrium}
        & \textbf{Stability}\\\hline
        \multirow{3}{*}{
            Real; distinct
        }
        & \multicolumn{2}{c|}{both positive} & Nodal source & Unstable\\\cline{2-5}
        & \multicolumn{2}{c|}{both negative} & Nodal sink & Asymptotically stable\\\cline{2-5}
        & \multicolumn{2}{c|}{one positive, one negative} & Saddle point & Unstable\\\hline
        \multirow{3}{*}{
            Complex
        }
        & \multicolumn{2}{c|}{positive real part} & Spiral source & Unstable\\\cline{2-5}
        & \multicolumn{2}{c|}{negative real part} & Spiral sink & Asymptotically stable\\\cline{2-5}
        & \multicolumn{2}{c|}{purely imaginary} & Center & Neutrally stable\\\hline
        \multirow{4}{*}{
            real repeated
        }
        & \multirow{2}{*}{\begin{minipage}{0.15\linewidth}two \\ eigenvectors \end{minipage}}
            & positive & \multirow{2}{*}{Proper node} & Unstable\\\cline{3-3}\cline{5-5}
        &   & negative &                              & Asymptotically stable \\\cline{2-5}
        & \multirow{2}{*}{\begin{minipage}{0.15\linewidth}one \\eigenvector \end{minipage}}
            & positive & \multirow{2}{*}{Improper node} & Unstable\\\cline{3-3}\cline{5-5}
        &   & negative &                              & Asymptotically stable \\\hline
    \end{tabular}
\end{center}
