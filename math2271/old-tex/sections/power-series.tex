\section{Power Series}
A \emph{power series expansion} of a function $y(x)$ about a point $x - x_0$
is a sum 
\[
    y(x) = \sum_{i=0}^\infty a_i (x-x_0)^i
\]
where the $a_i$ are constants. Note that such a series may not necessarily converge
and hence we must consider the set of points on which it is valid, also called 
the \emph{domain of validity}.

\paragraph*{side note} A power series need not have non-negative coefficients of
$(x-x_0)$ but we will speak of this later.

\subsubsection{Classification of points}
In this section, we will only focus on second order homogeneous linear differential equations
however methods can be extended to higher orders.
Consider the second order homogeneous linear equation
\[
    y'' + p(x)y' + q(x) y = 0
\]
Then the point $x=x_0$ is called
\begin{itemize}
    \item \emph{ordinary} if $p(x_0)$ and $q(x_0)$ are both finite
    \item \emph{singular} if $p(x)$ or $q(x)$ have a pole at $x=x_0$. Additionally
        $x=x_0$ can be further classified
        \begin{itemize}
            \item \emph{regular} if $p(x)(x-x_0)$ and $q(x)(x-x_0)^2$ are both finite as $x \to x_0$
            \item \emph{irregular} otherwise.
        \end{itemize}
\end{itemize}

\subsection{Solutions about ordinary points}
Let $x=x_0$ be an ordinary point of a linear second order homogeneous ODE. Then,
there exists a unique power series solution of this ODE about the 
point $x=x_0$ of the form
\[
    y(x) = \sum_{n=0}^\infty a_n (x-x_0)^n
\]
with two arbitrary constants and with domain of validity
\[
    |x-x_0| < R
\]
where $R$ is the distance between $x_0$ and the nearest singular point. If
there is no singular point, the solution is valid for all real numbers.

\subsection{Method of Frobenius: Solutions about regular singular points}
Let $x=x_0$  be a regular singular point of a linear second order homogeneous 
ODE. Then, the equation has at least one \emph{Frobenius series solution}
about that point of the form
\[
    y(x) = \sum_{n=0}^\infty a_n (x-x_0)^{n+c}
\]
where $c$ is a constant to be determined using the \emph{indicial equation}. 
The solution is valid for $|x-x_0| < R$
where $R$ is the distance between $x=x_0$ and the nearest singular point.
Note that if $c < 0$, the series does not converge at $x=x_0$ and this 
point must be removed from the domain of validity.

Also, note that there may only be one Frobenius series solution, so 
the other linearly independent solutions would have to be generated 
using another method.

\subsubsection{The indicial equation}
The indicial equation is the coefficient of the lowest power of $(x-x_0)$
when the series solution is substituted into the ODE. Note that since
we have second order differentials, this equation would be a polynomial
of degree $2$ in $c$ whose roots we will use to generate the Frobenius 
series solutions. 

Let $c_1$ and $c_2$ be the roots where $c_1 \geq c_2$, then we have the following
cases
\begin{itemize}
    \item Case 1: $c_1-c_2 \notin \mathbb{Z}$. In this case, there exists two Frobenius 
        series solutions
    \item Case 2: $c_1-c_2 = 0$. In this case, there is only one Frobenius series solution
        and the second linearly independent solution must be found using 
        another method such as reduction of order.
    \item $c_1 - c_2 \in \mathbb{Z}^+$. Then, we have two cases
        \begin{itemize}
            \item Case 3a: The larger root $c_1$ gives a solution but $c_2$ does not. In this case,
                the second linearly independent solution will have to be found using
                another method.
            \item Case 3b: the smaller root $c_2$ generates two linearly 
                independent Frobenius series solutions.
        \end{itemize}
\end{itemize}

\subsection{Bessel's Equation}
A second order ODE of the form 
\[
    x^2y'' + xy' + (x^2 - \alpha^2)y = 0 \quad\text{where }\alpha \geq 0
\]
is commonly referred to as \emph{Bessel's Equation} of order n.
Notice that $x=0$ is a regular singular point and hence, we can assume a 
Frobenius series form 
\[
    y = \sum_{n=0}^\infty a_n x^{n+c}
\]
and upon substitution into the equation, we obtain
\begin{align*}
    0 
    &= x^2 \sum_{n=0}^\infty (n+c)(n+c-1)a_nx^{n+c-2} + 
        x\sum_{n=0}^\infty (n+c)a_nx^{n+c-1} + 
        (x^2-\alpha^2) \sum_{n=0}^\infty a_n x^{n+c}\\
    &= \sum_{n=0}^\infty (n+c)(n+c-1)a_nx^{n+c} + 
        \sum_{n=0}^\infty (n+c)a_nx^{n+c} + 
        \sum_{n=0}^\infty a_n x^{n+c+2}
        - \sum_{n=0}^{\infty} \alpha^2a_n x^{n+c}\\
    &= \brack{c(c-1)a_0x^c + (c+1)(c)x^{c+1} + \sum_{n=2}^\infty (n+c)(n+c-1)a_nx^{n+c}}
        \\&\hspace{4em} + \brack{c a_0 x^c + (c+1)x^{c+1} + \sum_{n=2}^\infty (n+c)a_nx^{n+c}}
        + \sum_{n=2}^\infty a_{n-2} x^{n+c}
        \\&\hspace{4em}- \brack{\alpha^2a_0x^c + \alpha^2a_1x^{c+1} + \sum_{n=2}^{\infty} \alpha^2a_n x^{n+c}}\\
    &= (c(c-1) + c - \alpha^2)a_0x^c + ((c+1)c + (c+1) - \alpha^2)a_1x^{c+1}
      \\&\hspace{4em}+ \sum_{n=2}^\infty \brack{(n+c)(n+c-1)a_n + (n+c)a_n + a_{n-2} - \alpha^2a_n}x^{n+c}\\
    &= (c^2 - \alpha^2)a_0x^c + ((c+1)^2 - \alpha^2)a_1x^{c+1}
    + \sum_{n=2}^\infty \brack{((n+c)^2 - \alpha^2)a_n + a_{n-2}}x^{n+c}
\end{align*}
Then, from our indicial equation, we get that $c = \pm \alpha^2$. Also,
\[
    (n+c)^2 -\alpha^2 = (n\pm \alpha) - \alpha^2 = (n + \alpha \pm \alpha)(n-\alpha \pm\alpha)
    = n(n\pm 2\alpha)
\]
which may be zero when $n - 2\alpha = 0 \iff \alpha = \frac{n}{2}$. 


\subsection{Legendre equation}
The equation 
\[
    (1-x^2)y'' - 2xy' + \alpha(\alpha+1)y = 0 \quad\text{where } \alpha \in \mathbb{R}
\]
is called \emph{Legendre's equation}.
