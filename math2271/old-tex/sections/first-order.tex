\section{First-order differential equations}
Contradicting what the title suggests, the scope of this section is limited
further to set of first-degree ODEs.

Then in general, a first-order first-degree ODE can be written as one of the
following (equivalent) forms
\[
    \frac{dy}{dx} = f(x, y) 
    \quad\text{or}\quad 
    M(x, y) + N(x, y) \frac{dy}{dx} = 0
    \quad\text{or}\quad 
    M(x, y)dx + N(x, y)dy = 0
\]
\paragraph{Remark} yes, I too die every time I see the total differential but I do 
understand its usefulness (as it no longer restricts $x$ to be the independent variable).

\subsection{Existence and Uniqueness of solutions}
Consider the first order IVP
\[
    y' = f(x, y), \quad y(x_0) = y_0
\]
If \(f\) and \(\frac{\partial f}{\partial y}\) are continuous in a closed rectangle
\[
    R = \{|x - x_0| \leq a, |y - y_0| \leq b\}
\]
then $\exists$ a unique solution $y=y(x)$ of the IVP in an interval $|x - x_0| \leq h \leq a$.



\subsection{Linear Equations}
Consider a first-order linear ODE
\[
    \frac{dy}{dx} + p(x)y(x) = g(x)
\]
Then, suppose we multiply the equation by a function $u(x)$ such that the left hand side
can be written as the derivative of a product. That is,
\[
    u(x)g(x) = u(x)\brack{\frac{dy}{dx} + p(x)y(x)} = \frac{d}{dx}\brack{u(x)y(x)}
    \iff \frac{d}{dx}u(x) = p(x)u(x)
\]
Which occurs iff $u(x) = e^{\int p(x) dx}$. We call $u$ the integrating factor and we get that 
\begin{align*}
    &\frac{d}{dx}\brack{e^{\int p(x) dx}y(x)} = e^{\int p(x) dx}g(x)\\
    \implies & y(x)e^{\int p(x) dx} = \int  e^{\int p(x) dx}g(x) dx\\
    \implies & y(x) =e^{-\int p(x) dx} \int  e^{\int p(x) dx}g(x) dx
\end{align*}

\subsection{Separable Equations}
An first-order ODE of the form 
\[
    M(x, y) + N(x, y)\frac{dy}{dx} = 0
\]
is called \emph{separable} if $M$ is a function of $x$ only and $N$ is
a function of $y$ only. Then, the equation can be rewritten as
\[
    N(y) \frac{dy}{dx} = -M(x)
\]
which can be easily reduced by integration wrt $x$ to yield a possibly
implicit equation for $y$.

\paragraph{Note: }separable equations are also exact, as seen below.

\subsection{Exact Equations}
Consider a first-order ODE of the form
\begin{equation}
    \label{fo:exact:1}
    M(x, y)dx + N(x, y)dy = 0
\end{equation}
Then, the left hand side resembles the total differential of a function.
Let $u(x, y)$ be such a function (if it exists). Then
\[
    du = \frac{\partial u}{\partial x} dx + \frac{\partial u}{\partial y} dy
\]
And hence, if we can find a function $u$ such that $M = \frac{\partial u}{\partial x}$ and
$N = \frac{\partial u}{\partial y}$ then the solution to \eqref{fo:exact:1} is 
\[
    du(x, y) = 0 \iff u(x, y) = c
\]
where $c$ is a constant. In the case that $u(x, y)$ exists, then the ODE \eqref{fo:exact:1} is called
\emph{exact}.

\subsubsection{Necessary and sufficient}
If $M(x, y)$, $N(x, y)$, $\frac{\partial M}{\partial y}$ and $\frac{\partial M}{\partial x}$ are
all continuous, then
\[
    \eqref{fo:exact:1} \text{ is exact} \iff \frac{\partial M}{\partial y} = \frac{\partial N}{\partial x}
\]
\begin{proofi}
    If $\eqref{fo:exact:1}$ is exact, then $\exists u(x, y)$ such that 
    \[
        M = \frac{\partial u}{\partial x} \quad\text{and}\quad N = \frac{\partial u}{\partial y}
    \]
    then, by the premise and by Clairaunt's Theorem
    \[
        \frac{\partial M}{\partial y} 
        = \frac{\partial^2 u}{\partial y \partial x}
        = \frac{\partial^2 u}{\partial x \partial y }
        = \frac{\partial N}{\partial x} 
    \]
    Now, for the reverse direction. Let $w(x, y)$ be a function such that 
    \[
        M(x, y) = \frac{\partial w}{\partial x} 
    \]
    then, by the premise and by Clairaunt's Theorem
    \[
        \frac{\partial M}{\partial y} 
        = \frac{\partial^2 w}{\partial y \partial x}
        = \frac{\partial^2 w}{\partial x \partial y }
        = \frac{\partial N}{\partial x} 
    \]
    And, by integrating wrt $x$, we get that 
    \[
        \frac{\partial w}{\partial y}
        = \int \frac{\partial^2 w}{\partial x \partial y } dx
        = \int \frac{\partial N}{\partial x} dx
        = N(x, y) + f'(y)
    \]
    where $f(y)$ is an arbitrary function. Now, if we take 
    \[
        u(x, y) = w(x, y) - f(y)
    \]
    then, the ODE is exact since, 
    \[
        \frac{\partial u}{\partial x} = \frac{\partial w}{\partial x} = M(x, y)
        \quad\text{and}\quad
        \frac{\partial u}{\partial y} = \frac{\partial w}{\partial y} - f'(y) = N(x, y) + f'(y) - f'(y) = N(x, y)
    \]
\end{proofi}

\subsubsection{Use of Integrating Factor}
Suppose \eqref{fo:exact:1} is not exact, then one approach to make it exact 
is by multiplying by a function $\mu(x, y)$ to get
\[
    \mu(x, y)M(x, y)dx + \mu(x, y)N(x, y)dy = 0
\]
Then, by the above theorem, we must have that 
\[
    (\mu M)_y = (\mu N)_x \iff \mu_y M + \mu M_y = \mu_x N + \mu N_x
\]
Now, this equation is not simple to solve without some restrictions.
So, consider the following cases
\begin{itemize}
    \item $\mu$ is a function of $x$ only. Then 
            \[
                \frac{\mu_x}{\mu} = \frac{M_y - N_x}{N}
            \]
        must be a function of $x$ only and hence $\mu(x) = \exp\paren{\int \frac{M_y - N_x}{N} dx}$
        is the integrating factor to use.
    \item $\mu$ is a function of $y$ only. Then 
            \[
                \frac{\mu_y}{\mu} = \frac{M_y - N_x}{-M}
            \]
        must be a function of $y$ only and hence $\mu(y) = \exp\paren{\int \frac{M_y - N_x}{-M} dy}$
        is the integrating factor to use.
    % \item $\mu$ is a function of $xy$ (the product). Then let $\mu(x, y) = \eta(x, y)$ and hence
    %     we require
    %         \[
    %             x\eta' M + \eta M_y = y\eta' N + \eta N_x
    %         \]
    \item $\mu$ is a function of $u=xy$ only. Then
        \[
            \frac{\mu'}{\mu} = \frac{N_x - M_y}{xM - yN}
        \]
        must be a function of $xy$ only and hence $\mu(u) = \exp\paren{\int \frac{N_x - M_y}{xM - yN} du}$
        is the integrating factor to use.

        Note, that if $M = yf(xy)$ and $N = xg(xy)$ where $f \neq g$, then the integrating factor reduces 
        to
        \begin{align*}
            \mu(u) 
            &= \exp\paren{\int \frac{N_x - M_y}{xM - yN} du}\\
            &= \exp\paren{\int \frac{g(xy) +xyg'(xy) - f(xy)-xyf'(xy)}{xyf(xy) - xyg(xy)} du}\\
            &= \exp\paren{\int \frac{g(u) + ug'(u) - f(u)-uf'(u)}{uf(u) - ug(u)} du}\\
            &= \exp\paren{-\int \frac{1}{u} + \frac{f'(u) -g'(u)}{f(u) - g(u)} du}\\
            &= \exp\paren{-\ln(u) - \ln(f(u) - g(u))}\\
            &= \frac{1}{u\brack{f(u) - g(u)}}\\
            &= \frac{1}{xy\brack{f(xy) - g(xy)}}
        \end{align*}
    % \item[*****] TODO: If $M = yf(xy)$ and $N = xg(xy)$ where $f \neq g$, then the integrating factor is
    %         \[
    %             \frac{1}{xy\brack{f(xy) - g(xy)}}
    %         \]
\end{itemize}

\subsection{Homogenous Equations}
Recall that a first-order ODE is homogeneous iff it can be written as
\[
    M(x, y) + N(x, y)\frac{dy}{dx} = 0
\]
where both $M$ and $N$ are homogenous functions of the same degree. Then, let
$t = \tfrac{1}{x}$, we get that 
\begin{align*}
    & M(x, y) + N(x, y)\frac{dy}{dx} 
    = \paren{\frac{1}{x}}^k M\paren{1, \frac{y}{x}} + \paren{\frac{1}{x}}^k N\paren{1, \frac{y}{x}}\frac{dy}{dx} = 0
    \\
    & \iff \frac{dy}{dx} = \frac{M\paren{1, \frac{y}{x}}}{N\paren{1, \frac{y}{x}}} = f\paren{\frac{y}{x}}
\end{align*}
Then, by letting $y = ux$, $\frac{dy}{dx} = x\frac{du}{dx} + u$ and hence
\[
    x\frac{du}{dx} + u = \frac{dy}{dx} = f(u) \iff \paren{\frac{1}{f(u) - u}}\frac{du}{dx} = \frac{1}{x}
\]
which can be solved for $u$ and then solved for $y$ using substitution.

\subsection{Bernoulli Equations}
A Bernoulli equation is a first-order ODE of the form
\begin{equation}
    \label{fo:berno:1}
    y' + P(x)y = R(x)y^\alpha
\end{equation}
where $\alpha \in \mathbb{R}$. However, note that if $\alpha=0$, we get a linear first-order
while if $\alpha =1$, we get that the ODE is separable. Then, we will only consider 
$\alpha \in \mathbb{R} - \{0, 1\}$. Then, we use the substitution $\nu = y^{1-\alpha}$
which implies that 
\[
    \frac{d\nu}{dx} = (1-\alpha)y^{-\alpha}\frac{dy}{dx}  
\]
Now, by dividing \eqref{fo:berno:1} by $y^{\alpha}$ then using the substitution, we get that
\[
    y^{-\alpha}\frac{dy}{dx} + P(x)y^{1-\alpha} = R(x)
    \implies
    \frac{1}{1-\alpha}\frac{d\nu}{dx} + P(x)\nu = R(x)
\]
which is a first-order linear ODE and can be easily solved.

\subsection{Riccati Equations}
A Riccati equation is a first-order ODE of the form
\[
    y' = P(x)y^2 + Q(x)y + R(x)
\]
In order to generate the family of solutions, at least one must be known.
Let this solution be $S(x)$, then we can represent the difference 
between solutions as $y - S(x) = \tfrac{1}{z}$ and get
% \begin{align*}
%     &y' = P(x)y^2 + Q(x)y + R(x)\\
%     &\implies S' - \frac{z'}{z^2} = P(x) \paren{S + \frac{1}{z}}^2 + Q(x)\paren{S + \frac{1}{z}} + R(x)\\
%     &\implies S' - \frac{z'}{z^2} = P(x)S^2 + 2P(x)\frac{S}{z} + P(x)\frac{1}{z^2} + Q(x)S + Q(x)\frac{1}{z} + R(x)\\
%     &\implies - \frac{z'}{z^2} = 2P(x)\frac{S}{z} + P(x)\frac{1}{z^2} + Q(x)\frac{1}{z}\\
%     &\implies - z' = 2P(x)Sz + P(x) + Q(x)z\\
% \end{align*}
\begin{align*}
    &y' - S' = P(x)(y^2 - S^2) + Q(x)(y - S)\\
    &\implies \frac{d}{dx}\paren{\frac{1}{z}} = P(x)\paren{\frac{1}{z}}\paren{\frac{1}{z}+2S} + Q(x)\paren{\frac{1}{z}}\\
    &\implies -\frac{z'}{z^2} = P(x)\paren{\frac{1}{z}}\paren{\frac{1}{z}+2S} + Q(x)\paren{\frac{1}{z}}\\
    &\implies z' = -P(x)\paren{1+2Sz} - Q(x)z\\
    &\implies z' = -(2P(x)S + Q(x))z - P(x)
\end{align*}
which is a first-order linear ODE. Therefore for simplicity, the substitution $y = S(x) + \tfrac{1}{z}$
can be used to generate other solutions.
